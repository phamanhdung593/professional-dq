import React, { createContext, useReducer, useContext, ReactNode } from 'react';

interface SettingState {
  id: number;
  user_id: number;
  show_page_title: boolean;
  show_intro_text: boolean;
  show_footer_text: boolean;
  intro_text_content: string;
  page_title_content: string;
  footer_text_content: string;
  faq_sort_name: boolean;
  faq_uncategory_hidden: boolean;
  category_sort_name: boolean;
  dont_category_faq: boolean;
  faq_page_url: string;
  faq_template_number: number;
  page_under_contruction: string;
  search_placehoder: string;
  search_not_found: string;
  show_page_construction: boolean;
  yanet_logo_visible: boolean;
  tutorial_active: boolean;
  template_setting: any[];
  [key: string]: any;
}

const initialState: SettingState = {
  id: 1,
  user_id: 1,
  show_page_title: true,
  show_intro_text: true,
  show_footer_text: true,
  intro_text_content: JSON.stringify([
    {
      content: 'Check most frequently asked questions here',
      locale: 'default',
    },
  ]),
  page_title_content: JSON.stringify([
    { content: 'Frequently Asked Questions', locale: 'default' },
  ]),
  footer_text_content: JSON.stringify([
    { content: 'Anh Tu Check', locale: 'default' },
  ]),
  faq_sort_name: false,
  faq_uncategory_hidden: true,
  category_sort_name: false,
  dont_category_faq: false,
  faq_page_url: '/apps/faqs',
  faq_template_number: 2,
  page_under_contruction: JSON.stringify([
    { content: 'Page is under construction ???? ??????', locale: 'default' },
  ]),
  search_placehoder: JSON.stringify([
    { content: 'What can we help you with?', locale: 'default' },
  ]),
  search_not_found: JSON.stringify([{ content: '???', locale: 'default' }]),
  show_page_construction: false,
  yanet_logo_visible: true,
  tutorial_active: true,
  template_setting: [
    {
      id: 3,
      setting_id: 5,
      template_number: 2,
      faq_font_color: '#ffffff',
      faq_bg_color: '#0368a2',
      faq_font_size: 16,
      faq_font_weight: 'normal',
      category_font_color: '#000000',
      category_text_align: 'center',
      answer_font_color: '#000000',
      answer_bg_color: '#ffffff',
      banner_height: 300,
      banner_default: '',
      banner_visible: true,
      page_title_fontsize: 28,
      page_title_paddingtop: 14,
      page_title_paddingbottom: 14,
      page_title_color: '#ffffff',
      intro_text_fontsize: 16,
      intro_text_paddingtop: 10,
      intro_text_paddingbottom: 10,
      intro_text_color: '#ffffff',
      footer_text_fontsize: 16,
      footer_text_paddingtop: 10,
      footer_text_paddingbottom: 10,
      footer_text_color: '#000000',
      show_search_input: true,
      page_background_color: '#ffffff',
      category_bar_background: '#f6f6f7',
      category_bar_color: '#000000',
      btn_top_background: '#000000',
      btn_top_hover: '#383838',
      btn_top_visible: false,
      width_faqs_product: 1180,
    },
  ],
};

type SettingAction =
  | { type: 'SET_SETTINGS'; payload: SettingState }
  | { type: 'UPDATE_SETTING'; payload: Partial<SettingState> };

const DesignContext = createContext<{
  state: SettingState;
  dispatch: React.Dispatch<SettingAction>;
}>({
  state: initialState,
  dispatch: () => null,
});

const settingReducer = (
  state: SettingState,
  action: SettingAction,
): SettingState => {
  switch (action.type) {
    case 'SET_SETTINGS':
      return { ...action.payload };
    case 'UPDATE_SETTING':
      return { ...state, ...action.payload };
    default:
      return state;
  }
};

export const DesignProvider = ({ children }: { children: ReactNode }) => {
  const [state, dispatch] = useReducer(settingReducer, initialState);

  return (
    <DesignContext.Provider value={{ state, dispatch }}>
      {children}
    </DesignContext.Provider>
  );
};

export const useDesign = () => useContext(DesignContext);
