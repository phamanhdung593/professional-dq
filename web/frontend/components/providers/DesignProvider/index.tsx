import { createContext, useEffect, useState } from 'react';
import { DesignContextType } from '../../../types/design';

export const Context = createContext<DesignContextType | any>(null);

export const DesignProvider = ({ children }: any) => {
  // Head and checkbox
  const [bannerDefault, setBannerDefault] = useState();
  const [design, setDesign] = useState<any>();
  const [setting, setSetting] = useState<any>();
  const [uploadedFiles, setUploadedFiles] = useState<string>();
  const [isShowBanner, setIsShowBanner] = useState(true);
  const [showPageTitle, setShowPageTitle] = useState(true);
  const [isShowSearchBar, setIsShowSearchBar] = useState(true);
  const [customCss, setCustomCss] = useState('');
  const [showIntroText, setShowIntroText] = useState(true);
  const [isShowCategory, setIsShowCategory] = useState(true);
  const [showFooter, setShowFooter] = useState(true);
  const [headerHeight, setHeaderHeight] = useState(255);
  const [fontSize, setFontSize] = useState(32);
  const [paddingTop, setPaddingTop] = useState(10);
  const [paddingBottom, setPaddingBottom] = useState(10);
  const [headerFontColor, setHeaderFontColor] = useState('#ffffff');
  const [placeholderColor, setPlaceholderColor] = useState('#000000');
  const [introTextFontColor, setIntroTextFontColor] = useState('#ffffff');
  const [pageTitleContent, setPageTitleContent] = useState(
    'Frequently Asked Questions',
  );
  const [searchPlaceholder, setSearchPlaceholder] = useState(
    'What can we help you with?',
  );
  const [introTextContent, setIntroTextContent] = useState(
    'Check most frequently asked questions here',
  );
  const [font, setFont] = useState('unset');
  const [fontSearchbar, setFontSearchbar] = useState('unset');
  const [fontFamilyIntroText, setFontFamilyIntroText] = useState('unset');
  const [fontSizeIntrotext, setFontSizeIntrotext] = useState(10);
  const [paddingTopIntrotext, setPaddingTopIntrotext] = useState(10);
  const [paddingBottomIntrotext, setPaddingBottomIntrotext] = useState(10);
  const [styleSearchBar, setStyleSearchBar] = useState('1');
  const [files, setFiles] = useState<File[]>([]);
  const [microScopeColor, setMicroScopeColor] = useState('');
  const [usePageTitle, setUsePageTitle] = useState(true);

  //Body
  const [rangeValue, setRangeValue] = useState(15);
  const [styleBody, setStyleBody] = useState(1);
  const [bodyBackgroundColor, setBodyBackgroundColor] = useState('#ffffff');
  const [barFontColor, setBarFontColor] = useState('#000000');
  const [bodyFontColor, setBodyFontColor] = useState('#000000');
  const [fontBodyFamily, setFontBodyFamily] = useState('unset');
  const [textAlign, setTextAlign] = useState('center');
  const [fontBodyWeight, setFontBodyWeight] = useState('bold');
  const [fontBodyStyle, setFontBodyStyle] = useState('normal');
  const [fontSizeQuestion, setFontSizeQuestion] = useState(15);
  const [fontWeightQuestion, setFontWeightQuestion] = useState('bold');
  const [fontFamilyQuestion, setFontFamilyQuestion] = useState('unset');
  const [backgroundcolorQuestion, setBackgroundcolorQuestion] =
    useState('#6babdb');
  const [textcolorQuestion, setTextcolorQuestion] = useState('#000000');
  const [hovercolorQuestion, setHovercolorQuestion] = useState('#eeff00');
  const [fontSizeAnswer, setFontSizeAnswer] = useState(15);
  const [fontFamilyAnswer, setFontFamilyAnswer] = useState('unset');
  const [fontWeightAnswer, setFontWeightAnswer] = useState('normal');
  const [backgroundColorAnswer, setBackgroundColorAnswer] = useState('#ffffff');
  const [textColorAnswer, setTextColorAnswer] = useState('#000000');

  //Footer
  const [footerTextContent, setFooterTextContent] = useState(
    'Thanks for visiting our page!',
  );
  const [fontFamilyFooter, setFontFamilyFooter] = useState('unset');
  const [textColorFooter, setTextColorFooter] = useState('#000000');
  const [rangeFontSizeFooter, setRangeFontSizeFooter] = useState(10);
  const [rangePaddingTopFooter, setRangePaddingTopFooter] = useState(10);
  const [rangePaddingBottomFooter, setRangePaddingBottomFooter] = useState(10);

  //More
  const [buttonBackgroundColorMore, setButtonBackgroundColorMore] =
    useState<string>('#ffffff');
  const [rangeFaqPageMore, setRangeFaqPageMore] = useState(1280);
  const [rangeProductFaqMore, setRangeProductFaqMore] = useState(1280);
  const [buttonColorMore, setButtonColorMore] = useState<string>('#000000');
  const [buttonHoverColorMore, setButtonHoverColorMore] =
    useState<string>('#000000');

  const [templateNumber, setTemplateNumber] = useState('2');
  const [isBackToTopMore, setIsBackToTopMore] = useState(true);

  useEffect(() => {
    if (!design) return;
    if (design) {
      setTemplateNumber(design.template_number);
      setBannerDefault(design.banner_default);
      setUploadedFiles(design.image_banner);
      setFontSizeQuestion(design.faq_font_size);
      setFontWeightQuestion(design.faq_font_weight);
      setFontFamilyQuestion(design.faq_font_family);
      setBackgroundcolorQuestion(design.faq_bg_color);
      setTextcolorQuestion(design.faq_font_color);
      setHovercolorQuestion(design.faq_hover_color);

      setBodyFontColor(design.category_font_color);
      setRangeValue(design.category_font_size);
      setFontBodyWeight(design.category_font_weight);
      setFontBodyFamily(design.category_font_family);
      setFontBodyStyle(design.category_text_style);
      setTextAlign(design.category_text_align);
      setStyleBody(design.category_bar_number);
      setBodyBackgroundColor(design.category_bar_background);
      setBarFontColor(design.category_bar_color);
      setIsShowCategory(design.show_category_bar);

      setFontSizeAnswer(design.answer_font_size);
      setFontFamilyAnswer(design.answer_font_family);
      setFontWeightAnswer(design.answer_font_weight);
      setBackgroundColorAnswer(design.answer_bg_color);
      setTextColorAnswer(design.answer_font_color);
      setCustomCss(design.custom_css);

      setRangeFaqPageMore(design.width_faqs_accordian);
      setRangeProductFaqMore(design.width_faqs_product);
      setIsShowBanner(design.banner_visible);
      setHeaderHeight(design.banner_height);

      setFontFamilyIntroText(design.intro_text_font);
      setIntroTextFontColor(design.intro_text_color);
      setFontSizeIntrotext(design.intro_text_fontsize);
      setPaddingTopIntrotext(design.intro_text_paddingtop);
      setPaddingBottomIntrotext(design.intro_text_paddingbottom);

      setFont(design.page_title_font);
      setHeaderFontColor(design.page_title_color);
      setFontSize(design.page_title_fontsize);
      setPaddingTop(design.page_title_paddingtop);
      setPaddingBottom(design.page_title_paddingbottom);

      setRangePaddingTopFooter(design.footer_text_paddingtop);
      setRangeFontSizeFooter(design.footer_text_fontsize);
      setRangePaddingBottomFooter(design.footer_text_paddingbottom);
      setTextColorFooter(design.footer_text_color);
      setFontFamilyFooter(design.footer_text_font);

      setIsShowSearchBar(design.show_search_input);
      setStyleSearchBar(design.search_input_style);
      setFontSearchbar(design.search_placeholder_font);
      setPlaceholderColor(design.placeholder_color);
      setButtonBackgroundColorMore(design.page_background_color);

      setMicroScopeColor(design.micro_scope_color);
      setButtonColorMore(design.btn_top_background);
      setButtonHoverColorMore(design.btn_top_hover);
      setIsBackToTopMore(design.btn_top_visible);
    }

    if (!setting) return;
    if (setting) {
      setPageTitleContent(JSON.parse(setting?.page_title_content)[0].content);
      setFooterTextContent(JSON.parse(setting?.footer_text_content)[0].content);
      setSearchPlaceholder(JSON.parse(setting?.search_placehoder)[0].content);
      setIntroTextContent(JSON.parse(setting?.intro_text_content)[0].content);
      setShowPageTitle(setting?.show_page_title);
      setUsePageTitle(setting?.title_product_faq);
      setShowIntroText(setting?.show_intro_text);
      setShowFooter(setting?.show_footer_text);
    }
  }, [design, setting]);

  return (
    <Context.Provider
      value={{
        //Head and checkbox
        bannerDefault,
        setSetting,
        setDesign,
        templateNumber,
        setTemplateNumber,
        uploadedFiles,
        setUploadedFiles,
        usePageTitle,
        setUsePageTitle,
        isShowBanner,
        setIsShowBanner,
        showPageTitle,
        setShowPageTitle,
        isShowSearchBar,
        setIsShowSearchBar,
        showIntroText,
        setShowIntroText,
        isShowCategory,
        setIsShowCategory,
        showFooter,
        setShowFooter,
        headerHeight,
        setHeaderHeight,
        fontSize,
        setFontSize,
        paddingTop,
        setPaddingTop,
        paddingBottom,
        setPaddingBottom,
        headerFontColor,
        setHeaderFontColor,
        pageTitleContent,
        setPageTitleContent,
        searchPlaceholder,
        setSearchPlaceholder,
        placeholderColor,
        setPlaceholderColor,
        font,
        setFont,
        fontSearchbar,
        setFontSearchbar,
        fontFamilyIntroText,
        setFontFamilyIntroText,
        introTextContent,
        setIntroTextContent,
        introTextFontColor,
        setIntroTextFontColor,
        fontSizeIntrotext,
        setFontSizeIntrotext,
        paddingTopIntrotext,
        setPaddingTopIntrotext,
        paddingBottomIntrotext,
        setPaddingBottomIntrotext,
        styleSearchBar,
        setStyleSearchBar,
        files,
        setFiles,
        microScopeColor,
        setMicroScopeColor,

        // body
        rangeValue,
        setRangeValue,
        styleBody,
        setStyleBody,
        bodyBackgroundColor,
        setBodyBackgroundColor,
        barFontColor,
        setBarFontColor,
        bodyFontColor,
        setBodyFontColor,
        fontBodyFamily,
        setFontBodyFamily,
        textAlign,
        setTextAlign,
        fontBodyWeight,
        setFontBodyWeight,
        fontBodyStyle,
        setFontBodyStyle,
        fontSizeQuestion,
        setFontSizeQuestion,
        fontWeightQuestion,
        setFontWeightQuestion,
        fontFamilyQuestion,
        setFontFamilyQuestion,
        backgroundcolorQuestion,
        setBackgroundcolorQuestion,
        textcolorQuestion,
        setTextcolorQuestion,
        hovercolorQuestion,
        setHovercolorQuestion,
        fontSizeAnswer,
        setFontSizeAnswer,
        fontFamilyAnswer,
        setFontFamilyAnswer,
        fontWeightAnswer,
        setFontWeightAnswer,
        backgroundColorAnswer,
        setBackgroundColorAnswer,
        textColorAnswer,
        setTextColorAnswer,
        customCss,
        setCustomCss,

        //Footer
        footerTextContent,
        setFooterTextContent,
        fontFamilyFooter,
        setFontFamilyFooter,
        textColorFooter,
        setTextColorFooter,
        rangeFontSizeFooter,
        setRangeFontSizeFooter,
        rangePaddingTopFooter,
        setRangePaddingTopFooter,
        rangePaddingBottomFooter,
        setRangePaddingBottomFooter,

        //More
        buttonBackgroundColorMore,
        setButtonBackgroundColorMore,
        rangeFaqPageMore,
        setRangeFaqPageMore,
        rangeProductFaqMore,
        setRangeProductFaqMore,
        buttonColorMore,
        setButtonColorMore,
        buttonHoverColorMore,
        setButtonHoverColorMore,
        isBackToTopMore,
        setIsBackToTopMore,
      }}
    >
      {children}
    </Context.Provider>
  );
};
