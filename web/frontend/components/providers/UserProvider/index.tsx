import React, {
  createContext,
  useReducer,
  useContext,
  ReactNode,
  useEffect,
} from 'react';
import { useUserApi } from '../../../hook/user';
import { useCrisp } from '../../crisp';

// Define the User State interface
interface UserState {
  id: number;
  store_name: string;
  shopify_domain: string;
  email: string;
  phone: string;
  shopLocales: string;
  createdAt: string;
  updatedAt: string;
  plan_extra: any;
  faq: any[];
  faq_category: any[];
  faq_messages: any[];
  faq_messages_setting: any[];
  faq_more_page: any[];
  faq_more_page_setting: Record<string, any>;
  faq_product: any[];
  merchants_plan: any;
  merchants_rating: any;
  product: any[];
  setting: Record<string, any>;
}

// Initial User State
const initialUserState: UserState = {
  id: 1,
  store_name: '',
  shopify_domain: '',
  email: '',
  phone: '',
  shopLocales: '',
  createdAt: '',
  updatedAt: '',
  plan_extra: null,
  faq: [],
  faq_category: [],
  faq_messages: [],
  faq_messages_setting: [],
  faq_more_page: [],
  faq_more_page_setting: {},
  faq_product: [],
  merchants_plan: null,
  merchants_rating: null,
  product: [],
  setting: {},
};

// Define actions
type UserAction =
  | { type: 'SET_USER'; payload: UserState }
  | { type: 'UPDATE_USER'; payload: Partial<UserState> };

// Create User Context
const UserContext = createContext<{
  state: UserState;
  dispatch: React.Dispatch<UserAction>;
}>({
  state: initialUserState,
  dispatch: () => null,
});

// User Reducer
const userReducer = (state: UserState, action: UserAction): UserState => {
  switch (action.type) {
    case 'SET_USER':
      return { ...action.payload };
    case 'UPDATE_USER':
      return { ...state, ...action.payload };
    default:
      return state;
  }
};

export const UserProvider = ({ children }: { children: ReactNode }) => {
  const [state, dispatch] = useReducer(userReducer, initialUserState);
  const { initialize } = useCrisp();
  const { data } = useUserApi();

  useEffect(() => {
    if (data) {
      dispatch({ type: 'SET_USER', payload: data });
    }
  }, [data]);

  useEffect(() => {
    if (data) {
      initialize({
        id: `${data.id}`,
        email: data.email,
        name: data.store_name,
        shopDomain: data.shopify_domain,
      });
    }
  }, [initialize, data]);

  return (
    <UserContext.Provider value={{ state, dispatch }}>
      {children}
    </UserContext.Provider>
  );
};

export const useUserContext = () => useContext(UserContext);
