import React, { useEffect } from 'react';
import { DesignProvider } from './DesignProvider';
import { UserProvider } from './UserProvider';
import { useAppBridge } from '@shopify/app-bridge-react';
import { useLocation } from 'react-router-dom';

interface AppProvidersProps {
  children: React.ReactNode;
}

export const AppProviders: React.FC<AppProvidersProps> = ({ children }) => {
  const app = useAppBridge();
  const location = useLocation();

  useEffect(() => {
    app.loading(true);
    const timer = setTimeout(() => {
      app.loading(false);
    }, 1000);

    return () => clearTimeout(timer);
  }, [location, app]);

  return (
    <UserProvider>
      <DesignProvider> {children} </DesignProvider>;
    </UserProvider>
  );
};
