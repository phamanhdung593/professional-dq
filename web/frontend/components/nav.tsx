import { NavMenu } from '@shopify/app-bridge-react';
import { Link } from 'react-router-dom';
import { useTranslation } from 'react-i18next';

export default function Nav() {
  const { t } = useTranslation();
  return (
    <NavMenu>
      <Link to="/" rel="home">
        {t('NavigationMenu.faqs')}
      </Link>
      <Link to="/faqs">{t('NavigationMenu.faqs')}</Link>
      <Link to="/categories">{t('NavigationMenu.category')}</Link>
      <Link to="/designs">{t('NavigationMenu.design')}</Link>
      <Link to="/product-faqs">{t('NavigationMenu.product-faqs')}</Link>
      <Link to="/widget">{t('NavigationMenu.widgets')}</Link>
      <Link to="/guides">{t('NavigationMenu.guides')}</Link>
      <Link to="/plans">{t('NavigationMenu.plans')}</Link>
      <Link to="/settings">{t('NavigationMenu.setting')}</Link>
    </NavMenu>
  );
}
