import { Editor } from '@tinymce/tinymce-react';
import '../assets/css/editor.css';
import { FormError } from '@shopify/react-form';
import { Icon } from '@shopify/polaris';
import { AlertCircleIcon } from '@shopify/polaris-icons';

export type EditorProps = {
  content: string;
  setContent: (content: string) => void;
  height: number;
  error?: FormError[];
};

const TinyEditor = (props: EditorProps) => {
  const { content, setContent, height, error } = props;
  const descriptionError = error?.filter(({ message }) =>
    message.includes('Description'),
  );

  const toolbar =
    'undo redo blocks formatselect bold italic backcolor alignleft aligncenter alignright alignjustify bullist numlist outdent indent code preview';

  return (
    <>
      <Editor
        value={content}
        onEditorChange={(newValue) => {
          setContent(newValue);
        }}
        apiKey="gtp58s5fa9eoj1vho1lz5bluv5hjoeyfeuz8r8kt46hbciq6"
        init={{
          menubar: 'edit insert format',
          plugins: ['link'],
          height: height,
          toolbar: toolbar,
          content_style:
            'body { font-family: inherit, Arial,sans-serif; font-size:13px; line-height: 14px }',
        }}
      />
      {descriptionError && descriptionError.length > 0 && (
        <div id="value0Error" className="Polaris-InlineError">
          <div className="Polaris-InlineError__Icon">
            <span className="Polaris-Icon">
              <span className="Polaris-Text--root Polaris-Text--visuallyHidden"></span>
              <Icon source={AlertCircleIcon} tone="textCritical" />
            </span>
          </div>
          {descriptionError[0].message}
        </div>
      )}
    </>
  );
};

export default TinyEditor;
