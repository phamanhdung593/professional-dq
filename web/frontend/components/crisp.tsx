import { useCallback } from 'react';
import { ChatboxColors, Crisp } from 'crisp-sdk-web';

interface IInitializeArgs {
  id: string;
  email: string;
  name: string;
  shopDomain: string;
}

export function useCrisp() {
  const initialize = useCallback(
    ({ id, email, name, shopDomain }: IInitializeArgs) => {
      if (Crisp.isCrispInjected()) {
        return;
      }
      Crisp.configure('35cbcb5a-831c-47fb-9064-0bced009fca9');
      Crisp.setTokenId(id);
      Crisp.user.setEmail(email);
      Crisp.user.setNickname(`[FAQs] - ${name} - ${shopDomain}`);
      Crisp.setColorTheme(ChatboxColors.Orange);
      Crisp.session.setSegments(['faqs'], true);

      if (!Crisp.isCrispInjected()) {
        console.log('Crisp chat error');
      } else {
        console.log('Crisp chat....');
      }
    },
    [],
  );

  const toggle = useCallback(() => {
    try {
      const element: HTMLElement = document.getElementsByClassName(
        'cc-nsge',
      )[0] as HTMLElement;
      element.click();
    } catch (e) {
      window.open(
        'https://go.crisp.chat/chat/embed/?website_id=35cbcb5a-831c-47fb-9064-0bced009fca9',
        '_blank',
      );
    }
  }, []);

  return {
    toggle,
    initialize,
  };
}
