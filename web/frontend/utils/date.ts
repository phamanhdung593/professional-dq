import dayjs from 'dayjs';
import utc from 'dayjs/plugin/utc';
import timezone from 'dayjs/plugin/timezone';
dayjs.extend(utc);
dayjs.extend(timezone);

export function timeDifferenceFromNow(utcTime: string) {
  const utcDayjs = dayjs.utc(utcTime);
  const localDayjs = dayjs();

  const differenceInDays = localDayjs.diff(utcDayjs, 'day');
  const differenceInHours = localDayjs.diff(utcDayjs, 'hour');

  if (differenceInDays <= 7) {
    if (differenceInDays > 0) {
      return `${differenceInDays} day${differenceInDays > 1 ? 's' : ''} ago`;
    } else {
      return `${differenceInHours} hour${differenceInHours > 1 ? 's' : ''} ago`;
    }
  } else {
    return utcDayjs.local().format('YYYY-MM-DD HH:mm');
  }
}
