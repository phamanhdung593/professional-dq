import { PolarisProvider } from './components';
import { Outlet, useLocation } from 'react-router-dom';
import QueryProvider from './components/providers/QueryProvider';
import Nav from './components/nav';
import './index.css';
import { AppProviders } from './components/providers/AppProvider';
import { useAppBridge } from '@shopify/app-bridge-react';
import { useEffect } from 'react';

export default function App() {
  const app = useAppBridge();
  const location = useLocation();

  useEffect(() => {
    app.loading(true);
    const timer = setTimeout(() => {
      app.loading(false);
    }, 1000);

    return () => clearTimeout(timer);
  }, [location, app]);

  return (
    <QueryProvider>
      <AppProviders>
        <PolarisProvider>
          <Nav />
          <Outlet />
        </PolarisProvider>
      </AppProviders>
    </QueryProvider>
  );
}
