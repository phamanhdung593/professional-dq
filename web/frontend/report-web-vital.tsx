import {
  onCLS,
  onFCP,
  onFID,
  onINP,
  onLCP,
  onTTFB,
} from 'web-vitals/attribution';

const ReportWebVitals = () => {
  onCLS(console.log);
  onFID(console.log);
  onLCP(console.log, {
    reportAllChanges: true,
  });
  onFCP(console.log, {
    reportAllChanges: true,
    durationThreshold: 1,
  });
  onTTFB(console.log);
  onINP(console.log);
};

export default ReportWebVitals;
