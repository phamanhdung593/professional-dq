export type Unit = 'cm' | 'mm' | 'in' | 'pt' | 'px';

export const SHOPIFY_GRAPHQL_PRODUCT_ID_PREFIX = 'gid://shopify/Product/';
export const SHOPIFY_GRAPHQL_VARIANT_ID_PREFIX =
  'gid://shopify/ProductVariant/';
export const SHOPIFY_GRAPHQL_ORDER_ID_PREFIX = 'gid://shopify/Order/';
export const SHOPIFY_GRAPHQL_DRAFT_ORDER_ID_PREFIX =
  'gid://shopify/DraftOrder/';
export const SHOPIFY_GRAPHQL_INVENTORY_ITEM_ID_PREFIX =
  'gid://shopify/InventoryItem/';
export const SHOPIFY_GRAPHQL_APP_SUBSCRIPTION_ID_PREFIX =
  'gid://shopify/AppSubscription/';
export const SHOPIFY_GRAPHQL_APP_CHARGE_ONE_TIME_ID_PREFIX =
  'gid://shopify/AppPurchaseOneTime/';

export const GENERATE_TYPE_PRINT = 'print';
export const GENERATE_TYPE_NEW = 'new';
export const GENERATE_TYPE_REPLACE = 'replace';
export const GENERATE_TYPE_SKU = 'sku';

export const DESIGN_ADVANCED_TYPE = 'advanced';
export const DESIGN_SIMPLE_TYPE = 'simple';

export const DEFAULT_BARCODE_FORMAT = 'CODE128';

export const CUSTOM_TEXT_TYPE = 'customtext';
export const METAFIELD_TYPE = 'metafield';
export const CHECKOUT_URL_TYPE = 'checkoutUrl';
export const BARCODE_TYPE = 'barcode';
export const QRCODE_TYPE = 'qrcode';
export const METAFIELD_PRODUCT_ENTITY = 'product';
export const METAFIELD_VARIANT_ENTITY = 'variant';
export const SYMBOL_TYPE = 'symbol';
export const IMAGE_TYPE = 'image';
export const TIMESTAMP_TYPE = 'timestamp';
export const PRICE_TYPE = 'price';
export const COST_TYPE = 'cost';
export const COMPARE_AT_PRICE_TYPE = 'compareAtPrice';
export const DISCOUNT_TYPE = 'discount';
export const PRODUCT_FEATURE_IMAGE = 'productFeaturedImage';
export const DIVIDE_TYPE = 'divide';

export const METAFIELDS_LIST_WEIGHT = 'list.weight';
export const METAFIELDS_LIST_VOLUME = 'list.volume';
export const METAFIELDS_LIST_DATE_TIME = 'list.date_time';
export const METAFIELDS_LIST_RATING = 'list.rating';
export const METAFIELDS_LIST_COLOR = 'list.color';
export const METAFIELDS_LIST_DATE = 'list.date';
export const METAFIELDS_DATE = 'date';
export const METAFIELDS_COLOR = 'color';
export const METAFIELDS_MONEY = 'money';
export const METAFIELDS_VOLUME = 'volume';
export const METAFIELDS_WEIGHT = 'weight';
export const METAFIELDS_DATE_TIME = 'date_time';
export const METAFIELDS_RATING = 'rating';
export const METAFIELDS_BOOLEAN = 'boolean';
export const DEFAULT_SIZE_ICON_BOOLEAN = 6;
export const METAFIELDS_TRUE_ICON =
  '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16"><path d="M3 14.5A1.5 1.5 0 0 1 1.5 13V3A1.5 1.5 0 0 1 3 1.5h8a.5.5 0 0 1 0 1H3a.5.5 0 0 0-.5.5v10a.5.5 0 0 0 .5.5h10a.5.5 0 0 0 .5-.5V8a.5.5 0 0 1 1 0v5a1.5 1.5 0 0 1-1.5 1.5z"/><path d="m8.354 10.354 7-7a.5.5 0 0 0-.708-.708L8 9.293 5.354 6.646a.5.5 0 1 0-.708.708l3 3a.5.5 0 0 0 .708 0"/></svg>';
export const METAFIELDS_FALSE_ICON =
  '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M64 80c-8.8 0-16 7.2-16 16V416c0 8.8 7.2 16 16 16H448c8.8 0 16-7.2 16-16V96c0-8.8-7.2-16-16-16H64zM0 96C0 60.7 28.7 32 64 32H448c35.3 0 64 28.7 64 64V416c0 35.3-28.7 64-64 64H64c-35.3 0-64-28.7-64-64V96zm175 79c9.4-9.4 24.6-9.4 33.9 0l47 47 47-47c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9l-47 47 47 47c9.4 9.4 9.4 24.6 0 33.9s-24.6 9.4-33.9 0l-47-47-47 47c-9.4 9.4-24.6 9.4-33.9 0s-9.4-24.6 0-33.9l47-47-47-47c-9.4-9.4-9.4-24.6 0-33.9z"/></svg>';

export const MAX_LABEL_TO_PRINT_PER_TIME = 2000;
export const DEFAULT_FONT = 'open_sans';
export const UNIT_LABEL = {
  in: 'Inchs',
  cm: 'Centimets',
  mm: 'Milimets',
};

export const DEFAULT_TEXT_SIZE = 6;
