export enum ESessionStorageKeys {
  SSK_WIDGET_BANNER = 'SSK_WIDGET_BANNER',
  SSK_PRODUCT_BANNER = 'SSK_PRODUCT_BANNER',
  SSK_FAQ_MORE_PAGE_BANNER = 'SSK_FAQ_MORE_PAGE_BANNER',
  SSK_CREATE_SUCCESS = 'SSK_CREATE_SUCCESS',
}

export interface ProductImage {
  id: string;
  originalSrc: string;
}

export interface ProductOption {
  id: string;
  name: string;
  position: number;
  values: string[];
}

export interface FulfillmentService {
  id: string;
  inventoryManagement: boolean;
  productBased: boolean;
  serviceName: string;
  type: string;
}

export interface InventoryItem {
  __typename: string;
  id: string;
}

export interface SelectedOption {
  __typename: string;
  value: string;
}

export interface ProductVariant {
  availableForSale: boolean;
  barcode: string | null;
  compareAtPrice: string | null;
  createdAt: string;
  displayName: string;
  fulfillmentService: FulfillmentService;
  id: string;
  inventoryItem: InventoryItem;
  inventoryManagement: string;
  inventoryPolicy: string;
  inventoryQuantity: number;
  position: number;
  price: string;
  product: {
    __typename: string;
    id: string;
  };
  requiresShipping: boolean;
  selectedOptions: SelectedOption[];
  sku: string;
  taxable: boolean;
  title: string;
  updatedAt: string;
  weight: number;
  weightUnit: string;
}

export interface Product {
  availablePublicationCount: number;
  createdAt: string;
  descriptionHtml: string;
  handle: string;
  hasOnlyDefaultVariant: boolean;
  id: string;
  images: ProductImage[];
  options: ProductOption[];
  productType: string;
  tags: string[];
  templateSuffix: string;
  title: string;
  totalInventory: number;
  totalVariants: number;
  tracksInventory: boolean;
  updatedAt: string;
  variants: ProductVariant[];
  vendor: string;
  status: string;
}
