export interface IUser {
  id: number;
  store_name: string;
  shopify_domain: string;
  email: string;
  phone: null;
  shopLocales: string;
  createdAt: string;
  updatedAt: string;
  plan_extra: null;
  merchants_plan: {
    id: number;
    user_id: number;
    plan?: string;
    plan_extra: string | null;
    shopify_plan_id: string;
    expiry_date: string;
    purchase_date: string;
    createdAt: string;
    updatedAt: string;
  };
  faq_more_page_setting: {
    id: number;
    user_id: number;
    home_page_visible: boolean;
    product_page_visible: boolean;
    cart_page_visible: boolean;
    collection_page_visible: boolean;
    cms_page_visible: boolean;
    createdAt: string;
    updatedAt: string;
    active_feature?: string | null;
    active_template?: string | null;
  };
}
