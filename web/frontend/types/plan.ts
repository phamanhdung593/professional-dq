import {
  ReactElement,
  JSXElementConstructor,
  ReactNode,
  ReactPortal,
} from 'react';

export interface PlanType {
  confirmationUrl?: string;
  appSubscription?: {
    id: string;
    status: string;
    test: boolean;
    trialDays: number;
  };
}

export interface PlanCardProps {
  title:
    | string
    | number
    | boolean
    | ReactElement<any, string | JSXElementConstructor<any>>
    | Iterable<ReactNode>
    | ReactPortal
    | Iterable<ReactNode>
    | null
    | undefined;
  price:
    | string
    | number
    | boolean
    | ReactElement<any, string | JSXElementConstructor<any>>
    | Iterable<ReactNode>
    | ReactPortal
    | Iterable<ReactNode>
    | null
    | undefined;
  pricePeriod:
    | string
    | number
    | boolean
    | ReactElement<any, string | JSXElementConstructor<any>>
    | Iterable<ReactNode>
    | ReactPortal
    | Iterable<ReactNode>
    | null
    | undefined;
  buttonText: string | string[] | undefined;
  features: any[];
  isCurrentPlan: number | boolean;
  currentPlanFree: boolean;
  handleSubmitPlan: () => void;
  handleCancel: (() => void) | null;
}
