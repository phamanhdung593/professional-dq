export type FaqsApi = Faq[];

export type Faq = {
  category_identify?: string;
  content: string;
  createdAt?: string;
  feature_faq: boolean;
  id?: number | string;
  identify?: string;
  is_visible: boolean;
  locale: string;
  position?: number;
  title: string;
  updatedAt?: string;
  user_id?: number;
  category_name?: string;
  isDisable?: boolean;
  created_at?: string;
  updated_at?: string;
};

export type FaqsIdString = {
  category_identify?: string;
  content?: string;
  createdAt?: string;
  feature_faq?: boolean;
  id?: string;
  identify?: string;
  is_visible?: boolean;
  locale?: string;
  position?: number;
  title?: string;
  updatedAt?: string;
  user_id?: number;
  category_name?: string;
  isDisable?: boolean;
};

export interface FaqV2 {
  id: number;
  user_id: number;
  category_identify: string;
  identify: string;
  title: string;
  content: string;
  is_visible: boolean;
  position: number;
  language?: string[] | ['default'];
  locale: string;
  createdAt: string;
  updatedAt: string;
  feature_faq: boolean;
  faq_more_page: any[];
  faq_product: any[];
  faq_category: FaqCategory;
}

export interface FaqCategory {
  id: number;
  user_id: number;
  identify: string;
  title: string;
  description: string;
  position: number;
  locale: string;
  is_visible: boolean;
  createdAt: string; // ISO date string
  updatedAt: string; // ISO date string
  feature_category: boolean;
}
