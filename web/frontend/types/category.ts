export type CategoryApi = Category[];

export type Category = {
  id: number;
  user_id: number;
  identify: string;
  title: string;
  description: string;
  position: number;
  locale: string;
  is_visible: boolean;
  createdAt: string;
  updatedAt: string;
  feature_category: boolean;
  faq: any[];
  language: string[];
};
