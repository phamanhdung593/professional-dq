export type CategoryApi = Category[];

export type Category = {
  createdAt?: string;
  description?: string;
  feature_category?: boolean;
  id?: number;
  identify?: string;
  is_visible?: boolean;
  locale?: string;
  position?: number;
  title?: string;
  updatedAt?: string;
  user_id?: number;
};
