import { createBrowserRouter } from 'react-router-dom';
import App from './App';
import HomePage from './pages/dashboard';
import Error from './pages/Error';
import NotFound from './pages/NotFound';
import ExitIframe from './pages/ExitIframe';
import { Guides } from './pages/guides';
import { Designs } from './pages/designs';
import { Widget } from './pages/widget';
import { ProductFaq } from './pages/product-faqs';
import { Categories } from './pages/categories';
import { Plans } from './pages/plans';
import Faqs from './pages/faqs';
import { Settings } from './pages/settings';
import FaqMorePage from './pages/faq-more-page/Index';
import CreateFaq from './pages/faqs/create';
import CreateCategory from './pages/categories/CreateCategory';
import EditCategory from './pages/categories/UpdataCategory';
import EditFaq from './pages/faqs/Edit';
import AddFaqToProduct from './pages/product-faqs/AddFaqToProduct';

export const router = createBrowserRouter([
  {
    path: '/',
    element: <App />,
    errorElement: <Error />,
    children: [
      { path: '/', element: <HomePage />, index: true },
      { path: '/faqs', element: <Faqs /> },
      { path: '/faq/:id', element: <EditFaq /> },
      { path: '/faq-more-page', element: <FaqMorePage /> },
      { path: '/faq/create', element: <CreateFaq /> },
      { path: '/categories', element: <Categories /> },
      { path: '/categories/create', element: <CreateCategory /> },
      { path: '/categories/:id', element: <EditCategory /> },
      { path: '/designs', element: <Designs /> },
      { path: '/product-faqs', element: <ProductFaq /> },
      { path: '/product-faqs/add-product', element: <AddFaqToProduct /> },
      { path: '/product-faqs/:product_id/faq', element: <ProductFaq /> },
      { path: '/widget', element: <Widget /> },
      { path: '/guides', element: <Guides /> },
      { path: '/plans', element: <Plans /> },
      { path: '/settings', element: <Settings /> },
      { path: '/exitiframe', element: <ExitIframe /> },
      { path: '*', element: <NotFound /> },
    ],
  },
]);
