import { t } from 'i18next';
import { BlockStack, Page } from '@shopify/polaris';
import AdvanceConfiguration from './AdvanceConfiguration';
import DisplaySettings from './DisplaySettings';
import SearchBarSettings from './SearchBarSettings';
import GlobalSettings from './GlobalSettings';
import { Setting } from '../../types/setting';
import { useEffect, useState } from 'react';
import { DataSettings } from '../../types/translation';
import { useSettingApi, useUpdateSettingApi } from '../../hook/setting';
import { useAppBridge } from '@shopify/app-bridge-react';
import { General } from './General';

export const Settings = () => {
  const app = useAppBridge();
  const [settings, setSettings] = useState<Setting>();
  const { data: settingsData } = useSettingApi() as { data: DataSettings };
  const { mutate: updateSetting } = useUpdateSettingApi();
  const [isLoading, setIsLoading] = useState(false);

  const props = { setting: settingsData, setSettings: setSettings };

  const [isSeoFaqPage, setIsSeoFaqPage] = useState<boolean>(false);
  const [isSeoProductPage, setIsSeoProductPage] = useState<boolean>(false);
  const [metaTagDescription, setMetaTagDescription] = useState<string>('');
  const [viewFaqPage, setViewFaqPage] = useState<string>('');
  const [isSortByCategory, setIsSortByCategory] = useState<boolean>(false);
  const [isSortByFaqs, setIsSortByFaqs] = useState<boolean>(false);
  const [isDontCategory, setIsDontCategory] = useState<boolean>(false);
  const [isShowIntroText, setIsShowIntroText] = useState<boolean>(false);
  const [isShowPageTitle, setIsShowPageTitle] = useState<boolean>(false);
  const [isShowFooterText, setIsShowFooterText] = useState<boolean>(false);
  const [isShowPageUnderConstruction, setIsShowPageUnderConstruction] =
    useState<boolean>(false);

  const generalProps = {
    viewFaqPage,
    setViewFaqPage,
    settings,
  };

  const AdvanceConfigurationProps = {
    isSeoFaqPage,
    isSeoProductPage,
    metaTagDescription,
    setIsSeoFaqPage,
    setIsSeoProductPage,
    setMetaTagDescription,
  };

  const DisplaySettingsProps = {
    isSortByCategory,
    isSortByFaqs,
    isDontCategory,
    setIsSortByCategory,
    setIsSortByFaqs,
    setIsDontCategory,
  };

  useEffect(() => {
    if (settingsData) {
      setIsSeoFaqPage(settingsData.faq_page_schema);
      setIsSeoProductPage(settingsData.more_page_schema);
      setMetaTagDescription(settingsData.meta_tag_description);
      setViewFaqPage(settingsData.faq_page_url);
      setIsSortByCategory(settingsData.category_sort_name);
      setIsSortByFaqs(settingsData.faq_sort_name);
      setIsDontCategory(settingsData.dont_category_faq);
      setIsShowIntroText(settingsData.show_intro_text);
      setIsShowPageTitle(settingsData.show_page_title);
      setIsShowFooterText(settingsData.show_footer_text);
      setIsShowPageUnderConstruction(settingsData.show_page_construction);
    }
  }, [settingsData]);

  const updateSettings = () => {
    setIsLoading(true);
    const dataUpdate = {
      id: settingsData.id,
      faq_page_url: viewFaqPage,
      category_sort_name: isSortByCategory,
      faq_sort_name: isSortByFaqs,
      dont_category_faq: isDontCategory,
      faq_page_schema: isSeoFaqPage,
      more_page_schema: isSeoProductPage,
      meta_tag_description: metaTagDescription,
      show_intro_text: isShowIntroText,
      show_page_title: isShowPageTitle,
      show_footer_text: isShowFooterText,
      show_page_construction: isShowPageUnderConstruction,
    };

    updateSetting(
      {
        ...dataUpdate,
      },
      {
        onSuccess() {
          setIsLoading(false);
          app.toast.show('Update setting success!');
        },
        onError: () => {
          setIsLoading(false);
          app.toast.show('Update setting error!');
        },
      },
    );
  };

  const GlobalSettingsProps = {
    isShowIntroText,
    isShowPageTitle,
    isShowFooterText,
    isShowPageUnderConstruction,
    setIsShowIntroText,
    setIsShowPageTitle,
    setIsShowFooterText,
    setIsShowPageUnderConstruction,
    settings,
    updateSettings,
  };

  return (
    <Page
      title={t('Settings')}
      primaryAction={{
        content: 'Save',
        onAction: () => updateSettings(),
        loading: isLoading,
      }}
    >
      <BlockStack gap={'400'}>
        <General {...generalProps} />
        <AdvanceConfiguration {...AdvanceConfigurationProps} />
        <DisplaySettings {...DisplaySettingsProps} />
        <SearchBarSettings {...props} />
        <GlobalSettings {...GlobalSettingsProps} />
      </BlockStack>
    </Page>
  );
};
