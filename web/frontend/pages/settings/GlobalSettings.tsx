import {
  Layout,
  Card,
  Text,
  TextField,
  BlockStack,
  Icon,
} from '@shopify/polaris';
import { useState, useCallback, useEffect } from 'react';
import { LanguageFilledIcon } from '@shopify/polaris-icons';
import ModalTranslationSetting from '../components/settings/ModalTranslationSetting';
import CheckBox from '../components/settings/CheckBox';
import { Setting } from '../../types/setting';
import { useSettingApi } from '../../hook/setting';
import { DataSettings } from '../../types/translation';

type GlobalSettingsProps = {
  isShowIntroText: boolean;
  isShowPageTitle: boolean;
  isShowFooterText: boolean;
  isShowPageUnderConstruction: boolean;
  setIsShowIntroText: (value: boolean) => void;
  setIsShowPageTitle: (value: boolean) => void;
  setIsShowFooterText: (value: boolean) => void;
  setIsShowPageUnderConstruction: (value: boolean) => void;
  settings?: Setting;
  updateSettings: () => void;
};

const GlobalSettings = (props: GlobalSettingsProps) => {
  const { data: settingsData } = useSettingApi() as { data: DataSettings };

  const [introText, setIntroText] = useState('');
  const [pageTitle, setPageTitle] = useState('');
  const [footerText, setFooterText] = useState('');
  const [pageUnderConstruction, setPageUnderConstruction] = useState('');

  useEffect(() => {
    const introTextContent = settingsData
      ? JSON.parse(settingsData.intro_text_content)[0]?.content
      : null;
    const pagetitleContent = settingsData
      ? JSON.parse(settingsData?.page_title_content)[0]?.content
      : null;
    const footerTextContent = settingsData
      ? JSON.parse(settingsData?.footer_text_content)[0]?.content
      : null;
    const pageUnderContruction = settingsData
      ? JSON.parse(settingsData?.page_under_contruction)[0]?.content
      : null;

    setIntroText(introTextContent);
    setFooterText(footerTextContent);
    setPageTitle(pagetitleContent);
    setPageUnderConstruction(pageUnderContruction);
  }, [settingsData]);

  const handleIntroTextChange = useCallback(
    (newValue: string) => setIntroText(newValue),
    [],
  );
  const handleFooterTextChange = useCallback(
    (newValue: string) => setFooterText(newValue),
    [],
  );
  const handleUnderConstructionChange = useCallback(
    (newValue: string) => setPageUnderConstruction(newValue),
    [],
  );
  const handlePageTitleChange = useCallback(
    (newValue: string) => setPageTitle(newValue),
    [],
  );
  const [modalType, setModalType] = useState<string>('');
  const [isActive, seIsActiveModal] = useState<boolean>(false);

  useEffect(() => {
    if (props.settings) {
      setIntroText(
        props?.settings?.intro_text_content
          ? JSON.parse(props?.settings?.intro_text_content)[0].content
          : '',
      );
      setFooterText(
        props?.settings.footer_text_content
          ? JSON.parse(props?.settings.footer_text_content)[0].content
          : '',
      );
      setPageTitle(
        props?.settings?.page_title_content
          ? JSON.parse(props?.settings?.page_title_content)[0].content
          : '',
      );
      setPageUnderConstruction(
        props.settings.page_under_contruction
          ? JSON.parse(props.settings.page_under_contruction)[0].content
          : '',
      );
    }
  }, [props]);

  const showModal = useCallback((type: string) => {
    setModalType(type);
    seIsActiveModal(true);
  }, []);

  let modalProps: any;

  switch (modalType) {
    case 'Intro text':
      modalProps = {
        title: modalType,
        value: settingsData?.intro_text_content,
        action: setIntroText,
        isActive: isActive,
        seIsActive: seIsActiveModal,
        updateSettings: props?.updateSettings,
        type: 'intro_text_content',
        settingId: settingsData?.id,
      };
      break;
    case 'Footer text':
      modalProps = {
        title: modalType,
        value: settingsData?.footer_text_content,
        action: setFooterText,
        isActive: isActive,
        seIsActive: seIsActiveModal,
        updateSettings: props?.updateSettings,
        type: 'footer_text_content',
        settingId: settingsData?.id,
      };
      break;
    case 'FAQs page title':
      modalProps = {
        title: modalType,
        value: settingsData?.page_title_content,
        action: setPageTitle,
        isActive: isActive,
        seIsActive: seIsActiveModal,
        updateSettings: props?.updateSettings,
        type: 'page_title_content',
        settingId: settingsData?.id,
      };
      break;
    case 'Page under construction':
      modalProps = {
        title: modalType,
        value: settingsData?.page_under_contruction,
        action: setPageUnderConstruction,
        isActive: isActive,
        seIsActive: seIsActiveModal,
        updateSettings: props?.updateSettings,
        type: 'page_under_contruction',
        settingId: settingsData?.id,
      };
      break;
    default:
      modalProps = {
        title: modalType,
        value: settingsData?.intro_text_content,
        action: setIntroText,
        isActive: isActive,
        seIsActive: seIsActiveModal,
        updateSettings: props?.updateSettings,
        type: 'intro_text_content',
        settingId: props?.settings?.id,
      };
      break;
  }

  return (
    <>
      <Layout>
        <Layout.Section variant="oneThird">
          <div>
            <>
              <Text id="storeDetails" variant="headingMd" as="h2">
                Global settings
              </Text>
              <Text tone="subdued" as="p">
                You can setup header and footer text along with choosing right
                template for customer facing FAQ page.
              </Text>
            </>
          </div>
        </Layout.Section>
        <Layout.Section>
          <Card>
            <BlockStack gap={'400'}>
              <div>
                <CheckBox
                  label="Show intro text"
                  value={props.isShowIntroText}
                  onChange={(value) => {
                    props.setIsShowIntroText(value);
                  }}
                />
                {props.isShowIntroText && (
                  <div className="ms-7">
                    <div onClick={() => showModal('Intro text')}>
                      <TextField
                        label=""
                        value={introText || ''}
                        onChange={handleIntroTextChange}
                        autoComplete="off"
                        placeholder="Check most frequently asked questions here"
                      />
                    </div>
                    <div className="flex justify-between items-start">
                      <div className="opacity-70 text-sm  pointer-events-none">
                        <Text variant="bodyMd" as="p">
                          This would be shown above all FAQs. Please don't
                          forget single quotes around href tag if you change
                          this.
                        </Text>
                      </div>
                    </div>
                    <div
                      onClick={() => showModal('Intro text')}
                      className="flex justify-end cursor-pointer text-orange-500"
                    >
                      <div>
                        <Icon source={LanguageFilledIcon}></Icon>
                      </div>
                      <p className="ms-1">Edit & Add Translation</p>
                    </div>
                  </div>
                )}
              </div>
              <div>
                <CheckBox
                  label="Show footer text"
                  value={props.isShowFooterText}
                  onChange={(value) => {
                    props.setIsShowFooterText(value);
                  }}
                />
                {props.isShowFooterText && (
                  <div className="ms-7">
                    <div onClick={() => showModal('Footer text')}>
                      <TextField
                        label=""
                        value={footerText || ''}
                        onChange={handleFooterTextChange}
                        autoComplete="off"
                        placeholder="Thanks for visiting our page!"
                      />
                    </div>
                    <div className="flex justify-between items-start">
                      <div className="opacity-70 text-sm  pointer-events-none">
                        <Text variant="bodyMd" as="p">
                          This would be shown below all FAQs.
                        </Text>
                      </div>
                    </div>
                    <div
                      onClick={() => showModal('Footer text')}
                      className="flex justify-end cursor-pointer text-orange-500"
                    >
                      <div>
                        <Icon source={LanguageFilledIcon}></Icon>
                      </div>
                      <p className="ms-1">Edit & Add Translation</p>
                    </div>
                  </div>
                )}
              </div>
              <div>
                <CheckBox
                  label="Show page title"
                  value={props.isShowPageTitle}
                  onChange={(value) => {
                    props.setIsShowPageTitle(value);
                  }}
                />
                {props.isShowPageTitle && (
                  <div className="ms-7">
                    <div onClick={() => showModal('FAQs page title')}>
                      <TextField
                        label=""
                        value={pageTitle || ''}
                        onChange={handlePageTitleChange}
                        autoComplete="off"
                        placeholder="Thanks for visiting our page!"
                      />
                    </div>
                    <div className="flex justify-between items-start">
                      <div className="opacity-70 text-sm  pointer-events-none">
                        <Text variant="bodyMd" as="p">
                          Title for your dedicated FAQ page.
                        </Text>
                      </div>
                    </div>
                    <div
                      onClick={() => showModal('FAQs page title')}
                      className="flex justify-end cursor-pointer text-orange-500"
                    >
                      <div>
                        <Icon source={LanguageFilledIcon}></Icon>
                      </div>
                      <p className="ms-1">Edit & Add Translation</p>
                    </div>
                  </div>
                )}
              </div>
              <div>
                <CheckBox
                  label="Show page under construction"
                  value={props.isShowPageUnderConstruction}
                  onChange={(value) => {
                    props.setIsShowPageUnderConstruction(value);
                  }}
                />
                {props.isShowPageUnderConstruction && (
                  <div className="ms-7">
                    <div onClick={() => showModal('Page under construction')}>
                      <TextField
                        label=""
                        value={pageUnderConstruction || ''}
                        onChange={handleUnderConstructionChange}
                        autoComplete="off"
                        placeholder="Thanks for visiting our page!"
                      />
                    </div>
                    <div className="flex justify-between items-start">
                      <div>
                        <div className="opacity-70 text-sm  pointer-events-none">
                          <Text variant="bodyMd" as="p">
                            Text shown when you haven't published any FAQs.
                          </Text>
                        </div>
                      </div>
                    </div>
                    <div
                      onClick={() => showModal('Page under construction')}
                      className="flex justify-end cursor-pointer text-orange-500"
                    >
                      <div>
                        <Icon source={LanguageFilledIcon}></Icon>
                      </div>
                      <p className="ms-1">Edit & Add Translation</p>
                    </div>
                  </div>
                )}
              </div>
            </BlockStack>
          </Card>
        </Layout.Section>
      </Layout>
      <ModalTranslationSetting {...modalProps}></ModalTranslationSetting>
    </>
  );
};

export default GlobalSettings;
