import {
  Layout,
  Card,
  Text,
  Button,
  BlockStack,
  TextField,
  Badge,
} from '@shopify/polaris';
import { useCallback, useEffect, useState } from 'react';
import { RefreshIcon } from '@shopify/polaris-icons';
import CheckBox from '../components/settings/CheckBox';
import { IUser } from '../../types/user';
import { useUserApi, useUserSyncLocaleApi } from '../../hook/user';
import { useAppBridge } from '@shopify/app-bridge-react';

type AdvanceConfigurationProps = {
  isSeoFaqPage: boolean;
  setIsSeoFaqPage: (value: boolean) => void;
  isSeoProductPage: boolean;
  setIsSeoProductPage: (value: boolean) => void;
  setMetaTagDescription: (value: string) => void;
  metaTagDescription: string;
};

const AdvanceConfiguration = (props: AdvanceConfigurationProps) => {
  const app = useAppBridge();
  const { data, refetch } = useUserApi();
  const { mutate } = useUserSyncLocaleApi();
  const [isLoading, setIsLoading] = useState(false);

  const user = data as IUser;
  const [currentPlan, setCurrentPlan] = useState<any>('free');

  useEffect(() => {
    user?.merchants_plan?.plan
      ? setCurrentPlan(user?.merchants_plan?.plan.toLocaleLowerCase())
      : setCurrentPlan('free');
  }, [user]);

  const shopLocalesOption = user?.shopLocales
    ? JSON.parse(user.shopLocales).shopLocales
    : '';

  const syncStoreLanguage = () => {
    setIsLoading(true);
    mutate(
      {},
      {
        onSuccess: () => {
          setIsLoading(false);
          app.toast.show('Sync languages success !');
          refetch();
        },
      },
    );
  };

  const handleChange = useCallback(
    (newValue: string) => props.setMetaTagDescription(newValue),
    [props],
  );

  return (
    <>
      <Layout>
        <Layout.Section variant="oneThird">
          <div>
            <>
              <Text id="storeDetails" variant="headingMd" as="h2">
                Advance configuration
              </Text>
              <Text tone="subdued" as="p">
                You would rarely use these settings. Please don't use if you
                aren't sure what you are doing. Contact support to learn more.
              </Text>
            </>
          </div>
        </Layout.Section>
        <Layout.Section>
          <Card>
            <BlockStack gap={'400'}>
              <div className="flex justify-between items-start">
                <div>
                  <div className="font-bold flex items-center">
                    <div>Available store languages</div>
                    <div className="ml-1">
                      {currentPlan === 'free' && (
                        <Badge tone="attention">Pro Plan</Badge>
                      )}
                    </div>
                  </div>
                  <div className="mt-2 ms-7">
                    <div className="flex">
                      {shopLocalesOption
                        ? shopLocalesOption?.map((item: any, index: number) => (
                            <p
                              key={index}
                              className={`${index > 0 ? 'ml-2' : ''} pe-2`}
                            >
                              <Badge progress="complete" tone="success">
                                {item.locale}
                              </Badge>
                            </p>
                          ))
                        : ''}
                    </div>
                    <div className="opacity-70 text-sm  pointer-events-none">
                      <Text variant="bodyMd" as="p">
                        Available languages are pulled from your store settings.
                        Sync only if you you've recently added or removed a new
                        language through Shopify admin.
                      </Text>
                    </div>
                  </div>
                </div>
                <div>
                  <Button
                    icon={RefreshIcon}
                    onClick={() => syncStoreLanguage()}
                    loading={isLoading}
                    disabled={
                      !(
                        currentPlan === 'pro' ||
                        currentPlan === 'ultimate' ||
                        currentPlan === 'free_01' ||
                        currentPlan === 'free extra'
                      )
                    }
                  >
                    Sync
                  </Button>
                </div>
              </div>
              <div>
                <p className="font-bold mb-1">Google SEO snippets</p>
                <div>
                  <CheckBox
                    label="Use Google SEO Snippets for Faq page"
                    value={props.isSeoFaqPage}
                    onChange={(value) => {
                      props.setIsSeoFaqPage(value);
                    }}
                  />
                  {props.isSeoFaqPage && (
                    <div className="ms-7">
                      <TextField
                        label="Meta tag description content"
                        value={props.metaTagDescription}
                        onChange={handleChange}
                        multiline={3}
                        autoComplete="off"
                        placeholder="FAQ professional"
                      />
                    </div>
                  )}
                </div>
                <CheckBox
                  label="Use Google SEO Snippets for other page (ex. Homepage, Products, Cart etc...)"
                  value={props.isSeoProductPage}
                  onChange={(value) => {
                    props.setIsSeoProductPage(value);
                  }}
                />
              </div>
            </BlockStack>
          </Card>
        </Layout.Section>
      </Layout>
    </>
  );
};

export default AdvanceConfiguration;
