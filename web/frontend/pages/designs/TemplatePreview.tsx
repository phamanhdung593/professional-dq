import Template01 from './components/templates-preview/Template01';
import Template02 from './components/templates-preview/Template02';
import Template03 from './components/templates-preview/Template03';
import Template05 from './components/templates-preview/Template05';
import Template06 from './components/templates-preview/Template06';
import Template07 from './components/templates-preview/Template07';
import Template08 from './components/templates-preview/Template08';
import Template04 from './components/templates-preview/Template04';
import { templateProps } from './TemplateSelection';

const TemplatePreview = (props: templateProps) => {
  const {
    setDataTemplate1,
    setDataTemplate2,
    setDataTemplate3,
    setDataTemplate4,
    setDataTemplate5,
    setDataTemplate6,
    setDataTemplate7,
    setDataTemplate8,
  } = props;

  return (
    <>
      {props.templateSelected === '1' && (
        <Template01 {...props} setDataTemplate1={setDataTemplate1}></Template01>
      )}
      {props.templateSelected === '2' && (
        <Template02 {...props} setDataTemplate2={setDataTemplate2}></Template02>
      )}
      {props.templateSelected === '3' && (
        <Template03 {...props} setDataTemplate2={setDataTemplate3}></Template03>
      )}
      {props.templateSelected === '4' && (
        <Template04 {...props} setDataTemplate2={setDataTemplate4}></Template04>
      )}
      {props.templateSelected === '5' && (
        <Template05 {...props} setDataTemplate2={setDataTemplate5}></Template05>
      )}
      {props.templateSelected === '6' && (
        <Template06 {...props} setDataTemplate2={setDataTemplate6}></Template06>
      )}
      {props.templateSelected === '7' && (
        <Template07 {...props} setDataTemplate2={setDataTemplate7}></Template07>
      )}
      {props.templateSelected === '8' && (
        <Template08 {...props} setDataTemplate2={setDataTemplate8}></Template08>
      )}
    </>
  );
};

export default TemplatePreview;
