import { DropZone, Text, Thumbnail, LegacyStack } from '@shopify/polaris';
import { useCallback, useEffect, useState } from 'react';
import { NoteIcon } from '@shopify/polaris-icons';

interface DropZoneExampleProps {
  templateSelected: string;
  handleFilesUpload: (data: any) => void;
}

const UploadBanner = (prop: DropZoneExampleProps) => {
  const [file, setFile] = useState<any>();
  const handleDropZoneDrop = useCallback(
    (_dropFiles: File[], acceptedFiles: File[]) => setFile(acceptedFiles[0]),
    [],
  );

  const validImageTypes = [
    'image/gif',
    'image/jpeg',
    'image/png',
    'image/gif',
    'image/webp',
  ];

  const fileUpload = !file && <DropZone.FileUpload />;

  const uploadedFile = file && (
    <LegacyStack>
      <Thumbnail
        size="small"
        alt={file.name}
        source={
          validImageTypes.includes(file.type)
            ? window.URL.createObjectURL(file)
            : NoteIcon
        }
      />
      <div>
        {file.name}{' '}
        <Text variant="bodySm" as="p">
          {file.size} bytes
        </Text>
      </div>
    </LegacyStack>
  );

  useEffect(() => {
    if (file) {
      prop.handleFilesUpload(file);
    }
  }, [file]);

  useEffect(() => {
    if (prop.templateSelected) {
      setFile(null);
    }
  }, [prop.templateSelected]);

  return (
    <>
      <DropZone allowMultiple={false} onDrop={handleDropZoneDrop}>
        {uploadedFile}
        {fileUpload}
      </DropZone>
    </>
  );
};

export default UploadBanner;
