import {
  BlockStack,
  Button,
  Card,
  Collapsible,
  Select,
  TextField,
} from '@shopify/polaris';
import CheckBox from './components/CheckBox';
import RangeSliderComponent from './components/RangeSliderComponent';
import { useState, useContext } from 'react';
import { fontFamily } from '../../assets/data/font';
import { Context } from '../../components/providers/DesignProvider';

const Header = () => {
  const [toggles, setToggles] = useState('');
  const { isShowBanner, setIsShowBanner } = useContext(Context);
  const { showPageTitle, setShowPageTitle } = useContext(Context);
  const { fontSize, setFontSize } = useContext(Context);
  const { font, setFont } = useContext(Context);
  const { pageTitleContent, setPageTitleContent } = useContext(Context);
  const { usePageTitle, setUsePageTitle } = useContext(Context);
  const { isShowSearchBar, setIsShowSearchBar } = useContext(Context);
  const { showIntroText, setShowIntroText } = useContext(Context);
  const { headerHeight, setHeaderHeight } = useContext(Context);
  const { paddingTop, setPaddingTop } = useContext(Context);
  const { paddingBottom, setPaddingBottom } = useContext(Context);
  const { headerFontColor, setHeaderFontColor } = useContext(Context);
  const { searchPlaceholder, setSearchPlaceholder } = useContext(Context);
  const { placeholderColor, setPlaceholderColor } = useContext(Context);
  const { fontSearchbar, setFontSearchbar } = useContext(Context);
  const { fontFamilyIntroText, setFontFamilyIntroText } = useContext(Context);
  const { introTextContent, setIntroTextContent } = useContext(Context);
  const { introTextFontColor, setIntroTextFontColor } = useContext(Context);
  const { fontSizeIntrotext, setFontSizeIntrotext } = useContext(Context);
  const { paddingTopIntrotext, setPaddingTopIntrotext } = useContext(Context);
  const { paddingBottomIntrotext, setPaddingBottomIntrotext } =
    useContext(Context);
  const { styleSearchBar, setStyleSearchBar } = useContext(Context);

  const handleToggle = (toggle: any) => {
    setToggles((e) => (e === toggle ? null : toggle));
  };

  return (
    <div className="w-full">
      <BlockStack gap={'400'}>
        <CheckBox
          label="Show banner"
          value={isShowBanner}
          onChange={(value) => setIsShowBanner(value)}
        />
        <RangeSliderComponent
          label="Header height"
          value={headerHeight}
          min={210}
          max={460}
          onChange={(value) => setHeaderHeight(value)}
        />
        <Card>
          <div className="flex justify-between items-center">
            <div>
              <b>Page title</b>
            </div>
            <Button
              onClick={() => handleToggle('PageTitle')}
              ariaExpanded={toggles === 'Pagetitle'}
              ariaControls="basic-collapsible"
            >
              Toggle
            </Button>
          </div>
          <Collapsible
            open={toggles === 'PageTitle'}
            id="basic-collapsible"
            transition={{ duration: '500ms', timingFunction: 'ease-in-out' }}
            expandOnPrint
          >
            <div className="mt-3">
              <BlockStack gap={'400'}>
                <CheckBox
                  label="Show page title"
                  value={showPageTitle}
                  onChange={(value) => setShowPageTitle(value)}
                />
                <CheckBox
                  label="Use page title in Product page"
                  value={usePageTitle}
                  onChange={(value) => setUsePageTitle(value)}
                />
                {showPageTitle && (
                  <div>
                    <BlockStack gap={'400'}>
                      <TextField
                        label="Page title content"
                        value={pageTitleContent}
                        onChange={(newValue) => setPageTitleContent(newValue)}
                        autoComplete="off"
                      />
                      <div>
                        <p>Font color</p>
                        <div className="flex items-center">
                          <input
                            type="color"
                            className="h-[36px] mr-1 w-[60px] cursor-pointer"
                            onChange={(e) => setHeaderFontColor(e.target.value)}
                            value={headerFontColor}
                          ></input>
                          <div className="flex-1">
                            <TextField
                              label=""
                              value={headerFontColor}
                              onChange={(value) => setHeaderFontColor(value)}
                              autoComplete="off"
                            />
                          </div>
                        </div>
                      </div>
                      <Select
                        label="Font family"
                        options={fontFamily}
                        onChange={(e) => setFont(e)}
                        value={font}
                      />
                      <RangeSliderComponent
                        label="Font size"
                        value={fontSize}
                        min={10}
                        max={60}
                        onChange={(value) => setFontSize(value)}
                      />
                      <RangeSliderComponent
                        label="Padding top"
                        value={paddingTop}
                        min={0}
                        max={60}
                        onChange={(value) => setPaddingTop(value)}
                      />
                      <RangeSliderComponent
                        label="Padding bottom"
                        value={paddingBottom}
                        min={0}
                        max={60}
                        onChange={(value) => setPaddingBottom(value)}
                      />
                    </BlockStack>
                  </div>
                )}
              </BlockStack>
            </div>
          </Collapsible>
        </Card>
        <Card>
          <div className="flex justify-between items-center">
            <div>
              <b>Search bar</b>
            </div>
            <Button
              onClick={() => setToggles('SearchBar')}
              ariaExpanded={toggles === 'SearchBar'}
              ariaControls="basic-collapsible"
            >
              Toggle
            </Button>
          </div>
          <Collapsible
            open={toggles === 'SearchBar'}
            id="basic-collapsible"
            transition={{ duration: '500ms', timingFunction: 'ease-in-out' }}
            expandOnPrint
          >
            <BlockStack gap={'400'}>
              <CheckBox
                label="Show search bar"
                value={isShowSearchBar}
                onChange={(value) => setIsShowSearchBar(value)}
              />
              {isShowSearchBar && (
                <div>
                  <BlockStack gap={'400'}>
                    <TextField
                      label="Search placeholder"
                      value={searchPlaceholder}
                      onChange={(newValue) => setSearchPlaceholder(newValue)}
                      autoComplete="off"
                    />
                    <Select
                      label="Font family"
                      options={fontFamily}
                      onChange={(e) => setFontSearchbar(e)}
                      value={fontSearchbar}
                    />
                    <div>
                      <div>Style</div>
                      <div className="flex justify-center">
                        <div
                          className={`rounded p-3 border border-1 border-gray-400 mr-4 cursor-pointer ${
                            styleSearchBar === '1' ? 'bg-orange-500' : ''
                          }`}
                          aria-expanded={styleSearchBar}
                          onClick={() => setStyleSearchBar('1')}
                        >
                          Style 1
                        </div>
                        <div
                          className={`rounded p-3 border border-1 border-gray-400 mr-4 cursor-pointer ${
                            styleSearchBar === '2' ? 'bg-orange-500' : ''
                          }`}
                          aria-expanded={styleSearchBar}
                          onClick={() => setStyleSearchBar('2')}
                        >
                          Style 2
                        </div>
                      </div>
                    </div>
                    <div>
                      <p>Placeholder color</p>
                      <div className="flex items-center">
                        <input
                          type="color"
                          className="h-[36px] mr-1 w-[60px] cursor-pointer"
                          onChange={(e) => setPlaceholderColor(e.target.value)}
                          value={placeholderColor}
                        ></input>
                        <div className="flex-1">
                          <TextField
                            label=""
                            value={placeholderColor}
                            onChange={(value) => setPlaceholderColor(value)}
                            autoComplete="off"
                          />
                        </div>
                      </div>
                    </div>
                    {/* <div>
                        <p>Micro scope color</p>
                        <div className="flex items-center">
                          <input
                            type="color"
                            className="h-[36px] mr-1 w-[60px] cursor-pointer"
                            onChange={(e) => setMicroScopeColor(e.target.value)}
                            value={microScopeColor}
                          ></input>
                          <div className="flex-1">
                            <TextField
                              label=""
                              value={microScopeColor}
                              onChange={(value) => setMicroScopeColor(value)}
                              autoComplete="off"
                            />
                          </div>
                        </div>
                      </div> */}
                  </BlockStack>
                </div>
              )}
            </BlockStack>
          </Collapsible>
        </Card>
        <Card>
          <div className="flex justify-between items-center">
            <div>
              <b>Intro text</b>
            </div>
            <Button
              onClick={() => setToggles('IntroText')}
              ariaExpanded={toggles === 'IntroText'}
              ariaControls="basic-collapsible"
            >
              Toggle
            </Button>
          </div>
          <Collapsible
            open={toggles === 'IntroText'}
            id="basic-collapsible"
            transition={{ duration: '500ms', timingFunction: 'ease-in-out' }}
            expandOnPrint
          >
            <BlockStack gap={'400'}>
              <CheckBox
                label="Show intro text"
                value={showIntroText}
                onChange={(value) => setShowIntroText(value)}
              />
              {showIntroText && (
                <div>
                  <BlockStack gap={'400'}>
                    <TextField
                      label="Intro text content"
                      value={introTextContent}
                      onChange={(newValue) => setIntroTextContent(newValue)}
                      autoComplete="off"
                    />
                    <Select
                      label="Font family"
                      options={fontFamily}
                      onChange={(e) => setFontFamilyIntroText(e)}
                      value={fontFamilyIntroText}
                    />
                    <div>
                      <p>Text color</p>
                      <div className="flex items-center">
                        <input
                          type="color"
                          className="h-[36px] mr-1 w-[60px] cursor-pointer"
                          onChange={(e) =>
                            setIntroTextFontColor(e.target.value)
                          }
                          value={introTextFontColor}
                        ></input>
                        <div className="flex-1">
                          <TextField
                            label=""
                            value={introTextFontColor}
                            onChange={(value) => setIntroTextFontColor(value)}
                            autoComplete="off"
                          />
                        </div>
                      </div>
                    </div>
                    <RangeSliderComponent
                      label="Font size"
                      value={fontSizeIntrotext}
                      min={0}
                      max={60}
                      onChange={(value) => setFontSizeIntrotext(value)}
                    />
                    <RangeSliderComponent
                      label="Padding top"
                      value={paddingTopIntrotext}
                      min={0}
                      max={60}
                      onChange={(value) => setPaddingTopIntrotext(value)}
                    />
                    <RangeSliderComponent
                      label="Padding bottom"
                      value={paddingBottomIntrotext}
                      min={0}
                      max={60}
                      onChange={(value) => setPaddingBottomIntrotext(value)}
                    />
                  </BlockStack>
                </div>
              )}
            </BlockStack>
          </Collapsible>
        </Card>
      </BlockStack>
    </div>
  );
};

export default Header;
