import { Tabs } from '@shopify/polaris';
import Header from './Header';
import Body from './Body';
import Footer from './Foodter';
import More from './More';
import { useState, useCallback } from 'react';
import { templateProps } from './TemplateSelection';

const TemplateConfiguration = (props: templateProps) => {
  const [selected, setSelected] = useState(0);

  const handleTabChange = useCallback(
    (selectedTabIndex: number) => setSelected(selectedTabIndex),
    [],
  );
  const tabs = [
    {
      id: '1',
      content: 'Header',
      accessibilityLabel: 'All customers',
      panelID: 'all-customers-content-1',
    },
    {
      id: '2',
      content: 'Body',
      panelID: 'accepts-marketing-content-1',
    },
    {
      id: '3',
      content: 'Footer',
      panelID: 'repeat-customers-content-1',
    },
    {
      id: '4',
      content: 'More settings',
      panelID: 'prospects-content-1',
    },
  ];
  return (
    <>
      <Tabs tabs={tabs} selected={selected} onSelect={handleTabChange} />
      <div className="flex mt-3 w-full">
        {selected === 0 && <Header />}
        {selected === 1 && (
          <Body selectTemplate={props.templateSelected}></Body>
        )}
        {selected === 2 && <Footer></Footer>}
        {selected === 3 && <More></More>}
      </div>
    </>
  );
};

export default TemplateConfiguration;
