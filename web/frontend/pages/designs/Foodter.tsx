import { BlockStack, Select, TextField } from '@shopify/polaris';
import CheckBox from './components/CheckBox';
import RangeSliderComponent from './components/RangeSliderComponent';
import { fontFamily } from '../../assets/data/font';
import { Context } from '../../components/providers/DesignProvider';
import { useContext } from 'react';

const Footer = () => {
  const { showFooter, setShowFooter } = useContext(Context);
  const { footerTextContent, setFooterTextContent } = useContext(Context);
  const { fontFamilyFooter, setFontFamilyFooter } = useContext(Context);
  const { textColorFooter, setTextColorFooter } = useContext(Context);
  const { rangeFontSizeFooter, setRangeFontSizeFooter } = useContext(Context);
  const { rangePaddingTopFooter, setRangePaddingTopFooter } =
    useContext(Context);
  const { rangePaddingBottomFooter, setRangePaddingBottomFooter } =
    useContext(Context);

  return (
    <div className="w-full">
      <BlockStack gap={'400'}>
        <CheckBox
          label="Show Footer text"
          value={showFooter}
          onChange={(value) => setShowFooter(value)}
        />
        {showFooter && (
          <div>
            <BlockStack gap={'400'}>
              <TextField
                label="Footer text content"
                value={footerTextContent}
                onChange={(newValue) => setFooterTextContent(newValue)}
                autoComplete="off"
              />
              <Select
                label="Font family"
                options={fontFamily}
                onChange={(e) => setFontFamilyFooter(e)}
                value={fontFamilyFooter}
              />
              <div>
                <p>Text color</p>
                <div className="flex items-center">
                  <input
                    type="color"
                    className="h-[36px] mr-1 w-[60px] cursor-pointer"
                    onChange={(e) => setTextColorFooter(e.target.value)}
                    value={textColorFooter}
                  ></input>
                  <div className="flex-1">
                    <TextField
                      label=""
                      value={textColorFooter}
                      onChange={(value) => setTextColorFooter(value)}
                      autoComplete="off"
                    />
                  </div>
                </div>
              </div>
              <RangeSliderComponent
                label="Font size"
                value={rangeFontSizeFooter}
                min={0}
                max={30}
                onChange={(value) => setRangeFontSizeFooter(value)}
              />
              <RangeSliderComponent
                label="Padding top"
                value={rangePaddingTopFooter}
                min={0}
                max={60}
                onChange={(value) => setRangePaddingTopFooter(value)}
              />
              <RangeSliderComponent
                label="Padding bottom"
                value={rangePaddingBottomFooter}
                min={0}
                max={60}
                onChange={(value) => setRangePaddingBottomFooter(value)}
              />
            </BlockStack>
          </div>
        )}
      </BlockStack>
    </div>
  );
};

export default Footer;
