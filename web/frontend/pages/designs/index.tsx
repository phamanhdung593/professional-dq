import { t } from 'i18next';
import { Card, Icon, Layout, Page, RangeSlider } from '@shopify/polaris';
import UploadBanner from './UploadBanner';
import TemplateConfiguration from './TemplateConfiguration';
import TemplateSelection from './TemplateSelection';
import { useContext, useEffect, useState } from 'react';
import TemplatePreview from './TemplatePreview';
import { templates } from '../../assets/data/templates';
import { Context } from '../../components/providers/DesignProvider';
import { useSettingApi, useUpdateSettingApi } from '../../hook/setting';
import { useUpdateTemplateSettingApi } from '../../hook/template_setting';
import { useAppBridge } from '@shopify/app-bridge-react';
import { useUploadFileApi } from '../../hook/uploadfile';
import { useUserApi } from '../../hook/user';
import { IUser } from '../../types/user';
import { ExternalSmallIcon, ViewIcon } from '@shopify/polaris-icons';

export const Designs = () => {
  const { mutate } = useUpdateTemplateSettingApi();
  const { mutateAsync } = useUploadFileApi();
  const { mutate: updateSetting } = useUpdateSettingApi();
  const { data } = useSettingApi();
  const settingId = data?.id;
  const templateId = data?.template_setting[0]?.id;
  const templateNumberData = data?.template_setting[0].template_number;
  const app = useAppBridge();
  const [loadingBtn, setLoadingBtn] = useState(false);
  const { data: users } = useUserApi();
  const user = users as IUser;

  const {
    templateNumber,
    setTemplateNumber,
    setSetting,
    setDesign,
    uploadedFiles,
    setUploadedFiles,
    showPageTitle,
    showIntroText,
    showFooter,
    pageTitleContent,
    searchPlaceholder,
    introTextContent,
    footerTextContent,
    usePageTitle,
    customCss,
    isBackToTopMore,
    bannerDefault,
    headerHeight,
    setHeaderHeight,
    hovercolorQuestion,
    backgroundColorAnswer,
    backgroundcolorQuestion,
  } = useContext(Context);

  const [dataTemplate1, setDataTemplate1] = useState<any>({});
  const [dataTemplate2, setDataTemplate2] = useState<any>({});
  const [dataTemplate3, setDataTemplate3] = useState<any>({});
  const [dataTemplate4, setDataTemplate4] = useState<any>({});
  const [dataTemplate5, setDataTemplate5] = useState<any>({});
  const [dataTemplate6, setDataTemplate6] = useState<any>({});
  const [dataTemplate7, setDataTemplate7] = useState<any>({});
  const [dataTemplate8, setDataTemplate8] = useState<any>({});

  const handleFilesUpload = async (data: any) => {
    const formData = new FormData();
    formData.append('file', data);
    if (data) {
      mutateAsync(formData, {
        onSuccess(data: any) {
          setLoadingBtn(false);
          setUploadedFiles(data?.name);
          app.toast.show('Upload success!');
        },
        onError: () => {
          app.toast.show('Upload error!');
        },
      });
    }
  };

  useEffect(() => {
    switch (templateNumber) {
      case '1':
        if (templateNumber == templateNumberData) {
          setDesign(data?.template_setting[0]);
          setSetting(data);
        } else setDesign(templates.template_1);
        break;
      case '2':
        if (templateNumber == templateNumberData) {
          setDesign(data?.template_setting[0]);
          setSetting(data);
        } else setDesign(templates.template_2);
        break;
      case '3':
        if (templateNumber == templateNumberData) {
          setDesign(data?.template_setting[0]);
          setSetting(data);
        } else setDesign(templates.template_3);
        break;
      case '4':
        if (templateNumber == templateNumberData) {
          setDesign(data?.template_setting[0]);
          setSetting(data);
        } else setDesign(templates.template_4);
        break;
      case '5':
        if (templateNumber == templateNumberData) {
          setDesign(data?.template_setting[0]);
          setSetting(data);
        } else setDesign(templates.template_5);
        break;
      case '6':
        if (templateNumber == templateNumberData) {
          setDesign(data?.template_setting[0]);
          setSetting(data);
        } else setDesign(templates.template_6);
        break;
      case '7':
        if (templateNumber == templateNumberData) {
          setDesign(data?.template_setting[0]);
          setSetting(data);
        } else setDesign(templates.template_7);
        break;
      case '8':
        if (templateNumber == templateNumberData) {
          setDesign(data?.template_setting[0]);
          setSetting(data);
        } else setDesign(templates.template_8);
        break;
      default:
        console.log('Invalid templateSelected');
    }
  }, [data, templateNumber]);

  const handleSave = () => {
    setLoadingBtn(true);
    let data: any;
    switch (templateNumber) {
      case '1':
        data = dataTemplate1;
        break;
      case '2':
        data = dataTemplate2;
        break;
      case '3':
        data = dataTemplate3;
        break;
      case '4':
        data = dataTemplate4;
        break;
      case '5':
        data = dataTemplate5;
        break;
      case '6':
        data = dataTemplate6;
        break;
      case '7':
        data = dataTemplate7;
        break;
      case '8':
        data = dataTemplate8;
        break;
      default:
        console.log('Invalid templateSelected');
    }

    const body: any = {
      template_number: parseInt(templateNumber),
      setting_id: settingId,
      width_faqs_accordian: data.rangeFaqPageMore,
      width_faqs_product: data.rangeProductFaqMore,
      faq_font_color: data.textcolorQuestion,
      faq_font_size: data.fontSizeQuestion,
      faq_bg_color: backgroundcolorQuestion,
      faq_font_weight: data.fontWeightQuestion,
      faq_font_family: data.fontFamilyQuestion,
      faq_hover_color: hovercolorQuestion,
      category_font_color: data.bodyFontColor,
      category_font_size: data.rangeValue,
      category_font_weight: data.fontBodyWeight,
      category_font_family: data.fontBodyFamily,
      category_text_style: data.fontBodyStyle,
      category_text_align: data.textAlign,
      category_bar_number: data.styleBody,
      category_bar_background: data.bodyBackgroundColor,
      category_bar_color: data.barFontColor,
      show_category_bar: data.isShowCategory,
      answer_font_size: data.fontSizeAnswer,
      answer_font_weight: data.fontWeightAnswer,
      answer_font_color: data.textColorAnswer,
      answer_bg_color: backgroundColorAnswer,
      answer_font_family: data.fontFamilyAnswer,
      custom_css: customCss,
      banner_height: data.headerHeight,
      banner_visible: data.isShowBanner,
      banner_default: bannerDefault,
      intro_text_paddingtop: data.paddingTopIntrotext,
      intro_text_fontsize: data.fontSizeIntrotext,
      intro_text_paddingbottom: data.paddingBottomIntrotext,
      intro_text_color: data.introTextFontColor,
      intro_text_font: data.fontFamilyIntroText,
      page_title_paddingtop: data.paddingTop,
      page_title_fontsize: data.fontSize,
      page_title_paddingbottom: data.paddingBottom,
      page_title_color: data.headerFontColor,
      page_title_font: data.font,
      page_background_color: data.buttonBackgroundColorMore,
      footer_text_paddingtop: data.rangePaddingTopFooter,
      footer_text_fontsize: data.rangeFontSizeFooter,
      footer_text_paddingbottom: data.rangePaddingBottomFooter,
      footer_text_color: data.textColorFooter,
      footer_text_font: data.fontFamilyFooter,
      show_search_input: data.isShowSearchBar,
      search_input_style: String(data.styleSearchBar),
      search_placeholder_font: data.fontSearchbar,
      placeholder_color: data.placeholderColor,
      micro_scope_color: data.microScopeColor,
      btn_top_background: data.buttonColorMore,
      btn_top_hover: data.buttonHoverColorMore,
      btn_top_visible: isBackToTopMore,
      image_banner: uploadedFiles,
    };

    const dataSetting = {
      show_page_title: showPageTitle,
      title_product_faq: usePageTitle,
      page_title_content: JSON.stringify([
        {
          content: pageTitleContent,
          locale: 'default',
        },
      ]),
      search_placehoder: JSON.stringify([
        {
          content: searchPlaceholder,
          locale: 'default',
        },
      ]),
      intro_text_content: JSON.stringify([
        {
          content: introTextContent,
          locale: 'default',
        },
      ]),
      footer_text_content: JSON.stringify([
        {
          content: footerTextContent,
          locale: 'default',
        },
      ]),
      show_intro_text: showIntroText,
      show_footer_text: showFooter,
    };

    mutate(
      {
        id: templateId,
        ...body,
      },
      {
        onSuccess() {
          setLoadingBtn(false);
          app.toast.show('Update template setting success!');
        },
        onError: () => {
          app.toast.show('Update template setting error!');
        },
      },
    );

    updateSetting(
      {
        id: settingId,
        ...dataSetting,
      },
      {
        onSuccess() {
          setLoadingBtn(false);
          app.toast.show('Update setting success!');
        },
        onError: () => {
          app.toast.show('Update setting error!');
        },
      },
    );
  };

  return (
    <Page
      // fullWidth
      title={t('Design')}
      primaryAction={{
        content: 'Save',
        onAction: handleSave,
        loading: loadingBtn,
      }}
    >
      <Layout>
        <Layout.Section variant="oneThird">
          <Card>
            <div className="pb-5 border-b-[1px]">
              <div className="mb-3">
                <b>Template Selection</b>
                <p className="">Choose one of our 8 pre-existing template.</p>
              </div>
              <TemplateSelection
                uploadedFiles={uploadedFiles}
                templateSelected={templateNumber}
                setTemplateSelected={setTemplateNumber}
                templateNumberData={templateNumberData}
              />
            </div>
            <div className="pt-4 pb-5 border-b-[1px]">
              <div className="mb-3">
                <b>Upload Banner</b>
                <p className="">
                  Select a photo in your computer to change the store banner.
                </p>
              </div>
              <UploadBanner
                templateSelected={templateNumber}
                handleFilesUpload={handleFilesUpload}
              ></UploadBanner>
            </div>
            <div className="pt-4">
              <div className="mb-3">
                <b>Template configuration</b>
              </div>
              <TemplateConfiguration
                templateSelected={templateNumber}
                setTemplateSelected={setTemplateNumber}
              />
            </div>
          </Card>
        </Layout.Section>

        <Layout.Section>
          <Card>
            <div className="mb-5 flex">
              <b className="flex items-center gap-1">
                <Icon source={ViewIcon} />
                Preview
              </b>
              <div className="flex-1 text-center">
                <a
                  target="_blank"
                  href={`https://${user?.shopify_domain}${data?.faq_page_url}`}
                  rel="noreferrer"
                  className="text-sky-600 inline-flex items-center"
                >
                  https://{user?.shopify_domain}
                  {data?.faq_page_url}
                  <Icon source={ExternalSmallIcon} tone="info" />
                </a>
              </div>
              <div style={{ visibility: 'hidden' }}>Live preview</div>
            </div>
            <hr />
            <div className="mb-5 mt-5">
              <RangeSlider
                output
                label=""
                min={210}
                max={380}
                value={headerHeight}
                onChange={(value) => setHeaderHeight(value)}
                prefix={<p>Header height</p>}
                suffix={
                  <p
                    style={{
                      minWidth: '24px',
                      textAlign: 'right',
                    }}
                  >
                    {headerHeight}
                  </p>
                }
              />
            </div>
            <TemplatePreview
              templateSelected={templateNumber}
              setTemplateSelected={setTemplateNumber}
              uploadedFiles={uploadedFiles}
              setDataTemplate1={setDataTemplate1}
              setDataTemplate2={setDataTemplate2}
              setDataTemplate3={setDataTemplate3}
              setDataTemplate4={setDataTemplate4}
              setDataTemplate5={setDataTemplate5}
              setDataTemplate6={setDataTemplate6}
              setDataTemplate7={setDataTemplate7}
              setDataTemplate8={setDataTemplate8}
            ></TemplatePreview>
          </Card>
        </Layout.Section>
      </Layout>
    </Page>
  );
};
