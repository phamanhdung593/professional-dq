import { BlockStack, TextField } from '@shopify/polaris';
import RangeSliderComponent from './components/RangeSliderComponent';
import { useState } from 'react';
import CheckBox from './components/CheckBox';
import Switch from './components/Switch';
import { useContext } from 'react';
import { Context } from '../../components/providers/DesignProvider';

export default function More() {
  const [isRemoveBrandingMore, setIsRemoveBrandingMore] = useState(false);
  const [isProductFaqWidthMore, setIsProductFaqWidthMore] = useState(true);
  const { buttonBackgroundColorMore, setButtonBackgroundColorMore } =
    useContext(Context);
  const { rangeFaqPageMore, setRangeFaqPageMore } = useContext(Context);
  const { rangeProductFaqMore, setRangeProductFaqMore } = useContext(Context);
  const { buttonColorMore, setButtonColorMore } = useContext(Context);
  const { buttonHoverColorMore, setButtonHoverColorMore } = useContext(Context);
  const { isBackToTopMore, setIsBackToTopMore } = useContext(Context);

  return (
    <div className="w-full max-h-96 overflow-y-auto product-scroll">
      <BlockStack gap={'400'}>
        <div>
          <p>Background color</p>
          <div className="flex items-center">
            <input
              type="color"
              className="h-[36px] mr-1 w-[60px] cursor-pointer"
              onChange={(e) => setButtonBackgroundColorMore(e.target.value)}
              value={buttonBackgroundColorMore}
            ></input>
            <div className="flex-1">
              <TextField
                label=""
                value={buttonBackgroundColorMore}
                onChange={(value) => setButtonBackgroundColorMore(value)}
                autoComplete="off"
              />
            </div>
          </div>
        </div>
        {/* <ColorPickerDesign
          label="Page Background Color"
          value={buttonBackgroundColorMore}
          onChange={(value) => setButtonBackgroundColorMore(value)}
        /> */}
        <RangeSliderComponent
          label="Faq page max width"
          value={rangeFaqPageMore}
          min={800}
          max={1800}
          onChange={(value) => setRangeFaqPageMore(value)}
        />
        <div>
          <CheckBox
            label="Set Product Faq width"
            value={isProductFaqWidthMore}
            onChange={(value) => setIsProductFaqWidthMore(value)}
          />
          {isProductFaqWidthMore && (
            <div className="ms-7">
              <RangeSliderComponent
                label="Product Faq max width"
                value={rangeProductFaqMore}
                min={400}
                max={1800}
                onChange={(value) => setRangeProductFaqMore(value)}
              />
            </div>
          )}
        </div>
        <CheckBox
          label="Button back to top"
          value={isBackToTopMore}
          onChange={(value) => setIsBackToTopMore(value)}
        />
        {isBackToTopMore && (
          <>
            <div>
              <p>Button Background Color</p>
              <div className="flex items-center">
                <input
                  type="color"
                  className="h-[36px] mr-1 w-[60px] cursor-pointer"
                  onChange={(e) => setButtonColorMore(e.target.value)}
                  value={buttonColorMore}
                ></input>
                <div className="flex-1">
                  <TextField
                    label=""
                    value={buttonColorMore}
                    onChange={(value) => setButtonColorMore(value)}
                    autoComplete="off"
                  />
                </div>
              </div>
            </div>

            <div>
              <p>Button Hover Color</p>
              <div className="flex items-center">
                <input
                  type="color"
                  className="h-[36px] mr-1 w-[60px] cursor-pointer"
                  onChange={(e) => setButtonHoverColorMore(e.target.value)}
                  value={buttonHoverColorMore}
                ></input>
                <div className="flex-1">
                  <TextField
                    label=""
                    value={buttonHoverColorMore}
                    onChange={(value) => setButtonHoverColorMore(value)}
                    autoComplete="off"
                  />
                </div>
              </div>
            </div>
          </>
        )}

        <div className="flex items-center justify-between">
          <label
            htmlFor="auto_break_line"
            className="block text-sm font-medium text-gray-700"
          >
            Powered by Yanet
          </label>
          <Switch
            checked={isRemoveBrandingMore}
            onChange={(value) => setIsRemoveBrandingMore(value)}
          />
        </div>
      </BlockStack>
    </div>
  );
}
