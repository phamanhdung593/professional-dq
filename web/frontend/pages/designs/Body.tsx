import {
  BlockStack,
  Button,
  Card,
  Collapsible,
  Select,
  TextField,
} from '@shopify/polaris';
// import CheckBox from "./components/CheckBox"
import RangeSliderComponent from './components/RangeSliderComponent';
import { useState, useContext } from 'react';
import CheckBox from './components/CheckBox';
import { fontFamily } from '../../assets/data/font';
import { Align } from '../../assets/data/align';
import { Context } from '../../components/providers/DesignProvider';
import { FontWeight } from '../../assets/data/fontweight';
import { fontStyle } from '../../assets/data/fontStyle';

const Body = (selectTemplate: any) => {
  const select = selectTemplate.selectTemplate;
  const [toggles, setToggles] = useState('');
  //Category
  const { customCss, setCustomCss } = useContext(Context);
  const { fontBodyFamily, setFontBodyFamily } = useContext(Context);
  const { rangeValue, setRangeValue } = useContext(Context);
  const { styleBody, setStyleBody } = useContext(Context);
  const { bodyBackgroundColor, setBodyBackgroundColor } = useContext(Context);
  const { barFontColor, setBarFontColor } = useContext(Context);
  const { bodyFontColor, setBodyFontColor } = useContext(Context);
  const { textAlign, setTextAlign } = useContext(Context);
  const { fontBodyWeight, setFontBodyWeight } = useContext(Context);
  const { isShowCategory, setIsShowCategory } = useContext(Context);
  const { fontBodyStyle, setFontBodyStyle } = useContext(Context);
  const { fontSizeQuestion, setFontSizeQuestion } = useContext(Context);
  const { fontWeightQuestion, setFontWeightQuestion } = useContext(Context);
  const { fontFamilyQuestion, setFontFamilyQuestion } = useContext(Context);
  const { backgroundcolorQuestion, setBackgroundcolorQuestion } =
    useContext(Context);
  const { textcolorQuestion, setTextcolorQuestion } = useContext(Context);
  const { hovercolorQuestion, setHovercolorQuestion } = useContext(Context);
  const { fontSizeAnswer, setFontSizeAnswer } = useContext(Context);
  const { fontFamilyAnswer, setFontFamilyAnswer } = useContext(Context);
  const { fontWeightAnswer, setFontWeightAnswer } = useContext(Context);
  const { backgroundColorAnswer, setBackgroundColorAnswer } =
    useContext(Context);
  const { textColorAnswer, setTextColorAnswer } = useContext(Context);

  const handleToggle = (toggle: any) => {
    setToggles((e) => (e === toggle ? null : toggle));
  };

  return (
    <div className="w-full">
      <BlockStack gap={'400'}>
        {/* <CheckBox
            label="Show Footer text"
            value={isShowFooterText}
            onChange={(value) => setIsShowFooterText(value)}
          /> */}
        <Card>
          <div className="flex justify-between items-center">
            <div>
              <b>Category</b>
            </div>
            <Button
              onClick={() => handleToggle('Category')}
              ariaExpanded={toggles === 'Category'}
              ariaControls="basic-collapsible"
            >
              Toggle
            </Button>
          </div>
          <Collapsible
            open={toggles === 'Category'}
            id="basic-collapsible"
            transition={{ duration: '500ms', timingFunction: 'ease-in-out' }}
            expandOnPrint
          >
            <div className="mt-3">
              <BlockStack gap={'400'}>
                {select === '3' ||
                select === '4' ||
                select === '6' ||
                select === '8' ? null : (
                  <CheckBox
                    label="Show Category bar"
                    value={isShowCategory}
                    onChange={(value) => setIsShowCategory(value)}
                  />
                )}
                {isShowCategory && (
                  <div>
                    {select === '3' ||
                    select === '4' ||
                    select === '6' ||
                    select === '8' ? null : (
                      <div>
                        <div>Style</div>
                        <div className="flex justify-center">
                          <div
                            className={`rounded p-3 border border-1 border-gray-400 mr-4 cursor-pointer ${
                              styleBody === 1 ? 'bg-orange-500' : ''
                            }`}
                            aria-expanded={styleBody}
                            onClick={() => setStyleBody(1)}
                          >
                            Style 1
                          </div>
                          <div
                            className={`rounded p-3 border border-1 border-gray-400 mr-4 cursor-pointer ${
                              styleBody === 2 ? 'bg-orange-500' : ''
                            }`}
                            aria-expanded={styleBody}
                            onClick={() => setStyleBody(2)}
                          >
                            Style 2
                          </div>
                        </div>
                      </div>
                    )}
                    <div>
                      <p>Category bar background color</p>
                      <div className="flex items-center pt-3">
                        <input
                          type="color"
                          className="h-[36px] mr-1 w-[60px] cursor-pointer"
                          onChange={(e) =>
                            setBodyBackgroundColor(e.target.value)
                          }
                          value={bodyBackgroundColor}
                        ></input>
                        <div className="flex-1">
                          <TextField
                            label=""
                            value={bodyBackgroundColor}
                            onChange={(value) => setBodyBackgroundColor(value)}
                            autoComplete="off"
                          />
                        </div>
                      </div>
                    </div>
                    <div className="pt-3">
                      <p>Category bar font color</p>
                      <div className="flex items-center pt-3">
                        <input
                          type="color"
                          className="h-[36px] mr-1 w-[60px] cursor-pointer"
                          onChange={(e) => setBarFontColor(e.target.value)}
                          value={barFontColor}
                        ></input>
                        <div className="flex-1">
                          <TextField
                            label=""
                            value={barFontColor}
                            onChange={(value) => setBarFontColor(value)}
                            autoComplete="off"
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                )}
                <RangeSliderComponent
                  label="Font size"
                  value={rangeValue}
                  min={10}
                  max={40}
                  onChange={(value) => setRangeValue(value)}
                />
                <Select
                  label="Font family"
                  options={fontFamily}
                  onChange={(value) => setFontBodyFamily(value)}
                  value={fontBodyFamily}
                />
                <Select
                  label="Font Weight"
                  options={FontWeight}
                  onChange={(value) => setFontBodyWeight(value)}
                  value={fontBodyWeight}
                />
                <Select
                  label="Text Style"
                  options={fontStyle}
                  onChange={(value) => setFontBodyStyle(value)}
                  value={fontBodyStyle}
                />
                <Select
                  label="Text Align"
                  options={Align}
                  onChange={(value) => setTextAlign(value)}
                  value={textAlign}
                />
                <div>
                  <p>Font color</p>
                  <div className="flex items-center">
                    <input
                      type="color"
                      className="h-[36px] mr-1 w-[60px] cursor-pointer"
                      onChange={(e) => setBodyFontColor(e.target.value)}
                      value={bodyFontColor}
                    ></input>
                    <div className="flex-1">
                      <TextField
                        label=""
                        value={bodyFontColor}
                        onChange={(value) => setBodyFontColor(value)}
                        autoComplete="off"
                      />
                    </div>
                  </div>
                </div>
              </BlockStack>
            </div>
          </Collapsible>
        </Card>
        <Card>
          <div className="flex justify-between items-center">
            <div>
              <b>Question</b>
            </div>
            <Button
              onClick={() => handleToggle('Question')}
              ariaExpanded={toggles === 'Question'}
              ariaControls="basic-collapsible"
            >
              Toggle
            </Button>
          </div>
          <Collapsible
            open={toggles === 'Question'}
            id="basic-collapsible"
            transition={{ duration: '500ms', timingFunction: 'ease-in-out' }}
            expandOnPrint
          >
            <div className="mt-3">
              <BlockStack gap={'400'}>
                <RangeSliderComponent
                  label="Font size"
                  value={fontSizeQuestion}
                  min={10}
                  max={36}
                  onChange={(value) => setFontSizeQuestion(value)}
                />
                <Select
                  label="Font weight"
                  options={FontWeight}
                  onChange={(e) => setFontWeightQuestion(e)}
                  value={fontWeightQuestion}
                />
                <Select
                  label="Font family"
                  options={fontFamily}
                  onChange={(e) => setFontFamilyQuestion(e)}
                  value={fontFamilyQuestion}
                />
                {select === '1' ||
                select === '3' ||
                select === '4' ||
                select === '6' ||
                select === '7' ||
                select === '8' ? null : (
                  <div>
                    <p>Background color</p>
                    <div className="flex items-center">
                      <input
                        type="color"
                        className="h-[36px] mr-1 w-[60px] cursor-pointer"
                        onChange={(e) =>
                          setBackgroundcolorQuestion(e.target.value)
                        }
                        value={backgroundcolorQuestion}
                      ></input>
                      <div className="flex-1">
                        <TextField
                          label=""
                          value={backgroundcolorQuestion}
                          onChange={(value) =>
                            setBackgroundcolorQuestion(value)
                          }
                          autoComplete="off"
                        />
                      </div>
                    </div>
                  </div>
                )}
                <div>
                  <p>Text color</p>
                  <div className="flex items-center">
                    <input
                      type="color"
                      className="h-[36px] mr-1 w-[60px] cursor-pointer"
                      onChange={(e) => setTextcolorQuestion(e.target.value)}
                      value={textcolorQuestion}
                    ></input>
                    <div className="flex-1">
                      <TextField
                        label=""
                        value={textcolorQuestion}
                        onChange={(value) => setTextcolorQuestion(value)}
                        autoComplete="off"
                      />
                    </div>
                  </div>
                </div>
                {select === '1' ||
                select === '3' ||
                select === '4' ||
                select === '5' ||
                select === '6' ||
                select === '7' ||
                select === '8' ? null : (
                  <div>
                    <p>Hover color</p>
                    <div className="flex items-center">
                      <input
                        type="color"
                        className="h-[36px] mr-1 w-[60px] cursor-pointer"
                        onChange={(e) => setHovercolorQuestion(e.target.value)}
                        value={hovercolorQuestion}
                      ></input>
                      <div className="flex-1">
                        <TextField
                          label=""
                          value={hovercolorQuestion}
                          onChange={(value) => setHovercolorQuestion(value)}
                          autoComplete="off"
                        />
                      </div>
                    </div>
                  </div>
                )}
              </BlockStack>
            </div>
          </Collapsible>
        </Card>
        <Card>
          <div className="flex justify-between items-center">
            <div>
              <b>Answer</b>
            </div>
            <Button
              onClick={() => handleToggle('Answer')}
              ariaExpanded={toggles === 'Answer'}
              ariaControls="basic-collapsible"
            >
              Toggle
            </Button>
          </div>
          <Collapsible
            open={toggles === 'Answer'}
            id="basic-collapsible"
            transition={{ duration: '500ms', timingFunction: 'ease-in-out' }}
            expandOnPrint
          >
            <div className="mt-3">
              <BlockStack gap={'400'}>
                <RangeSliderComponent
                  label="Font size"
                  value={fontSizeAnswer}
                  min={10}
                  max={40}
                  onChange={(value) => setFontSizeAnswer(value)}
                />
                <Select
                  label="Font family"
                  options={fontFamily}
                  onChange={(e) => setFontFamilyAnswer(e)}
                  value={fontFamilyAnswer}
                />
                <Select
                  label="Font weight"
                  options={FontWeight}
                  onChange={(e) => setFontWeightAnswer(e)}
                  value={fontWeightAnswer}
                />
                {select === '1' ||
                select === '3' ||
                select === '4' ||
                select === '6' ||
                select === '7' ||
                select === '8' ? null : (
                  <div>
                    <p>Background color</p>
                    <div className="flex items-center">
                      <input
                        type="color"
                        className="h-[36px] mr-1 w-[60px] cursor-pointer"
                        onChange={(e) =>
                          setBackgroundColorAnswer(e.target.value)
                        }
                        value={backgroundColorAnswer}
                      ></input>
                      <div className="flex-1">
                        <TextField
                          label=""
                          value={backgroundColorAnswer}
                          onChange={(value) => setBackgroundColorAnswer(value)}
                          autoComplete="off"
                        />
                      </div>
                    </div>
                  </div>
                )}
                <div>
                  <p>Text color</p>
                  <div className="flex items-center">
                    <input
                      type="color"
                      className="h-[36px] mr-1 w-[60px] cursor-pointer"
                      onChange={(e) => setTextColorAnswer(e.target.value)}
                      value={textColorAnswer}
                    ></input>
                    <div className="flex-1">
                      <TextField
                        label=""
                        value={textColorAnswer}
                        onChange={(value) => setTextColorAnswer(value)}
                        autoComplete="off"
                      />
                    </div>
                  </div>
                </div>
              </BlockStack>
            </div>
          </Collapsible>
        </Card>
        <Card>
          <div className="flex justify-between items-center">
            <div>
              <b>Custom</b>
            </div>
            <Button
              onClick={() => handleToggle('Custom')}
              ariaExpanded={toggles === 'Custom'}
              ariaControls="basic-collapsible"
            >
              Toggle
            </Button>
          </div>
          <Collapsible
            open={toggles === 'Custom'}
            id="basic-collapsible"
            transition={{ duration: '500ms', timingFunction: 'ease-in-out' }}
            expandOnPrint
          >
            <div className="mt-3">
              <TextField
                label="Custom CSS"
                value={customCss}
                onChange={(value) => setCustomCss(value)}
                multiline={4}
                autoComplete="off"
                placeholder={`.faq-page-title{
                                color: #ffffff
                               }`}
              />
              <p className="mt-2">
                This will be applicable to your entire FAQ page. As this is page
                related setting, changes you make here won't be reflected on
                live view on left side. Please don't touch this section if you
                don't know what you are doing. Feel free to contact our support
                and we are more than welcome to help.
              </p>
            </div>
          </Collapsible>
        </Card>
      </BlockStack>
    </div>
  );
};

export default Body;
