import { Select } from '@shopify/polaris';
import { useEffect, useState } from 'react';
import template1 from './../../assets/images/template1.png';
import template2 from './../../assets/images/template2.png';
import template3 from './../../assets/images/template3.png';
import template4 from './../../assets/images/template4.png';
import template5 from './../../assets/images/template5.png';
import template6 from './../../assets/images/template6.png';
import template7 from './../../assets/images/template7.png';
import template8 from './../../assets/images/template8.png';

export type templateProps = {
  templateSelected: string;
  setTemplateSelected: (templateSelected: string) => void;
  uploadedFiles?: string | null;
  templateNumberData?: number;
  setDataTemplate1?: (data: any) => void;
  setDataTemplate2?: (data: any) => void;
  setDataTemplate3?: (data: any) => void;
  setDataTemplate4?: (data: any) => void;
  setDataTemplate5?: (data: any) => void;
  setDataTemplate6?: (data: any) => void;
  setDataTemplate7?: (data: any) => void;
  setDataTemplate8?: (data: any) => void;
};
export default function TemplateSelection(props: templateProps) {
  const selectionNumber = props?.templateNumberData
    ? props?.templateNumberData
    : 2;
  const templates = [
    template1,
    template2,
    template3,
    template4,
    template5,
    template6,
    template7,
    template8,
  ];

  const [image, setImage] = useState(templates[selectionNumber - 1]);

  useEffect(() => {
    setImage(templates[selectionNumber - 1]);
  }, [props?.templateNumberData]);

  useEffect(() => {
    props.setTemplateSelected((templates.indexOf(image) + 1).toString());
  }, [props, image]);

  const options = templates.map((template, index) => ({
    label: `Template ${String(index + 1).padStart(2, '0')}`,
    value: template,
  }));

  return (
    <>
      <Select
        label=""
        options={options}
        value={image}
        onChange={(value) => {
          setImage(value);
        }}
      />
      <div className="p-3">
        {props.uploadedFiles ? (
          <div className="mt-4">
            <img
              src={`${import.meta.env.VITE_HOST}${props?.uploadedFiles}`}
              alt=""
              className="border rounded"
              width={800}
              height={500}
            />
          </div>
        ) : (
          <div className="mt-4">
            <img
              src={image}
              alt=""
              className="border rounded"
              width={800}
              height={500}
            />
          </div>
        )}
      </div>
    </>
  );
}
