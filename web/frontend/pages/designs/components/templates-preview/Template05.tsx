import { useContext, useEffect, useState } from 'react';
import { Context } from '../../../../components/providers/DesignProvider';
import template5 from './../../../../assets/images/banner-preview/banner-default-3.png';

const Template05 = (props: any) => {
  const [isHover, setIsHover] = useState<number>();

  const { setDataTemplate5 } = props;
  const {
    // Header
    isShowBanner,
    showPageTitle,
    isShowSearchBar,
    showIntroText,
    isShowCategory,
    showFooter,
    headerHeight,
    fontSize,
    paddingTop,
    paddingBottom,
    headerFontColor,
    pageTitleContent,
    searchPlaceholder,
    placeholderColor,
    font,
    fontSearchbar,
    fontFamilyIntroText,
    introTextContent,
    introTextFontColor,
    fontSizeIntrotext,
    paddingTopIntrotext,
    paddingBottomIntrotext,
    styleSearchBar,
    microScopeColor,

    // Body
    rangeValue,
    styleBody,
    bodyBackgroundColor,
    barFontColor,
    bodyFontColor,
    fontBodyFamily,
    textAlign,
    fontBodyWeight,
    fontBodyStyle,
    fontSizeQuestion,
    fontWeightQuestion,
    fontFamilyQuestion,
    backgroundcolorQuestion,
    textcolorQuestion,
    fontSizeAnswer,
    fontFamilyAnswer,
    fontWeightAnswer,
    backgroundColorAnswer,
    textColorAnswer,

    // Footer
    footerTextContent,
    fontFamilyFooter,
    textColorFooter,
    rangeFontSizeFooter,
    rangePaddingTopFooter,
    rangePaddingBottomFooter,

    // More
    buttonBackgroundColorMore,
    rangeFaqPageMore,
    rangeProductFaqMore,
    buttonColorMore,
    buttonHoverColorMore,
    isBackToTopMore,
  } = useContext(Context);

  useEffect(() => {
    const dataTemplate5 = {
      isShowBanner,
      showPageTitle,
      isShowSearchBar,
      showIntroText,
      isShowCategory,
      showFooter,
      headerHeight,
      fontSize,
      paddingTop,
      paddingBottom,
      headerFontColor,
      pageTitleContent,
      searchPlaceholder,
      placeholderColor,
      font,
      fontSearchbar,
      fontFamilyIntroText,
      introTextContent,
      introTextFontColor,
      fontSizeIntrotext,
      paddingTopIntrotext,
      paddingBottomIntrotext,
      styleSearchBar,
      microScopeColor,

      // Body
      rangeValue,
      styleBody,
      bodyBackgroundColor,
      barFontColor,
      bodyFontColor,
      fontBodyFamily,
      textAlign,
      fontBodyWeight,
      fontBodyStyle,
      fontSizeQuestion,
      fontWeightQuestion,
      fontFamilyQuestion,
      backgroundcolorQuestion,
      textcolorQuestion,
      fontSizeAnswer,
      fontFamilyAnswer,
      fontWeightAnswer,
      backgroundColorAnswer,
      textColorAnswer,

      // Footer
      footerTextContent,
      fontFamilyFooter,
      textColorFooter,
      rangeFontSizeFooter,
      rangePaddingTopFooter,
      rangePaddingBottomFooter,

      // More
      buttonBackgroundColorMore,
      rangeFaqPageMore,
      rangeProductFaqMore,
      buttonColorMore,
      buttonHoverColorMore,
    };
    setDataTemplate5(dataTemplate5);
  }, [
    isShowBanner,
    showPageTitle,
    isShowSearchBar,
    showIntroText,
    isShowCategory,
    showFooter,
    headerHeight,
    fontSize,
    paddingTop,
    paddingBottom,
    headerFontColor,
    pageTitleContent,
    searchPlaceholder,
    placeholderColor,
    font,
    fontSearchbar,
    fontFamilyIntroText,
    introTextContent,
    introTextFontColor,
    fontSizeIntrotext,
    paddingTopIntrotext,
    paddingBottomIntrotext,
    styleSearchBar,
    microScopeColor,

    // Body
    rangeValue,
    styleBody,
    bodyBackgroundColor,
    barFontColor,
    bodyFontColor,
    fontBodyFamily,
    textAlign,
    fontBodyWeight,
    fontBodyStyle,
    fontSizeQuestion,
    fontWeightQuestion,
    fontFamilyQuestion,
    backgroundcolorQuestion,
    textcolorQuestion,
    fontSizeAnswer,
    fontFamilyAnswer,
    fontWeightAnswer,
    backgroundColorAnswer,
    textColorAnswer,

    // Footer
    footerTextContent,
    fontFamilyFooter,
    textColorFooter,
    rangeFontSizeFooter,
    rangePaddingTopFooter,
    rangePaddingBottomFooter,

    // More
    buttonBackgroundColorMore,
    rangeFaqPageMore,
    rangeProductFaqMore,
    buttonColorMore,
    buttonHoverColorMore,
  ]);

  return (
    <div
      className="bg-red-300 border"
      style={{ backgroundColor: buttonBackgroundColorMore }}
    >
      <div
        className="flex flex-col justify-between items-center text-center"
        style={{
          backgroundImage: isShowBanner
            ? `url(${
                props.uploadedFiles
                  ? `${import.meta.env.VITE_HOST}${props.uploadedFiles}`
                  : template5
              })`
            : 'none',
          backgroundSize: 'cover',
          minHeight: headerHeight,
        }}
      >
        <div className="w-full">
          <div
            style={{
              // padding: `${design?.page_title_paddingtop}px 0 ${design?.page_title_paddingbottom}px 0`,
              padding: `${paddingTop}px 0 ${paddingBottom}px 0`,
            }}
          >
            <p
              className="text-xl py-5 text-3xl font-bold"
              style={{
                color: headerFontColor,
                fontSize: fontSize,
                fontFamily: font,
                lineHeight: '1.5',
              }}
            >
              {showPageTitle && pageTitleContent}
            </p>
          </div>
          {/* {isShowSearchBar && design?.search_input_style === styleSearchBar && ( */}
          {isShowSearchBar && styleSearchBar === '1' && (
            <div
              className="text-center w-100 m-auto text-base px-28 py-2"
              style={{
                position: 'relative',
                width: '90%',
                borderRadius: '100px',
              }}
            >
              <input
                type="text"
                placeholder={searchPlaceholder}
                className="form-control m-auto rounded"
                style={{
                  boxShadow: 'none',
                  outline: 'none',
                  border: 'none',
                  width: '100%',
                  padding: '16px',
                  lineHeight: '1',
                  fontFamily: fontSearchbar,
                  color: placeholderColor,
                }}
              />
            </div>
          )}
          {/* {isShowSearchBar && design?.search_input_style === styleSearchBar && ( */}
          {isShowSearchBar && styleSearchBar === '2' && (
            <div className="help__desk-search px-28 py-2">
              <div
                className="help__desk-search-main flex justify-between items-center"
                style={{
                  border: '1px solid #ccc',
                  borderRadius: '30px',
                  overflow: 'hidden',
                  height: '50px',
                  backgroundColor: '#ffffff',
                }}
              >
                <svg
                  height={'18px'}
                  style={{ fill: microScopeColor }}
                  className="fa-solid fa-magnifying-glass pl-3 pr-2 pt-[2px]"
                  viewBox="0 0 512 512"
                >
                  <path d="M416 208c0 45.9-14.9 88.3-40 122.7L502.6 457.4c12.5 12.5 12.5 32.8 0 45.3s-32.8 12.5-45.3 0L330.7 376c-34.4 25.2-76.8 40-122.7 40C93.1 416 0 322.9 0 208S93.1 0 208 0S416 93.1 416 208zM208 352a144 144 0 1 0 0-288 144 144 0 1 0 0 288z" />
                </svg>
                <input
                  className="text-sm"
                  id="search-faq"
                  style={{
                    flex: '1',
                    border: 'none',
                    outline: 'none',
                    fontFamily: fontSearchbar,
                    color: placeholderColor,
                    backgroundColor: '#ffffff',
                  }}
                  type="text"
                  placeholder={searchPlaceholder}
                />
              </div>
            </div>
          )}
        </div>
        <div>
          {showIntroText && (
            <div
              style={{
                fontFamily: fontFamilyIntroText,
                color: introTextFontColor,
                fontSize: fontSizeIntrotext,
                paddingTop: paddingTopIntrotext,
                paddingBottom: paddingBottomIntrotext,
              }}
            >
              <div>{introTextContent}</div>
            </div>
          )}
        </div>
      </div>

      <div
        className="flex flex-col items-center m-auto py-8 px-4"
        style={{
          maxWidth: '800px',
        }}
      >
        {isShowCategory && styleBody === 1 && (
          <div className="flex mb-5">
            <button
              className="px-5 py-2 border rounded"
              style={{ backgroundColor: bodyBackgroundColor }}
            >
              <div
                style={{
                  color: barFontColor,
                  fontFamily: fontBodyFamily,
                  textTransform: fontBodyStyle,
                }}
              >
                <div
                  style={{
                    fontWeight: fontBodyWeight,
                  }}
                >
                  Placing an Order
                </div>
              </div>
            </button>
            <button
              className="ml-3 px-5 py-3 border rounded ms-3"
              style={{ backgroundColor: bodyBackgroundColor }}
            >
              <div
                style={{
                  color: barFontColor,
                  fontFamily: fontBodyFamily,
                  textTransform: fontBodyStyle,
                }}
              >
                <div style={{ fontWeight: fontBodyWeight }}>Refunds</div>
              </div>
            </button>
          </div>
        )}
        {isShowCategory && styleBody === 2 && (
          <div className="flex mb-5">
            <button
              className="px-5 py-3 border rounded-full"
              style={{ backgroundColor: bodyBackgroundColor }}
            >
              <div
                style={{
                  color: barFontColor,
                  fontFamily: fontBodyFamily,
                  textTransform: fontBodyStyle,
                }}
              >
                <div style={{ fontWeight: fontBodyWeight }}>
                  Placing an Order
                </div>
              </div>
            </button>
            <button
              className="ml-3 px-5 py-3 border rounded-full ms-3"
              style={{ backgroundColor: bodyBackgroundColor }}
            >
              <div
                style={{
                  color: barFontColor,
                  fontFamily: fontBodyFamily,
                  textTransform: fontBodyStyle,
                }}
              >
                <div style={{ fontWeight: fontBodyWeight }}>Refunds</div>
              </div>
            </button>
          </div>
        )}
        <div>
          <div>
            <div
              style={{
                fontSize: rangeValue,
                color: bodyFontColor,
                fontFamily: fontBodyFamily,
                textAlign: textAlign,
                textTransform: fontBodyStyle,
              }}
            >
              <div
                className="pb-1 pt-3 mb-5"
                style={{ fontWeight: fontBodyWeight }}
              >
                Placing an Order
              </div>
            </div>
            <div className="mx-3">
              <div className="flex items-center">
                <div
                  className="flex items-center justify-between p-3 bg-red-300 rounded cursor-pointer ml-8 p-6"
                  style={{
                    fontSize: fontSizeQuestion,
                    backgroundColor: backgroundcolorQuestion,
                    width: '100%',
                  }}
                >
                  <div
                    style={{
                      fontWeight: fontWeightQuestion,
                      fontFamily: fontFamilyQuestion,
                      color: textcolorQuestion,
                    }}
                  >
                    When will I be charged for my order?
                  </div>
                </div>
                <div
                  className="mr-6"
                  style={{
                    position: 'relative',
                    height: 0,
                    borderTop: '15px solid transparent',
                    borderBottom: '15px solid transparent',
                    borderLeft: `15px solid ${backgroundcolorQuestion}`,
                  }}
                ></div>
              </div>
              <div className="flex items-center">
                <div
                  className="ml-4"
                  style={{
                    position: 'relative',
                    marginTop: '10px',
                    height: 0,
                    borderTop: '15px solid transparent',
                    borderBottom: '15px solid transparent',
                    borderRight: `15px solid ${backgroundcolorQuestion}`,
                  }}
                ></div>
                <div
                  className={`flex items-center justify-between p-3 bg-red-300 cursor-pointer mt-3 mr-9 p-6`}
                  style={{
                    fontSize: fontSizeQuestion,
                    backgroundColor: backgroundcolorQuestion,
                    width: '100%',
                    borderTopLeftRadius: '5px',
                    borderTopRightRadius: '5px',
                  }}
                >
                  <div
                    style={{
                      fontWeight: fontWeightQuestion,
                      fontFamily: fontFamilyQuestion,
                      color: textcolorQuestion,
                    }}
                  >
                    How do I redeem my One 4 All card?
                  </div>
                </div>
              </div>

              <div
                className="ml-8 mr-9 pl-6 pr-6 pb-6"
                style={{
                  fontSize: fontSizeAnswer,
                  fontFamily: fontFamilyAnswer,
                  fontWeight: fontWeightAnswer,
                  backgroundColor: backgroundColorAnswer,
                  color: textColorAnswer,
                }}
              >
                We are currently accepting One 4 All cards instore only. Please
                retain your card after making your purchase, as should you wish
                to return any items bought using a One 4 All card, we will use
                this payment method to refund you.
              </div>
              <br />

              <div
                // className="p-3"
                style={{
                  fontSize: fontSizeAnswer,
                  fontFamily: fontFamilyAnswer,
                  fontWeight: fontWeightAnswer,
                  backgroundColor: backgroundColorAnswer,
                  color: textColorAnswer,
                }}
              >
                {/* Payment is taken at the point of ordering. If for any reason
                your item is not dispatched we will ensure you are refunded
                within 3-5 working days. */}
              </div>
            </div>
          </div>
          <div className="mt-5 mx-3">
            <div
              style={{
                fontSize: rangeValue,
                color: bodyFontColor,
                fontFamily: fontBodyFamily,
                textAlign: textAlign,
                textTransform: fontBodyStyle,
              }}
            >
              <p className="pb-1 pt-3" style={{ fontWeight: fontBodyWeight }}>
                Refunds
              </p>
            </div>
            <br />
            <div className="flex items-center">
              <div
                className="flex items-center justify-between p-3 bg-red-300 rounded cursor-pointer ml-8 p-6"
                style={{
                  fontSize: fontSizeQuestion,
                  backgroundColor: backgroundcolorQuestion,
                  width: '100%',
                }}
              >
                <div
                  style={{
                    fontWeight: fontWeightQuestion,
                    fontFamily: fontFamilyQuestion,
                    color: textcolorQuestion,
                  }}
                >
                  I have returned my order – will you refund my delivery?
                </div>
              </div>
              <div
                className="mr-6"
                style={{
                  position: 'relative',
                  height: 0,
                  borderTop: '15px solid transparent',
                  borderBottom: '15px solid transparent',
                  borderLeft: `15px solid ${backgroundcolorQuestion}`,
                }}
              ></div>
            </div>
            <div
              style={{
                fontSize: fontSizeAnswer,
                fontFamily: fontFamilyAnswer,
                fontWeight: fontWeightAnswer,
                backgroundColor: backgroundColorAnswer,
                color: textColorAnswer,
              }}
            >
              {/* If you have returned your full order within 30 days of receipt, we
              will refund the full amount, including delivery costs. Please
              note, delivery costs are refunded additionally and will show as a
              separate refund. */}
            </div>
          </div>
        </div>
      </div>

      <div className="mt-8 text-center">
        {showFooter && (
          <div
            style={{
              padding: '12px',
              fontFamily: fontFamilyFooter,
              color: textColorFooter,
              fontSize: rangeFontSizeFooter,
              paddingTop: rangePaddingTopFooter,
              paddingBottom: rangePaddingBottomFooter,
            }}
          >
            {footerTextContent}
          </div>
        )}
        {isBackToTopMore && (
          <div
            className="mr-3 pb-3"
            style={{ position: 'relative', marginBottom: '20px' }}
          >
            <div style={{ position: 'absolute', right: '0' }}>
              <button
                style={{
                  border: 'none',
                  width: '50px',
                  height: '50px',
                  display: 'flex',
                  justifyContent: 'center',
                  alignItems: 'center',
                  position: 'relative',
                  boxShadow: '0 4px 6px rgba(0, 0, 0, 0.1)',
                  transition: 'background-color 0.3s ease',
                  cursor: 'pointer',
                  marginBottom: '20px',
                  borderRadius: '10%',
                  backgroundColor:
                    isHover === 4 ? buttonHoverColorMore : buttonColorMore,
                }}
                onMouseEnter={() => setIsHover(4)}
                onMouseLeave={() => setIsHover(0)}
              >
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 24 24"
                  strokeWidth="2"
                  stroke="white"
                  className="w-6 h-6"
                  style={{
                    position: 'absolute',
                    top: '0',
                    marginTop: '10px',
                  }}
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M5 15l7-7 7 7"
                  />
                </svg>
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 24 24"
                  strokeWidth="2"
                  stroke="white"
                  className="w-6 h-6"
                  style={{
                    position: 'absolute',
                    marginTop: '3px',
                  }}
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M5 15l7-7 7 7"
                  />
                </svg>
              </button>
            </div>
          </div>
        )}
        <div>
          <div style={{ paddingBottom: '10px', textAlign: 'center' }}>
            <span
              style={{
                color: 'rgb(75, 75, 75)',
                fontSize: '15px',
                textDecoration: 'none',
                fontFamily: 'Times New Roman, Times, serif',
              }}
            >
              Powered by Yanet
            </span>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Template05;
