import { useContext, useEffect, useState } from 'react';
import { Context } from '../../../../components/providers/DesignProvider';
import template6 from './../../../../assets/images/banner-preview/banner-default-3.png';

const Template06 = (props: any) => {
  const [isHover, setIsHover] = useState<number>();

  const { setDataTemplate6 } = props;
  const {
    // Header
    isShowBanner,
    showPageTitle,
    isShowSearchBar,
    showIntroText,
    headerHeight,
    fontSize,
    paddingTop,
    paddingBottom,
    headerFontColor,
    pageTitleContent,
    searchPlaceholder,
    placeholderColor,
    font,
    fontSearchbar,
    fontFamilyIntroText,
    introTextContent,
    introTextFontColor,
    fontSizeIntrotext,
    paddingTopIntrotext,
    paddingBottomIntrotext,
    styleSearchBar,
    microScopeColor,

    // Body
    rangeValue,
    bodyBackgroundColor,
    barFontColor,
    bodyFontColor,
    fontBodyFamily,
    textAlign,
    fontBodyWeight,
    fontBodyStyle,
    fontSizeQuestion,
    fontWeightQuestion,
    fontFamilyQuestion,
    textcolorQuestion,
    fontSizeAnswer,
    fontFamilyAnswer,
    fontWeightAnswer,
    textColorAnswer,

    // Footer
    footerTextContent,
    fontFamilyFooter,
    textColorFooter,
    rangeFontSizeFooter,
    rangePaddingTopFooter,
    rangePaddingBottomFooter,
    showFooter,

    // More
    buttonBackgroundColorMore,
    rangeFaqPageMore,
    rangeProductFaqMore,
    buttonColorMore,
    buttonHoverColorMore,
    isBackToTopMore,
  } = useContext(Context);

  useEffect(() => {
    const datatemplate6 = {
      isShowBanner,
      showPageTitle,
      isShowSearchBar,
      showIntroText,
      headerHeight,
      fontSize,
      paddingTop,
      paddingBottom,
      headerFontColor,
      pageTitleContent,
      searchPlaceholder,
      placeholderColor,
      font,
      fontSearchbar,
      fontFamilyIntroText,
      introTextContent,
      introTextFontColor,
      fontSizeIntrotext,
      paddingTopIntrotext,
      paddingBottomIntrotext,
      styleSearchBar,
      microScopeColor,

      // Body
      rangeValue,
      bodyBackgroundColor,
      barFontColor,
      bodyFontColor,
      fontBodyFamily,
      textAlign,
      fontBodyWeight,
      fontBodyStyle,
      fontSizeQuestion,
      fontWeightQuestion,
      fontFamilyQuestion,
      textcolorQuestion,
      fontSizeAnswer,
      fontFamilyAnswer,
      fontWeightAnswer,
      textColorAnswer,

      // Footer
      footerTextContent,
      fontFamilyFooter,
      textColorFooter,
      rangeFontSizeFooter,
      rangePaddingTopFooter,
      rangePaddingBottomFooter,
      showFooter,

      // More
      buttonBackgroundColorMore,
      rangeFaqPageMore,
      rangeProductFaqMore,
      buttonColorMore,
      buttonHoverColorMore,
    };
    setDataTemplate6(datatemplate6);
  }, [
    isShowBanner,
    showPageTitle,
    isShowSearchBar,
    showIntroText,
    headerHeight,
    fontSize,
    paddingTop,
    paddingBottom,
    headerFontColor,
    pageTitleContent,
    searchPlaceholder,
    placeholderColor,
    font,
    fontSearchbar,
    fontFamilyIntroText,
    introTextContent,
    introTextFontColor,
    fontSizeIntrotext,
    paddingTopIntrotext,
    paddingBottomIntrotext,
    styleSearchBar,
    microScopeColor,

    // Body
    rangeValue,
    bodyBackgroundColor,
    barFontColor,
    bodyFontColor,
    fontBodyFamily,
    textAlign,
    fontBodyWeight,
    fontBodyStyle,
    fontSizeQuestion,
    fontWeightQuestion,
    fontFamilyQuestion,
    textcolorQuestion,
    fontSizeAnswer,
    fontFamilyAnswer,
    fontWeightAnswer,
    textColorAnswer,

    // Footer
    footerTextContent,
    fontFamilyFooter,
    textColorFooter,
    rangeFontSizeFooter,
    rangePaddingTopFooter,
    rangePaddingBottomFooter,
    showFooter,

    // More
    buttonBackgroundColorMore,
    rangeFaqPageMore,
    rangeProductFaqMore,
    buttonColorMore,
    buttonHoverColorMore,
  ]);
  return (
    <div
      className="border"
      style={{ backgroundColor: buttonBackgroundColorMore }}
    >
      <div
        className="flex flex-col justify-between items-center text-center relative"
        style={{
          backgroundImage: isShowBanner
            ? `url(${
                props.uploadedFiles
                  ? `${import.meta.env.VITE_HOST}${props.uploadedFiles}`
                  : template6
              })`
            : 'none',
          backgroundSize: 'cover',
          minHeight: headerHeight,
        }}
      >
        <div className="w-full">
          <div>
            <p
              className="text-xl text-3xl font-bold"
              style={{
                color: headerFontColor,
                fontSize: fontSize,
                fontFamily: font,
                lineHeight: '1.5',
                padding: `${paddingTop}px 0 ${paddingBottom}px 0`,
              }}
            >
              {showPageTitle && pageTitleContent}
            </p>
          </div>

          {isShowSearchBar && styleSearchBar === '1' && (
            <div
              className="text-center w-100 m-auto text-base px-28"
              style={{
                position: 'relative',
                width: '90%',
                borderRadius: '100px',
              }}
            >
              <input
                type="text"
                placeholder={searchPlaceholder}
                className="form-control m-auto rounded"
                style={{
                  boxShadow: 'none',
                  outline: 'none',
                  border: 'none',
                  width: '100%',
                  padding: '16px',
                  lineHeight: '1',
                  fontFamily: fontSearchbar,
                  color: placeholderColor,
                }}
              />
            </div>
          )}
          {isShowSearchBar && styleSearchBar === '2' && (
            <div className="help__desk-search px-28">
              <div
                className="help__desk-search-main flex justify-between items-center"
                style={{
                  border: '1px solid #ccc',
                  borderRadius: '30px',
                  overflow: 'hidden',
                  height: '50px',
                  backgroundColor: '#ffffff',
                }}
              >
                <svg
                  height={'18px'}
                  style={{ fill: microScopeColor }}
                  className="fa-solid fa-magnifying-glass pl-3 pr-2"
                  viewBox="0 0 512 512"
                >
                  <path d="M416 208c0 45.9-14.9 88.3-40 122.7L502.6 457.4c12.5 12.5 12.5 32.8 0 45.3s-32.8 12.5-45.3 0L330.7 376c-34.4 25.2-76.8 40-122.7 40C93.1 416 0 322.9 0 208S93.1 0 208 0S416 93.1 416 208zM208 352a144 144 0 1 0 0-288 144 144 0 1 0 0 288z" />
                </svg>
                <input
                  className="text-sm"
                  id="search-faq"
                  style={{
                    flex: '1',
                    border: 'none',
                    outline: 'none',
                    fontFamily: fontSearchbar,
                    color: placeholderColor,
                    backgroundColor: '#ffffff',
                  }}
                  type="text"
                  placeholder={searchPlaceholder}
                />
              </div>
            </div>
          )}

          <div className="p-3">
            {showIntroText && (
              <div
                style={{
                  fontFamily: fontFamilyIntroText,
                  color: introTextFontColor,
                  fontSize: fontSizeIntrotext,
                  paddingTop: paddingTopIntrotext,
                  paddingBottom: `calc(${paddingBottomIntrotext}px + 50px)`,
                }}
              >
                <div>{introTextContent}</div>
              </div>
            )}
          </div>

          <div className="absolute bottom-0 left-0 w-full flex justify-center">
            <button className="px-2 py-1 border-none rounded">
              <div
                className="p-3"
                style={{
                  color: barFontColor,
                  fontFamily: fontBodyFamily,
                  textTransform: fontBodyStyle,
                }}
              >
                <p> Account</p>
              </div>
            </button>
            <button
              className="px-2 py-1"
              style={{
                backgroundColor: bodyBackgroundColor,
                borderTopLeftRadius: '5px',
                borderTopRightRadius: '5px',
              }}
            >
              <div
                className="p-3"
                style={{
                  color: barFontColor,
                  fontFamily: fontBodyFamily,
                  textTransform: fontBodyStyle,
                }}
              >
                <p>Placing an Order</p>
              </div>
            </button>
            <button className="px-2 py-1 border-none rounded">
              <div
                className="p-3"
                style={{
                  color: barFontColor,
                  fontFamily: fontBodyFamily,
                  textTransform: fontBodyStyle,
                }}
              >
                <p> Shipping</p>
              </div>
            </button>
          </div>
        </div>
      </div>
      <div
        className="flex flex-col items-center m-auto py-8 px-4"
        style={{ maxWidth: '800px' }}
      >
        <div className="mx-16">
          <div
            style={{
              fontSize: rangeValue,
              color: bodyFontColor,
              fontFamily: fontBodyFamily,
              textAlign: textAlign,
              textTransform: fontBodyStyle,
            }}
          >
            <div className="pb-1 pt-3" style={{ fontWeight: fontBodyWeight }}>
              Placing an Order
            </div>
          </div>
          <div className="flex items-center cursor-pointer mt-8">
            <svg
              fill="#696969"
              viewBox="0 0 448 512"
              style={{ width: '14px', height: '14px' }}
            >
              <path d="M416 208H272V64c0-17.67-14.33-32-32-32h-32c-17.67 0-32 14.33-32 32v144H32c-17.67 0-32 14.33-32 32v32c0 17.67 14.33 32 32 32h144v144c0 17.67 14.33 32 32 32h32c17.67 0 32-14.33 32-32V304h144c17.67 0 32-14.33 32-32v-32c0-17.67-14.33-32-32-32z"></path>
            </svg>
            <div
              className="flex items-center justify-between rounded cursor-pointer p-6"
              style={{
                fontSize: fontSizeQuestion,
              }}
            >
              <div
                style={{
                  fontWeight: fontWeightQuestion,
                  fontFamily: fontFamilyQuestion,
                  color: textcolorQuestion,
                }}
              >
                When will I be charged for my order?
              </div>
            </div>
          </div>
          <div className="flex items-center cursor-pointer">
            <svg
              fill="#696969"
              viewBox="0 0 448 512"
              style={{ width: '14px', height: '14px' }}
            >
              <path d="M416 208H32c-17.67 0-32 14.33-32 32v32c0 17.67 14.33 32 32 32h384c17.67 0 32-14.33 32-32v-32c0-17.67-14.33-32-32-32z"></path>
            </svg>
            <div
              className="flex items-center justify-between rounded cursor-pointer pl-6"
              style={{
                fontSize: fontSizeQuestion,
              }}
            >
              <div
                style={{
                  fontWeight: fontWeightQuestion,
                  fontFamily: fontFamilyQuestion,
                  color: textcolorQuestion,
                }}
              >
                How do I redeem my One 4 All card?
              </div>
            </div>
          </div>
          <div
            className="flex items-center flex-col pl-9 pr-9 pb-8 pt-3"
            style={{ borderColor: '#cccccc' }}
          >
            <div
              style={{
                fontSize: fontSizeAnswer,
                fontFamily: fontFamilyAnswer,
                fontWeight: fontWeightAnswer,
                color: textColorAnswer,
              }}
            >
              We are currently accepting One 4 All cards instore only. Please
              retain your card after making your purchase, as should you wish to
              return any items bought using a One 4 All card, we will use this
              payment method to refund you.
            </div>
          </div>
        </div>
      </div>
      <div className="mt-8 text-center">
        {showFooter && (
          <div
            style={{
              padding: '12px',
              fontFamily: fontFamilyFooter,
              color: textColorFooter,
              fontSize: rangeFontSizeFooter,
              paddingTop: rangePaddingTopFooter,
              paddingBottom: rangePaddingBottomFooter,
            }}
          >
            {footerTextContent}
          </div>
        )}
        {isBackToTopMore && (
          <div
            className="mr-3 pb-3"
            style={{ position: 'relative', marginBottom: '20px' }}
          >
            <div style={{ position: 'absolute', right: '0' }}>
              <button
                style={{
                  border: 'none',
                  width: '50px',
                  height: '50px',
                  display: 'flex',
                  justifyContent: 'center',
                  alignItems: 'center',
                  position: 'relative',
                  boxShadow: '0 4px 6px rgba(0, 0, 0, 0.1)',
                  transition: 'background-color 0.3s ease',
                  cursor: 'pointer',
                  marginBottom: '20px',
                  borderRadius: '10%',
                  backgroundColor:
                    isHover === 4 ? buttonHoverColorMore : buttonColorMore,
                }}
                onMouseEnter={() => setIsHover(4)}
                onMouseLeave={() => setIsHover(0)}
              >
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 24 24"
                  strokeWidth="2"
                  stroke="white"
                  className="w-6 h-6"
                  style={{
                    position: 'absolute',
                    top: '0',
                    marginTop: '10px',
                  }}
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M5 15l7-7 7 7"
                  />
                </svg>
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 24 24"
                  strokeWidth="2"
                  stroke="white"
                  className="w-6 h-6"
                  style={{
                    position: 'absolute',
                    marginTop: '3px',
                  }}
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M5 15l7-7 7 7"
                  />
                </svg>
              </button>
            </div>
          </div>
        )}
        <div>
          <div style={{ paddingBottom: '10px', textAlign: 'center' }}>
            <span
              style={{
                color: 'rgb(75, 75, 75)',
                fontSize: '15px',
                textDecoration: 'none',
                fontFamily: 'Times New Roman, Times, serif',
              }}
            >
              Powered by Yanet
            </span>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Template06;
