import { BlockStack, Text, Layout, Page, Modal } from '@shopify/polaris';
import { useTranslation } from 'react-i18next';
import ThemeAppActivate from '../components/theme-app-active';
import Helpdesk from '../components/help-desk';
import {
  ChatIcon,
  ExternalIcon,
  SendIcon,
  StarIcon,
} from '@shopify/polaris-icons';
import {
  BannerWithDismiss,
  ESessionStorageKeys,
} from '../components/banner-with-dismiss/BannerWithDismiss';
import { useState } from 'react';
import { useForm } from 'react-hook-form';
import PartnerApps from '../components/partner-app';
import { useUserContext } from '../../components/providers/UserProvider';
import { useAppBridge } from '@shopify/app-bridge-react';

export default function HomePage() {
  const app = useAppBridge();

  const { state: data } = useUserContext();

  app.loading(false);

  const [star, setStar] = useState(0);
  const [hover, setHover] = useState(0);
  const [openNoteModal, setOpenNoteModal] = useState(false);
  const { t } = useTranslation();
  const [submitting, setSubmitting] = useState(false);

  const chooseStar = (value: number) => {
    setStar(value);
    setOpenNoteModal(true);
  };

  const { handleSubmit } = useForm();

  const onSubmit = async (data: any) => {
    console.log(data);
    setSubmitting(true);
  };

  return (
    <Page title={t('Yanet: FAQ Page, Product FAQs')}>
      <Layout sectioned>
        <BlockStack gap={'400'}>
          <ThemeAppActivate />
          <BannerWithDismiss
            title={`Hi ${data?.store_name} Welcome to Yanet: FAQ Page, Product FAQs`}
            tone="info"
            sessionKey={ESessionStorageKeys.SSK_BANNER_DASHBOARD}
          >
            <BlockStack gap={'200'}>
              <Text as="p" fontWeight="medium">
                {t('I wish you all the best on this day!')}
              </Text>
              {true ? (
                <>
                  <Text as="p">
                    {t(
                      'How is your experience with Yanet: FAQ Page, Product FAQs?',
                    )}
                  </Text>
                  <div className="flex gap-1 items-center">
                    {[...Array(5)].map((num, index) => {
                      index += 1;
                      const isActive = index <= (hover || num);
                      return (
                        <button
                          key={index}
                          onClick={() => chooseStar(index)}
                          onMouseEnter={() => setHover(index)}
                          onMouseLeave={() => setHover(star)}
                          type="button"
                        >
                          {isActive ? (
                            <StarIcon
                              fill="currentColor"
                              className="w-5 h-5 md:w-6 md:h-6 text-green-800"
                            />
                          ) : (
                            <StarIcon
                              fill="currentColor"
                              className="w-5 h-5 md:w-6 md:h-6 text-gray-500"
                            />
                          )}
                        </button>
                      );
                    })}
                  </div>
                  {!false ? (
                    <Text as="p" fontWeight="medium" tone="success">
                      {'YOUR REVIEW WILL BE OUR MOTIVATION!'}
                    </Text>
                  ) : null}
                </>
              ) : null}
            </BlockStack>
          </BannerWithDismiss>
          <PartnerApps
            apps={[
              {
                name: 'Yanet Volume Discount',
                appStoreUrl:
                  'https://apps.shopify.com/yanet-volume-discount?utm_source=Yanet-Barcode',
                hasFreePlan: true,
                hasPaidPlan: false,
                imageUrl:
                  'https://cdn.shopify.com/app-store/listing_images/3fba472eb02f1f8d36882dd13b12782b/icon/CN3Do4WzwoADEAE=.jpeg',
                reviewAvg: 4.7,
                reviewCount: 9,
              },
              {
                name: 'Yanet: Retail Barcode Labels',
                appStoreUrl: 'https://apps.shopify.com/yanet-barcode-labels',
                hasFreePlan: true,
                hasPaidPlan: true,
                imageUrl:
                  'https://cdn.shopify.com/app-store/listing_images/ab16c393b1e5044fbdf7ed14523974b9/icon/CMqJoa27zfkCEAE=.png',
                reviewAvg: 4.8,
                reviewCount: 141,
              },
              {
                name: 'Yanet: Returns and Exchanges',
                appStoreUrl:
                  'https://apps.shopify.com/yanet-return-and-exchange',
                hasFreePlan: true,
                hasPaidPlan: false,
                imageUrl:
                  'https://cdn.shopify.com/app-store/listing_images/b7526c91f1f73e87ba06bef5236e4b7e/icon/CLmw6I_eo_4CEAE=.png',
                reviewAvg: 5.0,
                reviewCount: 24,
              },
            ]}
          />
          <Helpdesk
            methods={[
              {
                title: t('Get in touch via Email'),
                helpText: t(
                  'Drop us an email and we will get back to you at the earliest.',
                ),
                icon: SendIcon,
                url: 'mailto:support@yanet.io',
                variant: 'plain',
              },
              {
                title: t('Start Live Chat'),
                helpText: t('Get assisted instantly via our Live Chat system.'),
                icon: ChatIcon,
                onClick: () => {
                  (window as any)?.MyChat?.toggleMyChat();
                  console.log((window as any)?.MyChat?.toggleMyChat());
                },
                variant: 'plain',
              },
              {
                title: t('Help center'),
                helpText: t(
                  'Explore our help desk documents and find the solution by yourself.',
                ),
                icon: ExternalIcon,
                url: 'https://help.yanet.io/',
                variant: 'plain',
                target: '_blank',
              },
            ]}
          />
        </BlockStack>
      </Layout>
      <Modal
        open={openNoteModal}
        onClose={() => {
          // set rating cho user
          setStar(0);
          setHover(0);
          setOpenNoteModal(false);
        }}
        title={t('You rate %1-stars out of 5').replace('%1', star.toString())}
        primaryAction={{
          content: t('Send'),
          onAction: handleSubmit(onSubmit),
          disabled: submitting,
        }}
      ></Modal>
    </Page>
  );
}
