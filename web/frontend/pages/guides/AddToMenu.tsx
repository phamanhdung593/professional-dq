import { useUserApi } from '../../hook/user';
import { IUser } from '../../types/user';
import {
  BannerWithDismiss,
  ESessionStorageKeys,
} from '../components/banner-with-dismiss/BannerWithDismiss';
import imageMenu1 from './../../assets/images/add-block-shopify/Menu-item-1.png';
import imageMenu2 from './../../assets/images/add-block-shopify/Menu-item-2.png';
import imageMenu3 from './../../assets/images/add-block-shopify/Menu-item-3.png';
import banner from './../../assets/images/Banner-return-app.png';

export default function AddToMenu() {
  const { data } = useUserApi();
  const user = data as IUser;
  return (
    <div className="bg-white">
      <a href="#">
        {' '}
        <BannerWithDismiss
          icon={''}
          title=""
          sessionKey={ESessionStorageKeys.SSK_PRODUCT_BANNER}
        >
          <img src={banner} className="w-full h-full object-cover" />
        </BannerWithDismiss>
      </a>
      <div className="">
        <div className="border-b mb-3 pb-3">
          <p className="font-bold text-lg">
            Add FAQ page to your store navigation/menu.
          </p>
        </div>
        <div className="mb-4">
          <b className="text-base">Step 1: </b>
          <span>Visit your store admin panel. </span>{' '}
          <a
            target="_blank"
            href={`https://${user?.shopify_domain}/admin/menus`}
            rel="noreferrer"
            className="text-blue-600 inline-flex items-center"
          >
            Go to store admin
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              strokeWidth="1.5"
              stroke="currentColor"
              className="w-5 h-5 ml-1 text-blue-600 "
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M17.25 6.75L6.75 17.25M6.75 6.75h10.5v10.5"
              />
            </svg>
          </a>
        </div>
        <div className="mb-4">
          <p>
            <b className="text-base">Step 2: </b>
            <span>
              Click on <b>"Online store"</b>
              <b>"Navigation"</b> from left side menu and <b></b>click on{' '}
              <b>"Main menu"</b> or <b>"Footer menu"</b> where you would like to
              add FAQs page
            </span>
          </p>
          <img src={`${imageMenu1}`} className="w-100 my-3" />
        </div>
        <div className="mb-4">
          <b className="text-base">Step 3: </b> Click <b>"Add menu item"</b>.
          <img src={`${imageMenu2}`} className="w-100 mb-3 mt-2" alt="" />
        </div>
        <div className="mb-4">
          <p>
            <b className="text-base">Step 4:</b> Enter a menu name you would
            like along with your FAQ page URL which you got from app and click{' '}
            <b>"Add" </b>
            <span className="text-center">
              (FAQ page Url:{' '}
              <a
                target={'_blank'}
                href={`https://${user?.shopify_domain}/apps/faqs`}
                className="text-blue-600"
                rel="noreferrer"
              >
                https://{user?.shopify_domain}/apps/faqs
                <i className="fa-solid fa-arrow-up-right-from-square"></i>
              </a>{' '}
              or <b>/apps/faqs</b> )
            </span>
          </p>
          <img src={`${imageMenu3}`} className="w-100 mb-3 mt-2" alt="" />
        </div>
        <p>
          <b className="text-base">Step 5: </b>Final click <b>"Save"</b> on{' '}
          <b>"Main menu"</b> page and you should be all set.
        </p>
      </div>
    </div>
  );
}
