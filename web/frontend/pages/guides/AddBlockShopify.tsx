import imageStep1 from './../../assets/images/add-block-shopify/Step-1.png';
import imageStep1_1 from './../../assets/images/add-block-shopify/Step-1-1.png';
import imageStep2 from './../../assets/images/add-block-shopify/Step-2.png';
import imageStep3 from './../../assets/images/add-block-shopify/Step-3.png';
import banner from './../../assets/images/Banner-return-app.png';
import { useUserApi } from '../../hook/user';
import { IUser } from '../../types/user';
import {
  BannerWithDismiss,
  ESessionStorageKeys,
} from '../components/banner-with-dismiss/BannerWithDismiss';

export default function AddBlockShopify() {
  const { data } = useUserApi();
  const user = data as IUser;

  return (
    <div className="flex items-center text-sm">
      <div className="bg-white m-auto">
        <a href="#">
          {' '}
          <BannerWithDismiss
            icon={''}
            title=""
            sessionKey={ESessionStorageKeys.SSK_PRODUCT_BANNER}
          >
            <img src={banner} className="w-full h-full object-cover" />
          </BannerWithDismiss>
        </a>
        <div className="border-b pb-3">
          <p className="font-bold text-lg mb-3">Add Product FAQs block</p>
          <span className="fs-6">
            Your published theme supports app blocks on the Product page. This
            makes installation a breeze! Alternatively we provide instructions
            for legacy themes at the bottom. If you need help,{' '}
            <b style={{ color: '#ff8200', cursor: 'pointer' }}>
              <button
                onClick={() => {
                  window.open(
                    `https://go.crisp.chat/chat/embed/?website_id=35cbcb5a-831c-47fb-9064-0bced009fca9&user_nickname=[FAQ]${user?.shopify_domain}`,
                    '_blank',
                  );
                }}
              >
                Contact with us
              </button>
            </b>{' '}
            or email <b>support@yanet.io</b> for assistance.
          </span>
        </div>
        <div className="py-3 border-b">
          <p className="text-md">
            <b>Step 1: </b>
          </p>
          <span>
            From the shopify sidebar navigate to{' '}
            <b>
              {' '}
              Sales Channels {'>'} Online Store {'>'} Themes
            </b>
          </span>
          <img src={`${imageStep1}`} className="w-full my-3" alt="" />
          <span>
            Find the theme you wish to add our block to and click Customize.
          </span>
          <img src={`${imageStep1_1}`} className="w-full my-3" alt="" />
        </div>
        <div className="py-3 border-b">
          <p className="text-md">
            <b>Step 2: </b>
          </p>
          <span>
            From the top bar select the template you wish to add our block to.
            Most commonly this will be the default product template. We support
            product, collection, home, cart and page templates.{' '}
          </span>
          .{' '}
          <a
            href={`https://${user?.shopify_domain}/admin/themes/current/editor?template=product&activateAppId=3c16d53e-037f-4a3a-abf1-e054532428ff/app-block`}
            target="_blank"
            rel="noreferrer"
            className="text-blue-600 inline-flex items-center"
          >
            Go to theme editor
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              strokeWidth="1.5"
              stroke="currentColor"
              className="w-5 h-5 ml-1 text-blue-600 "
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M17.25 6.75L6.75 17.25M6.75 6.75h10.5v10.5"
              />
            </svg>
          </a>
          <img src={`${imageStep2}`} className="w-full my-3" alt="" />
        </div>
        <div className="py-3">
          <p className="text-md">
            <b>Step 3: </b>
          </p>
          <span>
            Find the section you wish to add our block to and click{' '}
            <b>Add block</b>. In the apps section of the add block popup you
            will find the <b>Yanet Professional FAQs</b> block. Click to add it
            and <b>Save</b>.
          </span>
          <img src={`${imageStep3}`} className="w-full mt-3" alt="" />
        </div>
      </div>
    </div>
  );
}
