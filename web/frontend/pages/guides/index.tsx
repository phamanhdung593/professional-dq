import { Page, Card, Tabs } from '@shopify/polaris';
import { t } from 'i18next';
import { useState, useCallback } from 'react';
import { useNavigate } from 'react-router-dom';
import AddBlockMorePage from './AddBlockMorePage';
import AddBlockShopify from './AddBlockShopify';
import AddToMenu from './AddToMenu';

export function Guides() {
  const navigate = useNavigate();
  const [selected, setSelected] = useState(0);

  const handleTabChange = useCallback((selectedTabIndex: number) => {
    setSelected(selectedTabIndex);
  }, []);

  console.log('~');

  const tabs = [
    {
      id: 'add-faq-page-to-menu',
      content: 'Add Faq page to Menu',
      accessibilityLabel: 'Add Faq page to Menu',
    },
    {
      id: 'add-faq-to-product-page',
      content: 'Add Product FAQs block',
    },
    {
      id: 'add-faq-to-other-pages',
      content: 'Add FAQs on other pages',
    },
  ];

  return (
    <Page
      title={t('Guides')}
      backAction={{ content: '', onAction: () => navigate(-1) }}
    >
      <Card>
        <Tabs
          tabs={tabs}
          selected={selected}
          onSelect={(index) => {
            handleTabChange(index);
          }}
        ></Tabs>
        <br />
        {selected === 0 && <AddToMenu></AddToMenu>}
        {selected === 1 && <AddBlockShopify></AddBlockShopify>}
        {selected === 2 && <AddBlockMorePage></AddBlockMorePage>}
      </Card>
    </Page>
  );
}
