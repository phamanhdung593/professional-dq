import {
  BlockStack,
  Card,
  Checkbox,
  Layout,
  TextField,
  Text,
  Select,
  Badge,
} from '@shopify/polaris';
import TinyEditor from '../../components/Editor';
import { notEmpty } from '@shopify/react-form';
import { FormMapping, useField, useForm } from '@shopify/react-form';
import { useCallback, useEffect, useState } from 'react';
import { SaveBar, useAppBridge } from '@shopify/app-bridge-react';
import { useGetCategoryApi } from '../../hook/category';
import { useCreateFaqApi, useEditFaqApi } from '../../hook/faq';
import { useNavigate } from 'react-router-dom';
import { useUserApi } from '../../hook/user';
import { IUser } from '../../types/user';
import { Language } from '../../types/translation';

export default function FaqForm(props?: any) {
  const { data: categoriesData } = useGetCategoryApi({});
  const navigate = useNavigate();
  const [currentPlan, setCurrentPlan] = useState<any>('free');
  const { data } = useUserApi();
  const user = data as IUser;

  useEffect(() => {
    user?.merchants_plan?.plan
      ? setCurrentPlan(user?.merchants_plan?.plan.toLocaleLowerCase())
      : setCurrentPlan('free');
  }, [user]);

  const { mutate } = useCreateFaqApi();
  const { mutate: editFaqs } = useEditFaqApi();
  const [selected, setSelected] = useState('Uncategoried');

  const [id, setId] = useState<number | undefined>();
  useEffect(() => {
    setId(props[0]?.id);
  }, [props]);

  const [options, setOptions] = useState([
    { label: 'Uncategorized', value: 'Uncategorized1' },
  ]);

  useEffect(() => {
    if (categoriesData) {
      const apiOptions = categoriesData.map((category: any) => ({
        label: category.title,
        value: category.identify,
      }));

      setOptions((prevOptions) => {
        const mergedOptions = [...prevOptions, ...apiOptions];
        const uniqueOptions = mergedOptions.filter(
          (option, index, self) =>
            index === self.findIndex((o) => o.value === option.value),
        );
        return uniqueOptions;
      });
    }
  }, [categoriesData]);

  const shopLocalesOption = user?.shopLocales
    ? JSON.parse(user?.shopLocales).shopLocales?.map((item: any) => ({
        label: Language[item.locale],
        value: item.locale,
      }))
    : [];

  const { fields, submit, reset, dirty } = useForm({
    fields: {
      title: useField({
        value: props[0]?.title || '',
        validates: [notEmpty('Question should not be empty')],
      }),
      content: useField({
        value: props[0]?.content || '',
        validates: [notEmpty('Answer should not be empty')],
      }),
      is_visible: useField({
        value: props[0]?.is_visible || false,
        validates: [],
      }),
      feature_faq: useField({
        value: props[0]?.feature_faq || false,
        validates: [],
      }),
      locale: useField({
        value: props[0]?.locale || 'default',
        validates: [],
      }),
      category_identify: useField({
        value: props[0]?.category_identify || 'Uncategorized1',
        validates: [],
      }),
    },
    onSubmit: async (fieldValues: FormMapping<any, any>) => {
      if (!id) {
        mutate(
          {
            ...fieldValues,
          },
          {
            onSuccess: () => {
              useAppBridge().toast.show('Faq was created!');
              navigate('/faqs');
            },
            onError: () => {
              useAppBridge().toast.show('Faq was error!');
            },
          },
        );
      } else {
        editFaqs(
          { id: id, ...fieldValues },
          {
            onSuccess() {
              useAppBridge().toast.show('Update category success!');
              navigate('/faqs');
            },
            onError: () => {
              useAppBridge().toast.show('Update category error!');
            },
          },
        );
      }

      return { status: 'success', data: fieldValues };
    },

    makeCleanAfterSubmit: true,
  });

  const editorBodyProps = {
    content: fields.content.value,
    setContent: fields.content.onChange,
    height: 500,
  };

  const handleSelectChange = useCallback((value: string) => {
    setSelected(value);
    fields.category_identify.onChange(value);
  }, []);

  return (
    <div>
      <SaveBar open={dirty}>
        <button variant="primary" onClick={submit}></button>
        <button onClick={reset}></button>
      </SaveBar>
      <Layout>
        <Layout.Section>
          <Card>
            <BlockStack gap="400">
              <TextField
                label="Question"
                requiredIndicator
                {...fields.title}
                autoComplete="off"
              />
            </BlockStack>
            <div className="mt-5">
              <BlockStack gap="400">
                <TinyEditor {...editorBodyProps} />
              </BlockStack>
            </div>
          </Card>
        </Layout.Section>
        <Layout.Section variant="oneThird">
          <Card>
            <div className="pt-3">
              <Checkbox
                label="Show FAQs on page"
                checked={fields.is_visible.value}
                onChange={fields.is_visible.onChange}
              />
              <div className="ms-7 opacity-70 text-sm  pointer-events-none">
                <Text variant="bodyMd" as="p">
                  This will enable this FAQ so that your customers can see it on
                  FAQ page.
                </Text>
              </div>
            </div>
            <div className="mt-4">
              <div className="flex items-center gap-4">
                <div>
                  <Checkbox
                    label="Featured"
                    checked={fields.feature_faq.value}
                    onChange={fields.feature_faq.onChange}
                    disabled={
                      !(
                        currentPlan === 'pro' ||
                        currentPlan === 'ultimate' ||
                        currentPlan === 'free_01' ||
                        currentPlan === 'free extra'
                      )
                    }
                  />
                </div>
                {currentPlan === 'free' && (
                  <div>
                    <Badge tone="attention">Pro Plan</Badge>
                  </div>
                )}
              </div>
              <div className="ms-7 opacity-70 text-sm  pointer-events-none">
                <Text variant="bodyMd" as="p">
                  This will show this FAQ so that your customers can see it on
                  Widget.
                </Text>
              </div>
            </div>
            <div className="mt-4">
              <div className="mt-2">
                {id && (
                  <div className="flex items-center space-x-4">
                    <Select
                      label="Add Translation"
                      options={shopLocalesOption}
                      {...fields.locale}
                      disabled={
                        !(
                          currentPlan === 'pro' ||
                          currentPlan === 'ultimate' ||
                          currentPlan === 'free_01' ||
                          currentPlan === 'free extra'
                        )
                      }
                    />
                    {currentPlan === 'free' && (
                      <div className="pt-3">
                        <Badge tone="attention">Pro Plan</Badge>
                      </div>
                    )}
                  </div>
                )}
                <div className="mt-3">
                  <Select
                    label="Selected Categories"
                    options={options}
                    onChange={handleSelectChange}
                    value={selected}
                  />
                </div>
              </div>
            </div>
          </Card>
        </Layout.Section>
      </Layout>
    </div>
  );
}
