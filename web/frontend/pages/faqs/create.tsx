import { Page } from '@shopify/polaris';
import FaqForm from './FaqForm';

export default function CreateFaq() {
  return (
    <Page title="Create FAQ" backAction={{ content: '', url: '/faqs' }}>
      <FaqForm />
    </Page>
  );
}
