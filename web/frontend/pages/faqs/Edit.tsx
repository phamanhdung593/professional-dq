import { Page } from '@shopify/polaris';
import FaqForm from './FaqForm';
import { Params, useParams } from 'react-router-dom';
import { useEffect, useState } from 'react';
import { FaqV2 } from '../../types/faq';
import { useGetFaqsIdApi } from '../../hook/faq';

export default function EditFaq() {
  const param = useParams<Params<string>>();
  const [faqs, setFaqs] = useState<FaqV2>();
  const { data } = useGetFaqsIdApi(param.id || '') as {
    data: FaqV2;
  };

  useEffect(() => {
    if (param) {
      setFaqs(data);
    }
  }, [data]);

  return (
    <Page title="Edit FAQ" backAction={{ content: '', url: '/faqs' }}>
      <FaqForm {...faqs} />
    </Page>
  );
}
