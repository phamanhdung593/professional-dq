import { SkeletonTabs, Divider } from '@shopify/polaris';
import { Fragment } from 'react/jsx-runtime';

export function FaqTableSkeletonTable() {
  return (
    <div className="">
      {[1, 2, 3, 4, 5, 6, 7, 8, 9, 10].map((index) => (
        <Fragment key={index}>
          <div className={`px-4 py-${index === 1 ? '[10px]' : '[14px]'}`}>
            <div className="item" style={{ display: 'flex' }}>
              <SkeletonTabs count={4} fitted />
            </div>
          </div>
          {index < 11 && <Divider />}
        </Fragment>
      ))}
    </div>
  );
}
