import {
  DndContext,
  PointerSensor,
  closestCenter,
  useSensor,
  useSensors,
} from '@dnd-kit/core';
import {
  SortableContext,
  arrayMove,
  useSortable,
  verticalListSortingStrategy,
} from '@dnd-kit/sortable';
import { CSS } from '@dnd-kit/utilities';
import {
  restrictToVerticalAxis,
  restrictToParentElement,
} from '@dnd-kit/modifiers';

import {
  Badge,
  Icon,
  Checkbox,
  Tooltip,
  Modal,
  EmptyState,
  Card,
  Link,
  Button,
} from '@shopify/polaris';

import React, { SyntheticEvent, useCallback, useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { Category } from '../../types/category';
import { useUserApi } from '../../hook/user';
import { IUser } from '../../types/user';
import {
  useDeleteCategoryApi,
  useGetCategoryApi,
  useSortCategoryApi,
  useShowHideCategoryApi,
} from '../../hook/category';
import { useAppBridge } from '@shopify/app-bridge-react';

import { useSettingApi } from '../../hook/setting';
import { DataSettings } from '../../types/translation';
import {
  DeleteIcon,
  ChevronDownIcon,
  ChevronUpIcon,
  PlusIcon,
} from '@shopify/polaris-icons';
import { FaqV2 } from '../../types/faq';
import {
  useDeleteFaqApi,
  useSortFaqsApi,
  useShowHideFaqApi,
  useListFaqApi,
} from '../../hook/faq';

export function FaqsList() {
  const app = useAppBridge();
  const [currentPlan, setCurrentPlan] = useState<any>('free');
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [isModalOpenFaq, setIsModalOpenFaq] = useState(false);
  const [categoryToDelete, setCategoryToDelete] = useState<Category | null>(
    null,
  );
  const [dataCategory, setDataCategory] = useState<any[]>([]);
  const [openDropdown, setOpenDropdown] = useState<number | null>(null);
  const [faqToDelete, setFaqToDelete] = useState<FaqV2 | null>(null);
  const [dataFaqs, setDataFaqs] = useState<any[]>([]);

  const navigate = useNavigate();
  const { mutate: deleteFaqID } = useDeleteFaqApi();
  const { mutateAsync } = useShowHideCategoryApi();
  const { mutate: UpdateCheckboxFaq } = useShowHideFaqApi();
  const { mutate: deleteCategoryID } = useDeleteCategoryApi();
  const { data: settingsData } = useSettingApi() as { data: DataSettings };
  const { data } = useUserApi();
  const user = data as IUser;

  const { mutate: SortPosition } = useSortCategoryApi();

  const { mutate: SortPositionFaqs } = useSortFaqsApi();

  const { data: listFaqs } = useListFaqApi({});

  useEffect(() => {
    const faqs: any = [];
    if (Array.isArray(listFaqs)) {
      listFaqs?.map((faq: FaqV2) => {
        faqs.push(faq);
      });
    }

    setDataFaqs(faqs);
  }, [listFaqs]);

  const handleClickDropdown = (categoryId: number) => {
    setOpenDropdown((prev) => (prev === categoryId ? null : categoryId));
  };

  const handleDeleteClickCategory = (category: Category) => {
    setCategoryToDelete(category);
    setIsModalOpen(true);
  };

  const handleModalClose = () => {
    setIsModalOpen(false);
  };

  const handleDeleteConfirm = () => {
    if (categoryToDelete) {
      deleteCategory(categoryToDelete);
    }
    setIsModalOpen(false);
  };

  const handleModalCancel = () => {
    setIsModalOpen(false);
  };

  const deleteFaq = useCallback(
    (faq: FaqV2) => {
      deleteFaqID(faq.id, {
        onSuccess() {
          app.toast.show('Delete success!');
        },
        onError() {
          app.toast.show('Delete error!');
        },
      });
    },
    [deleteFaqID, app],
  );

  const handleDeleteClickFaq = (faq: FaqV2) => {
    setFaqToDelete(faq);
    setIsModalOpenFaq(true);
  };

  const handleModalCloseFaq = () => {
    setIsModalOpenFaq(false);
  };

  const handleDeleteConfirmFaq = () => {
    if (faqToDelete) {
      deleteFaq(faqToDelete);
    }
    setIsModalOpenFaq(false);
  };

  const handleModalCancelFaq = () => {
    setIsModalOpenFaq(false);
  };

  useEffect(() => {
    user?.merchants_plan?.plan
      ? setCurrentPlan(user?.merchants_plan?.plan.toLocaleLowerCase())
      : setCurrentPlan('free');
  }, [user]);

  const { data: dataCategories, refetch } = useGetCategoryApi({});

  useEffect(() => {
    const categories: Category[] = [];
    if (dataCategories) {
      dataCategories?.map((category: Category) => {
        categories?.push(category);
      });
    }
    setDataCategory(categories);
  }, [dataCategories]);

  const handleChangeVisible = (id: number) => async (newChecked: boolean) => {
    const body = {
      id: id,
      is_visible: newChecked,
    };
    await mutateAsync(body, {
      onSuccess: () => {
        app.toast.show('Category was updated!');
      },
    });
  };

  const handleChangeVisibleFaq =
    (id: number) => async (newChecked: boolean) => {
      const body = {
        id: id,
        is_visible: newChecked,
      };
      UpdateCheckboxFaq(body, {
        onSuccess: () => {
          app.toast.show('Faq was updated!');
        },
      });
    };

  const handleChangeFeatured = (id: number) => async (newChecked: boolean) => {
    const body = {
      id: id,
      feature_category: newChecked,
    };

    mutateAsync(body, {
      onSuccess: () => {
        app.toast.show('Category was updated!');
      },
    });
  };

  const handleChangeFeaturedFaq =
    (id: number) => async (newChecked: boolean) => {
      const body = {
        id: id,
        feature_faq: newChecked,
      };

      UpdateCheckboxFaq(body, {
        onSuccess: () => {
          app.toast.show('Faq was updated!');
        },
      });
    };

  const deleteCategory = useCallback(
    (category: Category) => {
      deleteCategoryID(category.id, {
        onSuccess() {
          app.toast.show('Delete success!');
          refetch();
        },
        onError() {
          app.toast.show('Delete error!');
        },
      });
    },
    [deleteCategoryID, app],
  );

  const sensors = useSensors(
    useSensor(PointerSensor, {
      activationConstraint: { delay: 250, distance: 10 },
    }),
  );

  const handleDragEndFaq = (event: any) => {
    const { active, over } = event;

    if (active.id !== over.id) {
      setDataCategory((prev) => {
        const updatedCategories = prev.map((category) => {
          const faqs = category.faq;
          const oldIndex = faqs.findIndex((faq: any) => faq.id === active.id);
          const newIndex = faqs.findIndex((faq: any) => faq.id === over.id);

          if (oldIndex !== -1 && newIndex !== -1) {
            const body = {
              category_id: category.id,
              old_index: oldIndex,
              new_index: newIndex,
              faq_id: active.id,
            };

            SortPositionFaqs(body, {
              onError() {
                app.toast.show('Position error!');
              },
            });

            const newFaqOrder = arrayMove(faqs, oldIndex, newIndex);

            return {
              ...category,
              faq: newFaqOrder,
            };
          }

          return category;
        });

        return updatedCategories;
      });
    }
  };

  const handleDragEndCategory = (event: any) => {
    const { active, over } = event;
    if (active.id !== over.id) {
      setDataCategory((prev) => {
        const oldIndex = prev.findIndex((item) => item.id === active.id);
        const newIndex = prev.findIndex((item) => item.id === over.id);
        const body = {
          category_id: active.id,
          old_index: oldIndex,
          new_index: newIndex,
        };

        SortPosition(body, {
          onError() {
            app.toast.show('Position error!');
          },
        });
        return arrayMove(prev, oldIndex, newIndex);
      });
    }
  };

  const SortableRowFaq: React.FC<{ item: any }> = ({ item }) => {
    const { attributes, listeners, setNodeRef, transform, transition } =
      useSortable({ id: item?.id });
    const style = {
      transform: CSS.Transform.toString(transform),
      transition,
    };
    const [isFaq, setIsFaq] = useState(false);

    return (
      <tr
        ref={setNodeRef}
        style={style}
        {...attributes}
        {...listeners}
        className="hover:bg-[#f7f7f7] cursor-pointer cursor-grab"
        onMouseEnter={() => setIsFaq(true)}
        onMouseLeave={() => setIsFaq(false)}
      >
        <td className="pl-3 py-4 w-[35%]">
          <div className="flex pl-8">
            <div className="pt-1">
              <svg viewBox="0 0 20 20" width="12">
                <path d="M7 2a2 2 0 1 0 .001 4.001A2 2 0 0 0 7 2zm0 6a2 2 0 1 0 .001 4.001A2 2 0 0 0 7 8zm0 6a2 2 0 1 0 .001 4.001A2 2 0 0 0 7 14zm6-8a2 2 0 1 0-.001-4.001A2 2 0 0 0 13 6zm0 2a2 2 0 1 0 .001 4.001A2 2 0 0 0 13 8zm0 6a2 2 0 1 0 .001 4.001A2 2 0 0 0 13 14z"></path>
              </svg>
            </div>
            <div className="pl-8 relative group">
              <Link url={`/faq/${item?.id}`}>
                {item?.title && item.title.length > 33
                  ? `${item.title.substring(0, 30)}...`
                  : item.title}
              </Link>

              {item?.title && item.title.length > 33 && (
                <div className="p-3 absolute text-center top-0 left-0 mb-1 w-max max-w-xs p-2 bg-white text-black text-sm border border-gray-300 rounded-full shadow-lg opacity-0 group-hover:opacity-100 transition-opacity pointer-events-none z-10 whitespace-normal break-words">
                  {item.title}
                </div>
              )}
            </div>
          </div>
        </td>
        <td className="py-4 text-center w-[15%]">
          <Checkbox
            label=""
            checked={item?.is_visible}
            onChange={handleChangeVisibleFaq(item.id)}
          />
        </td>
        <td className="py-4 text-center w-[15%]">
          <div className="flex items-center justify-center gap-2 w-full">
            <div className="flex items-center justify-center">
              <Checkbox
                label=""
                checked={item?.feature_faq}
                onChange={handleChangeFeaturedFaq(item?.id)}
                disabled={
                  !(
                    currentPlan === 'pro' ||
                    currentPlan === 'ultimate' ||
                    currentPlan === 'free_01' ||
                    currentPlan === 'free extra'
                  )
                }
              />
            </div>
            {currentPlan === 'free' && (
              <div className="flex items-center">
                <Badge tone="attention">Pro Plan</Badge>
              </div>
            )}
          </div>
        </td>
        <td className="py-4 text-center w-[25%]">
          <div>
            {item.language &&
              [...new Set(item.language)]?.map((language: any, index: any) => (
                <Badge tone={'success'} key={index}>
                  {language}
                </Badge>
              ))}
          </div>
        </td>
        <td className="border-gray-100 py-4 text-center w-[10%] pr-5">
          {isFaq && (
            <Tooltip content="Delete">
              <div
                className="cursor-pointer"
                onClick={(event: SyntheticEvent) => {
                  event.stopPropagation();
                  handleDeleteClickFaq(item);
                }}
              >
                <Icon source={DeleteIcon} />
              </div>
            </Tooltip>
          )}
        </td>
      </tr>
    );
  };

  const SortableRowCategory: React.FC<{ item: any }> = ({ item }) => {
    const { attributes, listeners, setNodeRef, transform, transition } =
      useSortable({ id: item.id });

    const style = {
      transform: CSS.Transform.toString(transform),
      transition,
    };

    const [isCategory, setIsCategory] = useState(false);

    return (
      <>
        <tr
          ref={setNodeRef}
          style={style}
          {...attributes}
          {...listeners}
          className="hover:bg-[#f7f7f7] cursor-pointer cursor-grab"
          onMouseOver={() => setIsCategory(true)}
          onMouseOut={() => setIsCategory(false)}
        >
          <td className="border-t border-b border-gray-100 py-4 w-[35%]">
            <div className="flex items-center gap-2 pl-5">
              <div>
                <svg viewBox="0 0 20 20" width="12">
                  <path d="M7 2a2 2 0 1 0 .001 4.001A2 2 0 0 0 7 2zm0 6a2 2 0 1 0 .001 4.001A2 2 0 0 0 7 8zm0 6a2 2 0 1 0 .001 4.001A2 2 0 0 0 7 14zm6-8a2 2 0 1 0-.001-4.001A2 2 0 0 0 13 6zm0 2a2 2 0 1 0 .001 4.001A2 2 0 0 0 13 8zm0 6a2 2 0 1 0 .001 4.001A2 2 0 0 0 13 14z"></path>
                </svg>
              </div>
              <div
                className="flex items-center text-[8px] cursor-pointer pl-2"
                onClick={() => handleClickDropdown(item.id)}
              >
                <Icon
                  source={
                    openDropdown === item.id ? ChevronUpIcon : ChevronDownIcon
                  }
                />
              </div>
              <div
                onClick={(event: SyntheticEvent) => {
                  event.stopPropagation();
                  navigate(`/categories/${item.id}`);
                }}
                className="cursor-pointer text-blue-700 ml-3 relative group"
              >
                <div className="underline">
                  {item.title.length > 33
                    ? `${item.title.substring(0, 30)}...`
                    : item.title}
                </div>

                {item.title.length > 33 && (
                  <div className="p-3 absolute text-center bottom-full left-0 mb-1 w-max max-w-xs p-2 bg-white text-black text-sm border border-gray-100 rounded-full shadow-lg opacity-0 group-hover:opacity-100 transition-opacity pointer-events-none z-10 whitespace-normal break-words">
                    {item.title}
                  </div>
                )}
              </div>
            </div>
          </td>
          <td className="border-t border-b border-gray-100 py-4 text-center w-[15%]">
            <Checkbox
              label=""
              checked={item.is_visible}
              onChange={handleChangeVisible(item.id)}
            />
          </td>
          <td className="border-t border-b border-gray-100 py-4 text-center w-[15%]">
            <div className="flex items-center justify-center gap-2 w-full">
              <div className="flex items-center justify-center">
                <Checkbox
                  label=""
                  checked={item.feature_category}
                  onChange={handleChangeFeatured(item.id)}
                  disabled={
                    !(
                      currentPlan === 'pro' ||
                      currentPlan === 'ultimate' ||
                      currentPlan === 'free_01' ||
                      currentPlan === 'free extra'
                    )
                  }
                />
              </div>
              {currentPlan === 'free' && (
                <div className="flex items-center">
                  <Badge tone="attention">Pro Plan</Badge>
                </div>
              )}
            </div>
          </td>
          <td className="border-t border-b border-gray-100 py-4 text-center w-[25%]">
            {item.language &&
              [...new Set(item.language)]?.map((language: any, index: any) => (
                <Badge tone={'success'} key={index}>
                  {language}
                </Badge>
              ))}
          </td>
          <td className="border-t border-b border-gray-100 py-4 text-center w-[10%] pr-8">
            {isCategory && (
              <Tooltip content="Delete">
                <div
                  onClick={(event: SyntheticEvent) => {
                    event.stopPropagation();
                    handleDeleteClickCategory(item);
                  }}
                >
                  <Icon source={DeleteIcon} />
                </div>
              </Tooltip>
            )}
          </td>
        </tr>

        {openDropdown === item?.id && (
          <DndContext
            collisionDetection={closestCenter}
            onDragEnd={handleDragEndFaq}
            sensors={sensors}
            modifiers={[restrictToVerticalAxis, restrictToParentElement]}
          >
            <SortableContext
              items={item?.faq?.map((faq: any) => faq?.id)}
              strategy={verticalListSortingStrategy}
            >
              <Modal
                open={isModalOpenFaq}
                onClose={handleModalCloseFaq}
                title="Confirm Deletion"
                primaryAction={{
                  content: 'Delete',
                  onAction: handleDeleteConfirmFaq,
                }}
                secondaryActions={[
                  {
                    content: 'Cancel',
                    onAction: handleModalCancelFaq,
                  },
                ]}
              >
                <Modal.Section>
                  <p className="text-sm text-gray-700">
                    Are you sure you want to delete this Faq?
                  </p>
                </Modal.Section>
              </Modal>
              <tr>
                <td colSpan={5} className="p-0">
                  <div className="border-gray-100 max-w-full border bg-white overflow-x-auto">
                    <table className="table-auto w-full border-collapse">
                      <tbody>
                        {dataFaqs?.map((faq: any) => (
                          <>
                            {item?.identify === faq?.category_identify && (
                              <SortableRowFaq key={faq?.id} item={faq} />
                            )}
                          </>
                        ))}
                      </tbody>
                    </table>
                  </div>
                </td>
              </tr>
            </SortableContext>
            <div className="py-3 pl-12">
              <Button icon={PlusIcon} onClick={() => navigate('/faq/create')}>
                Add FAQ
              </Button>
            </div>
          </DndContext>
        )}
      </>
    );
  };

  return (
    <>
      <DndContext
        collisionDetection={closestCenter}
        onDragEnd={handleDragEndCategory}
        sensors={sensors}
        modifiers={[restrictToVerticalAxis, restrictToParentElement]}
      >
        {dataCategory?.length > 0 ? (
          <SortableContext
            items={dataCategory?.map((item) => item.id)}
            strategy={verticalListSortingStrategy}
          >
            <div className="p-4">
              <Modal
                open={isModalOpen}
                onClose={handleModalClose}
                title="Confirm Deletion"
                primaryAction={{
                  content: 'Delete',
                  onAction: handleDeleteConfirm,
                }}
                secondaryActions={[
                  {
                    content: 'Cancel',
                    onAction: handleModalCancel,
                  },
                ]}
              >
                <Modal.Section>
                  <p className="text-sm text-gray-700">
                    Are you sure you want to delete this Category?
                  </p>
                </Modal.Section>
              </Modal>

              <div className="border border-gray-300 rounded-2xl shadow-md bg-white overflow-x-auto">
                <table className="w-full table-auto border-collapse bg-white">
                  <thead className="bg-[#f7f7f7]">
                    <tr>
                      <th className="border-t border-b text-left border-gray-200 pl-8 py-4 text-sm font-[650] text-[#616161] w-[35%]">
                        Category
                      </th>
                      <th className="border-t border-b border-gray-200 py-4 text-sm font-[650] text-[#616161] w-[15%]">
                        Visibility
                      </th>
                      <th className="border-t border-b border-gray-200 py-4 text-sm font-[650] text-[#616161] w-[15%]">
                        Featured
                      </th>
                      <th className="border-t border-b border-gray-200 py-4 text-sm font-[650] text-[#616161] w-[25%]">
                        Languages
                      </th>
                      <th className="border-t border-b border-gray-200 py-4 text-sm font-[650] text-[#616161] w-[10%]"></th>
                    </tr>
                  </thead>
                  <tbody>
                    {dataCategory?.map((item) => (
                      <SortableRowCategory key={item.id} item={item} />
                    ))}
                  </tbody>
                </table>
              </div>
            </div>
          </SortableContext>
        ) : (
          <div>
            <Card>
              <EmptyState
                heading="Create a category to get started"
                action={{ content: 'Add Category', url: '/categories/create' }}
                image="https://cdn.shopify.com/s/files/1/0262/4071/2726/files/emptystate-files.png"
              ></EmptyState>
            </Card>
          </div>
        )}
      </DndContext>

      <div className="text-center mt-5">
        View FAQs Page:{' '}
        <a
          href={`https://${user?.shopify_domain}${
            settingsData?.faq_page_url || '/apps/faqs'
          }`}
          target="_blank"
          rel="noreferrer"
          className="text-blue-600 underline"
        >
          https://{user?.shopify_domain}
          {settingsData?.faq_page_url || '/apps/faqs'}
        </a>
      </div>
    </>
  );
}
