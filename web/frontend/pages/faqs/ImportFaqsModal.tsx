import { DropZone, Modal, Thumbnail, Text } from '@shopify/polaris';
import { useCallback, useState } from 'react';
import { NoteIcon } from '@shopify/polaris-icons';
import { useImportCsv } from '../../hook/faq';
import { useAppBridge } from '@shopify/app-bridge-react';

export default function ImportFaqsModal(props: ImportFaqsModalProps) {
  const { isOpen, onRequestClose } = props;
  const [validMessages, setValidMessages] = useState<string>('');
  const [file, setFile] = useState<File>();
  const validImageTypes = ['text/csv'];
  const [loading, setLoading] = useState(false);
  const { mutateAsync } = useImportCsv();

  const handleDropZoneDrop = useCallback(
    (_dropFiles: File[], acceptedFiles: File[]) => {
      if (validImageTypes.includes(acceptedFiles[0].type)) {
        setValidMessages('');
        setFile(acceptedFiles[0]);
      } else {
        setValidMessages('File type is invalid !');
      }
    },
    [],
  );

  const uploadedFile = file && (
    <div className="flex items-center h-[100%] justify-center">
      <Thumbnail
        size="small"
        alt={file.name}
        source={
          validImageTypes.includes(file.type)
            ? NoteIcon
            : window.URL.createObjectURL(file)
        }
      />
      <div className="ml-3">
        {file.name}
        <Text variant="bodySm" as="p">
          {file.size} bytes
        </Text>
      </div>
    </div>
  );
  const uploadMessage = !uploadedFile && <DropZone.FileUpload />;

  const handleImport = () => {
    if (file && !validMessages) {
      setLoading(true);
      const formData = new FormData();
      formData.append('file', file);

      mutateAsync(formData, {
        onSuccess: () => {
          useAppBridge().toast.show('Import success!');
          onRequestClose();
        },
        onError: () => {
          useAppBridge().toast.show('Import fail!');
        },
      });
    }

    setLoading(false);
  };

  return (
    <Modal
      open={isOpen}
      onClose={onRequestClose}
      title="Import FAQs"
      primaryAction={{
        content: 'Import',
        onAction: handleImport,
        loading: loading,
      }}
      secondaryActions={[
        {
          content: 'Close',
          onAction: onRequestClose,
        },
      ]}
    >
      <Modal.Section>
        <div>
          You can download and use this{' '}
          <a
            onClick={() => onRequestClose()}
            target="_blank"
            href="/api/faq/down-csv-file/sample"
            className="text-primary"
          >
            <b style={{ color: '#ff8200' }} className="cursor-pointer">
              sample file{' '}
            </b>
          </a>
          to prepare for your import If you have any questions, please{' '}
          <b style={{ color: '#ff8200' }} className="cursor-pointer">
            chat with us{' '}
          </b>
          to get quick and direct support.
        </div>
        <br />
        <DropZone allowMultiple={false} dropOnPage onDrop={handleDropZoneDrop}>
          <div className={uploadedFile ? 'mt-[35px]' : ''}>{uploadedFile}</div>
          {uploadMessage}
        </DropZone>
        <div className="text-red-500 mt-2 text-center">{validMessages}</div>
        <br />
        <b>
          Note: that make sure the FAQ of CSV file is not duplicated with these
          ones on store FAQ.
        </b>
      </Modal.Section>
    </Modal>
  );
}

type ImportFaqsModalProps = {
  isOpen: boolean;
  onRequestClose: () => void;
  onSelection?: (products: any[]) => void;
  actionLabel?: string;
  form?: any;
};
