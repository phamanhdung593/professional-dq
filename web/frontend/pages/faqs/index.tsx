import { Page } from '@shopify/polaris';
import { useState } from 'react';
import ImportFaqsModal from './ImportFaqsModal';
import { useNavigate } from 'react-router-dom';
import { ExportIcon, ImportIcon, NoteIcon } from '@shopify/polaris-icons';
import { FaqsList } from './FaqsList';
import { saveAs } from 'file-saver';
import { useExportCsv } from '../../hook/faq';

export default function Faqs() {
  const navigate = useNavigate();
  const [isOpen, setIsOpen] = useState(false);
  const { mutateAsync } = useExportCsv();

  const onRequestClose = () => {
    setIsOpen(false);
  };

  const props = { isOpen, onRequestClose };

  const exportFaqs = async () => {
    const blob = await mutateAsync({});
    saveAs(blob, 'faqs.csv');
  };

  return (
    <Page
      title={'FAQs'}
      primaryAction={{
        content: 'Add FAQs',
        onAction: () => {
          navigate('/faq/create');
        },
      }}
      secondaryActions={[
        {
          content: 'Import FAQs',
          icon: ImportIcon,
          onAction: () => setIsOpen(true),
        },
        {
          content: 'Export FAQs',
          icon: ExportIcon,
          onAction: () => exportFaqs(),
        },
        {
          content: 'Add FAQs on other pages',
          icon: NoteIcon,
          onAction: () => {
            navigate('/faq-more-page');
          },
        },
      ]}
    >
      <FaqsList />
      <ImportFaqsModal {...props}></ImportFaqsModal>
    </Page>
  );
}
