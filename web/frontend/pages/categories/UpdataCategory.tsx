import { Page } from '@shopify/polaris';
import { Params, useNavigate, useParams } from 'react-router-dom';
import { useEffect, useState } from 'react';
// import { useAxios } from '../../contexts/axios';
import CreateOrEditCategory from './CreateOrEditCategory';
import { Category } from '../../types/category';

import { useGetCategoryIdApi } from '../../hook/category';

export default function EditCategory() {
  const navigate = useNavigate();
  const param = useParams<Params<string>>();
  const [category, setCategory] = useState<Category>();
  const { data } = useGetCategoryIdApi(param.id || '') as {
    data: Category;
  };

  useEffect(() => {
    if (param) {
      setCategory(data);
    }
  }, [data]);

  return (
    <Page
      title="Edit category"
      backAction={{ content: '', url: '/categories' }}
      primaryAction={{
        content: 'New category',
        onAction: () => {
          navigate('/categories/create');
        },
      }}
    >
      <CreateOrEditCategory {...category}></CreateOrEditCategory>
    </Page>
  );
}
