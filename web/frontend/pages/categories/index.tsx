import { Page } from '@shopify/polaris';
import { t } from 'i18next';
import { CategoriesList } from './CategoriesList';
import { useNavigate } from 'react-router-dom';

export const Categories = () => {
  const navigate = useNavigate();
  return (
    <Page
      title={t('Categories')}
      primaryAction={{
        content: t('Add Category'),
        onAction: () => navigate('/categories/create'),
      }}
    >
      <CategoriesList></CategoriesList>
    </Page>
  );
};
