import {
  DndContext,
  PointerSensor,
  closestCenter,
  useSensor,
  useSensors,
} from '@dnd-kit/core';
import {
  SortableContext,
  arrayMove,
  useSortable,
  verticalListSortingStrategy,
} from '@dnd-kit/sortable';
import { CSS } from '@dnd-kit/utilities';
import {
  restrictToVerticalAxis,
  restrictToParentElement,
} from '@dnd-kit/modifiers';

import {
  Badge,
  Icon,
  Checkbox,
  Tooltip,
  Text,
  Modal,
  EmptyState,
  Card,
} from '@shopify/polaris';

import React, { SyntheticEvent, useCallback, useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { Category } from '../../types/category';
import { DeleteIcon } from '@shopify/polaris-icons';
import { useUserApi } from '../../hook/user';
import { IUser } from '../../types/user';
import {
  useDeleteCategoryApi,
  useGetCategoryApi,
  useSortCategoryApi,
  useShowHideCategoryApi,
} from '../../hook/category';
import { useAppBridge } from '@shopify/app-bridge-react';

import { useSettingApi } from '../../hook/setting';
import { DataSettings } from '../../types/translation';

export function CategoriesList() {
  const app = useAppBridge();
  const { data } = useUserApi();
  const user = data as IUser;
  const [currentPlan, setCurrentPlan] = useState<any>('free');
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [categoryToDelete, setCategoryToDelete] = useState<Category | null>(
    null,
  );
  const [dataCategory, setDataCategory] = useState<any[]>([]);

  const { mutate: SortPosition } = useSortCategoryApi();

  const handleDeleteClick = (category: Category) => {
    setCategoryToDelete(category);
    setIsModalOpen(true);
  };

  const handleModalClose = () => {
    setIsModalOpen(false);
  };

  const handleDeleteConfirm = () => {
    if (categoryToDelete) {
      deleteCategory(categoryToDelete);
    }
    setIsModalOpen(false);
  };

  const handleModalCancel = () => {
    setIsModalOpen(false);
  };

  useEffect(() => {
    user?.merchants_plan?.plan
      ? setCurrentPlan(user?.merchants_plan?.plan.toLocaleLowerCase())
      : setCurrentPlan('free');
  }, [user]);

  const { data: dataCategories, refetch } = useGetCategoryApi({});

  useEffect(() => {
    const categories: Category[] = [];
    if (dataCategories) {
      dataCategories?.map((category: Category) => {
        categories.push(category);
      });
    }
    setDataCategory(categories);
  }, [dataCategories]);

  const handleDragEnd = (event: any) => {
    const { active, over } = event;
    if (active.id !== over.id) {
      setDataCategory((prev) => {
        const oldIndex = prev.findIndex((item) => item.id === active.id);
        const newIndex = prev.findIndex((item) => item.id === over.id);
        const body = {
          category_id: active.id,
          old_index: oldIndex,
          new_index: newIndex,
        };
        SortPosition(body, {
          onError() {
            app.toast.show('Position error!');
          },
        });
        return arrayMove(prev, oldIndex, newIndex);
      });
    }
  };

  const { mutate: deleteCategoryID } = useDeleteCategoryApi();

  const navigate = useNavigate();

  const { mutateAsync } = useShowHideCategoryApi();
  const { data: settingsData } = useSettingApi() as { data: DataSettings };

  const handleChangeVisible = (id: number) => async (newChecked: boolean) => {
    const body = {
      id: id,
      is_visible: newChecked,
    };
    await mutateAsync(body, {
      onSuccess: () => {
        app.toast.show('Category was updated!');
      },
    });
  };

  const handleChangeFeatured = (id: number) => async (newChecked: boolean) => {
    const body = {
      id: id,
      feature_category: newChecked,
    };

    mutateAsync(body, {
      onSuccess: () => {
        app.toast.show('Category was updated!');
      },
    });
  };

  const deleteCategory = useCallback(
    (category: Category) => {
      deleteCategoryID(category.id, {
        onSuccess() {
          app.toast.show('Delete success!');
          refetch();
        },
        onError() {
          app.toast.show('Delete error!');
        },
      });
    },
    [deleteCategoryID, app],
  );

  const SortableRow: React.FC<{ item: any }> = ({ item }) => {
    const { attributes, listeners, setNodeRef, transform, transition } =
      useSortable({ id: item.id });
    const style = {
      transform: CSS.Transform.toString(transform),
      transition,
    };

    const [isHovered, setIsHovered] = useState(false);

    return (
      <tr
        ref={setNodeRef}
        style={style}
        {...attributes}
        {...listeners}
        className="hover:bg-[#f7f7f7] cursor-pointer cursor-grab"
        onMouseEnter={() => setIsHovered(true)}
        onMouseLeave={() => setIsHovered(false)}
      >
        <td className="border-t border-b border-gray-100 py-4 w-[35%]">
          <div className="flex items-center gap-2">
            <div className="flex items-center gap-2 pl-5">
              <div className="pt-2">
                <svg viewBox="0 0 20 20" width="12">
                  <path d="M7 2a2 2 0 1 0 .001 4.001A2 2 0 0 0 7 2zm0 6a2 2 0 1 0 .001 4.001A2 2 0 0 0 7 8zm0 6a2 2 0 1 0 .001 4.001A2 2 0 0 0 7 14zm6-8a2 2 0 1 0-.001-4.001A2 2 0 0 0 13 6zm0 2a2 2 0 1 0 .001 4.001A2 2 0 0 0 13 8zm0 6a2 2 0 1 0 .001 4.001A2 2 0 0 0 13 14z"></path>
                </svg>
              </div>
              <div
                onClick={(event: SyntheticEvent) => {
                  event.stopPropagation();
                  navigate(`/categories/${item.id}`);
                }}
                className="cursor-pointer text-blue-700 ml-3 relative group"
              >
                <div className="underline">
                  {item.title.length > 33
                    ? `${item.title.substring(0, 30)}...`
                    : item.title}
                </div>

                {item.title.length > 33 && (
                  <div className="p-3 absolute text-center bottom-full left-0 mb-1 w-max max-w-xs p-2 bg-white text-black text-sm border border-gray-300 rounded-full shadow-lg opacity-0 group-hover:opacity-100 transition-opacity pointer-events-none z-10 whitespace-normal break-words">
                    {item.title}
                  </div>
                )}
              </div>
            </div>
          </div>
          <div className="pl-12 flex items-center text-[8px]">
            (<div>{item.faq.length}</div>
            <div>Faqs</div>)
          </div>
        </td>
        <td className="border-t border-b border-gray-100 py-4 pl-5 w-[10%]">
          <Text variant="bodyMd" tone="inherit" as="p">
            {item.description}
          </Text>
        </td>
        <td className="border-t border-b border-gray-100 py-4 text-center w-[10%]">
          <Checkbox
            label=""
            checked={item.is_visible}
            onChange={handleChangeVisible(item.id)}
          />
        </td>
        <td className="border-t border-b border-gray-100 py-4 text-center w-[15%]">
          <div className="flex items-center justify-center gap-2 w-full">
            <div className="flex items-center justify-center">
              <Checkbox
                label=""
                checked={item.feature_category}
                onChange={handleChangeFeatured(item.id)}
                disabled={
                  !(
                    currentPlan === 'pro' ||
                    currentPlan === 'ultimate' ||
                    currentPlan === 'free_01' ||
                    currentPlan === 'free extra'
                  )
                }
              />
            </div>
            {currentPlan === 'free' && (
              <div className="flex items-center">
                <Badge tone="attention">Pro Plan</Badge>
              </div>
            )}
          </div>
        </td>
        <td className="border-t border-b border-gray-100 py-4 text-center w-[20%]">
          {item.language &&
            [...new Set(item.language)]?.map((language: any, index: any) => (
              <Badge tone={'success'} key={index}>
                {language}
              </Badge>
            ))}
        </td>
        <td className="border-t border-b border-gray-100 py-4 text-center w-[10%] pr-5">
          {isHovered && (
            <Tooltip content="Delete">
              <div
                className="cursor-pointer border-red-400"
                onClick={(event: SyntheticEvent) => {
                  event.stopPropagation();
                  handleDeleteClick(item);
                }}
              >
                <Icon source={DeleteIcon} />
              </div>
            </Tooltip>
          )}
        </td>
      </tr>
    );
  };

  const sensors = useSensors(
    useSensor(PointerSensor, {
      activationConstraint: { delay: 250, distance: 5 },
    }),
  );

  return (
    <>
      <DndContext
        collisionDetection={closestCenter}
        onDragEnd={handleDragEnd}
        sensors={sensors}
        modifiers={[restrictToVerticalAxis, restrictToParentElement]}
      >
        {dataCategory?.length > 0 ? (
          <SortableContext
            items={dataCategory?.map((item) => item.id)}
            strategy={verticalListSortingStrategy}
          >
            <div className="p-4">
              <Modal
                open={isModalOpen}
                onClose={handleModalClose}
                title="Confirm Deletion"
                primaryAction={{
                  content: 'Delete',
                  onAction: handleDeleteConfirm,
                }}
                secondaryActions={[
                  {
                    content: 'Cancel',
                    onAction: handleModalCancel,
                  },
                ]}
              >
                <Modal.Section>
                  <p className="text-sm text-gray-700">
                    Are you sure you want to delete this Categories?
                  </p>
                </Modal.Section>
              </Modal>

              <div className="border border-gray-300 rounded-2xl shadow-md bg-white overflow-x-auto">
                <table className="w-full table-auto border-collapse bg-white">
                  <thead className="bg-[#f7f7f7]">
                    <tr>
                      <th className="pl-8 border-t border-b text-left border-gray-200 px-4 py-4 text-sm font-[650] text-[#616161] w-[35%]">
                        Category
                      </th>
                      <th className="border-t border-b text-left border-gray-200 px-4 py-4 text-sm font-[650] text-[#616161] w-[10%]">
                        Description
                      </th>
                      <th className="border-t border-b border-gray-200 px-4 py-4 text-sm font-[650] text-[#616161] w-[10%]">
                        Visibility
                      </th>
                      <th className="border-t border-b border-gray-200 px-4 py-4 text-sm font-[650] text-[#616161] w-[15%]">
                        Featured
                      </th>
                      <th className="border-t border-b border-gray-200 px-4 py-4 text-sm font-[650] text-[#616161] w-[20%]">
                        Languages
                      </th>
                      <th className="border-t border-b border-gray-200 px-4 py-4 text-sm font-[650] text-[#616161] w-[10%]"></th>
                    </tr>
                  </thead>
                  <tbody>
                    {dataCategory?.map((item) => (
                      <SortableRow key={item.id} item={item} />
                    ))}
                  </tbody>
                </table>
              </div>
            </div>
          </SortableContext>
        ) : (
          <div>
            <Card>
              <EmptyState
                heading="Create a category to get started"
                action={{ content: 'Add Category', url: '/categories/create' }}
                image="https://cdn.shopify.com/s/files/1/0262/4071/2726/files/emptystate-files.png"
              ></EmptyState>
            </Card>
          </div>
        )}
      </DndContext>

      <div className="text-center mt-5">
        View FAQs Page:{' '}
        <a
          href={`https://${user?.shopify_domain}${
            settingsData?.faq_page_url || '/apps/faqs'
          }`}
          target="_blank"
          rel="noreferrer"
          className="text-blue-600 underline"
        >
          https://{user?.shopify_domain}
          {settingsData?.faq_page_url || '/apps/faqs'}
        </a>
      </div>
    </>
  );
}
