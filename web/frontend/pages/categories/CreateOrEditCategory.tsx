import {
  BlockStack,
  Card,
  Checkbox,
  Layout,
  TextField,
  Text,
  Select,
  Badge,
} from '@shopify/polaris';
import { FormMapping, notEmpty, useField, useForm } from '@shopify/react-form';
import { Category } from '../../types/category';
import {
  useCategoryApi,
  useGetCategoryApi,
  useUpdateCategoryApi,
} from '../../hook/category';
import { SaveBar, useAppBridge } from '@shopify/app-bridge-react';
import { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { useUserApi } from '../../hook/user';
import { IUser } from '../../types/user';
import { Language } from '../../types/translation';

export default function CreateOrEditCategory(props?: any) {
  const navigate = useNavigate();
  const app = useAppBridge();
  const [id, setId] = useState<number | undefined>();

  const [currentPlan, setCurrentPlan] = useState<any>('free');
  const { data } = useUserApi();
  const user = data as IUser;

  useEffect(() => {
    user?.merchants_plan?.plan
      ? setCurrentPlan(user?.merchants_plan?.plan.toLocaleLowerCase())
      : setCurrentPlan('free');
  }, [user]);

  const { data: dataCategoriesData } = useGetCategoryApi({});

  const categoriesData: Category[] = [];

  if (dataCategoriesData) {
    dataCategoriesData.map((category: Category) => {
      categoriesData.push(category);
    });
  }

  const { mutate: createCategory } = useCategoryApi();
  const { mutate: updateCategory } = useUpdateCategoryApi();

  const shopLocalesOption = user?.shopLocales
    ? JSON.parse(user?.shopLocales).shopLocales?.map((item: any) => ({
        label: Language[item.locale],
        value: item.locale,
      }))
    : [];

  // const localeDefault = user?.shopLocales
  //   ? JSON.parse(user?.shopLocales).shopLocales?.find(
  //       (item: any) => item.primary === true,
  //     )?.locale || 'en'
  //   : '';

  const { fields, submit, reset, dirty } = useForm({
    fields: {
      title: useField({
        value: props[0]?.title || '',
        validates: [notEmpty('Category name should not be empty')],
      }),
      description: useField({
        value: props[0]?.description || '',
        validates: [notEmpty('Category description should not be empty')],
      }),
      is_visible: useField({
        value: props[0]?.is_visible || false,
        validates: [],
      }),
      feature_category: useField({
        value: props[0]?.feature_category || false,
        validates: [],
      }),
      locale: useField({
        value: props[0]?.locale || 'default',
        validates: [],
      }),
    },

    onSubmit: async (fieldValues: FormMapping<any, any>) => {
      if (!id) {
        createCategory(
          {
            ...fieldValues,
          },
          {
            onSuccess() {
              app.toast.show('Create category success!');
              navigate('/categories');
            },
            onError: () => {
              app.toast.show('Create category error!');
            },
          },
        );
        return { status: 'success', data: fieldValues };
      }

      if (id) {
        updateCategory(
          {
            id: id,
            ...fieldValues,
          },
          {
            onSuccess() {
              app.toast.show('Update category success!');
              navigate('/categories');
            },
            onError: () => {
              app.toast.show('Update category error!');
            },
          },
        );
      }

      return { status: 'success', data: fieldValues };
    },
    makeCleanAfterSubmit: true,
  });

  useEffect(() => {
    fields.title.onChange(props[0]?.title || '');
    fields.description.onChange(props[0]?.description || '');
    fields.is_visible.onChange(props[0]?.is_visible || false);
    fields.feature_category.onChange(props[0]?.feature_category || false);
    fields.locale.onChange(props[0]?.locale || 'default');
    setId(props[0]?.id);
  }, [props]);

  return (
    <>
      <SaveBar open={dirty}>
        <button variant="primary" onClick={submit}></button>
        <button onClick={reset}></button>
      </SaveBar>
      <BlockStack gap={'400'}>
        <Layout>
          <Layout.Section>
            <Card>
              <BlockStack gap={'400'}>
                <TextField
                  label="Category"
                  requiredIndicator
                  {...fields.title}
                  autoComplete="off"
                />
                <TextField
                  label="Description"
                  {...fields.description}
                  autoComplete="off"
                />
              </BlockStack>
            </Card>
          </Layout.Section>
          <Layout.Section variant="oneThird">
            <Card>
              <BlockStack gap="400">
                <div className="pt-3">
                  <Checkbox
                    label="Enable category"
                    checked={fields.is_visible.value}
                    onChange={fields.is_visible.onChange}
                  />
                  <div className="ms-7 opacity-70 text-sm  pointer-events-none">
                    <Text variant="bodyMd" as="p">
                      This will enable this Category so that your customers can
                      see it on FAQ page.
                    </Text>
                  </div>
                </div>
                <div>
                  <div className="flex items-center space-x-4">
                    <Checkbox
                      label="Featured"
                      checked={fields.feature_category.value}
                      onChange={fields.feature_category.onChange}
                      disabled={
                        !(
                          currentPlan === 'pro' ||
                          currentPlan === 'ultimate' ||
                          currentPlan === 'free_01' ||
                          currentPlan === 'free extra'
                        )
                      }
                    />
                    {currentPlan === 'free' && (
                      <div className="flex items-center">
                        <Badge tone="attention">Pro Plan</Badge>
                      </div>
                    )}
                  </div>

                  <div className="ms-7 opacity-70 text-sm  pointer-events-none">
                    <Text variant="bodyMd" as="p">
                      This will show this Category so that your customers can
                      see it on Widget.
                    </Text>
                  </div>
                </div>
                {id && (
                  <div>
                    <div className="flex items-center space-x-4">
                      <Select
                        label="Add Translation"
                        options={shopLocalesOption}
                        {...fields.locale}
                        disabled={
                          !(
                            currentPlan === 'pro' ||
                            currentPlan === 'ultimate' ||
                            currentPlan === 'free_01' ||
                            currentPlan === 'free extra'
                          )
                        }
                      />
                      {currentPlan === 'free' && (
                        <div className="pt-3">
                          <Badge tone="attention">Pro Plan</Badge>
                        </div>
                      )}
                    </div>
                    <div className="pt-3">
                      Available translations are pulled from your store
                      settings.
                      <a
                        target="_blank"
                        href={`#`}
                        rel="noreferrer"
                        className="text-blue-600 inline-flex items-center underline "
                      >
                        You can manage language settings here
                      </a>
                    </div>
                  </div>
                )}
              </BlockStack>
            </Card>
          </Layout.Section>
        </Layout>
      </BlockStack>
    </>
  );
}
