import {
  BlockStack,
  Card,
  Grid,
  Modal,
  Select,
  TextField,
} from '@shopify/polaris';
import { useCallback, useEffect, useMemo, useState } from 'react';
import translationData from './../../assets/data/translation.json';
import { useUpdateWidgetApi } from '../../hook/widget';
import { useAppBridge } from '@shopify/app-bridge-react';

type TranslationProps = {
  activeTranslation: boolean;
  setActiveTranslation: (value: boolean) => void;
  widgetData: any;
};

export default function Translation(props: TranslationProps) {
  const { mutate: updateWidget } = useUpdateWidgetApi();
  const primaryLanguagedb =
    props?.widgetData?.data?.primary_language || 'English';

  const [language, setLanguage] = useState(primaryLanguagedb);
  const translation = useMemo(() => {
    return translationData.filter((item) => item.country === language)[0];
  }, [language]);

  const translationdb = props?.widgetData?.data?.translation
    ? JSON.parse(props?.widgetData?.data?.translation)
    : [];

  const countries = translationdb?.filter(
    (item: any) => item?.country === language,
  );

  const [contactUs, setContactUs] = useState<any>();
  const [faqs, setFaqs] = useState<any>();
  const [categories, setCategories] = useState<any>();
  const [search, setSearch] = useState<any>();
  const [YourName, setYourName] = useState<any>();
  const [messages, setMessages] = useState<any>();
  const [send, setSend] = useState<any>();
  const [contact, setContact] = useState<any>();

  const closeModal = useCallback(
    () => props.setActiveTranslation(!props.activeTranslation),
    [props],
  );

  const handleSelectLanguage = useCallback(
    (value: string) => setLanguage(value),
    [],
  );

  useEffect(() => {
    setContactUs(
      countries[0]?.contact_us
        ? countries[0]?.contact_us
        : translation?.contact_us,
    );
    setFaqs(countries[0]?.faq ? countries[0]?.faq : translation?.faq);
    setCategories(
      countries[0]?.categories
        ? countries[0]?.categories
        : translation?.categories,
    );
    setSearch(
      countries[0]?.search ? countries[0]?.search : translation?.search,
    );
    setYourName(
      countries[0]?.your_name
        ? countries[0]?.your_name
        : translation?.your_name,
    );
    setMessages(
      countries[0]?.messages ? countries[0]?.messages : translation?.messages,
    );
    setSend(countries[0]?.send ? countries[0]?.send : translation?.send);
    setContact(
      countries[0]?.contact ? countries[0]?.contact : translation?.contact,
    );
  }, [
    translation?.contact,
    translation?.contact_us,
    translation?.categories,
    translation?.faq,
    translation?.messages,
    translation?.search,
    translation?.send,
    translation?.your_name,
    props,
  ]);

  const options = [
    { label: 'English', value: 'English' },
    { label: 'VietNamese', value: 'VietNamese' },
    { label: 'French', value: 'French' },
    { label: 'Thailand', value: 'Thailand' },
    { label: 'Portuguese', value: 'Portuguese' },
    { label: 'Germany', value: 'Germany' },
    { label: 'Japanese', value: 'Japanese' },
    { label: 'Spanish', value: 'Spanish' },
    { label: 'Laos', value: 'Laos' },
    { label: 'Russian', value: 'Russian' },
    { label: 'Other', value: 'Other' },
  ];

  const handleSaveTranslate = () => {
    const updatedTranslation = translationdb?.map((item: any) => {
      if (item?.country === language) {
        return {
          ...item,
          contact_us: contactUs,
          faq: faqs,
          categories: categories,
          search: search,
          your_name: YourName,
          messages: messages,
          send: send,
          contact: contact,
        };
      }
      return item;
    });

    const updatedTranslationString = JSON.stringify(updatedTranslation);

    const data = {
      primary_language: language,
      translation: updatedTranslationString,
    };

    updateWidget(data, {
      onSuccess: () => {
        useAppBridge().toast.show('Update translation success!');
      },
      onError: () => {
        useAppBridge().toast.show('Update translation error!');
      },
    });
  };

  return (
    <div>
      <Modal
        size="large"
        open={props.activeTranslation}
        onClose={closeModal}
        title="Translation"
        primaryAction={{
          content: 'Save',
          onAction: handleSaveTranslate,
        }}
      >
        <Modal.Section>
          <BlockStack gap={'400'}>
            <Card>
              <Select
                label="Choose the primary language"
                options={options}
                onChange={handleSelectLanguage}
                value={language}
              />
            </Card>
            <Card>
              <Grid>
                <Grid.Cell columnSpan={{ xs: 6, sm: 3, md: 3, lg: 6, xl: 6 }}>
                  <BlockStack gap={'400'}>
                    <TextField
                      label="Contact Us"
                      value={contactUs}
                      onChange={(value) => setContactUs(value)}
                      autoComplete="off"
                    />
                    <TextField
                      label="FAQs"
                      value={faqs}
                      onChange={(value) => setFaqs(value)}
                      autoComplete="off"
                    />
                    <TextField
                      label="Categories"
                      value={categories}
                      onChange={(value) => setCategories(value)}
                      autoComplete="off"
                    />
                    <TextField
                      label="Search"
                      value={search}
                      onChange={(value) => setSearch(value)}
                      autoComplete="off"
                    />
                  </BlockStack>
                </Grid.Cell>
                <Grid.Cell columnSpan={{ xs: 6, sm: 3, md: 3, lg: 6, xl: 6 }}>
                  <BlockStack gap={'400'}>
                    <TextField
                      label="Your Name"
                      value={YourName}
                      onChange={(value) => setYourName(value)}
                      autoComplete="off"
                    />
                    <TextField
                      label="Messages"
                      value={messages}
                      onChange={(value) => setMessages(value)}
                      autoComplete="off"
                    />
                    <TextField
                      label="Send"
                      value={send}
                      onChange={(value) => setSend(value)}
                      autoComplete="off"
                    />
                    <TextField
                      label="Contact"
                      value={contact}
                      onChange={(value) => setContact(value)}
                      autoComplete="off"
                    />
                  </BlockStack>
                </Grid.Cell>
              </Grid>
            </Card>
          </BlockStack>
        </Modal.Section>
      </Modal>
    </div>
  );
}
