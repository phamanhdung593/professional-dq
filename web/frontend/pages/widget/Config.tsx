import { BlockStack } from '@shopify/polaris';
import { WidgetProps } from '../../types/widget';
import ConfigWidgetButton from '../components/widget/ConfigWidgetButton';
import ConfigWidgetContact from '../components/widget/ConfigWidgetContact';
import ConfigWidgetTheme from '../components/widget/ConfigWidgetTheme';

const Config = (props: WidgetProps) => {
  return (
    <BlockStack gap={'400'}>
      <ConfigWidgetTheme {...props} />
      <ConfigWidgetContact {...props} />
      <ConfigWidgetButton {...props} />
    </BlockStack>
  );
};

export default Config;
