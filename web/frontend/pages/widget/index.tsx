import { t } from 'i18next';
import { Badge, Layout, List, Page, Spinner } from '@shopify/polaris';
import {
  LanguageIcon,
  AppsIcon,
  DisabledIcon,
  StatusActiveIcon,
  ConfettiIcon,
} from '@shopify/polaris-icons';
import { useEffect, useState } from 'react';
import { ESessionStorageKeys } from '../components/banner-with-dismiss/BannerWithDismiss';
import { BannerWithDismiss } from '../components/banner-with-dismiss/BannerWithDismiss';
import Preview from '../components/widget/Preview';
import Config from './Config';
import Translation from './Translation';
import translation from '../../assets/data/translation.json';
import { useGetWidgetApi, useUpdateWidgetApi } from '../../hook/widget';
import { useAppBridge } from '@shopify/app-bridge-react';
import BannerMessage from '../components/banner-message';
import { useNavigate } from 'react-router-dom';
import { useUserContext } from '../../components/providers/UserProvider';

export const Widget = () => {
  const { state: data } = useUserContext();
  const { data: widgetData, isLoading } = useGetWidgetApi();
  const { mutate: updateWidget } = useUpdateWidgetApi();
  const navigate = useNavigate();

  const [currentPlan, setCurrentPlan] = useState<any>('');

  useEffect(() => {
    if (data?.merchants_plan?.plan) {
      const newPlan = data.merchants_plan.plan.toLocaleLowerCase();
      setCurrentPlan(newPlan);
    }
  }, [data]);

  const [activeWidget, setActiveWidget] = useState<boolean>(true);
  const [iconNumber, setIconNumber] = useState<string>('1');
  const [isActiveAnimation, setIsActiveAnimation] = useState<boolean>(false);
  const [backgroundColor, setBackgroundColor] = useState<string>('#947451');
  const [buttonBackgroundColor, setButtonBackgroundColor] =
    useState<string>('#947451');
  const [headerColor, setHeaderColor] = useState<string>('#ffffff');
  const [textColor, setTextColor] = useState<string>('#5c6282');
  const [alignment, setAlignment] = useState<string>('right');
  const [fontFamily, setFontFamily] = useState<string>('right');
  const [isActiveContactUs, setIsActiveContactUs] = useState<boolean>(true);
  const [isActivePhone, setIsActivePhone] = useState<boolean>(true);
  const [isActiveMessage, setIsActiveMessage] = useState<boolean>(true);
  const [isActiveEmail, setIsActiveEmail] = useState<boolean>(true);
  const [isActiveWhatsApp, setIsActiveWhatsApp] = useState<boolean>(true);
  const [isActiveFaqs, setIsActiveFaqs] = useState<boolean>(true);
  const [isActiveCategories, setIsActiveCategories] = useState<boolean>(true);
  const [activeTranslation, setActiveTranslation] = useState(false);
  const [welcomeTitle, setWelcomeTitle] = useState('Hi✌️');
  const [descriptionTitle, setDescriptionTitle] = useState('May I help you?');
  const [phoneNumber, setPhoneNumber] = useState('');
  const [messageLink, setMessageLink] = useState('');
  const [emailLink, setEmailLink] = useState('');
  const [whatsAppLink, setWhatsAppLink] = useState('');
  const [loadingBtn, setLoadingBtn] = useState(false);

  useEffect(() => {
    if (widgetData?.data) {
      setActiveWidget(widgetData.data.faq_messages_visible);
      setIconNumber(widgetData.data.icon_number);
      setIsActiveAnimation(widgetData.data.animation_visible);
      setBackgroundColor(widgetData.data.theme_color);
      setButtonBackgroundColor(widgetData.data.button_background_color);
      setHeaderColor(widgetData.data.text_color);
      setTextColor(widgetData.data.send_messages_text_color);
      setAlignment(widgetData.data.aligment_faq_messages);
      setFontFamily(widgetData.data.font_family);
      setIsActiveContactUs(widgetData.data.contact_us_visible);
      setIsActivePhone(widgetData.data.phone_visible);
      setIsActiveMessage(widgetData.data.message_visible);
      setIsActiveEmail(widgetData.data.email_visible);
      setIsActiveWhatsApp(widgetData.data.whatsApp_visible);
      setIsActiveFaqs(widgetData.data.feature_questions_visible);
      setIsActiveCategories(widgetData.data.feature_categories_visible);
      setWelcomeTitle(widgetData.data.welcome_title);
      setDescriptionTitle(widgetData.data.description_title);
      setPhoneNumber(widgetData.data.phone_number);
      setMessageLink(widgetData.data.message_link);
      setEmailLink(widgetData.data.email_link);
      setWhatsAppLink(widgetData.data.whatsApp_number);
    }
  }, [widgetData]);

  useEffect(() => {
    if (currentPlan && currentPlan === 'free' && widgetData?.data) {
      const data = {
        help_desk_visible: false,
        faq_messages_visible: false,
      };

      updateWidget(data, {
        onSuccess: () => {
          setLoadingBtn(false);
        },
        onError: () => {
          setLoadingBtn(false);
        },
      });
    }
  }, [currentPlan, widgetData]);

  const save = () => {
    setLoadingBtn(true);
    const data = {
      help_desk_visible: activeWidget,
      sort_by: 'date',
      contact_us_visible: isActiveContactUs,
      phone_visible: isActivePhone,
      email_visible: isActiveEmail,
      whatsApp_visible: isActiveWhatsApp,
      animation_visible: isActiveAnimation,
      message_visible: isActiveMessage,
      faq_messages_visible: activeWidget,
      email_link: emailLink,
      phone_number: phoneNumber,
      whatsApp_number: whatsAppLink,
      message_link: messageLink,
      welcome_title: welcomeTitle,
      description_title: descriptionTitle,
      theme_color: backgroundColor,
      text_color: headerColor,
      icon_number: iconNumber,
      button_background_color: buttonBackgroundColor,
      aligment_faq_messages: alignment,
      button_title: 'Send',
      font_family: fontFamily,
      send_messages_text_color: textColor,
      feature_questions_visible: isActiveFaqs,
      feature_categories_visible: isActiveCategories,
      translation: widgetData?.data?.translation
        ? widgetData?.data?.translation
        : JSON.stringify(translation),
      show_default_locale: true,
      primary_language: widgetData?.data?.primary_language
        ? widgetData?.data?.primary_language
        : 'English',
    };

    updateWidget(data, {
      onSuccess: () => {
        setLoadingBtn(false);
        useAppBridge().toast.show('Update widget success!');
      },
      onError: () => {
        setLoadingBtn(false);
      },
    });
  };

  const enableThemeExtension = () => {
    window.open(
      `https://${data?.shopify_domain}/admin/themes/current/editor?context=apps&template=product&activateAppId=${import.meta.env.VITE_EMBEDDED_ID}/app-embed`,
    );
  };

  const widgetProps = {
    activeWidget,
    iconNumber,
    setIconNumber,
    setIsActiveAnimation,
    isActiveAnimation,
    setBackgroundColor,
    backgroundColor,
    buttonBackgroundColor,
    setButtonBackgroundColor,
    alignment,
    setAlignment,
    isActiveContactUs,
    isActivePhone,
    isActiveMessage,
    isActiveEmail,
    isActiveWhatsApp,
    isActiveFaqs,
    isActiveCategories,
    setIsActiveContactUs,
    setIsActivePhone,
    setIsActiveMessage,
    setIsActiveEmail,
    setIsActiveWhatsApp,
    setIsActiveFaqs,
    setIsActiveCategories,
    headerColor,
    setHeaderColor,
    textColor,
    setTextColor,
    fontFamily,
    setFontFamily,
    welcomeTitle,
    setWelcomeTitle,
    descriptionTitle,
    setDescriptionTitle,
    phoneNumber,
    setPhoneNumber,
    messageLink,
    setMessageLink,
    emailLink,
    setEmailLink,
    whatsAppLink,
    setWhatsAppLink,
  };

  return (
    <>
      {isLoading ? (
        <div className="h-[350px] flex items-center justify-center">
          <Spinner accessibilityLabel="Spinner example" size="large" />;
        </div>
      ) : (
        <Page
          title={t('Widget')}
          titleMetadata={
            activeWidget ? (
              <Badge tone="success">Active</Badge>
            ) : (
              <Badge tone="critical">Inactive</Badge>
            )
          }
          primaryAction={{
            content: 'Save',
            onAction: () => save(),
            loading: loadingBtn,
          }}
          secondaryActions={[
            {
              content: activeWidget ? 'Turn off' : 'Turn on',
              disabled: !(
                currentPlan === 'pro' ||
                currentPlan === 'ultimate' ||
                currentPlan === 'free_01' ||
                currentPlan === 'free extra'
              ),
              onAction: () => {
                setActiveWidget(!activeWidget);
              },
              icon: activeWidget ? DisabledIcon : StatusActiveIcon,
            },
            {
              content: 'Translation',
              onAction: () => setActiveTranslation(true),
              icon: LanguageIcon,
            },
            {
              content: 'Enable embed app',
              onAction: () => {
                enableThemeExtension();
              },
              icon: AppsIcon,
            },
          ]}
        >
          {currentPlan == 'free' && (
            <div className="pb-4">
              <BannerMessage
                title={'Access FAQs on different pages of store'}
                message={
                  <div>
                    <p>Add the Widget for your store</p>
                    <p>
                      Upgrade plan to add the widget to your online store and
                      get full access to other pro features. Cancel anytime.
                    </p>
                  </div>
                }
                type="warning"
                actions={[
                  {
                    content: 'Upgrade Now!',
                    onAction: () => navigate('/plans'),
                    variant: 'primary',
                  },
                ]}
              />
            </div>
          )}
          <BannerWithDismiss
            icon={ConfettiIcon}
            title="Widget Overview"
            sessionKey={ESessionStorageKeys.SSK_WIDGET_BANNER}
          >
            <b>
              This feature is developed to bring a better experience for users
              when visiting your store online.
            </b>
            <div className="mt-2 ms-7">
              <List>
                <List.Item>
                  Reach brilliant FAQs easily through a dynamic popup.
                </List.Item>
                <List.Item>
                  Easy to customize the layout to look nice and match your
                  brand.
                </List.Item>
                <List.Item>
                  Quick to contact and respond by integrating 3rd parties:
                  WhatsApp, messenger, contact form, and phone call.
                </List.Item>
              </List>
            </div>
          </BannerWithDismiss>
          <div className="widget-layout">
            <Layout>
              <Layout.Section>
                <Config widgetProps={widgetProps} />
              </Layout.Section>
              <Layout.Section variant="oneThird">
                <Preview widgetProps={widgetProps} />
              </Layout.Section>
            </Layout>
          </div>
          <Translation
            activeTranslation={activeTranslation}
            setActiveTranslation={setActiveTranslation}
            widgetData={widgetData}
          ></Translation>
        </Page>
      )}
    </>
  );
};
