import {
  Modal,
  IndexFilters,
  IndexTable,
  useSetIndexFiltersMode,
  useIndexResourceState,
  EmptySearchResult,
} from '@shopify/polaris';
import { ProductFaqTableSkeleton } from './ProductFaqTableSkeleton';
import { useState, useCallback, useEffect } from 'react';
import { useListFaqApi } from '../../hook/faq';
import { FaqV2 } from '../../types/faq';
import { timeDifferenceFromNow } from '../../utils/date';
import { useAppBridge } from '@shopify/app-bridge-react';

export function FaqModal(props: any) {
  const [selected, setSelected] = useState(0);
  const [sortSelected, setSortSelected] = useState(['order asc']);
  const { mode, setMode } = useSetIndexFiltersMode();
  const [queryValue, setQueryValue] = useState('');
  const [faqProducts, setFaqProducts] = useState<any[]>([]);
  const [active, setActive] = useState(props.active || false);

  const { data: listFaqs, isLoading, refetch } = useListFaqApi({});

  let faqs: any[] = [];

  if (listFaqs && listFaqs.length > 0) {
    faqs = listFaqs.map((faq: FaqV2) => {
      return faq;
    });
  }

  const app = useAppBridge();
  useEffect(() => {
    app.loading(true);
    const timer = setTimeout(() => {
      app.loading(false);
    }, 1000);

    return () => clearTimeout(timer);
  }, [app, listFaqs]);

  const handleFiltersQueryChange = useCallback(
    (value: string) => setQueryValue(value),
    [],
  );

  const handleQueryValueRemove = useCallback(() => setQueryValue(''), []);

  const handleFiltersClearAll = useCallback(() => {
    handleQueryValueRemove();
  }, [handleQueryValueRemove]);

  useEffect(() => {
    setActive(props.active);

    if (props.active) {
      refetch();
    }
  }, [props.active]);

  const toggleModal = () => {
    setActive(!active);
    props.getFaqsForProduct(faqProducts);
    if (props.onClose) {
      props.onClose();
    }
  };

  const { selectedResources, allResourcesSelected, handleSelectionChange } =
    useIndexResourceState(faqs);

  const emptyStateMarkup = (
    <EmptySearchResult
      title={'No Faq yet'}
      description={'Please choose faqs'}
      withIllustration
    />
  );

  const rowMarkup = faqs.map((item, index) => {
    return (
      <IndexTable.Row
        id={item.id}
        key={item.id}
        selected={selectedResources.includes(item.id)}
        position={index}
      >
        <IndexTable.Cell>{item.title}</IndexTable.Cell>
        <IndexTable.Cell>
          {timeDifferenceFromNow(item.createdAt)}
        </IndexTable.Cell>
      </IndexTable.Row>
    );
  });

  useEffect(() => {
    const selectedFaqs = faqs.filter((faq) =>
      selectedResources.includes(faq.id),
    );

    setFaqProducts(selectedFaqs);
  }, [selectedResources]);

  return (
    <Modal
      open={active}
      onClose={toggleModal}
      title="Choose Faqs"
      primaryAction={{
        content: 'Confirm',
        onAction: () => {
          toggleModal();
        },
      }}
      secondaryActions={[
        {
          content: 'Cancel',
          onAction: toggleModal,
        },
      ]}
    >
      <Modal.Section>
        <div>
          <IndexFilters
            sortSelected={sortSelected}
            queryValue={queryValue}
            queryPlaceholder="Searching commissions by email"
            onQueryChange={handleFiltersQueryChange}
            onQueryClear={() => setQueryValue('')}
            autoFocusSearchField={true}
            onSort={setSortSelected}
            tabs={[]}
            selected={selected}
            onSelect={setSelected}
            filters={[]}
            onClearAll={handleFiltersClearAll}
            mode={mode}
            setMode={setMode}
          />
          {isLoading ? (
            <ProductFaqTableSkeleton />
          ) : (
            <IndexTable
              itemCount={faqs.length}
              onSelectionChange={handleSelectionChange}
              selectedItemsCount={
                allResourcesSelected ? 'All' : selectedResources.length
              }
              emptyState={emptyStateMarkup}
              headings={[{ title: 'Title' }, { title: 'Created At' }]}
            >
              {rowMarkup}
            </IndexTable>
          )}
        </div>
      </Modal.Section>
    </Modal>
  );
}
