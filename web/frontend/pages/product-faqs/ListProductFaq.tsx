import {
  BlockStack,
  Card,
  EmptySearchResult,
  IndexTable,
  InlineStack,
  Layout,
  Thumbnail,
  Text,
  useBreakpoints,
  useIndexResourceState,
  Modal,
} from '@shopify/polaris';
import { useDeleteProductApi, useListProductFaqApi } from '../../hook/product';
import {
  BannerWithDismiss,
  ESessionStorageKeys,
} from '../components/banner-with-dismiss/BannerWithDismiss';
import { DeleteIcon } from '@shopify/polaris-icons';
import { timeDifferenceFromNow } from '../../utils/date';
import { ProductFaqTableSkeleton } from './ProductFaqTableSkeleton';
import { useAppBridge } from '@shopify/app-bridge-react';
import { useState } from 'react';

export default function ListProductFaq() {
  const { data: listProductFaq, isLoading } = useListProductFaqApi();
  const { mutate: deleteProductID } = useDeleteProductApi();
  const Toast = useAppBridge();
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [deletingItems, setDeletingItems] = useState<string[]>([]);

  const emptyStateMarkup = (
    <EmptySearchResult
      title={'No Product Faq yet'}
      description={'Please choose product to add faqs'}
      withIllustration
    />
  );

  let products: any[] = [];

  if (listProductFaq) {
    products = listProductFaq.data;
  }

  const {
    selectedResources,
    allResourcesSelected,
    handleSelectionChange,
    clearSelection,
  } = useIndexResourceState(products);

  const rowMarkup = products?.map((item: any, index: number) => (
    <IndexTable.Row
      id={item.id}
      key={item.id}
      selected={selectedResources.includes(item.id)}
      position={index}
    >
      <IndexTable.Cell>
        <Thumbnail
          source={item.product_image}
          size="small"
          alt="Black choker necklace"
        />
      </IndexTable.Cell>
      <IndexTable.Cell>
        <div style={{ padding: '12px 16px', width: '100%' }}>
          <BlockStack gap="100">
            <InlineStack align="space-between">
              <Text as="span" variant="bodyMd" fontWeight="semibold">
                {item.product_title}
              </Text>
            </InlineStack>
            <InlineStack align="start" gap="100">
              {`( ${item.faq_product.length} FAQs )`}
            </InlineStack>
          </BlockStack>
        </div>{' '}
      </IndexTable.Cell>
      <IndexTable.Cell>{timeDifferenceFromNow(item.createdAt)}</IndexTable.Cell>
    </IndexTable.Row>
  ));

  const handleDelete = () => {
    deleteProductID(deletingItems, {
      onSuccess() {
        clearSelection();
        setIsModalOpen(false);
        Toast.toast.show('Delete success!');
      },
      onError() {
        setIsModalOpen(false);
        Toast.toast.show('Delete error!');
      },
    });
  };

  const promotedBulkActions = [
    {
      content: 'Remove product',
      onAction: () => {
        setDeletingItems(selectedResources);
        setIsModalOpen(true);
      },
    },
  ];
  const bulkActions = [
    {
      icon: DeleteIcon,
      destructive: true,
      content: 'Delete All Product Faq',
      onAction: () => {
        setDeletingItems(products.map((product) => product.id));
        setIsModalOpen(true);
      },
    },
  ];

  return (
    <div>
      <Layout>
        <Layout.Section>
          <BannerWithDismiss
            title="Add FAQ to product"
            tone="info"
            sessionKey={ESessionStorageKeys.SSK_BANNER_LIST_PRODUCT_FAQ}
          >
            <p>List product have faqs</p>
          </BannerWithDismiss>
          <BlockStack gap="200">
            <Card padding={'0'}>
              <IndexTable
                condensed={useBreakpoints().smDown}
                itemCount={products?.length ?? 0}
                selectedItemsCount={
                  allResourcesSelected ? 'All' : selectedResources.length
                }
                onSelectionChange={handleSelectionChange}
                headings={[
                  { title: '' },
                  { title: 'Title' },
                  { title: 'Created At' },
                ]}
                bulkActions={bulkActions}
                emptyState={emptyStateMarkup}
                promotedBulkActions={promotedBulkActions}
              >
                {isLoading ? <ProductFaqTableSkeleton /> : rowMarkup}
              </IndexTable>
            </Card>
          </BlockStack>
        </Layout.Section>
      </Layout>
      <Modal
        open={isModalOpen}
        onClose={() => setIsModalOpen(false)}
        title="Are you sure you want to delete?"
        primaryAction={{
          content: 'Confirm',
          destructive: true,
          onAction: handleDelete,
        }}
        secondaryActions={[
          {
            content: 'Cancel',
            onAction: () => setIsModalOpen(false),
          },
        ]}
      >
        <Modal.Section>
          <Text as="p">
            This action cannot be undone. Are you sure you want to proceed?
          </Text>
        </Modal.Section>
      </Modal>
    </div>
  );
}
