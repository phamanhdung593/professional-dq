import {
  ResourcePicker,
  Button as ButtonAppBride,
} from '@shopify/app-bridge/actions';
import { createApp } from '@shopify/app-bridge/client';
import {
  Badge,
  Button,
  Text,
  Card,
  EmptySearchResult,
  IndexTable,
  InlineGrid,
  Thumbnail,
  useBreakpoints,
  useIndexResourceState,
} from '@shopify/polaris';
import { DeleteIcon, PlusIcon } from '@shopify/polaris-icons';
import { useEffect, useState } from 'react';
import { Modal } from '@shopify/app-bridge/actions';

export const ProductTable = (props: any) => {
  const [products, setProducts] = useState<any[]>([]);
  const app = createApp({
    apiKey: shopify.config.apiKey,
    host: shopify.config.host || '',
  });

  useEffect(() => {
    props.getProducts(products);
  }, [products]);

  const resourcePicker = ResourcePicker.create(app, {
    resourceType: ResourcePicker.ResourceType.Product,
    options: {
      initialSelectionIds: [],
    },
  });

  resourcePicker.showVariants = false;

  resourcePicker.subscribe(ResourcePicker.Action.SELECT, (selection: any) => {
    setProducts(selection.selection);
  });

  const okButton = ButtonAppBride.create(app, { label: 'Confirm' });
  okButton.subscribe(ButtonAppBride.Action.CLICK, () => {
    setProducts([]);
    myModal.dispatch(Modal.Action.CLOSE);
  });

  const cancelButton = ButtonAppBride.create(app, { label: 'Cancel' });
  cancelButton.subscribe(ButtonAppBride.Action.CLICK, () => {
    myModal.dispatch(Modal.Action.CLOSE);
  });

  const okButtonRemoveProduct = ButtonAppBride.create(app, {
    label: 'Confirm',
  });
  okButtonRemoveProduct.subscribe(ButtonAppBride.Action.CLICK, () => {
    const data = products.filter(
      (product) => product.id !== selectedResources[0],
    );

    setProducts(data);
    myModalRemoveProduct.dispatch(Modal.Action.CLOSE);
  });

  const cancelButtonRemoveProduct = ButtonAppBride.create(app, {
    label: 'Cancel',
  });
  cancelButtonRemoveProduct.subscribe(ButtonAppBride.Action.CLICK, () => {
    myModalRemoveProduct.dispatch(Modal.Action.CLOSE);
  });

  const modalOptions = {
    title: 'Confirm',
    message: 'Are you sure?',
    footer: {
      buttons: {
        primary: okButton,
        secondary: [cancelButton],
      },
    },
  };

  const modalOptionsRemoveProduct = {
    title: 'Confirm',
    message: 'Are you sure?',
    footer: {
      buttons: {
        primary: okButtonRemoveProduct,
        secondary: [cancelButtonRemoveProduct],
      },
    },
  };

  const myModal = Modal.create(app, modalOptions);

  const myModalRemoveProduct = Modal.create(app, modalOptionsRemoveProduct);

  const { selectedResources, allResourcesSelected, handleSelectionChange } =
    useIndexResourceState(products);

  const handleDeleteAllProductFaq = async () => {
    myModal.dispatch(Modal.Action.OPEN);
  };

  const handleRemoveProductFaq = async () => {
    myModalRemoveProduct.dispatch(Modal.Action.OPEN);
  };

  const emptyStateMarkup = (
    <EmptySearchResult
      title={'No product yet'}
      description={'Please choose product to add faqs'}
      withIllustration
    />
  );

  const rowMarkup = products.map(({ id, title, images, status }, index) => (
    <IndexTable.Row
      id={id}
      key={`${id}`}
      selected={selectedResources.includes(id)}
      position={index}
    >
      <IndexTable.Cell>
        <Thumbnail
          source={images[0].originalSrc}
          size="small"
          alt="Black choker necklace"
        />
      </IndexTable.Cell>
      <IndexTable.Cell>{title}</IndexTable.Cell>
      <IndexTable.Cell>
        <Badge tone="success">{status}</Badge>
      </IndexTable.Cell>
    </IndexTable.Row>
  ));

  const promotedBulkActions = [
    {
      content: 'Remove product',
      onAction: () => handleRemoveProductFaq(),
    },
  ];
  const bulkActions = [
    {
      icon: DeleteIcon,
      destructive: true,
      content: 'Delete All Product Faq',
      onAction: () => handleDeleteAllProductFaq(),
    },
  ];

  return (
    <div>
      <InlineGrid columns="1fr auto">
        <Text as="h2" variant="headingSm">
          Product of faq
        </Text>
        <Button
          onClick={() => resourcePicker.dispatch(ResourcePicker.Action.OPEN)}
          accessibilityLabel="Add variant"
          icon={PlusIcon}
        >
          choose product
        </Button>
      </InlineGrid>
      <div className="mt-4">
        <Card padding={'0'}>
          <IndexTable
            condensed={useBreakpoints().smDown}
            itemCount={products.length}
            selectedItemsCount={
              allResourcesSelected ? 'All' : selectedResources.length
            }
            onSelectionChange={handleSelectionChange}
            headings={[
              { title: '' },
              { title: 'Product' },
              { title: 'Status' },
            ]}
            bulkActions={bulkActions}
            emptyState={emptyStateMarkup}
            promotedBulkActions={promotedBulkActions}
          >
            {rowMarkup}
          </IndexTable>
        </Card>
      </div>
    </div>
  );
};
