import {
  BlockStack,
  Text,
  InlineGrid,
  Button,
  Card,
  IndexTable,
  useBreakpoints,
  EmptySearchResult,
  useIndexResourceState,
} from '@shopify/polaris';
import { DeleteIcon, PlusIcon } from '@shopify/polaris-icons';
import { useCallback, useEffect, useState } from 'react';
import { FaqModal } from './FaqModal';
import { timeDifferenceFromNow } from '../../utils/date';

export const FaqTable = (props: any) => {
  const [faqs, setFaqs] = useState<any[]>([]);
  const [isFaqModalActive, setIsFaqModalActive] = useState(false);

  const toggleModal = () => setIsFaqModalActive(!isFaqModalActive);

  const { selectedResources, allResourcesSelected, handleSelectionChange } =
    useIndexResourceState(faqs);

  const emptyStateMarkup = (
    <EmptySearchResult
      title={'No Faq yet'}
      description={'Please choose product to add faqs'}
      withIllustration
    />
  );

  useEffect(() => {
    props.getFaqs(faqs);
  }, [faqs]);

  const rowMarkup = faqs.map(({ id, title, createdAt }, index) => (
    <IndexTable.Row
      id={id}
      key={id}
      selected={selectedResources.includes(id)}
      position={index}
    >
      <IndexTable.Cell>{title}</IndexTable.Cell>
      <IndexTable.Cell>{timeDifferenceFromNow(createdAt)}</IndexTable.Cell>
    </IndexTable.Row>
  ));

  const promotedBulkActions = [
    {
      content: 'Remove product',
      onAction: () => console.log('Remove product'),
    },
  ];
  const bulkActions = [
    {
      icon: DeleteIcon,
      destructive: true,
      content: 'Delete All Product Faq',
      onAction: () => setFaqs([]),
    },
  ];

  const getFaqsForProduct = useCallback((faqs: any) => {
    setFaqs(faqs);
  }, []);

  return (
    <div>
      <BlockStack gap="200">
        <InlineGrid columns="1fr auto">
          <Text as="h2" variant="headingSm">
            Choose Faq
          </Text>
          <Button
            onClick={toggleModal}
            accessibilityLabel="Add variant"
            icon={PlusIcon}
          >
            choose faq
          </Button>
        </InlineGrid>
        <div className="mt-4">
          <Card padding={'0'}>
            <IndexTable
              condensed={useBreakpoints().smDown}
              itemCount={faqs.length}
              selectedItemsCount={
                allResourcesSelected ? 'All' : selectedResources.length
              }
              onSelectionChange={handleSelectionChange}
              headings={[{ title: 'Title' }, { title: 'Created At' }]}
              bulkActions={bulkActions}
              emptyState={emptyStateMarkup}
              promotedBulkActions={promotedBulkActions}
            >
              {rowMarkup}
            </IndexTable>
          </Card>
        </div>
        <FaqModal
          active={isFaqModalActive}
          onClose={() => setIsFaqModalActive(false)}
          getFaqsForProduct={getFaqsForProduct}
        />
      </BlockStack>
    </div>
  );
};
