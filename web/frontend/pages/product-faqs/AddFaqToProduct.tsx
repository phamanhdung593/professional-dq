import { Card, BlockStack, Layout, Page } from '@shopify/polaris';
import {
  BannerWithDismiss,
  ESessionStorageKeys,
} from '../components/banner-with-dismiss/BannerWithDismiss';
import { useNavigate } from 'react-router-dom';
import { ProductTable } from './ProductTable';
import { FaqTable } from './FaqTable';
import { useCallback, useEffect, useState } from 'react';
import { useAddFaqProductApi } from '../../hook/faq';
import { useAppBridge } from '@shopify/app-bridge-react';

export default function AddFaqToProduct() {
  const navigate = useNavigate();
  const [products, setProducts] = useState<any>([]);
  const [isLoading, setIsLoading] = useState(false);
  const [faqs, setFaqs] = useState<any>([]);
  const { mutateAsync } = useAddFaqProductApi();
  const getProducts = useCallback((data: any) => {
    setProducts(data);
  }, []);

  const app = useAppBridge();

  useEffect(() => {
    app.loading(true);
    const timer = setTimeout(() => {
      app.loading(false);
    }, 1000);

    return () => clearTimeout(timer);
  }, [app, products, faqs]);

  const getFaqs = useCallback((data: any) => {
    setFaqs(data);
  }, []);

  const handleSaveAddProductFaqs = async () => {
    if (faqs.length === 0 || products.length === 0) {
      return;
    }

    setIsLoading(true);

    await mutateAsync(
      { products: products, faqs: faqs },
      {
        onSuccess: () => {
          setIsLoading(false);
          useAppBridge().toast.show('App product faq successfully!');
        },
      },
    );
  };

  return (
    <Page
      title="Add product faqs"
      primaryAction={{
        content: 'Save',
        onAction: () => handleSaveAddProductFaqs(),
        loading: isLoading,
      }}
      backAction={{ content: '', onAction: () => navigate('/product-faqs') }}
    >
      <Layout>
        <Layout.Section>
          <BannerWithDismiss
            title="Add FAQ to product"
            tone="info"
            sessionKey={ESessionStorageKeys.SSK_BANNER_PRODUCT_FAQ}
          >
            <p>Add FAQ to product page to show faqs on product page.</p>
          </BannerWithDismiss>
          <Card>
            <BlockStack gap={'600'}>
              <ProductTable getProducts={getProducts} />
              <FaqTable getFaqs={getFaqs} />
            </BlockStack>
          </Card>
        </Layout.Section>
      </Layout>
    </Page>
  );
}
