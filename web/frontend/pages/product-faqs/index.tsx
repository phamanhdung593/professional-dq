import { Layout, Page } from '@shopify/polaris';
import ListProductFaq from './ListProductFaq';
import { useNavigate } from 'react-router-dom';

export function ProductFaq() {
  const navigate = useNavigate();

  return (
    <Page
      title="Product FAQs"
      primaryAction={{
        content: 'Add FAQs to Product Page',
        onAction: () => navigate('/product-faqs/add-product'),
      }}
    >
      <Layout>
        <Layout.Section>
          <ListProductFaq />
        </Layout.Section>
      </Layout>
    </Page>
  );
}
