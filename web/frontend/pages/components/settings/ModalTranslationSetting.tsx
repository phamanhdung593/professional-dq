import { Badge, BlockStack, Modal, Select, TextField } from '@shopify/polaris';
import { useCallback, useEffect, useState } from 'react';
import { useUserApi } from '../../../hook/user';
import { useUpdateSettingApi } from '../../../hook/setting';
import { useAppBridge } from '@shopify/app-bridge-react';

export type ModalTranslationProps = {
  title: string;
  value: string;
  action: (value: string) => void;
  isActive: boolean;
  seIsActive: (active: boolean) => void;
  updateSettings?: () => void;
  type: string;
  settingId: number;
};

export default function ModalTranslationSetting(props: ModalTranslationProps) {
  const { data: user } = useUserApi();
  const [locale, setLocale] = useState('');
  const [content, setContent] = useState('');
  const { mutate } = useUpdateSettingApi();
  const [settingId, setSettingId] = useState<number | null>(null);
  const [shopLocalesOptions, setShopLocalesOptions] = useState<string[]>([]);

  const [currentPlan, setCurrentPlan] = useState<any>('free');

  useEffect(() => {
    user?.merchants_plan?.plan
      ? setCurrentPlan(user?.merchants_plan?.plan.toLocaleLowerCase())
      : setCurrentPlan('free');
  }, [user]);

  const closeModal = useCallback(() => {
    props.seIsActive(!props.isActive);
  }, [props]);

  const submit = () => {
    const newTranslation = {
      locale: locale,
      content: content,
    };

    const parsedArray = JSON.parse(props.value);
    const index = parsedArray.findIndex(
      (item: any) => item.locale === newTranslation.locale,
    );

    if (index !== -1) {
      parsedArray[index].content = newTranslation.content;
    } else {
      parsedArray.push(newTranslation);
    }

    const updatedJsonString = JSON.stringify(parsedArray);

    const key = props.type;

    console.log(settingId);
    if (settingId) {
      mutate(
        {
          id: settingId,
          [key]: updatedJsonString,
        },
        {
          onSuccess: () => {
            useAppBridge().toast.show('Update translation success!');
            props.seIsActive(false);
          },
        },
      );
    }
  };

  useEffect(() => {
    if (props?.value) {
      const translations = JSON.parse(props.value);

      const defaultTranslation = translations.find(
        (item: any) => item.locale === 'default',
      );
      const firstLocale = translations.length > 0 ? translations[0].locale : '';

      setLocale(defaultTranslation ? defaultTranslation.locale : firstLocale);
      setContent(defaultTranslation ? defaultTranslation.content : '');
    }

    setSettingId(props.settingId);
  }, [props]);

  useEffect(() => {
    if (user) {
      const locales = JSON.parse(user.shopLocales).shopLocales;
      const primaryItem = locales.find((item: any) => item.primary);
      const defaultItem = primaryItem ? ['default'] : [];
      const otherItems = locales
        .filter((item: any) => !item.primary)
        .map((item: any) => item.locale);

      const result = defaultItem.concat(otherItems);
      setShopLocalesOptions(result);
    }
  }, [user]);

  useEffect(() => {
    if (props.value) {
      const translations = JSON.parse(props.value);
      const selectedTranslation = translations.find(
        (item: any) => item.locale === locale,
      );
      setContent(selectedTranslation ? selectedTranslation.content : '');
    }
  }, [props.value]);

  useEffect(() => {
    if (props?.value) {
      const translations = JSON.parse(props.value);
      const defaultTranslation = translations.find(
        (item: any) => item.locale === 'default',
      );
      setContent(defaultTranslation ? defaultTranslation.content : '');

      if (defaultTranslation) {
        setContent(defaultTranslation.content);
      }
      const selectedTranslation = translations.find(
        (item: any) => item.locale === locale,
      );

      if (selectedTranslation) {
        setContent(selectedTranslation ? selectedTranslation.content : '');
      }
    }
  }, [locale]);

  return (
    <Modal
      open={props.isActive}
      onClose={closeModal}
      title={props.title}
      size="small"
      primaryAction={{
        content: 'Save',
        onAction: submit,
      }}
      secondaryActions={[
        {
          content: 'Cancel',
          onAction: closeModal,
        },
      ]}
    >
      <Modal.Section>
        <BlockStack gap="400">
          <Select
            label=""
            options={shopLocalesOptions}
            onChange={(value) => {
              return setLocale(value);
            }}
            value={locale}
          />
          <div className="flex items-center space-x-2 ">
            <TextField
              label="Add Translation"
              value={content}
              onChange={(value) => setContent(value)}
              autoComplete="off"
              disabled={
                !(
                  currentPlan === 'pro' ||
                  currentPlan === 'ultimate' ||
                  currentPlan === 'free_01' ||
                  currentPlan === 'free extra'
                )
              }
            />
            <div className="flex-grow pt-5">
              <Badge tone="attention">Pro Plan</Badge>
            </div>
          </div>
        </BlockStack>
      </Modal.Section>
    </Modal>
  );
}
