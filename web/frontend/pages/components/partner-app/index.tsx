import { Button, InlineStack, Text, Thumbnail } from '@shopify/polaris';
import { StarIcon } from '@shopify/polaris-icons';
import LegacyCard from '../legacy-card';

type PartnerApp = {
  name: string;
  appStoreUrl: string;
  reviewCount?: number;
  reviewAvg: number;
  imageUrl: string;
  hasFreePlan?: boolean;
  hasPaidPlan?: boolean;
};

type PartnerAppsProps = {
  apps?: PartnerApp[];
};

export default function PartnerApps({ apps = [] }: PartnerAppsProps) {
  if (!apps?.length) {
    return null;
  }

  return (
    <LegacyCard title={'Maybe you are interested?'}>
      {apps.map((app, idx) => (
        <LegacyCard.Section key={idx}>
          <InlineStack gap={'400'}>
            <Thumbnail alt={app.name} source={app.imageUrl} size="medium" />
            <div className="flex flex-auto items-center justify-between gap-4">
              <div className="flex flex-col gap-1">
                <Text as="p" fontWeight="medium">
                  {app.name}
                </Text>
                {app.hasFreePlan ? (
                  <Text as="p" tone="subdued">
                    {app?.hasPaidPlan ? 'Free plan available' : 'Free'}
                  </Text>
                ) : null}
                <div className="flex gap-1 items-center">
                  {[...Array(5)].map((num, index) => {
                    index += 1;
                    const isActive = index <= Math.round(app.reviewAvg);
                    return (
                      <button key={index} type="button">
                        {isActive ? (
                          <StarIcon
                            fill="currentColor"
                            className="w-5 h-5 md:w-6 md:h-6 text-primary"
                          />
                        ) : (
                          <StarIcon
                            fill="currentColor"
                            className="w-5 h-5 md:w-6 md:h-6 text-gray-500"
                          />
                        )}
                      </button>
                    );
                  })}
                  <span>{`(${app.reviewCount})`}</span>
                </div>
              </div>
              <div>
                <Button url={app.appStoreUrl}>{'View app'}</Button>
              </div>
            </div>
          </InlineStack>
        </LegacyCard.Section>
      ))}
    </LegacyCard>
  );
}
