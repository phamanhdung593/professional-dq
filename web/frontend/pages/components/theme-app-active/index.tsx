import { useUserContext } from '../../../components/providers/UserProvider';
import BannerMessage from '../banner-message';

export default function ThemeAppActivate() {
  const { state: data } = useUserContext();

  const gotoThemeActiveUrl = () => {
    if (data?.shopify_domain) {
      window.open(
        `https://${data?.shopify_domain}/admin/themes/current/editor?context=apps&template=product&activateAppId=${import.meta.env.VITE_EMBEDDED_ID}/app-embed`,
        '_blank',
      );
    } else {
      console.error('Shopify domain is not available.');
    }
  };

  return (
    <BannerMessage
      title={'Activate our app on your theme'}
      message={
        'Our application is still disabled in your theme. It is required to be enabled to start storefront integration.'
      }
      type="warning"
      actions={[
        {
          content: 'Activate App',
          onAction: gotoThemeActiveUrl,
          variant: 'primary',
        },
      ]}
    />
  );
}
