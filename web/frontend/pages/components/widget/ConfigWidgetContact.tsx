import { BlockStack, Card, Checkbox, TextField, Text } from '@shopify/polaris';
import { WidgetProps } from '../../../types/widget';

const ConfigWidgetContact = (props: WidgetProps) => {
  return (
    <>
      <Card>
        <BlockStack gap={'400'}>
          <div>
            <Checkbox
              label="Contact"
              checked={props.widgetProps.isActiveContactUs}
              onChange={() =>
                props.widgetProps.setIsActiveContactUs(
                  !props.widgetProps.isActiveContactUs,
                )
              }
            />
            <div className="ml-7 opacity-70 pointer-events-none">
              <Text variant="bodyMd" as="p">
                Show contact options when customers need support.
              </Text>
            </div>
            {props.widgetProps.isActiveContactUs && (
              <div className="ml-7">
                <BlockStack gap={'400'}>
                  <div className="mt-4">
                    <Checkbox
                      label="Phone"
                      checked={props.widgetProps.isActivePhone}
                      onChange={() =>
                        props.widgetProps.setIsActivePhone(
                          !props.widgetProps.isActivePhone,
                        )
                      }
                    />
                    {props.widgetProps.isActivePhone && (
                      <div className="ml-7">
                        <TextField
                          label=""
                          value={props.widgetProps.phoneNumber}
                          onChange={(newValue) =>
                            props.widgetProps.setPhoneNumber(newValue)
                          }
                          autoComplete="off"
                        />
                      </div>
                    )}
                  </div>
                  <div>
                    <Checkbox
                      label="Message link"
                      checked={props.widgetProps.isActiveMessage}
                      onChange={() =>
                        props.widgetProps.setIsActiveMessage(
                          !props.widgetProps.isActiveMessage,
                        )
                      }
                    />
                    {props.widgetProps.isActiveMessage && (
                      <div className="ml-7">
                        <TextField
                          label=""
                          value={props.widgetProps.messageLink}
                          onChange={(newValue) =>
                            props.widgetProps.setMessageLink(newValue)
                          }
                          autoComplete="off"
                        />
                      </div>
                    )}
                  </div>
                  <div>
                    <Checkbox
                      label="Email"
                      checked={props.widgetProps.isActiveEmail}
                      onChange={() =>
                        props.widgetProps.setIsActiveEmail(
                          !props.widgetProps.isActiveEmail,
                        )
                      }
                    />
                    {props.widgetProps.isActiveEmail && (
                      <div className="ml-7">
                        <TextField
                          label=""
                          value={props.widgetProps.emailLink}
                          onChange={(newValue) =>
                            props.widgetProps.setEmailLink(newValue)
                          }
                          autoComplete="off"
                        />
                      </div>
                    )}
                  </div>
                  <div>
                    <Checkbox
                      label="WhatsApp"
                      checked={props.widgetProps.isActiveWhatsApp}
                      onChange={() =>
                        props.widgetProps.setIsActiveWhatsApp(
                          !props.widgetProps.isActiveWhatsApp,
                        )
                      }
                    />
                    {props.widgetProps.isActiveWhatsApp && (
                      <div className="ml-7">
                        <TextField
                          label=""
                          value={props.widgetProps.whatsAppLink}
                          onChange={(newValue) =>
                            props.widgetProps.setWhatsAppLink(newValue)
                          }
                          autoComplete="off"
                        />
                      </div>
                    )}
                  </div>
                </BlockStack>
              </div>
            )}
          </div>
          <div>
            <Checkbox
              label="Featured Questions"
              checked={props.widgetProps.isActiveFaqs}
              onChange={() =>
                props.widgetProps.setIsActiveFaqs(
                  !props.widgetProps.isActiveFaqs,
                )
              }
            />
            <div className="ml-7 opacity-70 pointer-events-none">
              <Text variant="bodyMd" as="p">
                Show featured questions configured in FAQs settings.
              </Text>
            </div>
          </div>
          <div>
            <Checkbox
              label="Featured Categories"
              checked={props.widgetProps.isActiveCategories}
              onChange={() =>
                props.widgetProps.setIsActiveCategories(
                  !props.widgetProps.isActiveCategories,
                )
              }
            />
            <div className="ml-7 opacity-70 pointer-events-none">
              <Text variant="bodyMd" as="p">
                Show featured categories configured in FAQs settings.
              </Text>
            </div>
          </div>
        </BlockStack>
      </Card>
    </>
  );
};

export default ConfigWidgetContact;
