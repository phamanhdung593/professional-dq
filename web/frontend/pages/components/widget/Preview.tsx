import { useMemo, useState } from 'react';
import { Icon } from '@shopify/polaris';
import { CartIcon, SearchIcon } from '@shopify/polaris-icons';
import { WidgetProps } from '../../../types/widget';

export default function Preview(props: WidgetProps) {
  const [activeWidget, setActiveWidget] = useState(true);
  const [hoverCategory, setHoverCategory] = useState<number>();
  const [hoverFaq, setHoverFaq] = useState<number>();

  const listIconWidget = useMemo(() => {
    return [
      {
        id: '1',
        content: (
          <svg
            className={`${
              props.widgetProps.isActiveAnimation &&
              props.widgetProps.iconNumber === '1'
                ? 'help__desk-open-icon'
                : ''
            }`}
            viewBox="0 0 512 512"
            fill="white"
            height={'26px'}
          >
            <path d="M64 112c-8.8 0-16 7.2-16 16l0 22.1L220.5 291.7c20.7 17 50.4 17 71.1 0L464 150.1l0-22.1c0-8.8-7.2-16-16-16L64 112zM48 212.2L48 384c0 8.8 7.2 16 16 16l384 0c8.8 0 16-7.2 16-16l0-171.8L322 328.8c-38.4 31.5-93.7 31.5-132 0L48 212.2zM0 128C0 92.7 28.7 64 64 64l384 0c35.3 0 64 28.7 64 64l0 256c0 35.3-28.7 64-64 64L64 448c-35.3 0-64-28.7-64-64L0 128z" />
          </svg>
        ),
      },
      {
        id: '2',
        content: (
          <svg
            className={`${
              props.widgetProps.isActiveAnimation &&
              props.widgetProps.iconNumber === '2'
                ? 'help__desk-open-icon'
                : ''
            }`}
            viewBox="0 0 512 512"
            fill="white"
            height={'26px'}
            xmlns="http://www.w3.org/2000/svg"
          >
            <path d="M160 368c26.5 0 48 21.5 48 48l0 16 72.5-54.4c8.3-6.2 18.4-9.6 28.8-9.6L448 368c8.8 0 16-7.2 16-16l0-288c0-8.8-7.2-16-16-16L64 48c-8.8 0-16 7.2-16 16l0 288c0 8.8 7.2 16 16 16l96 0zm48 124l-.2 .2-5.1 3.8-17.1 12.8c-4.8 3.6-11.3 4.2-16.8 1.5s-8.8-8.2-8.8-14.3l0-21.3 0-6.4 0-.3 0-4 0-48-48 0-48 0c-35.3 0-64-28.7-64-64L0 64C0 28.7 28.7 0 64 0L448 0c35.3 0 64 28.7 64 64l0 288c0 35.3-28.7 64-64 64l-138.7 0L208 492z" />
          </svg>
        ),
      },
      {
        id: '3',
        content: (
          <svg
            className={`${
              props.widgetProps.isActiveAnimation &&
              props.widgetProps.iconNumber === '3'
                ? 'help__desk-open-icon'
                : ''
            }`}
            viewBox="0 0 512 512"
            fill="white"
            height={'26px'}
            xmlns="http://www.w3.org/2000/svg"
          >
            <path d="M123.6 391.3c12.9-9.4 29.6-11.8 44.6-6.4c26.5 9.6 56.2 15.1 87.8 15.1c124.7 0 208-80.5 208-160s-83.3-160-208-160S48 160.5 48 240c0 32 12.4 62.8 35.7 89.2c8.6 9.7 12.8 22.5 11.8 35.5c-1.4 18.1-5.7 34.7-11.3 49.4c17-7.9 31.1-16.7 39.4-22.7zM21.2 431.9c1.8-2.7 3.5-5.4 5.1-8.1c10-16.6 19.5-38.4 21.4-62.9C17.7 326.8 0 285.1 0 240C0 125.1 114.6 32 256 32s256 93.1 256 208s-114.6 208-256 208c-37.1 0-72.3-6.4-104.1-17.9c-11.9 8.7-31.3 20.6-54.3 30.6c-15.1 6.6-32.3 12.6-50.1 16.1c-.8 .2-1.6 .3-2.4 .5c-4.4 .8-8.7 1.5-13.2 1.9c-.2 0-.5 .1-.7 .1c-5.1 .5-10.2 .8-15.3 .8c-6.5 0-12.3-3.9-14.8-9.9c-2.5-6-1.1-12.8 3.4-17.4c4.1-4.2 7.8-8.7 11.3-13.5c1.7-2.3 3.3-4.6 4.8-6.9l.3-.5z" />
          </svg>
        ),
      },
      {
        id: '4',
        content: (
          <svg
            className={`${
              props.widgetProps.isActiveAnimation &&
              props.widgetProps.iconNumber === '4'
                ? 'help__desk-open-icon'
                : ''
            }`}
            viewBox="0 0 300 512"
            fill="white"
            height={'26px'}
            xmlns="http://www.w3.org/2000/svg"
          >
            <path d="M80 160c0-35.3 28.7-64 64-64l32 0c35.3 0 64 28.7 64 64l0 3.6c0 21.8-11.1 42.1-29.4 53.8l-42.2 27.1c-25.2 16.2-40.4 44.1-40.4 74l0 1.4c0 17.7 14.3 32 32 32s32-14.3 32-32l0-1.4c0-8.2 4.2-15.8 11-20.2l42.2-27.1c36.6-23.6 58.8-64.1 58.8-107.7l0-3.6c0-70.7-57.3-128-128-128l-32 0C73.3 32 16 89.3 16 160c0 17.7 14.3 32 32 32s32-14.3 32-32zm80 320a40 40 0 1 0 0-80 40 40 0 1 0 0 80z" />
          </svg>
        ),
      },
      {
        id: '5',
        content: (
          <svg
            className={`${
              props.widgetProps.isActiveAnimation &&
              props.widgetProps.iconNumber === '5'
                ? 'help__desk-open-icon'
                : ''
            }`}
            viewBox="30 10 160 512"
            fill="white"
            height={'26px'}
            xmlns="http://www.w3.org/2000/svg"
          >
            <path d="M48 80a48 48 0 1 1 96 0A48 48 0 1 1 48 80zM0 224c0-17.7 14.3-32 32-32l64 0c17.7 0 32 14.3 32 32l0 224 32 0c17.7 0 32 14.3 32 32s-14.3 32-32 32L32 512c-17.7 0-32-14.3-32-32s14.3-32 32-32l32 0 0-192-32 0c-17.7 0-32-14.3-32-32z" />
          </svg>
        ),
      },
    ];
  }, [props.widgetProps.iconNumber, props.widgetProps.isActiveAnimation]);

  return (
    <div
      className="border-4 overflow-hidden bg-gray-100 relative max-w-[320px]"
      style={{
        borderRadius: '30px',
        borderColor: '#ccc',
        fontFamily: `${props.widgetProps.fontFamily} !important`,
      }}
    >
      {activeWidget ? (
        <div
          className="h-[560px] help__desk-scroll"
          style={{ overflowY: 'auto', overflowX: 'hidden' }}
        >
          <div className="help__desk-home">
            <div
              style={{
                height: '138px',
                margin: '-35px',
                position: 'relative',
              }}
            >
              <div className="help__desk-background"></div>
              <div
                className="help__desk-header text-white m-3 p-5"
                style={{
                  position: 'absolute',
                  top: '23px',
                  width: 'calc(100% - 20px)',
                  height: '190px',
                  borderBottomLeftRadius: '100px',
                  borderBottomRightRadius: '100px',
                  backgroundColor: props.widgetProps.backgroundColor,
                }}
              >
                <div
                  className="ms-[20px]"
                  style={{ color: props.widgetProps.headerColor }}
                >
                  <div className=" mb-3">
                    <p className="help__desk-hi text-3xl">
                      {props.widgetProps.welcomeTitle}
                    </p>
                  </div>
                  <span className="help__desk-title text-base">
                    {props.widgetProps.descriptionTitle}
                  </span>
                </div>
                <div
                  onClick={() => setActiveWidget(false)}
                  className="help__desk-icon"
                  title="Close"
                  style={{
                    position: 'absolute',
                    top: '20px',
                    right: '35px',
                    cursor: 'pointer',
                  }}
                >
                  <svg
                    width="26"
                    height="26"
                    fill="#fff"
                    style={{ margin: 'auto' }}
                    xmlns="http://www.w3.org/2000/svg"
                    viewBox="0 0 320 512"
                  >
                    <path d="M310.6 150.6c12.5-12.5 12.5-32.8 0-45.3s-32.8-12.5-45.3 0L160 210.7 54.6 105.4c-12.5-12.5-32.8-12.5-45.3 0s-12.5 32.8 0 45.3L114.7 256 9.4 361.4c-12.5 12.5-12.5 32.8 0 45.3s32.8 12.5 45.3 0L160 301.3 265.4 406.6c12.5 12.5 32.8 12.5 45.3 0s12.5-32.8 0-45.3L205.3 256 310.6 150.6z" />
                  </svg>
                </div>
              </div>
            </div>
            <div
              className="help__desk-body help__desk-scroll mx-2"
              style={{
                position: 'relative',
                marginTop: '45px',
                paddingBottom: '1px',
                borderRadius: '6px',
              }}
            >
              <div className="mb-4" style={{ borderRadius: '6px' }}>
                {props.widgetProps.isActiveContactUs && (
                  <div
                    className="help__desk-contact-items help__desk-component mb-5 bg-white p-3 border"
                    style={{ borderRadius: '6px' }}
                  >
                    <span
                      className="help__desk-label m-0"
                      style={{ color: props.widgetProps.textColor }}
                    >
                      <b> Contact Us</b>
                    </span>
                    <div className="help__desk-items flex items-center pt-5">
                      {props.widgetProps.isActivePhone && (
                        <a
                          href=""
                          target="_blank"
                          style={{
                            height: '40px',
                            width: '40px',
                            textDecoration: 'none',
                            marginLeft: '18px',
                          }}
                          className="border bg-green-700 rounded flex items-center justify-center "
                        >
                          <svg
                            height={'26px'}
                            fill="white"
                            viewBox="0 0 512 512"
                          >
                            <path d="M164.9 24.6c-7.7-18.6-28-28.5-47.4-23.2l-88 24C12.1 30.2 0 46 0 64C0 311.4 200.6 512 448 512c18 0 33.8-12.1 38.6-29.5l24-88c5.3-19.4-4.6-39.7-23.2-47.4l-96-40c-16.3-6.8-35.2-2.1-46.3 11.6L304.7 368C234.3 334.7 177.3 277.7 144 207.3L193.3 167c13.7-11.2 18.4-30 11.6-46.3l-40-96z" />
                          </svg>
                        </a>
                      )}
                      {props.widgetProps.isActiveMessage && (
                        <a
                          target="_blank"
                          style={{
                            height: '40px',
                            width: '40px',
                            textDecoration: 'none',
                            marginLeft: '18px',
                          }}
                          className="rounded flex items-center justify-center border bg-blue-600"
                        >
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 512 512"
                            height={'26px'}
                            fill="white"
                          >
                            <path d="M256.6 8C116.5 8 8 110.3 8 248.6c0 72.3 29.7 134.8 78.1 177.9 8.4 7.5 6.6 11.9 8.1 58.2A19.9 19.9 0 0 0 122 502.3c52.9-23.3 53.6-25.1 62.6-22.7C337.9 521.8 504 423.7 504 248.6 504 110.3 396.6 8 256.6 8zm149.2 185.1l-73 115.6a37.4 37.4 0 0 1 -53.9 9.9l-58.1-43.5a15 15 0 0 0 -18 0l-78.4 59.4c-10.5 7.9-24.2-4.6-17.1-15.7l73-115.6a37.4 37.4 0 0 1 53.9-9.9l58.1 43.5a15 15 0 0 0 18 0l78.4-59.4c10.4-8 24.1 4.5 17.1 15.6z" />
                          </svg>
                        </a>
                      )}
                      {props.widgetProps.isActiveEmail && (
                        <a
                          target="_blank"
                          style={{
                            height: '40px',
                            width: '40px',
                            textDecoration: 'none',
                            backgroundColor: '#479dde',
                            marginLeft: '18px',
                          }}
                          className="rounded border flex items-center justify-center"
                        >
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 512 512"
                            height={'26px'}
                            fill="white"
                          >
                            <path d="M48 64C21.5 64 0 85.5 0 112c0 15.1 7.1 29.3 19.2 38.4L236.8 313.6c11.4 8.5 27 8.5 38.4 0L492.8 150.4c12.1-9.1 19.2-23.3 19.2-38.4c0-26.5-21.5-48-48-48L48 64zM0 176L0 384c0 35.3 28.7 64 64 64l384 0c35.3 0 64-28.7 64-64l0-208L294.4 339.2c-22.8 17.1-54 17.1-76.8 0L0 176z" />
                          </svg>
                        </a>
                      )}
                      {props.widgetProps.isActiveWhatsApp && (
                        <a
                          target="_blank"
                          // v-if="messages_settings.whatsApp_visible"
                          style={{
                            height: '40px',
                            width: '40px',
                            textDecoration: 'none',
                            marginLeft: '18px',
                          }}
                          className="rounded border bg-green-700 flex items-center justify-center"
                        >
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 448 512"
                            height={'26px'}
                            fill="white"
                          >
                            <path d="M380.9 97.1C339 55.1 283.2 32 223.9 32c-122.4 0-222 99.6-222 222 0 39.1 10.2 77.3 29.6 111L0 480l117.7-30.9c32.4 17.7 68.9 27 106.1 27h.1c122.3 0 224.1-99.6 224.1-222 0-59.3-25.2-115-67.1-157zm-157 341.6c-33.2 0-65.7-8.9-94-25.7l-6.7-4-69.8 18.3L72 359.2l-4.4-7c-18.5-29.4-28.2-63.3-28.2-98.2 0-101.7 82.8-184.5 184.6-184.5 49.3 0 95.6 19.2 130.4 54.1 34.8 34.9 56.2 81.2 56.1 130.5 0 101.8-84.9 184.6-186.6 184.6zm101.2-138.2c-5.5-2.8-32.8-16.2-37.9-18-5.1-1.9-8.8-2.8-12.5 2.8-3.7 5.6-14.3 18-17.6 21.8-3.2 3.7-6.5 4.2-12 1.4-32.6-16.3-54-29.1-75.5-66-5.7-9.8 5.7-9.1 16.3-30.3 1.8-3.7 .9-6.9-.5-9.7-1.4-2.8-12.5-30.1-17.1-41.2-4.5-10.8-9.1-9.3-12.5-9.5-3.2-.2-6.9-.2-10.6-.2-3.7 0-9.7 1.4-14.8 6.9-5.1 5.6-19.4 19-19.4 46.3 0 27.3 19.9 53.7 22.6 57.4 2.8 3.7 39.1 59.7 94.8 83.8 35.2 15.2 49 16.5 66.6 13.9 10.7-1.6 32.8-13.4 37.4-26.4 4.6-13 4.6-24.1 3.2-26.4-1.3-2.5-5-3.9-10.5-6.6z" />
                          </svg>
                        </a>
                      )}
                    </div>
                  </div>
                )}
                {props.widgetProps.isActiveFaqs && (
                  <div
                    v-if="messages_settings.feature_questions_visible"
                    className="help__desk-contact-items help__desk-component mb-3 pt-3 bg-white border"
                    style={{ borderRadius: '6px' }}
                  >
                    <span
                      className="help__desk-label m-0 px-3 pt-3"
                      style={{ color: props.widgetProps.textColor }}
                    >
                      <b>FAQs</b>
                    </span>
                    <div className="help__desk-search px-3 py-2">
                      <div
                        className="help__desk-search-main flex justify-between items-center"
                        style={{
                          border: '1px solid #ccc',
                          borderRadius: '20px',
                          overflow: 'hidden',
                          height: '36px',
                        }}
                      >
                        <svg
                          height={'18px'}
                          className="fa-solid fa-magnifying-glass pl-3 pr-2 pt-[2px]"
                          viewBox="0 0 512 512"
                        >
                          <path d="M416 208c0 45.9-14.9 88.3-40 122.7L502.6 457.4c12.5 12.5 12.5 32.8 0 45.3s-32.8 12.5-45.3 0L330.7 376c-34.4 25.2-76.8 40-122.7 40C93.1 416 0 322.9 0 208S93.1 0 208 0S416 93.1 416 208zM208 352a144 144 0 1 0 0-288 144 144 0 1 0 0 288z" />
                        </svg>
                        <input
                          className="text-sm"
                          id="search-faq"
                          style={{ flex: '1', border: 'none', outline: 'none' }}
                          type="text"
                          placeholder="Search"
                        />
                      </div>
                    </div>
                    <div
                      className="help__desk-main mb-2 help__desk-scroll"
                      style={{ maxHeight: '190px', overflowY: 'auto' }}
                    >
                      <div
                        // v-for="(item,index) in faqs"
                        className="help__desk-list"
                      >
                        <div
                          // v-if="item.feature_faq"
                          className="help__desk-item flex justify-between items-center px-4 py-2"
                          style={{
                            cursor: 'pointer',
                            fontSize: '13px',
                            backgroundColor: `${
                              hoverFaq === 1 ? '#e9edf1' : ''
                            }`,
                          }}
                          onMouseOver={() => setHoverFaq(1)}
                          onMouseOut={() => setHoverFaq(0)}
                        >
                          <span
                            style={{
                              wordBreak: 'break-all',
                              fontWeight: '400',
                            }}
                          >
                            Why should I choose your lactation cookies over
                            other products on the market?
                          </span>
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 320 512"
                            height={'26px'}
                            className="ml-3"
                          >
                            <path d="M278.6 233.4c12.5 12.5 12.5 32.8 0 45.3l-160 160c-12.5 12.5-32.8 12.5-45.3 0s-12.5-32.8 0-45.3L210.7 256 73.4 118.6c-12.5-12.5-12.5-32.8 0-45.3s32.8-12.5 45.3 0l160 160z" />
                          </svg>
                        </div>
                        <div
                          // v-if="item.feature_faq"
                          className="help__desk-item flex justify-between items-center px-4 py-2"
                          style={{
                            cursor: 'pointer',
                            fontSize: '13px',
                            backgroundColor: `${
                              hoverFaq === 2 ? '#e9edf1' : ''
                            }`,
                          }}
                          onMouseOver={() => setHoverFaq(2)}
                          onMouseOut={() => setHoverFaq(0)}
                        >
                          <span
                            style={{
                              wordBreak: 'break-all',
                              fontWeight: '400',
                            }}
                          >
                            What are the estimated delivery times for domestic
                            and international orders, and do you provide
                            tracking information for all shipments?
                          </span>
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 320 512"
                            height={'26px'}
                            className="ml-3"
                          >
                            <path d="M278.6 233.4c12.5 12.5 12.5 32.8 0 45.3l-160 160c-12.5 12.5-32.8 12.5-45.3 0s-12.5-32.8 0-45.3L210.7 256 73.4 118.6c-12.5-12.5-12.5-32.8 0-45.3s32.8-12.5 45.3 0l160 160z" />
                          </svg>
                        </div>
                        <div
                          // v-if="item.feature_faq"
                          className="help__desk-item flex justify-between items-center px-4 py-2"
                          style={{
                            cursor: 'pointer',
                            fontSize: '13px',
                            backgroundColor: `${
                              hoverFaq === 3 ? '#e9edf1' : ''
                            }`,
                          }}
                          onMouseOver={() => setHoverFaq(3)}
                          onMouseOut={() => setHoverFaq(0)}
                        >
                          <span
                            style={{
                              wordBreak: 'break-all',
                              fontWeight: '400',
                            }}
                          >
                            Can you explain your 30-day money-back guarantee
                            policy and what conditions need to be met for a full
                            refund on returned items?
                          </span>
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 320 512"
                            height={'26px'}
                            className="ml-3"
                          >
                            <path d="M278.6 233.4c12.5 12.5 12.5 32.8 0 45.3l-160 160c-12.5 12.5-32.8 12.5-45.3 0s-12.5-32.8 0-45.3L210.7 256 73.4 118.6c-12.5-12.5-12.5-32.8 0-45.3s32.8-12.5 45.3 0l160 160z" />
                          </svg>
                        </div>
                        <div
                          // v-if="item.feature_faq"
                          className="help__desk-item flex justify-between items-center px-4 py-2"
                          style={{
                            cursor: 'pointer',
                            fontSize: '13px',
                            backgroundColor: `${
                              hoverFaq === 4 ? '#e9edf1' : ''
                            }`,
                          }}
                          onMouseOver={() => setHoverFaq(4)}
                          onMouseOut={() => setHoverFaq(0)}
                        >
                          <span
                            style={{
                              wordBreak: 'break-all',
                              fontWeight: '400',
                            }}
                          >
                            What security measures do you have in place to
                            protect customer payment information, and which
                            payment methods are currently accepted?
                          </span>
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 320 512"
                            height={'26px'}
                            className="ml-3"
                          >
                            <path d="M278.6 233.4c12.5 12.5 12.5 32.8 0 45.3l-160 160c-12.5 12.5-32.8 12.5-45.3 0s-12.5-32.8 0-45.3L210.7 256 73.4 118.6c-12.5-12.5-12.5-32.8 0-45.3s32.8-12.5 45.3 0l160 160z" />
                          </svg>
                        </div>
                      </div>
                    </div>
                  </div>
                )}
                {props.widgetProps.isActiveCategories && (
                  <div
                    v-if="messages_settings.feature_categories_visible"
                    className="help__desk-contact-items help__desk-component bg-white border pt-3"
                    style={{ borderRadius: '6px' }}
                  >
                    <span
                      className="help__desk-label m-0 p-3 "
                      style={{ color: props.widgetProps.textColor }}
                    >
                      <b> Categories</b>
                    </span>
                    <div
                      className="help__desk-main mb-2 help__desk-scroll"
                      style={{ maxHeight: '190px', overflowY: 'auto' }}
                    >
                      <div
                        v-for="(item, index) in categories"
                        className="help__desk-list"
                      >
                        <div
                          v-if="item.feature_category"
                          className="help__desk-item border-bottom flex justify-between items-center py-2 px-4"
                          onMouseOver={() => setHoverCategory(1)}
                          onMouseOut={() => setHoverCategory(0)}
                          style={{
                            cursor: 'pointer',
                            backgroundColor: `${
                              hoverCategory === 1 ? '#e9edf1' : ''
                            }`,
                          }}
                        >
                          <div>
                            <span style={{ wordBreak: 'break-all' }}>
                              Categorys
                            </span>
                            <br />
                            <span
                              style={{
                                color: '#98c6cd',
                                fontSize: '13px',
                                fontWeight: '400',
                              }}
                            >
                              {' '}
                              5 FAQs
                            </span>
                          </div>
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 320 512"
                            height={'13px'}
                            className="ml-3"
                          >
                            <path d="M278.6 233.4c12.5 12.5 12.5 32.8 0 45.3l-160 160c-12.5 12.5-32.8 12.5-45.3 0s-12.5-32.8 0-45.3L210.7 256 73.4 118.6c-12.5-12.5-12.5-32.8 0-45.3s32.8-12.5 45.3 0l160 160z" />
                          </svg>
                        </div>
                      </div>
                    </div>
                  </div>
                )}
              </div>
            </div>
          </div>
        </div>
      ) : (
        <div className="h-[600px]">
          <div className="pt-5 mx-4 flex justify-between">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              height={'20px'}
              width={'20px'}
              viewBox="0 0 448 512"
            >
              <path d="M0 96C0 78.3 14.3 64 32 64H416c17.7 0 32 14.3 32 32s-14.3 32-32 32H32C14.3 128 0 113.7 0 96zM0 256c0-17.7 14.3-32 32-32H416c17.7 0 32 14.3 32 32s-14.3 32-32 32H32c-17.7 0-32-14.3-32-32zM448 416c0 17.7-14.3 32-32 32H32c-17.7 0-32-14.3-32-32s14.3-32 32-32H416c17.7 0 32 14.3 32 32z" />
            </svg>
            <p>YOUR STORE</p>
            <div className="flex">
              <Icon source={SearchIcon} tone="base" />
              <div className="ms-3">
                <Icon source={CartIcon} tone="base" />
              </div>
            </div>
          </div>
        </div>
      )}
      <div
        className={`absolute bottom-[10px] ${
          props.widgetProps.alignment === 'left'
            ? 'left-[10px]'
            : 'right-[10px]'
        }`}
      >
        <div className="flex">
          {!activeWidget &&
            listIconWidget.map((item, index) => (
              <div
                key={index}
                onClick={() => {
                  setActiveWidget(!activeWidget);
                }}
                className="p-1 mt-2 rounded me-3 me-sm-4"
                style={{
                  cursor: 'pointer',
                  display:
                    props.widgetProps.iconNumber === item.id ? '' : 'none',
                }}
              >
                <div
                  style={{
                    height: '50px',
                    width: '50px',
                    borderRadius: '50%',
                    backgroundColor: props.widgetProps.buttonBackgroundColor,
                  }}
                  className="px-2 flex items-center justify-center"
                >
                  {item.content}
                </div>
              </div>
            ))}
          {activeWidget && (
            <div
              onClick={() => {
                setActiveWidget(!activeWidget);
              }}
              className="p-1 mt-2 rounded me-3 me-sm-4"
              style={{
                cursor: 'pointer',
              }}
            >
              <div
                style={{
                  height: '50px',
                  width: '50px',
                  borderRadius: '50%',
                  backgroundColor: props.widgetProps.backgroundColor,
                }}
                className="px-2 flex items-center justify-center"
              >
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke="currentColor"
                  className="w-6 h-6 text-white"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth="2"
                    d="M19 9l-7 7-7-7"
                  />
                </svg>
              </div>
            </div>
          )}
        </div>
      </div>
    </div>
  );
}
