import { BlockStack, Card, Select, TextField } from '@shopify/polaris';
import { WidgetProps } from '../../../types/widget';

const ConfigWidgetTheme = (props: WidgetProps) => {
  const handleChange = (newValue: string) =>
    props.widgetProps.setWelcomeTitle(newValue);

  const fontFamily = [
    {
      label: 'Questrial',
      value: 'Questrial',
    },
    {
      label: 'Time New Roman',
      value: 'Time New Roman',
    },
    {
      label: 'Lucida Sans',
      value: 'Lucida Sans',
    },
    {
      label: 'Courier',
      value: 'Courier',
    },
    {
      label: 'Garamond',
      value: 'Garamond',
    },
    {
      label: 'Arial',
      value: 'Arial',
    },
    {
      label: 'Calibri',
      value: 'Calibri',
    },
    {
      label: 'Playfair Display',
      value: 'Playfair Display',
    },
    {
      label: 'Varela Round',
      value: 'Varela Round',
    },
    {
      label: 'Use your store font (not available in live preview)',
      value: 'unset',
    },
  ];

  return (
    <>
      <Card>
        <b className="text-sm">Theme</b>
        <div className="mt-4 ml-7">
          <BlockStack gap={'400'}>
            <TextField
              label="Welcome title"
              value={props.widgetProps.welcomeTitle}
              onChange={handleChange}
              autoComplete="off"
            />
            <TextField
              label="Description title"
              value={props.widgetProps.descriptionTitle}
              onChange={(value) => props.widgetProps.setDescriptionTitle(value)}
              autoComplete="off"
            />
            <div>
              <p>Theme color</p>
              <div className="flex items-center">
                <input
                  type="color"
                  className="h-[36px] mr-1 w-[60px] cursor-pointer"
                  onChange={(e) =>
                    props.widgetProps.setBackgroundColor(e.target.value)
                  }
                  value={props.widgetProps.backgroundColor}
                ></input>
                <div className="flex-1">
                  <TextField
                    label=""
                    value={props.widgetProps.backgroundColor}
                    onChange={(value) =>
                      props.widgetProps.setBackgroundColor(value)
                    }
                    autoComplete="off"
                  />
                </div>
              </div>
            </div>
            <div>
              <p>Header text color</p>
              <div className="flex items-center">
                <input
                  type="color"
                  className="h-[36px] mr-1 w-[60px] cursor-pointer"
                  onChange={(e) =>
                    props.widgetProps.setHeaderColor(e.target.value)
                  }
                  value={props.widgetProps.headerColor}
                ></input>
                <div className="flex-1">
                  <TextField
                    label=""
                    value={props.widgetProps.headerColor}
                    onChange={(value) =>
                      props.widgetProps.setHeaderColor(value)
                    }
                    autoComplete="off"
                  />
                </div>
              </div>
            </div>
            <div>
              <p>Title color</p>
              <div className="flex items-center">
                <input
                  type="color"
                  className="h-[36px] mr-1 w-[60px] cursor-pointer"
                  onChange={(e) =>
                    props.widgetProps.setTextColor(e.target.value)
                  }
                  value={props.widgetProps.textColor}
                ></input>
                <div className="flex-1">
                  <TextField
                    label=""
                    value={props.widgetProps.textColor}
                    onChange={(value) => props.widgetProps.setTextColor(value)}
                    autoComplete="off"
                  />
                </div>
              </div>
            </div>
            <div>
              <Select
                label="Font Family"
                options={fontFamily}
                onChange={(e) => props.widgetProps.setFontFamily(e)}
                value={props.widgetProps.fontFamily}
              />
            </div>
          </BlockStack>
        </div>
      </Card>
    </>
  );
};

export default ConfigWidgetTheme;
