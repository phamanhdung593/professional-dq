import { BlockStack, Card, Checkbox, TextField } from '@shopify/polaris';
import { useMemo } from 'react';
import { WidgetProps } from '../../../types/widget';

const ConfigWidgetButton = (props: WidgetProps) => {
  const listIconWidget = useMemo(() => {
    return [
      {
        id: '1',
        content: (
          <svg
            className={`${
              props.widgetProps.isActiveAnimation &&
              props.widgetProps.iconNumber === '1'
                ? 'help__desk-open-icon'
                : ''
            }`}
            viewBox="0 0 512 512"
            fill="white"
            height={'26px'}
          >
            <path d="M64 112c-8.8 0-16 7.2-16 16l0 22.1L220.5 291.7c20.7 17 50.4 17 71.1 0L464 150.1l0-22.1c0-8.8-7.2-16-16-16L64 112zM48 212.2L48 384c0 8.8 7.2 16 16 16l384 0c8.8 0 16-7.2 16-16l0-171.8L322 328.8c-38.4 31.5-93.7 31.5-132 0L48 212.2zM0 128C0 92.7 28.7 64 64 64l384 0c35.3 0 64 28.7 64 64l0 256c0 35.3-28.7 64-64 64L64 448c-35.3 0-64-28.7-64-64L0 128z" />
          </svg>
        ),
      },
      {
        id: '2',
        content: (
          <svg
            className={`${
              props.widgetProps.isActiveAnimation &&
              props.widgetProps.iconNumber === '2'
                ? 'help__desk-open-icon'
                : ''
            }`}
            viewBox="0 0 512 512"
            fill="white"
            height={'26px'}
            xmlns="http://www.w3.org/2000/svg"
          >
            <path d="M160 368c26.5 0 48 21.5 48 48l0 16 72.5-54.4c8.3-6.2 18.4-9.6 28.8-9.6L448 368c8.8 0 16-7.2 16-16l0-288c0-8.8-7.2-16-16-16L64 48c-8.8 0-16 7.2-16 16l0 288c0 8.8 7.2 16 16 16l96 0zm48 124l-.2 .2-5.1 3.8-17.1 12.8c-4.8 3.6-11.3 4.2-16.8 1.5s-8.8-8.2-8.8-14.3l0-21.3 0-6.4 0-.3 0-4 0-48-48 0-48 0c-35.3 0-64-28.7-64-64L0 64C0 28.7 28.7 0 64 0L448 0c35.3 0 64 28.7 64 64l0 288c0 35.3-28.7 64-64 64l-138.7 0L208 492z" />
          </svg>
        ),
      },
      {
        id: '3',
        content: (
          <svg
            className={`${
              props.widgetProps.isActiveAnimation &&
              props.widgetProps.iconNumber === '3'
                ? 'help__desk-open-icon'
                : ''
            }`}
            viewBox="0 0 512 512"
            fill="white"
            height={'26px'}
            xmlns="http://www.w3.org/2000/svg"
          >
            <path d="M123.6 391.3c12.9-9.4 29.6-11.8 44.6-6.4c26.5 9.6 56.2 15.1 87.8 15.1c124.7 0 208-80.5 208-160s-83.3-160-208-160S48 160.5 48 240c0 32 12.4 62.8 35.7 89.2c8.6 9.7 12.8 22.5 11.8 35.5c-1.4 18.1-5.7 34.7-11.3 49.4c17-7.9 31.1-16.7 39.4-22.7zM21.2 431.9c1.8-2.7 3.5-5.4 5.1-8.1c10-16.6 19.5-38.4 21.4-62.9C17.7 326.8 0 285.1 0 240C0 125.1 114.6 32 256 32s256 93.1 256 208s-114.6 208-256 208c-37.1 0-72.3-6.4-104.1-17.9c-11.9 8.7-31.3 20.6-54.3 30.6c-15.1 6.6-32.3 12.6-50.1 16.1c-.8 .2-1.6 .3-2.4 .5c-4.4 .8-8.7 1.5-13.2 1.9c-.2 0-.5 .1-.7 .1c-5.1 .5-10.2 .8-15.3 .8c-6.5 0-12.3-3.9-14.8-9.9c-2.5-6-1.1-12.8 3.4-17.4c4.1-4.2 7.8-8.7 11.3-13.5c1.7-2.3 3.3-4.6 4.8-6.9l.3-.5z" />
          </svg>
        ),
      },
      {
        id: '4',
        content: (
          <svg
            className={`${
              props.widgetProps.isActiveAnimation &&
              props.widgetProps.iconNumber === '4'
                ? 'help__desk-open-icon'
                : ''
            }`}
            viewBox="0 0 300 512"
            fill="white"
            height={'26px'}
            xmlns="http://www.w3.org/2000/svg"
          >
            <path d="M80 160c0-35.3 28.7-64 64-64l32 0c35.3 0 64 28.7 64 64l0 3.6c0 21.8-11.1 42.1-29.4 53.8l-42.2 27.1c-25.2 16.2-40.4 44.1-40.4 74l0 1.4c0 17.7 14.3 32 32 32s32-14.3 32-32l0-1.4c0-8.2 4.2-15.8 11-20.2l42.2-27.1c36.6-23.6 58.8-64.1 58.8-107.7l0-3.6c0-70.7-57.3-128-128-128l-32 0C73.3 32 16 89.3 16 160c0 17.7 14.3 32 32 32s32-14.3 32-32zm80 320a40 40 0 1 0 0-80 40 40 0 1 0 0 80z" />
          </svg>
        ),
      },
      {
        id: '5',
        content: (
          <svg
            className={`${
              props.widgetProps.isActiveAnimation &&
              props.widgetProps.iconNumber === '5'
                ? 'help__desk-open-icon'
                : ''
            }`}
            viewBox="30 10 160 512"
            fill="white"
            height={'28px'}
          >
            <path d="M48 80a48 48 0 1 1 96 0A48 48 0 1 1 48 80zM0 224c0-17.7 14.3-32 32-32l64 0c17.7 0 32 14.3 32 32l0 224 32 0c17.7 0 32 14.3 32 32s-14.3 32-32 32L32 512c-17.7 0-32-14.3-32-32s14.3-32 32-32l32 0 0-192-32 0c-17.7 0-32-14.3-32-32z" />
          </svg>
        ),
      },
    ];
  }, [props.widgetProps.iconNumber, props.widgetProps.isActiveAnimation]);

  return (
    <Card>
      <BlockStack gap={'400'}>
        <b>Widget Button</b>
        <div className="ml-7">
          <Checkbox
            label="Icon animation"
            checked={props.widgetProps.isActiveAnimation}
            onChange={() =>
              props.widgetProps.setIsActiveAnimation(
                !props.widgetProps.isActiveAnimation,
              )
            }
          />
          <div></div>
        </div>
        <div className="ml-7">
          <p>Select icon</p>
          <div className="flex">
            {listIconWidget.map((item, index) => (
              <div
                key={index}
                onClick={() => {
                  props.widgetProps.setIconNumber(item.id);
                }}
                className="p-1 mt-2 rounded mr-4 sm:mr-3 border-2"
                style={{
                  cursor: 'pointer',
                  borderColor: `${
                    props.widgetProps.iconNumber === item.id ? '#ff8200' : ''
                  }`,
                }}
              >
                <div
                  style={{
                    height: '50px',
                    width: '50px',
                    borderRadius: '50%',
                    backgroundColor: props.widgetProps.buttonBackgroundColor,
                    transition:
                      props.widgetProps.isActiveAnimation &&
                      props.widgetProps.iconNumber === item.id
                        ? 'transform 5s ease'
                        : '',
                  }}
                  className="px-2 flex items-center justify-center"
                >
                  <div
                    style={{
                      transition:
                        props.widgetProps.isActiveAnimation &&
                        props.widgetProps.iconNumber === item.id
                          ? 'transform 5s ease'
                          : '',
                      transform:
                        props.widgetProps.isActiveAnimation &&
                        props.widgetProps.iconNumber === item.id
                          ? 'rotateY(360deg)'
                          : 'none',
                    }}
                  >
                    {item.content}
                  </div>
                </div>
              </div>
            ))}
          </div>
        </div>
        <div className="ml-7">
          <p>Background color</p>
          <div className="flex items-center">
            <input
              type="color"
              className="h-[36px] mr-1 w-[60px] cursor-pointer"
              onChange={(e) =>
                props.widgetProps.setButtonBackgroundColor(e.target.value)
              }
              value={props.widgetProps.buttonBackgroundColor}
            ></input>
            <div className="flex-1">
              <TextField
                label=""
                value={props.widgetProps.buttonBackgroundColor}
                onChange={(value) =>
                  props.widgetProps.setButtonBackgroundColor(value)
                }
                autoComplete="off"
              />
            </div>
          </div>
        </div>
        <div className="ml-7">
          <p>Alignment</p>
          <div className="flex ">
            <div>
              <div
                onClick={() => props.widgetProps.setAlignment('left')}
                className="w-[80px] h-[130px] border-[3px] relative cursor-pointer"
                style={{
                  borderColor:
                    props.widgetProps.alignment === 'left' ? '#ff8200' : '',
                  borderRadius: '12px',
                }}
              >
                <div
                  className="absolute w-6 h-6 rounded-full bottom-[6px] left-[6px]"
                  style={{
                    backgroundColor:
                      props.widgetProps.alignment === 'left'
                        ? '#ff8200'
                        : '#cccccc',
                  }}
                ></div>
              </div>
              <p>Bottom left</p>
            </div>
            <div className="ml-4">
              <div
                onClick={() => props.widgetProps.setAlignment('right')}
                className="w-[80px] h-[130px] border-[3px] relative cursor-pointer ms-4"
                style={{
                  borderColor:
                    props.widgetProps.alignment === 'right' ? '#ff8200' : '',
                  borderRadius: '12px',
                }}
              >
                <div
                  className="absolute w-6 h-6 rounded-full bottom-[6px] right-[6px]"
                  style={{
                    backgroundColor:
                      props.widgetProps.alignment === 'right'
                        ? '#ff8200'
                        : '#cccccc',
                  }}
                ></div>
              </div>
              <p className="ms-3">Bottom right</p>
            </div>
          </div>
        </div>
      </BlockStack>
    </Card>
  );
};

export default ConfigWidgetButton;
