import React from 'react';
import {
  Text,
  ButtonGroup,
  InlineStack,
  Divider,
  buttonsFrom,
  buttonFrom,
  type ComplexAction,
  ButtonProps,
} from '@shopify/polaris';

export interface LegacyCardSectionProps {
  title?: React.ReactNode;
  children?: React.ReactNode;
  actions?: (ComplexAction & Partial<ButtonProps>)[];
  divider?: boolean;
}

export default function Section({
  children,
  title,
  actions,
  divider = true,
}: LegacyCardSectionProps) {
  const actionMarkup = actions ? (
    <ButtonGroup>{buttonsFrom(actions, { variant: 'plain' })}</ButtonGroup>
  ) : null;
  // const actionMarkup = actions ? (
  //   <ButtonGroup>
  //     {actions.map((action, index) => {
  //       const overrides: Partial<ButtonProps> = { variant: 'plain' };
  //       if (action.destructive) {
  //         overrides.tone = 'critical';
  //       }

  //       return buttonFrom(action, overrides, index);
  //     })}
  //   </ButtonGroup>
  // ) : null;

  const titleMarkup =
    typeof title === 'string' ? (
      <Text variant="headingSm" as="h3" fontWeight="medium">
        {title}
      </Text>
    ) : (
      title
    );

  const titleAreaMarkup =
    titleMarkup || actionMarkup ? (
      <div className="pb-2">
        {actionMarkup ? (
          <InlineStack align="space-between" blockAlign="center">
            {titleMarkup}
            {actionMarkup}
          </InlineStack>
        ) : (
          titleMarkup
        )}
      </div>
    ) : null;

  return (
    <>
      <div className="p-4">
        {titleAreaMarkup}
        {children}
      </div>
      {divider ? (
        <div className="last:hidden">
          <Divider />
        </div>
      ) : null}
    </>
  );
}
