import React, { isValidElement } from 'react';
import {
  ButtonGroup,
  Text,
  InlineStack,
  Divider,
  Box,
  Breadcrumbs,
  buttonsFrom,
  type DisableableAction,
  BreadcrumbsProps,
} from '@shopify/polaris';

export interface LegacyCardHeaderProps {
  title?: React.ReactNode;
  actions?: DisableableAction[];
  children?: React.ReactNode;
  backAction?: BreadcrumbsProps['backAction'];
}

export default function Header({
  children,
  title,
  actions,
  backAction,
}: LegacyCardHeaderProps) {
  const actionMarkup = actions ? (
    <ButtonGroup>{buttonsFrom(actions, { variant: 'plain' })}</ButtonGroup>
  ) : null;

  const backActionMarkup = backAction ? (
    <Box maxWidth="100%" paddingInlineEnd="100" printHidden>
      <Breadcrumbs backAction={backAction} />
    </Box>
  ) : null;

  const titleMarkup = isValidElement(title) ? (
    title
  ) : (
    <Text variant="headingSm" as="h2">
      {title}
    </Text>
  );

  const headingMarkup =
    actionMarkup || children ? (
      <InlineStack
        wrap={false}
        gap="200"
        align="space-between"
        blockAlign="center"
      >
        <InlineStack wrap={false} gap="100" blockAlign="center">
          {backActionMarkup}
          {titleMarkup}
        </InlineStack>
        <InlineStack wrap={false} gap="400" blockAlign="center">
          {actionMarkup}
          {children}
        </InlineStack>
      </InlineStack>
    ) : (
      <InlineStack wrap={false} gap="100" blockAlign="center">
        {backActionMarkup}
        {titleMarkup}
      </InlineStack>
    );

  return (
    <div className="">
      <div className="p-4">{headingMarkup}</div>
      {title ? <Divider /> : null}
    </div>
  );
}
