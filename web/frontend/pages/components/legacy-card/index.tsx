import { ReactNode, useCallback, useState } from 'react';
import {
  BreadcrumbsProps,
  Button,
  ButtonGroup,
  ActionList,
  Card,
  ComplexAction,
  DisableableAction,
  Popover,
  buttonFrom,
} from '@shopify/polaris';
import cx from 'classnames';

import Header from './header';
import Section from './section';
import { useTranslation } from 'react-i18next';

export type LegacyCardProps = {
  title?: ReactNode;
  children?: ReactNode;
  actions?: DisableableAction[];
  sectioned?: boolean;
  backAction?: BreadcrumbsProps['backAction'];
  primaryFooterAction?: ComplexAction;
  secondaryFooterActions?: ComplexAction[];
  secondaryFooterActionsDisclosureText?: string;
  footerActionAlignment?: 'right' | 'left';
};

export default function LegacyCard(props: LegacyCardProps) {
  const { t } = useTranslation();
  const {
    title,
    children,
    actions,
    sectioned,
    backAction,
    primaryFooterAction,
    secondaryFooterActions,
    secondaryFooterActionsDisclosureText,
    footerActionAlignment = 'right',
  } = props;

  const [secondaryActionsPopoverOpen, setSecondaryActionsPopoverOpen] =
    useState(false);

  const toggleSecondaryActionsPopoverOpen = useCallback(
    () => setSecondaryActionsPopoverOpen(!secondaryActionsPopoverOpen),
    [secondaryActionsPopoverOpen],
  );

  const headerMarkup =
    title || actions ? (
      <Header actions={actions} title={title} backAction={backAction} />
    ) : null;
  const content = sectioned ? <Section>{children}</Section> : children;

  const primaryFooterActionMarkup = primaryFooterAction
    ? buttonFrom(primaryFooterAction, { variant: 'primary' })
    : null;

  let secondaryFooterActionsMarkup = null;
  if (secondaryFooterActions && secondaryFooterActions.length) {
    if (secondaryFooterActions.length === 1) {
      secondaryFooterActionsMarkup = buttonFrom(secondaryFooterActions[0]);
    } else {
      secondaryFooterActionsMarkup = (
        <>
          <Popover
            active={secondaryActionsPopoverOpen}
            activator={
              <Button disclosure onClick={toggleSecondaryActionsPopoverOpen}>
                {secondaryFooterActionsDisclosureText || t('More')}
              </Button>
            }
            onClose={toggleSecondaryActionsPopoverOpen}
          >
            <ActionList items={secondaryFooterActions} />
          </Popover>
        </>
      );
    }
  }

  const footerMarkup =
    primaryFooterActionMarkup || secondaryFooterActionsMarkup ? (
      <div
        className={cx('flex p-4 pt-0', {
          'justify-start': footerActionAlignment === 'left',
          'justify-end': footerActionAlignment === 'right',
        })}
      >
        {footerActionAlignment === 'right' ? (
          <ButtonGroup>
            {secondaryFooterActionsMarkup}
            {primaryFooterActionMarkup}
          </ButtonGroup>
        ) : (
          <ButtonGroup>
            {primaryFooterActionMarkup}
            {secondaryFooterActionsMarkup}
          </ButtonGroup>
        )}
      </div>
    ) : null;

  return (
    <Card padding={'0'}>
      {headerMarkup}
      {content}
      {footerMarkup}
    </Card>
  );
}

LegacyCard.Header = Header;
LegacyCard.Section = Section;
