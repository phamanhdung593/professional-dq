import { Card, Page } from '@shopify/polaris';
import { useNavigate } from 'react-router-dom';
import { AlertCircleIcon, QuestionCircleIcon } from '@shopify/polaris-icons';
import { ESessionStorageKeys } from '../../types/product';
import { useEffect, useState } from 'react';
import { FaqMorePageUpdateApi } from '../../types/faq-more-page';
import { BannerWithDismiss } from '../components/banner-with-dismiss/BannerWithDismiss';
import CartPage from './component/CartPage';
import ChooseFaqs from './component/ChooseFaqs';
import CmsPage from './component/CmsPage';
import CollectionPage from './component/CollectionPage';
import HomePage from './component/HomePage';
import { useUserApi } from '../../hook/user';
import { IUser } from '../../types/user';
import {
  useListFaqPageMoreSettingApi,
  useUpdateFaqPageMoreSettingApi,
} from '../../hook/faq_more_page_setting';
import { useAppBridge } from '@shopify/app-bridge-react';
import {
  useCreateFaqPageMoreApi,
  useListFaqPageMoreApi,
} from '../../hook/faq_more_page';
import BannerMessage from '../components/banner-message';

export default function FaqMorePage() {
  const app = useAppBridge();
  const navigate = useNavigate();
  const [pageSelected, setPageSelected] = useState<string>('index');
  const [homePageVisible, setHomePageVisible] = useState<boolean>(false);
  const [cartPageVisible, setCartPageVisible] = useState<boolean>(false);
  const [cmsPageVisible, setCmsPageVisible] = useState<boolean>(false);
  const [addFaqsIsLoading, setAddFaqsIsLoading] = useState<boolean>(false);
  const [collectionPageVisible, setCollectionPageVisible] =
    useState<boolean>(false);
  const [faqMorePage, setFaqMorePage] = useState<FaqMorePageUpdateApi[]>([]);
  const [showModal, setShowModal] = useState<boolean>(false);
  const [faqsMorePageSelected, setFaqsMorePageSelected] = useState<
    FaqMorePageUpdateApi[]
  >([]);

  const { data } = useUserApi();
  const user = data as IUser;
  const { data: ListFaqPageMore } = useListFaqPageMoreApi(pageSelected) as any;
  const { data: dataFaqSettingMorePage } = useListFaqPageMoreSettingApi();
  const { mutate: createMorePage } = useCreateFaqPageMoreApi();
  const { mutate: UpdateFaqPageMoreSetting } = useUpdateFaqPageMoreSettingApi();
  const [currentPlan, setCurrentPlan] = useState<any>('free');
  const IdSettingMorePage = dataFaqSettingMorePage?.[0]?.id;

  useEffect(() => {
    setFaqMorePage(ListFaqPageMore?.data);
    user?.merchants_plan?.plan
      ? setCurrentPlan(user?.merchants_plan?.plan.toLocaleLowerCase())
      : setCurrentPlan('free');
  }, [ListFaqPageMore, user]);

  useEffect(() => {
    setHomePageVisible(user?.faq_more_page_setting?.home_page_visible);
    setCmsPageVisible(user?.faq_more_page_setting?.cms_page_visible);
    setCartPageVisible(user?.faq_more_page_setting?.cart_page_visible);
    setCollectionPageVisible(
      user?.faq_more_page_setting?.collection_page_visible,
    );
  }, [user]);

  const updateFaqMorePageSetting = async () => {
    const dataUpdate = {
      user_id: user?.id,
      cart_page_visible: cartPageVisible,
      cms_page_visible: cmsPageVisible,
      home_page_visible: homePageVisible,
      collection_page_visible: collectionPageVisible,
    };

    UpdateFaqPageMoreSetting(
      { id: IdSettingMorePage, ...dataUpdate },
      {
        onSuccess: () => {
          app.toast.show('Faq-More-Page was update!');
        },
        onError: () => {
          app.toast.show('Faq-More-Page was update error!');
        },
      },
    );
  };

  const props = {
    currentPlan,
    homePageVisible,
    cartPageVisible,
    cmsPageVisible,
    collectionPageVisible,
    faqMorePage,
    setCartPageVisible,
    setCmsPageVisible,
    setCollectionPageVisible,
    setHomePageVisible,
    pageSelected,
    showModal,
    setShowModal,
    faqsMorePageSelected,
    setFaqsMorePageSelected,
    updateFaqMorePageSetting,
  };

  const Save = () => {
    const dataUpdate = {
      user_id: user?.id,
      cart_page_visible: cartPageVisible,
      cms_page_visible: cmsPageVisible,
      home_page_visible: homePageVisible,
      collection_page_visible: collectionPageVisible,
    };

    setAddFaqsIsLoading(true);
    UpdateFaqPageMoreSetting(
      { id: IdSettingMorePage, ...dataUpdate },
      {
        onSuccess: () => {
          setAddFaqsIsLoading(false);
          app.toast.show('Faq-More-Page was update!');
        },
        onError: () => {
          app.toast.show('Faq-More-Page was update error!');
        },
      },
    );

    if (!(faqsMorePageSelected.length > 0)) {
      return;
    }

    const existingFaqIds = faqMorePage?.map((item) => item.faq_id) || [];
    const newFaqIds = faqsMorePageSelected.map((ele) => ele.faq_id);
    const dataCreate = Array.from(new Set([...existingFaqIds, ...newFaqIds]));

    if (dataCreate.length === 0) {
      return;
    }

    createMorePage(
      { faq_ids: dataCreate, page_name: pageSelected },
      {
        onSuccess: () => {
          setFaqsMorePageSelected([]);
          app.toast.show('Faq-More-Page was created!');
        },
        onError: () => {
          app.toast.show('Faq-More-Page was error!');
        },
      },
    );
  };

  return (
    <>
      <Page
        title="Show FAQs on different pages of store"
        backAction={{ content: '', onAction: () => navigate(-1) }}
        primaryAction={{
          content: 'Save',
          onAction: () => Save(),
          loading: addFaqsIsLoading,
          disabled: !(
            currentPlan === 'pro' ||
            currentPlan === 'ultimate' ||
            currentPlan === 'free_01' ||
            currentPlan === 'free extra'
          ),
        }}
        secondaryActions={[
          {
            content: 'Add Faqs block',
            icon: QuestionCircleIcon,
            onAction: () => navigate('/documents/add-faq-to-other-pages'),
          },
        ]}
      >
        {currentPlan == 'free' && (
          <div className="pb-3">
            <BannerMessage
              title={'Access FAQs on different pages of store'}
              message={
                'Upgrade plan to add faqs to different pages and get full access to other pro features. Cancel anytime.'
              }
              type="warning"
              actions={[
                {
                  content: 'Upgrade Now!',
                  onAction: () => navigate('/plans'),
                  variant: 'primary',
                },
              ]}
            />
          </div>
        )}

        <BannerWithDismiss
          icon={AlertCircleIcon}
          title="FAQs on different pages of store"
          sessionKey={ESessionStorageKeys.SSK_FAQ_MORE_PAGE_BANNER}
        >
          <p>
            By default your FAQs get a dedicated page. Following feature allows
            you to show FAQs on other pages of your store. (ex. Homepage, Cart
            etc...). Note that this feature only works with Online Store 2.0
            compatible themes and don't forget to add a faq block to page
            detail.
          </p>
        </BannerWithDismiss>
        <Card>
          <div className="flex mb-3">
            <div
              className="px-2 py-1 cursor-pointer rounded-[8px] document-button"
              style={{
                backgroundColor: `${
                  pageSelected === 'index' ? '#eeeeee' : '#fff'
                }`,
              }}
              onClick={() => {
                setPageSelected('index');
              }}
            >
              Add on Home page
            </div>
            <div
              className="ms-2 px-2 py-1 cursor-pointer rounded-[8px] document-button"
              style={{
                backgroundColor: `${
                  pageSelected === 'cms' ? '#eeeeee' : '#fff'
                }`,
              }}
              onClick={() => {
                setPageSelected('cms');
              }}
            >
              Add on CMS page
            </div>
            <div
              className="ms-2 px-2 py-1 cursor-pointer rounded-[8px] document-button"
              style={{
                backgroundColor: `${
                  pageSelected === 'cart' ? '#eeeeee' : '#fff'
                }`,
              }}
              onClick={() => {
                setPageSelected('cart');
              }}
            >
              Add on Cart page
            </div>
            <div
              className="ms-2 px-2 py-1 cursor-pointer rounded-[8px] document-button"
              style={{
                backgroundColor: `${
                  pageSelected === 'collection' ? '#eeeeee' : '#fff'
                }`,
              }}
              onClick={() => {
                setPageSelected('collection');
              }}
            >
              Add on Collection page
            </div>
          </div>
          {pageSelected === 'cart' && <CartPage {...props}></CartPage>}
          {pageSelected === 'index' && <HomePage {...props}></HomePage>}
          {pageSelected === 'collection' && (
            <CollectionPage {...props}></CollectionPage>
          )}
          {pageSelected === 'cms' && <CmsPage {...props}></CmsPage>}
        </Card>
      </Page>
      <ChooseFaqs {...props}></ChooseFaqs>
    </>
  );
}
