import {
  Card,
  ResourceList,
  ResourceItem,
  ResourceListProps,
  Modal,
} from '@shopify/polaris';

import { useEffect, useState } from 'react';
import { FaqMorePageUpdateApi } from '../../../types/faq-more-page';
import { useListFaqApi } from '../../../hook/faq';
import { FaqV2 } from '../../../types/faq';
import { useGetCategoryApi } from '../../../hook/category';
import { Category } from '../../../types/category';

export default function ChooseFaqs(props: any) {
  const { data: listFaqs } = useListFaqApi({});

  const faqsData: FaqV2[] = [];

  if (listFaqs) {
    listFaqs.map((faq: FaqV2) => {
      faqsData.push(faq);
    });
  }

  const { data: dataCategories } = useGetCategoryApi({});

  const categoriesData: Category[] = [];

  if (dataCategories) {
    dataCategories.map((faq: Category) => {
      categoriesData.push(faq);
    });
  }

  const [selectedItems, setSelectedItems] = useState<
    ResourceListProps['selectedItems']
  >([]);
  const [faqs, setFaqsData] = useState(faqsData);

  useEffect(() => {
    const newFaqs: any = [];

    if (faqsData.length > 0) {
      faqsData.forEach((item) => {
        newFaqs.push({
          ...item,
          id: item.id ? item.id.toString() : '',
        });
      });
    }

    if (categoriesData.length > 0) {
      categoriesData.forEach((category) => {
        newFaqs.forEach((faq: any) => {
          if (category.identify === faq.category_identify) {
            faq.category_name = category.title || '';
          }
        });
      });
    }

    setFaqsData(newFaqs);
  }, [listFaqs, dataCategories]);

  const addFaqs = () => {
    if (selectedItems && faqsData.length > 0 && faqsData) {
      const listFaqs: FaqMorePageUpdateApi[] = [];
      faqsData.forEach((item) => {
        if (selectedItems.indexOf(item.id?.toString() || '') !== -1) {
          listFaqs.push({
            category_identify: item.category_identify || '',
            faq_id: parseInt(item.id.toString()) || 0,
            faq_identify: item.identify || '',
            faq_title: item.title || '',
            page_name: props.pageSelected,
          });
        }
      });
      props.setFaqsMorePageSelected(listFaqs);
      handleCloseModal();
    }
  };

  const handleCloseModal = () => {
    setSelectedItems([]);
    props.setShowModal(false);
  };

  return (
    <Modal
      open={props.showModal}
      onClose={() => handleCloseModal()}
      title="Choose Faqs"
      primaryAction={{
        content: 'Add Faqs',
        onAction: () => addFaqs(),
      }}
      secondaryActions={[
        {
          content: 'Cancel',
          onAction: () => handleCloseModal(),
        },
      ]}
    >
      <Modal.Section>
        <Card padding={'0'}>
          <ResourceList
            selectable
            onSelectionChange={setSelectedItems}
            selectedItems={selectedItems}
            resourceName={{ singular: 'faq', plural: 'faqs' }}
            items={faqs}
            renderItem={(item: any) => {
              return (
                <ResourceItem
                  id={item.id.toString() || ''}
                  accessibilityLabel={`View details for ${item.title}`}
                  url={''}
                  name={item.title}
                >
                  <p className="font-bold underline text-blue-600">
                    {item.title}
                  </p>
                  {item.category_name && (
                    <div className="opacity-70 pointer-events-none text-xs">
                      {item.category_name}
                    </div>
                  )}
                </ResourceItem>
              );
            }}
          />
        </Card>
      </Modal.Section>
    </Modal>
  );
}
