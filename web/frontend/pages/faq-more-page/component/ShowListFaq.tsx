import {
  BlockStack,
  Button,
  EmptyState,
  Modal,
  Tooltip,
} from '@shopify/polaris';
import { useEffect, useState } from 'react';

import { DeleteIcon } from '@shopify/polaris-icons';
import {
  FaqMorePageProps,
  FaqMorePageUpdateApi,
} from '../../../types/faq-more-page';
import { FaqV2 } from '../../../types/faq';
import { useListFaqApi } from '../../../hook/faq';
import { useGetCategoryApi } from '../../../hook/category';
import { Category } from '../../../types/category';
import { useDeleteFaqMorePageApi } from '../../../hook/faq_more_page';

export default function ShowListFaq(props: FaqMorePageProps) {
  const { mutate: deleteFaqMorePageID } = useDeleteFaqMorePageApi();
  const [indexProductFaqHover, setIndexProductFaqHover] = useState<number>(-1);
  const [faqsMorePage, setFaqsMorePage] = useState<any>(props?.faqMorePage);
  const [faqSelected, setFaqsMorePageSelected] = useState<
    FaqMorePageUpdateApi[]
  >([]);
  const [openModal, setOpenModal] = useState<boolean>(false);
  const [selectedFaqId, setSelectedFaqId] = useState<string>('');

  const { data: dataCategories } = useGetCategoryApi({});

  const categoriesData: Category[] = [];

  if (dataCategories) {
    dataCategories.map((faq: Category) => {
      categoriesData.push(faq);
    });
  }

  const { data: listFaqs } = useListFaqApi({});

  const faqsData: FaqV2[] = [];

  if (listFaqs) {
    listFaqs.map((faq: FaqV2) => {
      faqsData.push(faq);
    });
  }

  useEffect(() => {
    const newFaqs: FaqMorePageUpdateApi[] = [];

    if (
      !props?.faqsMorePageSelected ||
      props?.faqsMorePageSelected.length < 1
    ) {
      setFaqsMorePageSelected([]);
    }

    props?.faqsMorePageSelected?.forEach((item) => {
      newFaqs?.push({
        ...item,
        faq_id: item.faq_id,
        category_name: '',
        faq_title: item.faq_title,
      });
    });

    if (
      categoriesData &&
      categoriesData?.length > 0 &&
      newFaqs &&
      newFaqs?.length > 0
    ) {
      categoriesData?.forEach((category) => {
        newFaqs?.forEach((faq) => {
          if (category?.identify === faq?.category_identify) {
            faq.category_name = category.title || '';
          }
        });
      });
    }

    const [resultArray1] = deleteCommonObjects(newFaqs, faqsMorePage);

    if (resultArray1.length > 0) {
      setFaqsMorePageSelected(
        resultArray1.filter((item) => item.page_name === props?.pageSelected),
      );
    }
  }, [props.faqsMorePageSelected]);

  function deleteCommonObjects(
    new_faqs: FaqMorePageUpdateApi[],
    faqs_more_page: FaqMorePageUpdateApi[],
  ) {
    const setListId = new Set(faqs_more_page?.map((obj) => obj['faq_id']));
    const result = new_faqs?.filter((obj) => !setListId.has(obj['faq_id']));

    return [result];
  }

  const handleDeleteClick = (id: string) => {
    setSelectedFaqId(id);
    setOpenModal(true);
  };

  const handleModalClose = () => {
    setOpenModal(false);
    setSelectedFaqId('');
  };

  const handleConfirmDelete = () => {
    deleteFaqMorePageID(parseInt(selectedFaqId));
    handleModalClose();
  };

  useEffect(() => {
    const newFaqs: FaqMorePageUpdateApi[] = [];

    if (props?.faqMorePage && props?.faqMorePage.length > 0) {
      props?.faqMorePage
        ?.filter((item) => item.page_name === props.pageSelected)
        .forEach((item) => {
          newFaqs.push({
            ...item,
            faq_id: item.faq_id,
            category_name: '',
            faq_title: item.faq_title,
          });
        });
    }

    if (newFaqs?.length === 0) {
      setFaqsMorePage([]);
      return;
    }

    if (faqsData && faqsData?.length > 0) {
      faqsData?.forEach((item) => {
        newFaqs?.forEach((faq) => {
          if (item.id === faq.faq_id) {
            faq.faq_title = item.title || '';
          }
        });
      });
    }

    if (categoriesData && categoriesData?.length > 0) {
      categoriesData?.forEach((category) => {
        newFaqs?.forEach((faq) => {
          if (category.identify === faq.category_identify) {
            faq.category_name = category.title || '';
          }
        });
      });
    }
    setFaqsMorePage(
      newFaqs?.filter((item) => item.page_name === props.pageSelected),
    );
  }, [props.pageSelected, props.faqMorePage]);

  const deleteFaqSelected = (id: number) => {
    setFaqsMorePageSelected(faqSelected?.filter((item) => item.faq_id !== id));
  };

  return (
    <BlockStack gap={'200'}>
      {faqsMorePage?.length > 0 || faqSelected?.length > 0 ? (
        <>
          {faqsMorePage.map((item: any, index: number) => (
            <div
              key={index}
              className="flex justify-between border p-2 rounded-[8px]"
            >
              <div>
                <p className="font-bold text-blue-600">{item.faq_title}</p>
                {item.category_name && (
                  <div className="opacity-70 pointer-events-none text-xs">
                    {item.category_name}
                  </div>
                )}
              </div>
              <div>
                <Tooltip content="Delete">
                  <div
                    onClick={() => handleDeleteClick(item.id?.toString() || '')}
                    onMouseOver={() => setIndexProductFaqHover(index)}
                    onMouseOut={() => setIndexProductFaqHover(-1)}
                  >
                    <Button
                      tone={
                        indexProductFaqHover === index ? 'critical' : 'success'
                      }
                      icon={DeleteIcon}
                    ></Button>
                  </div>
                </Tooltip>
              </div>
            </div>
          ))}
          {faqSelected?.length > 0 && (
            <div className="mt-3">
              <p className="mb-1">New add</p>
              <BlockStack gap={'200'}>
                {faqSelected.map((item, index) => (
                  <div
                    key={index}
                    className="flex justify-between border p-2 rounded-[8px]"
                  >
                    <div>
                      <p className="font-bold text-blue-600">
                        {item.faq_title}
                      </p>
                      {item.category_name && (
                        <div className="opacity-70 pointer-events-none text-xs">
                          {item.category_name}
                        </div>
                      )}
                    </div>
                    <div>
                      <Tooltip content="Delete">
                        <div
                          onClick={() => deleteFaqSelected(item.faq_id)}
                          onMouseOver={() => setIndexProductFaqHover(index)}
                          onMouseOut={() => setIndexProductFaqHover(-1)}
                        >
                          <Button
                            tone={
                              indexProductFaqHover === index
                                ? 'critical'
                                : 'success'
                            }
                            icon={DeleteIcon}
                          ></Button>
                        </div>
                      </Tooltip>
                    </div>
                  </div>
                ))}
              </BlockStack>
            </div>
          )}
        </>
      ) : (
        <>
          {faqSelected?.length === 0 && faqsMorePage?.length === 0 && (
            <div>
              <EmptyState
                heading="No FAQs"
                image="https://cdn.shopify.com/s/files/1/0262/4071/2726/files/emptystate-files.png"
              ></EmptyState>
            </div>
          )}
        </>
      )}

      <Modal
        open={openModal}
        onClose={handleModalClose}
        title="Delete FAQ"
        primaryAction={{
          content: 'Delete',
          onAction: handleConfirmDelete,
          destructive: true,
        }}
        secondaryActions={[
          {
            content: 'Cancel',
            onAction: handleModalClose,
          },
        ]}
      >
        <Modal.Section>
          <p>Are you sure you want to delete this FAQ?</p>
        </Modal.Section>
      </Modal>
    </BlockStack>
  );
}
