import { Checkbox, Button, Spinner } from '@shopify/polaris';
import ShowListFaq from './ShowListFaq';

export default function CmsPage(props: any) {
  return (
    <div>
      <div className="flex items-center justify-between">
        <Checkbox
          label="Show FAQs on Cms page"
          checked={props.cmsPageVisible}
          onChange={(value) => props.setCmsPageVisible(value)}
          disabled={
            !(
              props.currentPlan === 'pro' ||
              props.currentPlan === 'ultimate' ||
              props.currentPlan === 'free_01' ||
              props.currentPlan === 'free extra'
            )
          }
        />
        <Button
          onClick={() => props.setShowModal(true)}
          variant="primary"
          disabled={
            !(
              props.currentPlan === 'pro' ||
              props.currentPlan === 'ultimate' ||
              props.currentPlan === 'free_01' ||
              props.currentPlan === 'free extra'
            )
          }
        >
          Add Faqs
        </Button>
      </div>
      <div className="mt-3">
        {!props.loading ? (
          props.currentPlan !== 'free' ? (
            <ShowListFaq {...props}></ShowListFaq>
          ) : null
        ) : (
          <div className="h-[350px] flex items-center justify-center">
            <Spinner accessibilityLabel="Spinner example" size="large" />;
          </div>
        )}
      </div>
    </div>
  );
}
