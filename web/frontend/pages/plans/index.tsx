import { useEffect, useState, useCallback } from 'react';
import {
  Page,
  Layout,
  InlineGrid,
  BlockStack,
  Button,
  Modal,
} from '@shopify/polaris';
import Reviews from './Reviews';
import { PlanType, PlanCardProps } from '../../types/plan';
import { t } from 'i18next';
import {
  useCancelApi,
  useCheckPaymentApi,
  useSubscriptionsApi,
} from '../../hook/plans';
import { useAppBridge } from '@shopify/app-bridge-react';
import { useUserApi } from '../../hook/user';
import { IUser } from '../../types/user';

export function Plans() {
  const [currentPlan, setCurrentPlan] = useState<any>('free');
  const [plans, setPlans] = useState<PlanType>();
  const [active, setActive] = useState(false);
  const [cancelModal, setCancelModal] = useState(false);
  const [plan_id, setPlan_id] = useState<number>();
  const { mutate: cancelPlan } = useCancelApi();
  const { mutate: Subscriptions } = useSubscriptionsApi();
  const { mutate: UpdatePlan } = useCheckPaymentApi();
  const app = useAppBridge();
  const { data } = useUserApi();
  const user = data as IUser;

  function getChargeId(): string | null {
    const urlParams = new URLSearchParams(window.location.search);
    return urlParams.get('charge_id');
  }

  useEffect(() => {
    const chargeId = getChargeId();
    if (chargeId) {
      UpdatePlan(
        {
          subscription_id: `gid://shopify/AppSubscription/${chargeId}`,
        },
        {
          onSuccess() {
            app.toast.show('Update plan success!');
          },
          onError: () => {
            app.toast.show('Update plan error!');
          },
        },
      );
    }
  }, []);

  useEffect(() => {
    if (plans?.confirmationUrl) {
      window.open(plans?.confirmationUrl, '_top');
    }

    user?.merchants_plan?.plan
      ? setCurrentPlan(user?.merchants_plan?.plan.toLocaleLowerCase())
      : setCurrentPlan('free');
  }, [plans, user]);

  const handleCancel = async () => {
    cancelPlan(
      { subscription_id: user?.merchants_plan?.shopify_plan_id },
      {
        onSuccess() {
          app.toast.show('Cancel plan success!');
        },
        onError: () => {
          app.toast.show('Cancel plan error!');
        },
      },
    );
    setCancelModal(false);
  };

  const handlePlanClick = () => {
    Subscriptions(
      { plan_id: plan_id },
      {
        onSuccess(data: any) {
          const confirmationUrl = data?.data?.appSubscriptionCreate;
          console.log('confirmationUrl', confirmationUrl);
          setPlans(confirmationUrl);
        },
        onError: () => {
          app.toast.show('Subscription error!');
        },
      },
    );
  };

  const handleShowModal = useCallback(() => setActive(!active), [active]);

  const handleShowModalCancel = useCallback(
    () => setCancelModal(!cancelModal),
    [cancelModal],
  );

  const PlanCard = (props: PlanCardProps) => {
    return (
      <div
        className={`${
          props.isCurrentPlan === true || props.currentPlanFree === true
            ? 'border-4 border-[#007F5F] bg-[#F1F8F5]'
            : 'border border-white bg-white'
        } p-10 text-center w-full mx-auto rounded-lg `}
      >
        <h2 style={{ color: '#f29500', fontSize: '24px', fontWeight: 'bold' }}>
          {props.title}
        </h2>
        <p className="text-3xl my-2.5 font-bold py-5">{props.price}</p>
        <p style={{ paddingBottom: '20px' }}>{props.pricePeriod}</p>
        <Button
          disabled={props.isCurrentPlan || props.isCurrentPlan === 0}
          onClick={() => {
            props.handleSubmitPlan();
            handleShowModal();
          }}
        >
          {props.isCurrentPlan ? 'Current Plan' : props.buttonText}
        </Button>
        <ul className="list-none pt-5 text-xs text-left">
          {props.features.map((feature, index) => (
            <li
              key={index}
              style={{
                marginBottom: '15px',
                marginRight: '15px',
                display: 'flex',
                alignItems: 'center',
              }}
            >
              <span style={{ color: '#f29500', marginRight: '8px' }}>✓</span>
              {feature}
            </li>
          ))}
        </ul>
        {props.isCurrentPlan ? (
          props.handleCancel === null ? null : (
            <Button
              onClick={() => {
                handleShowModalCancel();
                props.handleCancel;
              }}
            >
              Cancel
            </Button>
          )
        ) : null}
      </div>
    );
  };

  return (
    <Page title={t('Plans')}>
      <Layout sectioned>
        <Modal
          open={cancelModal}
          onClose={handleShowModalCancel}
          title="Cancel"
          primaryAction={{
            content: 'Cancel',
            onAction: handleCancel,
          }}
        >
          <div className="flex flex-col justify-center items-center p-3">
            <div className="text-xl">
              Are you sure you want to switch to the free plan?
            </div>
          </div>
        </Modal>

        <Modal
          open={active}
          onClose={handleShowModal}
          title="Upgrade"
          primaryAction={{
            content: 'Upgrade',
            onAction: handlePlanClick,
          }}
        >
          <Modal.Section>
            {plan_id === 1 ? (
              <div className="h-[400px] ml-10">
                <InlineGrid gap="400" columns={2}>
                  <div className="flex flex-col justify-center items-center">
                    <div className="mb-2">{`PROFESSIONAL (Pro)`}</div>
                    <div className="text-2xl">$2.99 /month</div>
                  </div>
                  <div>
                    <img
                      src="https://cdn.gift-card.vify.io/assets/upgrade-image-7ed22547.svg"
                      alt=""
                    ></img>
                  </div>
                </InlineGrid>
                <div>
                  <ul className="space-y-3 text-sm">
                    <li className="flex items-center">
                      <span className="text-green-500 font-bold mr-2">✓</span>{' '}
                      Everything in FREE plus
                    </li>
                    <li className="flex items-center">
                      <span className="text-green-500 font-bold mr-2">✓</span>{' '}
                      Widget
                    </li>
                    <li className="flex items-center">
                      <span className="text-green-500 font-bold mr-2">✓</span>7
                      days free trial
                    </li>
                    <li className="flex items-center">
                      <span className="text-green-500 font-bold mr-2">✓</span>{' '}
                      Unlimited FAQs
                    </li>
                    <li className="flex items-center">
                      <span className="text-green-500 font-bold mr-2">✓</span>{' '}
                      Pro template access
                    </li>
                    <li className="flex items-center">
                      <span className="text-green-500 font-bold mr-2">✓</span>{' '}
                      Translations support!
                    </li>
                    <li className="flex items-center">
                      <span className="text-green-500 font-bold mr-2">✓</span>{' '}
                      Show FAQs on any page
                    </li>
                    <li className="flex items-center">
                      <span className="text-green-500 font-bold mr-2">✓</span>{' '}
                      Unlimited FAQs on product pages (Only Online Store 2.0)
                    </li>
                    <li className="flex items-center">
                      <span className="text-green-500 font-bold mr-2">✓</span>{' '}
                      Remove WaterMark
                    </li>
                  </ul>
                </div>
              </div>
            ) : null}
            {plan_id === 2 ? (
              <div className="h-[400px] ml-10">
                <InlineGrid gap="400" columns={2}>
                  <div className="flex flex-col justify-center items-center">
                    <div className="mb-2">{`PROFESSIONAL (Ultimate)`}</div>
                    <div className="text-2xl">$9.99 /month</div>
                  </div>
                  <div>
                    <img
                      src="https://cdn.gift-card.vify.io/assets/upgrade-image-7ed22547.svg"
                      alt=""
                    ></img>
                  </div>
                </InlineGrid>
                <div>
                  {' '}
                  <ul className="space-y-3 text-sm">
                    <li className="flex items-center">
                      <span className="text-green-500 font-bold mr-2">✓</span>{' '}
                      Everything in PRO plus
                    </li>
                    <li className="flex items-center">
                      <span className="text-green-500 font-bold mr-2">✓</span>{' '}
                      Build a FAQ page on demand
                    </li>
                    <li className="flex items-center">
                      <span className="text-green-500 font-bold mr-2">✓</span>{' '}
                      Priority support
                    </li>{' '}
                  </ul>
                </div>
              </div>
            ) : null}
          </Modal.Section>
        </Modal>

        <BlockStack gap={'500'}>
          <Page>
            <div>
              <InlineGrid gap={'400'} columns={{ lg: 3 }}>
                <PlanCard
                  title="FREE"
                  price="$0"
                  pricePeriod="USD / mo"
                  buttonText="Continue with Free plan"
                  features={[
                    'Free 15 FAQs',
                    'Custom CSS',
                    'Import / Export FAQs',
                    'Unlimited categories',
                    'Unlimited customization',
                    'Search bar for customers',
                    'Rearrange FAQs / Categories',
                    '15 FAQs on product pages (Only Online Store 2.0)',
                    'Google SEO snippets',
                  ]}
                  currentPlanFree={currentPlan === undefined}
                  isCurrentPlan={currentPlan === 'free' || 0}
                  handleSubmitPlan={() => {
                    setPlan_id(0);
                  }}
                  handleCancel={null}
                />
                <PlanCard
                  title="PRO"
                  price="$2.99"
                  pricePeriod="USD / mo"
                  buttonText="Start 7-day free trial"
                  features={[
                    'Everything in FREE plus',
                    'Widget',
                    '7 days free trial',
                    'Unlimited FAQs',
                    'Pro template access',
                    'Translations support!',
                    'Show FAQs on any page',
                    'Unlimited FAQs on product pages (Only Online Store 2.0)',
                    'Remove WaterMark',
                  ]}
                  currentPlanFree={false}
                  isCurrentPlan={currentPlan === 'pro'}
                  handleSubmitPlan={() => {
                    setPlan_id(1);
                  }}
                  handleCancel={() => {
                    handleCancel();
                  }}
                />
                <PlanCard
                  title="ULTIMATE"
                  price="$9.99"
                  pricePeriod="USD / mo"
                  buttonText="Start 7-day free trial"
                  features={[
                    'Everything in PRO plus',
                    'Build a FAQ page on demand',
                    'Priority support',
                  ]}
                  currentPlanFree={false}
                  isCurrentPlan={currentPlan === 'ultimate'}
                  handleSubmitPlan={() => {
                    setPlan_id(2);
                  }}
                  handleCancel={() => {
                    handleCancel();
                  }}
                />
              </InlineGrid>
            </div>
          </Page>
        </BlockStack>
      </Layout>
      <Layout>
        <Reviews></Reviews>
      </Layout>
    </Page>
  );
}
