import { Card, Page } from '@shopify/polaris';
import { useTranslation } from 'react-i18next';

export default function NotFound() {
  const { t } = useTranslation();
  return (
    <Page>
      <Card>
        <Card>
          <p>{t('NotFound.description')}</p>
        </Card>
      </Card>
    </Page>
  );
}
