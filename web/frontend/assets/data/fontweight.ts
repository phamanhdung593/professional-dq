export const FontWeight = [
  { label: 'Normal', value: 'normal' },
  { label: 'Bold', value: 'bold' },
];
