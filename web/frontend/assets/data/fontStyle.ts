export const fontStyle = [
  {
    label: 'Uppercased',
    value: 'uppercase',
  },
  {
    label: 'Normal',
    value: 'normal',
  },
  {
    label: 'Lowercased',
    value: 'lowercase',
  },
  {
    label: 'Capitalized',
    value: 'capitalize',
  },
];
