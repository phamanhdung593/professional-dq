export const fontFamily = [
  { label: 'Questrial', value: 'Questrial' },
  { label: 'Time New Roman', value: 'Time New Roman' },
  { label: 'Lucida Sans', value: 'Lucida Sans' },
  { label: 'Courier', value: 'Courier' },
  { label: 'Garamond', value: 'today' },
  { label: 'Arial', value: 'Arial' },
  { label: 'Calibri', value: 'Calibri' },
  { label: 'Playfair Display', value: 'Playfair Display' },
  {
    label: 'Use your store font (not available in live preview)',
    value: 'unset',
  },
];
