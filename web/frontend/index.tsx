import React from 'react';
import ReactDOM from 'react-dom/client';
import { RouterProvider } from 'react-router-dom';
import { router } from './Routes';
import { initI18n } from './utils/i18nUtils';
import ReportWebVitals from './report-web-vital';

initI18n().then(() => {
  ReactDOM.createRoot(document.getElementById('app')!).render(
    <React.StrictMode>
      <RouterProvider router={router} />
    </React.StrictMode>,
  );

  ReportWebVitals();
});
