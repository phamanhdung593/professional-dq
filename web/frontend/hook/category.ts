import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';
import { useFetch } from './api';
import queryString from 'query-string';

export const useCategory = () => {
  const fetcher = useFetch();
  return {
    CreateCategory: async (body: any) => {
      return await fetcher('/api/categories/', {
        method: 'POST',
        body: JSON.stringify(body),
      });
    },

    getCategory: async (searchParams: string) => {
      return await fetcher<any[]>(`/api/categories${searchParams}`, {
        method: 'GET',
      });
    },

    getCategoryId: async (id: string) => {
      return await fetcher(`/api/categories/${id}`, {
        method: 'GET',
      });
    },

    updateCategoryId: async (payload: any) => {
      return await fetcher(`/api/categories/${payload.id}`, {
        method: 'PUT',
        body: JSON.stringify(payload),
      });
    },

    showHideCategory: async (payload: any) => {
      return await fetcher(`/api/categories/show-hide-category/${payload.id}`, {
        method: 'PUT',
        body: JSON.stringify(payload),
      });
    },

    deleteCategoryId: async (id: number) => {
      return await fetcher(`/api/categories/${id}`, {
        method: 'DELETE',
      });
    },

    SortCategory: async (body: any) => {
      return await fetcher('/api/categories/change-position', {
        method: 'POST',
        body: JSON.stringify(body),
      });
    },
  };
};

export const useCategoryApi = () => {
  const { CreateCategory } = useCategory();
  const queryClient = useQueryClient();

  return useMutation({
    mutationFn: CreateCategory,
    onSuccess: () => {
      queryClient.invalidateQueries();
    },
  });
};

export const useGetCategoryApi = (params: object) => {
  const { getCategory } = useCategory();
  const searchParams = `?${queryString.stringify(params, { skipEmptyString: true, arrayFormat: 'bracket' })}`;
  return useQuery({
    queryKey: ['getCategory'],
    queryFn: () => getCategory(searchParams),
  });
};

export const useGetCategoryIdApi = (id: string) => {
  const { getCategoryId } = useCategory();
  return useQuery({
    queryKey: ['getCategory', id],
    queryFn: () => getCategoryId(id),
  });
};

export const useDeleteCategoryApi = () => {
  const { deleteCategoryId } = useCategory();
  return useMutation({
    mutationFn: (id: number) => deleteCategoryId(id),
  });
};

export const useUpdateCategoryApi = () => {
  const { updateCategoryId } = useCategory();
  const queryClient = useQueryClient();

  return useMutation({
    mutationKey: ['UpdateStatus'],
    mutationFn: updateCategoryId,
    onSuccess: () => {
      queryClient.invalidateQueries();
    },
  });
};

export const useShowHideCategoryApi = () => {
  const { showHideCategory } = useCategory();
  const queryClient = useQueryClient();

  return useMutation({
    mutationKey: ['showHideCategory'],
    mutationFn: showHideCategory,
    onSuccess: () => {
      queryClient.invalidateQueries();
    },
  });
};

export const useSortCategoryApi = () => {
  const { SortCategory } = useCategory();
  const queryClient = useQueryClient();

  return useMutation({
    mutationFn: SortCategory,
    onSuccess: () => {
      queryClient.invalidateQueries();
    },
  });
};
