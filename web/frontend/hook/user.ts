import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';
import { useFetch } from './api';

export const useUser = () => {
  const fetcher = useFetch();

  return {
    getUser: async () => {
      return await fetcher<any>('/api/user', {
        method: 'GET',
      });
    },
    syncLocale: async (body: any) => {
      return await fetcher('/api/user/sync', {
        method: 'POST',
        body: JSON.stringify(body),
      });
    },
  };
};

export function useUserApi() {
  const { getUser } = useUser();

  return useQuery({
    queryKey: ['getUser'],
    queryFn: () => getUser(),
  });
}

export const useUserSyncLocaleApi = () => {
  const { syncLocale } = useUser();
  const queryClient = useQueryClient();

  return useMutation({
    mutationFn: syncLocale,
    onSuccess: () => {
      queryClient.invalidateQueries();
    },
  });
};
