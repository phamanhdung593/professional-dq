import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';
import { useFetch } from './api';
import { FaqMessageSettingsApi } from '../types/widget';

export const useWidgetApi = () => {
  const fetcher = useFetch();

  return {
    getWidget: async () => {
      return await fetcher<{
        code: number;
        message: string;
        data: FaqMessageSettingsApi;
      }>('/api/faq-message-settings', {
        method: 'GET',
      });
    },

    updateWidget: async (body: any) => {
      return await fetcher('/api/faq-message-settings/update', {
        method: 'PUT',
        body: JSON.stringify(body),
      });
    },
  };
};

export function useGetWidgetApi() {
  const { getWidget } = useWidgetApi();

  return useQuery({
    queryKey: ['getWidget'],
    queryFn: () => getWidget(),
  });
}

export function useUpdateWidgetApi() {
  const { updateWidget } = useWidgetApi();
  const queryClient = useQueryClient();

  return useMutation({
    mutationFn: updateWidget,
    onSuccess: () => {
      queryClient.invalidateQueries();
    },
  });
}
