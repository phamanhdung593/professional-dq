import { useMutation, useQueryClient } from '@tanstack/react-query';
import { useFetch } from './api';

export const useTemplateSetting = () => {
  const fetcher = useFetch();
  return {
    updateTemplateSettingId: async (payload: any) => {
      return await fetcher(`/api/template-settings/${payload.id}`, {
        method: 'PUT',
        body: JSON.stringify(payload),
      });
    },
  };
};

export const useUpdateTemplateSettingApi = () => {
  const { updateTemplateSettingId } = useTemplateSetting();
  const queryClient = useQueryClient();

  return useMutation({
    mutationKey: ['Template_Setting'],
    mutationFn: updateTemplateSettingId,
    onSuccess: () => {
      queryClient.invalidateQueries();
    },
  });
};
