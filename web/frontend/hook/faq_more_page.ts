import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';
import { useFetch } from './api';

export const useFaqPageMoreApi = () => {
  const fetcher = useFetch();

  return {
    listFaqPageMore: async (pageName?: string) => {
      return await fetcher<any[]>(`/api/faq-more-page?page_name=${pageName}`, {
        method: 'GET',
      });
    },

    createFaqPageMore: async (body: any) => {
      return await fetcher('/api/faq-more-page', {
        method: 'POST',
        body: JSON.stringify(body),
      });
    },

    deleteFaqPageMoreId: async (id: number) => {
      return await fetcher(`/api/faq-more-page/${id}`, {
        method: 'DELETE',
      });
    },
  };
};

export function useListFaqPageMoreApi(pageSelected?: string) {
  const { listFaqPageMore } = useFaqPageMoreApi();

  return useQuery({
    queryKey: ['listFaqPageMoreSetting', pageSelected],
    queryFn: () => listFaqPageMore(pageSelected),
  });
}

export const useCreateFaqPageMoreApi = () => {
  const { createFaqPageMore } = useFaqPageMoreApi();

  return useMutation({
    mutationFn: createFaqPageMore,
  });
};

export const useDeleteFaqMorePageApi = () => {
  const { deleteFaqPageMoreId } = useFaqPageMoreApi();
  const queryClient = useQueryClient();
  return useMutation({
    mutationFn: deleteFaqPageMoreId,
    onSuccess: () => {
      queryClient.invalidateQueries();
    },
  });
};
