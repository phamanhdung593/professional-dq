import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';
import { useFetch } from './api';
import queryString from 'query-string';

import { FaqV2 } from '../types/faq';

export const useFaqsApi = () => {
  const fetcher = useFetch();

  return {
    listFaqs: async (searchParams: string) => {
      return await fetcher<FaqV2[]>(`/api/faqs${searchParams}`, {
        method: 'GET',
      });
    },

    getLocales: async (identify: string) => {
      return await fetcher<any[]>(`/api/faqs/${identify}/locales`, {
        method: 'GET',
      });
    },

    createFaq: async (body: any) => {
      return await fetcher('/api/faqs', {
        method: 'POST',
        body: JSON.stringify(body),
      });
    },

    addFaqProduct: async (body: any) => {
      return await fetcher('/api/faq-products', {
        method: 'POST',
        body: JSON.stringify(body),
      });
    },

    updateFaq: (payload: any) => {
      return fetcher(`/api/faqs/${payload.id}`, {
        method: 'PUT',
        body: JSON.stringify(payload),
      });
    },

    showHideFaq: (payload: any) => {
      return fetcher(`/api/faqs/show-hide-faq/${payload.id}`, {
        method: 'PUT',
        body: JSON.stringify(payload),
      });
    },

    deleteFaqId: async (id: number) => {
      return await fetcher(`/api/faqs/${id}`, {
        method: 'DELETE',
      });
    },

    editFaqs: (id: number, body: FaqV2) => {
      return fetcher(`/api/faqs/${id}`, {
        method: 'PUT',
        body: JSON.stringify(body),
      });
    },

    getFaqsId: async (id: string) => {
      return await fetcher(`/api/faqs/${id}`, {
        method: 'GET',
      });
    },

    importCsv: (payload: FormData) => {
      return fetcher('/api/faqs/import-csv', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
        },
        body: payload,
      });
    },

    exportCsv: (body: object) => {
      return fetcher<Blob>('/api/faqs/export-csv', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
        },
        body: JSON.stringify(body),
      });
    },

    SortFaqs: async (body: any) => {
      return await fetcher('/api/faqs/change-position', {
        method: 'POST',
        body: JSON.stringify(body),
      });
    },
  };
};

export function useListFaqApi(params: object) {
  const { listFaqs } = useFaqsApi();
  const searchParams = `?${queryString.stringify(params, { skipEmptyString: true, arrayFormat: 'bracket' })}`;

  return useQuery({
    queryKey: ['listFaqs'],
    queryFn: () => listFaqs(searchParams),
  });
}

export function useGetLocalesApi(identify: string) {
  const { getLocales } = useFaqsApi();

  return useQuery({
    queryKey: ['getLocales'],
    queryFn: () => getLocales(identify),
  });
}

export const useCreateFaqApi = () => {
  const { createFaq } = useFaqsApi();

  return useMutation({
    mutationFn: createFaq,
  });
};

export const useUpdateFaqApi = () => {
  const { updateFaq } = useFaqsApi();
  const queryClient = useQueryClient();

  return useMutation({
    mutationKey: ['UpdateStatus'],
    mutationFn: updateFaq,
    onSuccess: () => {
      queryClient.invalidateQueries();
    },
  });
};

export const useShowHideFaqApi = () => {
  const { showHideFaq } = useFaqsApi();
  const queryClient = useQueryClient();

  return useMutation({
    mutationKey: ['showHideFaq'],
    mutationFn: showHideFaq,
    onSuccess: () => {
      queryClient.invalidateQueries();
    },
  });
};

export const useDeleteFaqApi = () => {
  const { deleteFaqId } = useFaqsApi();
  const queryClient = useQueryClient();
  return useMutation({
    mutationFn: (id: number) => deleteFaqId(id),
    onSuccess: () => {
      queryClient.invalidateQueries();
    },
  });
};

export const useEditFaqApi = () => {
  const queryClient = useQueryClient();
  const { editFaqs } = useFaqsApi();
  return useMutation({
    mutationFn: (body: any) => editFaqs(body.id, body),
    onSuccess: () => {
      queryClient.invalidateQueries();
    },
  });
};

export const useGetFaqsIdApi = (id: string) => {
  const { getFaqsId } = useFaqsApi();
  return useQuery({
    queryKey: ['getCategory', id],
    queryFn: () => getFaqsId(id),
  });
};

export const useAddFaqProductApi = () => {
  const { addFaqProduct } = useFaqsApi();
  const queryClient = useQueryClient();

  return useMutation({
    mutationKey: ['addFaqProduct'],
    mutationFn: addFaqProduct,
    onSuccess: () => {
      queryClient.invalidateQueries();
    },
  });
};

export function useImportCsv() {
  const { importCsv } = useFaqsApi();
  const queryClient = useQueryClient();

  return useMutation({
    mutationKey: ['importCsv'],
    mutationFn: importCsv,
    onSuccess: () => {
      queryClient.invalidateQueries();
    },
  });
}

export function useExportCsv() {
  const { exportCsv } = useFaqsApi();
  const queryClient = useQueryClient();

  return useMutation({
    mutationKey: ['exportCsv'],
    mutationFn: exportCsv,
    onSuccess: () => {
      queryClient.invalidateQueries();
    },
  });
}
export const useSortFaqsApi = () => {
  const { SortFaqs } = useFaqsApi();
  const queryClient = useQueryClient();

  return useMutation({
    mutationFn: SortFaqs,
    onSuccess: () => {
      queryClient.invalidateQueries();
    },
  });
};
