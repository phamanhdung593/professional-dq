import { useMutation, useQueryClient } from '@tanstack/react-query';
import { useFetch } from './api';

export const usePlan = () => {
  const fetcher = useFetch();
  return {
    subscriptions: async (payload: any) => {
      return await fetcher(`/api/plan/subscribe`, {
        method: 'POST',
        body: JSON.stringify(payload),
      });
    },

    checkPayMent: async (payload: any) => {
      return await fetcher(`/api/plan/detail`, {
        method: 'POST',
        body: JSON.stringify(payload),
      });
    },

    cancelPlan: async (payload: any) => {
      return await fetcher(`/api/plan/cancel`, {
        method: 'POST',
        body: JSON.stringify(payload),
      });
    },
  };
};

export const useSubscriptionsApi = () => {
  const { subscriptions } = usePlan();

  return useMutation({
    mutationKey: ['Subscriptions'],
    mutationFn: subscriptions,
  });
};

export const useCheckPaymentApi = () => {
  const { checkPayMent } = usePlan();
  const queryClient = useQueryClient();

  return useMutation({
    mutationKey: ['checkPayMent'],
    mutationFn: checkPayMent,
    onSuccess: () => {
      queryClient.invalidateQueries();
    },
  });
};

export const useCancelApi = () => {
  const { cancelPlan } = usePlan();
  const queryClient = useQueryClient();

  return useMutation({
    mutationKey: ['cancelPlan'],
    mutationFn: cancelPlan,
    onSuccess: () => {
      queryClient.invalidateQueries();
    },
  });
};
