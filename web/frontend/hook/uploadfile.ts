import { useMutation, useQueryClient } from '@tanstack/react-query';
import { useFetch } from './api';

export const useTemplateSetting = () => {
  const fetcher = useFetch();
  return {
    postMediaUpload: (payload: FormData) => {
      return fetcher('api/settings/upload-image', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
        },
        body: payload,
      });
    },
  };
};

export const useUploadFileApi = () => {
  const { postMediaUpload } = useTemplateSetting();
  const queryClient = useQueryClient();

  return useMutation({
    mutationFn: postMediaUpload,
    onSuccess: () => {
      queryClient.invalidateQueries();
    },
  });
};
