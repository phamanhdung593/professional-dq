import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';
import { useFetch } from './api';
import { DataSettings } from '../types/translation';

export const useSetting = () => {
  const fetcher = useFetch();

  return {
    getSettingData: async () => {
      return await fetcher<DataSettings>('/api/settings', {
        method: 'GET',
      });
    },

    updateSettingId: async (payload: any) => {
      return await fetcher(`/api/settings/${payload.id}`, {
        method: 'PUT',
        body: JSON.stringify(payload),
      });
    },
  };
};

export function useSettingApi() {
  const { getSettingData } = useSetting();

  return useQuery({
    queryKey: ['getSettingData'],
    queryFn: () => getSettingData(),
  });
}

export const useUpdateSettingApi = () => {
  const { updateSettingId } = useSetting();
  const queryClient = useQueryClient();

  return useMutation({
    mutationKey: ['Setting_Data'],
    mutationFn: updateSettingId,
    onSuccess: () => {
      queryClient.invalidateQueries();
    },
  });
};
