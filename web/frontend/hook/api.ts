import { useAppBridge } from '@shopify/app-bridge-react';
import { includes } from 'lodash-es';
import { useCallback } from 'react';

export const useFetch = () => {
  const app = useAppBridge();

  app.loading(true);

  const headers: Record<string, string> = {
    'Content-Type': 'application/json',
  };

  const fetcher = useCallback(
    async <T>(url: string, requestInit: RequestInit) => {
      const response = await fetch(url, {
        headers,
        ...requestInit,
      });

      if (response.headers.get('x-shopify-auth') === '1') {
        checkHeadersForReauthorization(response);
      }

      if (!response.ok) {
        let errMessage = '';
        switch (response.status) {
          case 404:
            errMessage = 'No data';
            break;
          case 400:
            errMessage = 'Bad request';
            break;
        }

        const errorInfo = await response.json();

        if (errorInfo) throw errorInfo.message;
        throw new Error(errMessage);
      }

      const responseType = response.headers.get('content-type');

      if (
        includes(
          [
            'application/pdf',
            'text/html; charset=UTF-8',
            'text/csv; charset=utf-8',
          ],
          responseType,
        )
      ) {
        return (await response.blob()) as T;
      }
      return (await response.json()) as T;
    },
    [],
  );

  return fetcher;
};

function checkHeadersForReauthorization(response: any) {
  const authUrlHeader =
    response.headers.get('X-Shopify-API-Request-Failure-Reauthorize-Url') ||
    `/api/auth?shop=${response.headers.get('x-shopify-shop')}`;

  window.open(authUrlHeader, '_top');
}
