import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';
import { useFetch } from './api';

export const useFaqPageMoreSettingApi = () => {
  const fetcher = useFetch();

  return {
    listFaqPageMoreSetting: async () => {
      return await fetcher<any[]>('/api/faq-more-page-settings', {
        method: 'GET',
      });
    },

    updateFaqPageMoreSetting: (payload: any) => {
      return fetcher(`/api/faq-more-page-settings/${payload.id}`, {
        method: 'PUT',
        body: JSON.stringify(payload),
      });
    },
  };
};

export function useListFaqPageMoreSettingApi() {
  const { listFaqPageMoreSetting } = useFaqPageMoreSettingApi();

  return useQuery({
    queryKey: ['listFaqPageMoreSetting'],
    queryFn: () => listFaqPageMoreSetting(),
  });
}

export const useUpdateFaqPageMoreSettingApi = () => {
  const { updateFaqPageMoreSetting } = useFaqPageMoreSettingApi();
  const queryClient = useQueryClient();

  return useMutation({
    mutationKey: [' updateFaqPageMoreSetting'],
    mutationFn: updateFaqPageMoreSetting,
    onSuccess: () => {
      queryClient.invalidateQueries();
    },
  });
};
