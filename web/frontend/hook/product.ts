import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';
import { useFetch } from './api';

export const useProductApi = () => {
  const fetcher = useFetch();

  return {
    listFaqs: async () => {
      return await fetcher<{ code: number; message: string; data: any[] }>(
        '/api/products/faq',
        {
          method: 'GET',
        },
      );
    },
    deleteProductId: async (ids: any[]) => {
      return await fetcher(`/api/products/delete`, {
        method: 'DELETE',
        body: JSON.stringify({ ids }),
      });
    },
  };
};

export function useListProductFaqApi() {
  const { listFaqs } = useProductApi();

  return useQuery({
    queryKey: ['listFaqs'],
    queryFn: () => listFaqs(),
  });
}

export const useDeleteProductApi = () => {
  const { deleteProductId } = useProductApi();
  const queryClient = useQueryClient();

  return useMutation({
    mutationFn: (ids: any[]) => deleteProductId(ids),
    onSuccess: () => {
      queryClient.invalidateQueries();
    },
  });
};
