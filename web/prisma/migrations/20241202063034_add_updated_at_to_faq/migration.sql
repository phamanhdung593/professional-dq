-- CreateTable
CREATE TABLE `shopify_sessions` (
    `id` VARCHAR(191) NOT NULL,
    `shop` VARCHAR(191) NOT NULL,
    `state` VARCHAR(191) NOT NULL,
    `isOnline` BOOLEAN NOT NULL DEFAULT false,
    `scope` VARCHAR(191) NULL,
    `expires` DATETIME(3) NULL,
    `accessToken` VARCHAR(191) NOT NULL,
    `createdAt` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    `updatedAt` DATETIME(3) NOT NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `faq` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `user_id` INTEGER NOT NULL,
    `category_identify` VARCHAR(255) NOT NULL,
    `identify` VARCHAR(255) NOT NULL,
    `title` VARCHAR(255) NOT NULL,
    `content` TEXT NOT NULL,
    `is_visible` BOOLEAN NULL DEFAULT true,
    `position` INTEGER NULL DEFAULT 0,
    `locale` VARCHAR(255) NOT NULL DEFAULT 'en',
    `createdAt` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    `updatedAt` DATETIME(3) NOT NULL,
    `feature_faq` BOOLEAN NULL,

    INDEX `text_idx`(`title`),
    UNIQUE INDEX `Items_unique`(`user_id`, `identify`, `category_identify`, `locale`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `faq_category` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `user_id` INTEGER NOT NULL,
    `identify` VARCHAR(255) NOT NULL,
    `title` VARCHAR(255) NOT NULL,
    `description` VARCHAR(255) NULL,
    `position` INTEGER NULL DEFAULT 0,
    `locale` VARCHAR(255) NOT NULL DEFAULT 'en',
    `is_visible` BOOLEAN NULL DEFAULT true,
    `createdAt` DATETIME(0) NOT NULL,
    `updatedAt` DATETIME(0) NOT NULL,
    `feature_category` BOOLEAN NULL,

    UNIQUE INDEX `Items_unique`(`user_id`, `identify`, `locale`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `faq_messages` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `user_id` INTEGER NOT NULL,
    `faq_title` VARCHAR(255) NOT NULL,
    `customer_name` VARCHAR(255) NOT NULL,
    `customer_contact` VARCHAR(255) NOT NULL,
    `createdAt` DATETIME(0) NOT NULL,
    `updatedAt` DATETIME(0) NOT NULL,
    `time` VARCHAR(255) NULL,

    INDEX `user_id`(`user_id`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `faq_messages_setting` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `user_id` INTEGER NOT NULL,
    `faq_messages_visible` BOOLEAN NOT NULL,
    `sort_by` VARCHAR(255) NOT NULL,
    `contact_us_visible` BOOLEAN NOT NULL,
    `phone_visible` BOOLEAN NOT NULL,
    `whatsApp_visible` BOOLEAN NOT NULL,
    `message_visible` BOOLEAN NOT NULL,
    `email_visible` BOOLEAN NOT NULL,
    `animation_visible` BOOLEAN NOT NULL,
    `email_link` VARCHAR(255) NOT NULL,
    `phone_number` VARCHAR(255) NOT NULL,
    `whatsApp_number` VARCHAR(255) NOT NULL,
    `message_link` VARCHAR(255) NOT NULL,
    `welcome_title` VARCHAR(255) NOT NULL,
    `description_title` VARCHAR(255) NOT NULL,
    `theme_color` VARCHAR(255) NOT NULL,
    `text_color` VARCHAR(255) NOT NULL,
    `icon_number` VARCHAR(255) NOT NULL,
    `button_background_color` VARCHAR(255) NOT NULL,
    `aligment_faq_messages` VARCHAR(255) NOT NULL,
    `button_title` VARCHAR(255) NOT NULL,
    `font_family` VARCHAR(255) NOT NULL,
    `createdAt` DATETIME(0) NOT NULL,
    `updatedAt` DATETIME(0) NOT NULL,
    `help_desk_visible` BOOLEAN NULL,
    `feature_questions_visible` BOOLEAN NULL,
    `feature_categories_visible` BOOLEAN NULL,
    `send_messages_text_color` VARCHAR(255) NULL,
    `translation` TEXT NULL,
    `primary_language` VARCHAR(255) NULL,
    `show_default_locale` BOOLEAN NULL,

    INDEX `user_id`(`user_id`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `faq_more_page` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `user_id` INTEGER NOT NULL,
    `faq_id` INTEGER NOT NULL,
    `faq_identify` VARCHAR(255) NOT NULL,
    `category_identify` VARCHAR(255) NOT NULL,
    `page_name` VARCHAR(255) NOT NULL,
    `createdAt` DATETIME(0) NOT NULL,
    `updatedAt` DATETIME(0) NOT NULL,

    INDEX `faq_id`(`faq_id`),
    UNIQUE INDEX `Items_unique`(`user_id`, `faq_identify`, `category_identify`, `page_name`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `faq_more_page_setting` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `user_id` INTEGER NOT NULL,
    `home_page_visible` BOOLEAN NOT NULL,
    `product_page_visible` BOOLEAN NOT NULL,
    `cart_page_visible` BOOLEAN NOT NULL,
    `collection_page_visible` BOOLEAN NOT NULL,
    `cms_page_visible` BOOLEAN NOT NULL,
    `createdAt` DATETIME(0) NOT NULL,
    `updatedAt` DATETIME(0) NOT NULL,
    `active_feature` BOOLEAN NULL,
    `active_template` BOOLEAN NULL,

    UNIQUE INDEX `Items_unique`(`user_id`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `faq_product` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `user_id` INTEGER NOT NULL,
    `product_id` INTEGER NOT NULL,
    `faq_id` INTEGER NOT NULL,
    `faq_identify` VARCHAR(255) NOT NULL,
    `category_identify` VARCHAR(255) NOT NULL,
    `createdAt` DATETIME(0) NOT NULL,
    `updatedAt` DATETIME(0) NOT NULL,

    INDEX `faq_id`(`faq_id`),
    INDEX `product_id`(`product_id`),
    INDEX `user_id`(`user_id`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `merchants_plan` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `user_id` INTEGER NULL,
    `plan` VARCHAR(255) NULL,
    `plan_extra` VARCHAR(255) NULL,
    `shopify_plan_id` VARCHAR(255) NULL,
    `expiry_date` VARCHAR(255) NULL,
    `purchase_date` VARCHAR(255) NULL,
    `createdAt` DATETIME(0) NOT NULL,
    `updatedAt` DATETIME(0) NOT NULL,

    UNIQUE INDEX `Items_unique`(`user_id`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `merchants_rating` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `user_id` INTEGER NULL,
    `star` VARCHAR(255) NULL,
    `comment` VARCHAR(255) NULL,
    `email` VARCHAR(255) NULL,
    `domain` VARCHAR(255) NULL,
    `createdAt` DATETIME(0) NOT NULL,
    `updatedAt` DATETIME(0) NOT NULL,

    UNIQUE INDEX `Items_unique`(`user_id`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `product` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `user_id` INTEGER NOT NULL,
    `product_id` VARCHAR(255) NOT NULL,
    `product_image` TEXT NULL,
    `product_title` VARCHAR(255) NOT NULL,
    `createdAt` DATETIME(0) NOT NULL,
    `updatedAt` DATETIME(0) NOT NULL,

    UNIQUE INDEX `Items_unique`(`user_id`, `product_id`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `setting` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `user_id` INTEGER NOT NULL,
    `show_page_title` BOOLEAN NULL,
    `show_intro_text` BOOLEAN NULL,
    `show_footer_text` BOOLEAN NULL,
    `intro_text_content` TEXT NULL,
    `page_title_content` TEXT NULL,
    `footer_text_content` TEXT NULL,
    `faq_sort_name` BOOLEAN NULL,
    `faq_uncategory_hidden` BOOLEAN NULL,
    `category_sort_name` BOOLEAN NULL,
    `dont_category_faq` BOOLEAN NULL,
    `faq_page_url` VARCHAR(255) NULL,
    `faq_template_number` INTEGER NULL,
    `page_under_contruction` TEXT NULL,
    `search_placehoder` TEXT NULL,
    `use_analytics_faq` BOOLEAN NULL,
    `use_analytics` BOOLEAN NULL,
    `view_faq_page` INTEGER NULL,
    `search_not_found` TEXT NULL,
    `show_page_construction` BOOLEAN NULL,
    `createdAt` DATETIME(0) NOT NULL,
    `updatedAt` DATETIME(0) NOT NULL,
    `faq_messages_visible` BOOLEAN NULL,
    `yanet_logo_visible` BOOLEAN NULL,
    `tutorial_active` BOOLEAN NULL,
    `faq_page_schema` BOOLEAN NULL,
    `more_page_schema` BOOLEAN NULL,
    `number_faq_show` INTEGER NULL,
    `show_more_faq` BOOLEAN NULL,
    `meta_tag_description` TEXT NULL,
    `set_width_product_faq` BOOLEAN NULL,
    `title_product_faq` BOOLEAN NULL,

    UNIQUE INDEX `user_id`(`user_id`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `template_setting` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `setting_id` INTEGER NOT NULL,
    `template_number` INTEGER NOT NULL,
    `image_banner` VARCHAR(255) NULL,
    `faq_font_color` VARCHAR(255) NULL,
    `faq_bg_color` VARCHAR(255) NULL,
    `faq_font_size` INTEGER NULL,
    `faq_font_weight` VARCHAR(255) NULL,
    `faq_font_family` VARCHAR(255) NULL,
    `faq_hover_color` VARCHAR(255) NULL,
    `width_faqs_accordian` INTEGER NULL,
    `category_font_size` INTEGER NULL,
    `category_font_weight` VARCHAR(255) NULL,
    `category_font_family` VARCHAR(255) NULL,
    `category_text_style` VARCHAR(255) NULL,
    `category_font_color` VARCHAR(255) NULL,
    `category_text_align` VARCHAR(255) NULL,
    `answer_font_size` INTEGER NULL,
    `answer_font_weight` VARCHAR(255) NULL,
    `answer_font_family` VARCHAR(255) NULL,
    `answer_font_color` VARCHAR(255) NULL,
    `answer_bg_color` VARCHAR(255) NULL,
    `custom_css` TEXT NULL,
    `banner_height` INTEGER NULL,
    `banner_default` VARCHAR(255) NULL,
    `createdAt` DATETIME(0) NOT NULL,
    `updatedAt` DATETIME(0) NOT NULL,
    `banner_visible` BOOLEAN NULL,
    `page_title_fontsize` INTEGER NULL,
    `page_title_paddingtop` INTEGER NULL,
    `page_title_paddingbottom` INTEGER NULL,
    `page_title_color` VARCHAR(255) NULL,
    `search_input_style` VARCHAR(255) NULL,
    `intro_text_fontsize` INTEGER NULL,
    `intro_text_paddingtop` INTEGER NULL,
    `intro_text_paddingbottom` INTEGER NULL,
    `intro_text_color` VARCHAR(255) NULL,
    `footer_text_fontsize` INTEGER NULL,
    `footer_text_paddingtop` INTEGER NULL,
    `footer_text_paddingbottom` INTEGER NULL,
    `footer_text_color` VARCHAR(255) NULL,
    `show_search_input` BOOLEAN NULL,
    `page_title_font` VARCHAR(255) NULL,
    `intro_text_font` VARCHAR(255) NULL,
    `search_placeholder_font` VARCHAR(255) NULL,
    `footer_text_font` VARCHAR(255) NULL,
    `show_category_bar` BOOLEAN NULL,
    `page_background_color` VARCHAR(255) NULL,
    `micro_scope_color` VARCHAR(255) NULL,
    `placeholder_color` VARCHAR(255) NULL,
    `category_bar_number` INTEGER NULL,
    `category_bar_background` VARCHAR(255) NULL,
    `category_bar_color` VARCHAR(255) NULL,
    `btn_top_background` VARCHAR(255) NULL,
    `btn_top_hover` VARCHAR(255) NULL,
    `btn_top_visible` BOOLEAN NULL,
    `width_faqs_product` INTEGER NULL,

    UNIQUE INDEX `Items_unique`(`setting_id`, `template_number`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `user` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `store_name` VARCHAR(255) NULL,
    `shopify_domain` VARCHAR(255) NOT NULL,
    `shopify_access_token` VARCHAR(255) NULL,
    `email` VARCHAR(255) NULL,
    `phone` VARCHAR(255) NULL,
    `shopLocales` TEXT NULL,
    `createdAt` DATETIME(0) NOT NULL,
    `updatedAt` DATETIME(0) NOT NULL,
    `plan_extra` BOOLEAN NULL,

    UNIQUE INDEX `shopify_domain`(`shopify_domain`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- AddForeignKey
ALTER TABLE `faq` ADD CONSTRAINT `faq_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `faq_category` ADD CONSTRAINT `faq_category_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `faq_messages` ADD CONSTRAINT `faq_messages_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `faq_messages_setting` ADD CONSTRAINT `faq_messages_setting_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `faq_more_page` ADD CONSTRAINT `faq_more_page_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `faq_more_page` ADD CONSTRAINT `faq_more_page_ibfk_2` FOREIGN KEY (`faq_id`) REFERENCES `faq`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `faq_more_page_setting` ADD CONSTRAINT `faq_more_page_setting_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `faq_product` ADD CONSTRAINT `faq_product_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `faq_product` ADD CONSTRAINT `faq_product_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `product`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `faq_product` ADD CONSTRAINT `faq_product_ibfk_3` FOREIGN KEY (`faq_id`) REFERENCES `faq`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `merchants_plan` ADD CONSTRAINT `merchants_plan_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `merchants_rating` ADD CONSTRAINT `merchants_rating_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `product` ADD CONSTRAINT `product_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `setting` ADD CONSTRAINT `setting_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `template_setting` ADD CONSTRAINT `template_setting_ibfk_1` FOREIGN KEY (`setting_id`) REFERENCES `setting`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;
