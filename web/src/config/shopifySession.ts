export type Session = {
  id?: string;
  session_id: string;
  shop: string;
  state: string;
  isOnline?: boolean;
  scope?: string | null;
  expires?: Date | string | null;
  accessToken: string;
  createdAt?: Date | string;
  updatedAt?: Date | string;
};
