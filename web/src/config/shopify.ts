import { registerAs } from '@nestjs/config';

export default registerAs('shopify', () => ({
  api_key: process.env.SHOPIFY_API_KEY,
  api_secret: process.env.SHOPIFY_API_SECRET,
  scopes: process.env.SHOPIFY_SCOPES || '',
  api_version: process.env.API_VERSION,
  auth_path: '/api/auth',
  auth_callback_path: '/api/auth/callback',
}));
