import { registerAs } from '@nestjs/config';

export default registerAs('app', () => ({
  name: 'tonify-app',
  path: 'tonify-app',
  scheme: process.env.APP_SCHEME || 'https',
  env: process.env.APP_ENV || 'production',
  port: process.env.PORT,
  url: process.env.APP_URL || process.env.HOST,
  host: process.env.HOST || 'localhost:8081',
  client_url: process.env.CLIENT_URL,
  UtcOffset: process.env.UTC_OFFSET || 0,
}));

export type Payload = {
  query: string;
  variables?: Record<string, unknown>;
};
export type Identifier = {
  shop: string;
  accessToken?: string;
};

export type ShopifyEdge<T> = {
  edges: Array<{
    cursor?: string;
    node: T;
  }>;
  pageInfo: {
    hasNextPage: boolean;
    hasPreviousPage: boolean;
  };
};
