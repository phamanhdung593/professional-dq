import { registerAs } from '@nestjs/config';

export default registerAs('aws', () => ({
  s3: {
    region: process.env.AWS_S3_REGION,
    bucket: process.env.AWS_S3_BUCKET,
    client_id: process.env.AWS_S3_CLIENT_ID,
    secret_key: process.env.AWS_S3_SECRET_KEY,
  },
  ses: {
    region: process.env.AWS_SES_REGION,
    access_key_id: process.env.AWS_SES_ACCESS_KEY_ID,
    secret_access_key: process.env.AWS_SES_SECRET_ACCESS_KEY,
  },
}));
