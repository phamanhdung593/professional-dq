import {
  ArgumentsHost,
  Catch,
  ExceptionFilter,
  UnprocessableEntityException,
} from '@nestjs/common';
import { Response } from 'express';
type ValidationError = {
  value: string;
  target: Record<string, string | number | unknown>;
  property: string;
  children: ValidationError[];
  constraints: Record<string, string>;
};

@Catch(UnprocessableEntityException)
export class ValidationExceptionFilter
  implements ExceptionFilter<UnprocessableEntityException>
{
  public catch(exception: UnprocessableEntityException, host: ArgumentsHost) {
    const context = host.switchToHttp();
    const response = context.getResponse() as Response;
    const responseData = exception.getResponse() as
      | string
      | { message: string };

    return response.status(422).json({
      message: exception.message,
      errors:
        typeof responseData == 'object'
          ? this.transformAsLaravelResponse(responseData.message)
          : responseData,
    });
  }

  private transformAsLaravelResponse(data: ValidationError[] | string) {
    if (typeof data == 'string') {
      return data;
    }
    const targetData: { [key: string]: string[] } = {};

    for (let i = 0; i < data.length; i++) {
      const item = data[i];
      if (item.children.length > 0) {
        for (let j = 0; j < item.children.length; j++) {
          const child1 = item.children[j];
          if (child1.children.length > 0) {
            for (let k = 0; k <= child1.children.length; k++) {
              const child2 = child1.children[k];
              if (child2) {
                targetData[
                  `${item.property}.${child1.property}.${child2.property}`
                ] = Object.values(child2.constraints);
              }
            }
          } else if (child1.constraints) {
            targetData[`${item.property}.${child1.property}`] = Object.values(
              child1.constraints,
            );
          }
        }
      } else {
        targetData[item.property] = Object.values(item.constraints);
      }
    }
    return targetData;
  }
}
