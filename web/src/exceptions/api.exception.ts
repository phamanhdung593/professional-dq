import {
  HttpException,
  HttpExceptionOptions,
  HttpStatus,
} from '@nestjs/common';

export class ApiException extends HttpException {
  constructor(
    response: string,
    status: number,
    options?: HttpExceptionOptions,
  ) {
    super(response, status, options);
  }

  public static badGateway(message: string) {
    throw new ApiException(message, HttpStatus.BAD_GATEWAY);
  }

  public static unauthorized(message: string) {
    throw new ApiException(message, HttpStatus.UNAUTHORIZED);
  }

  public static gatewayTimeout(message: string) {
    throw new ApiException(message, HttpStatus.GATEWAY_TIMEOUT);
  }

  public static internalServerError(message: string) {
    throw new ApiException(message, HttpStatus.INTERNAL_SERVER_ERROR);
  }

  public static badRequest(message: string) {
    throw new ApiException(message, HttpStatus.BAD_REQUEST);
  }

  public static unprocessableEntity(message: string) {
    throw new ApiException(message, HttpStatus.UNPROCESSABLE_ENTITY);
  }

  public static notFound(message: string) {
    throw new ApiException(message, HttpStatus.NOT_FOUND);
  }
}
