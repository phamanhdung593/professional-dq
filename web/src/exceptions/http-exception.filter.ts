import {
  ArgumentsHost,
  BadRequestException,
  Catch,
  ExceptionFilter,
  HttpException,
  InternalServerErrorException,
  LoggerService,
  MisdirectedException,
  PayloadTooLargeException,
} from '@nestjs/common';

@Catch()
export class HttpExceptionFilter implements ExceptionFilter {
  constructor(private readonly logger: LoggerService) {}

  catch(exception: Error, host: ArgumentsHost) {
    const context = host.switchToHttp();
    if (
      exception instanceof InternalServerErrorException ||
      exception instanceof BadRequestException ||
      exception instanceof MisdirectedException ||
      exception instanceof PayloadTooLargeException
    ) {
      this.logger.warn({
        message: exception.stack || '',
        title: `${exception.getStatus()} - ${exception.name}`,
        label: exception.message,
      });
    }

    const response = context.getResponse();

    if (exception instanceof HttpException) {
      this.logger.warn({
        message: exception.stack || '',
        title: `${exception.getStatus()} - ${exception.name}`,
        label: exception.message,
      });
      response.status(exception.getStatus());
    } else {
      response.status(400);
    }

    response.json({
      message: exception.message,
    });
  }
}
