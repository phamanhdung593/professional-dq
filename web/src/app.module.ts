import { MiddlewareConsumer, Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import jwtConfig from './config/jwt';
import { ServeStaticModule } from '@nestjs/serve-static';
import { AuthModule } from './modules/auth/auth.module';
import { ConfigModule } from '@nestjs/config';
import appConfig from './config/app';
import shopifyConfig from './config/shopify';
import { join } from 'path';
import { AuthenticateMiddleWare } from './middleware/authenticate';
import { UserModule } from './modules/user/user.module';
import { PrismaModule } from './modules/prisma/prisma.module';
import { JsonBodyMiddleware } from './middleware/body.parser';
import { FaqModule } from './modules/faq/faq.module';
import { CategoriesModule } from './modules/categories/categories.module';
import { ProductsModule } from './modules/products/products.module';
import { SettingsModule } from './modules/settings/settings.module';
import { TemplateSettingsModule } from './modules/template-settings/template-settings.module';
import { FaqProductsModule } from './modules/faq-products/faq-products.module';
import { FaqMorePageSettingsModule } from './modules/faq-more-page-settings/faq-more-page-settings.module';
import { FaqMorePageModule } from './modules/faq-more-page/faq-more-page.module';
import { HelpDeskModule } from './modules/help-desk/help-desk.module';
import { ThemeAppBlockModule } from './modules/theme-app-block/theme-app-block.module';
import { ProxyModule } from './modules/proxy/proxy.module';
import { PlanModule } from './modules/plan/plan.module';
import { CacheModule, CacheStore } from '@nestjs/cache-manager';
import { redisStore } from 'cache-manager-redis-yet';
import { WebhookModule } from './modules/webhook/webhook.module';
import { ComplianceWebhookModule } from './modules/compliance-webhook/compliance-webhook.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      envFilePath: `${process.cwd()}/.env`,
      load: [appConfig, jwtConfig, shopifyConfig],
    }),
    ServeStaticModule.forRoot({
      rootPath: join(
        __dirname,
        process.env.NODE_ENV == 'production'
          ? '../frontend/dist'
          : '../frontend',
      ),
      serveStaticOptions: {
        maxAge: 2592000000,
        etag: false,
        setHeaders: (res, path) => {
          if (path.endsWith('index.html')) {
            res.setHeader('Cache-Control', 'no-cache');
          }
        },
      },
    }),
    CacheModule.registerAsync({
      useFactory: async () => {
        const store = await redisStore({
          socket: {
            host: process.env.REDIS_HOST,
            port: parseInt(process.env.REDIS_PORT),
          },
          password: process.env.REDIS_PASSWORD,
        });

        return {
          store: store as unknown as CacheStore,
          ttl: 3 * 60000,
        };
      },
    }),
    AuthModule,
    PrismaModule,
    UserModule,
    CategoriesModule,
    ProductsModule,
    FaqModule,
    FaqProductsModule,
    FaqMorePageSettingsModule,
    FaqMorePageModule,
    SettingsModule,
    HelpDeskModule,
    ProxyModule,
    PlanModule,
    ThemeAppBlockModule,
    WebhookModule,
    TemplateSettingsModule,
    ComplianceWebhookModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(AuthenticateMiddleWare).forRoutes('*');
    consumer.apply(JsonBodyMiddleware).forRoutes('*');
  }
}
