import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import { LATEST_API_VERSION, Session, shopifyApi } from '@shopify/shopify-api';
import '@shopify/shopify-api/adapters/node';
import { AppModule } from 'src/app.module';
import { GET_WEBHOOKS } from 'src/modules/graphql/query/webhook';

async function bootstrap() {
  await NestFactory.createApplicationContext(AppModule).then(
    async (context) => {
      const config = context.get(ConfigService);

      const shopify = shopifyApi({
        apiKey: config.get('shopify.api_key'),
        apiSecretKey: config.get('shopify.api_secret'),
        scopes: config.get('shopify.scopes').replace(/\s/g, '').split(','),
        hostName: config.get('app.host').replace(/https?:\/\//, ''),
        apiVersion: LATEST_API_VERSION,
        isEmbeddedApp: true,
        hostScheme: config.get('app.scheme'),
      });

      const session = new Session({
        accessToken: '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',
        shop: 'decemberlights-1146.myshopify.com',
        isOnline: true,
        id: '',
        state: '',
      });

      const client = new shopify.clients.Graphql({
        session: session,
      });

      try {
        const result = await client.request(GET_WEBHOOKS, {
          variables: {},
          headers: { myHeader: '1' },
          retries: 2,
        });

        const listWebhooks = result.data.webhookSubscriptions.edges;
        for (const webhook of listWebhooks) {
          console.log(webhook.node.topic);
          // if (webhook.node.topic === 'APP_UNINSTALLED') {
          //   await client.request(DELETE_WEBHOOK, {
          //     variables: {
          //       id: webhook.node.id,
          //     },
          //     retries: 2,
          //   });

          //   const endpoint = 'https://faq.yanet.io/api/webhook/uninstall';
          //   const webhookSup = await client.request(SUBSCRIPTION_WEBHOOK, {
          //     variables: {
          //       topic: WEBHOOK_TOPIC.APP_UNINSTALLED,
          //       webhookSubscription: {
          //         callbackUrl: endpoint,
          //         format: WebhookSubscriptionFormat.JSON,
          //       },
          //     },
          //   });

          //   console.log(
          //     'NEW webhook...' +
          //       user.shopify_domain +
          //       '   =>   ' +
          //       webhookSup.data.webhookSubscriptionCreate.webhookSubscription
          //         .endpoint.callbackUrl,
          //   );
          // }
        }
      } catch (error) {
        console.log(error);
      }
    },
  );

  process.exit(0);
}

bootstrap();
