import { Exclude } from 'class-transformer';

export class UserDto {
  id: number;
  store_name?: string | null;
  shopify_domain: string;
  email?: string | null;
  phone?: string | null;
  shopLocales?: string | null;
  createdAt: Date;
  updatedAt: Date;
  plan_extra?: boolean | null;

  @Exclude()
  shopify_access_token?: string | null;

  faq?: any[];
  faq_category?: any[];
  faq_messages?: any[];
  faq_messages_setting?: any;
  faq_more_page?: any[];
  faq_more_page_setting?: any;
  faq_product?: any[];
  merchants_plan?: any;
  merchants_rating?: any;
  product?: any[];
  setting?: any;
}
