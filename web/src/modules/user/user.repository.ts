import { Injectable } from '@nestjs/common';
import { PrismaService } from '../prisma/prisma.service';

@Injectable()
export class UserRepository {
  constructor(private readonly prisma: PrismaService) {}

  async findByShopifyDomain(shopifyDomain: string) {
    return await this.prisma.user.findUnique({
      where: { shopify_domain: shopifyDomain },
    });
  }

  /**
   * Update or create user
   * @param user
   */
  async create(user: any) {
    return await this.prisma.user.upsert({
      where: { shopify_domain: user.shopify_domain },
      update: user,
      create: user,
    });
  }

  async update(userId: number, data: any) {
    return await this.prisma.user.update({
      where: { id: userId },
      data: data,
    });
  }

  async findById(id: number) {
    return await this.prisma.user.findUnique({
      where: { id },
      include: {
        faq: true,
        faq_category: true,
        faq_messages: true,
        faq_messages_setting: true,
        faq_more_page: true,
        faq_more_page_setting: true,
        faq_product: true,
        merchants_plan: true,
        merchants_rating: true,
        product: true,
        setting: true,
      },
    });
  }

  async findByShopDomain(shopDomain: string) {
    return await this.prisma.user.findUnique({
      where: { shopify_domain: shopDomain },
      include: {
        faq: true,
        faq_category: true,
        faq_messages: true,
        faq_messages_setting: true,
        faq_more_page: true,
        faq_more_page_setting: true,
        faq_product: true,
        merchants_plan: true,
        merchants_rating: true,
        product: true,
        setting: true,
      },
    });
  }
}
