import { Controller, Get, Post, Req, UseGuards } from '@nestjs/common';
import { UserService } from './user.service';
import { UserDto } from './user.dto';
import { plainToInstance } from 'class-transformer';
import { ShopifyGuard } from '../shopify/shopify.guard';
import { Request } from 'express';
import { ShopifyService } from '../shopify/shopify.service';
import { SHOP_LOCALES } from '../graphql/query/shop.query';

@Controller('api/user')
@UseGuards(ShopifyGuard)
export class UserController {
  constructor(
    private readonly userService: UserService,
    private readonly shopifyService: ShopifyService,
  ) {}

  @Get('/')
  async getUser(@Req() req: Request) {
    const user = (req as any).user as UserDto;

    return plainToInstance(UserDto, user);
  }

  @Post('/sync')
  async updateUser(@Req() req: Request) {
    const user = (req as any).user as UserDto;

    const shopLocales = await this.shopifyService.query(
      user.shopify_domain,
      SHOP_LOCALES,
      {},
    );

    const result = await this.userService.syncLocale(
      user.id,
      JSON.stringify(shopLocales.data),
    );

    return plainToInstance(UserDto, result);
  }
}
