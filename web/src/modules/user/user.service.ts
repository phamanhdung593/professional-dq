import { Injectable } from '@nestjs/common';
import { UserRepository } from './user.repository';

@Injectable()
export class UserService {
  constructor(private readonly userRepository: UserRepository) {}

  async createOrUpdateUser(user: any) {
    const existingUser = await this.userRepository.findByShopifyDomain(
      user.shopify_domain,
    );

    // vi nguoi thiet ke database lam created at va updated at khong tu dong update nen phai tu check rat kho hieu =))))
    if (existingUser) {
      delete user.createdAt;

      return await this.userRepository.update(existingUser.id, user);
    }

    return await this.userRepository.create(user);
  }

  async updateUser(userId: number, user: any) {
    return await this.userRepository.update(userId, user);
  }

  async findById(id: number) {
    return await this.userRepository.findById(id);
  }

  async findByShopDomain(shopDomain: string) {
    return await this.userRepository.findByShopDomain(shopDomain);
  }

  async syncLocale(userId: number, shopLocales: string) {
    return await this.userRepository.update(userId, {
      shopLocales: shopLocales,
    });
  }
}
