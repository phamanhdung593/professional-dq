import { Module } from '@nestjs/common';
import { FaqMessageController } from './faq-message.controller';
import { FaqMessageService } from './faq-message.service';

@Module({
  controllers: [FaqMessageController],
  providers: [FaqMessageService],
})
export class FaqMessageModule {}
