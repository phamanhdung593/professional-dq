import { Injectable } from '@nestjs/common';
import { PrismaService } from '../prisma/prisma.service';

@Injectable()
export class SettingsRepository {
  constructor(private readonly prisma: PrismaService) {}

  async getSettings() {
    return await this.prisma.setting.findMany();
  }

  async findById(id: number) {
    return await this.prisma.setting.findUnique({
      where: {
        id: id,
      },
    });
  }

  async getSettingsByUserId(userId: number) {
    return await this.prisma.setting.findUnique({
      where: {
        user_id: userId,
      },
      include: {
        template_setting: true,
      },
    });
  }

  async create(user_id: number, createSettingsDto: any) {
    return await this.prisma.setting.create({
      data: {
        ...createSettingsDto,
        user: {
          connect: {
            id: user_id,
          },
        },
      },
    });
  }

  async update(id: number, createSettingsDto: any) {
    return await this.prisma.setting.update({
      where: {
        id: id,
      },
      data: createSettingsDto,
    });
  }

  async findByIdAndUserId(id: number, userId: number) {
    return await this.prisma.setting.findUnique({
      where: {
        id: id,
        user_id: userId,
      },
    });
  }
}
