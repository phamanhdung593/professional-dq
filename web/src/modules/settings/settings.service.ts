import { FaqMessageSettingsService } from './../faq-message-settings/faq-message-settings.service';
import { FaqMorePageSettingsService } from './../faq-more-page-settings/faq-more-page-settings.service';
import { BadRequestException, Injectable } from '@nestjs/common';
import { SettingsRepository } from './settings.repository';
import { FaqMessageSettingData, SettingDefault } from 'src/types/setting';

@Injectable()
export class SettingsService {
  constructor(
    private readonly settingRepository: SettingsRepository,
    private readonly faqMorePageSettingsService: FaqMorePageSettingsService,
    private readonly faqMessageSettingsService: FaqMessageSettingsService,
  ) {}

  async findByUserId(userId: number) {
    return this.settingRepository.getSettingsByUserId(userId);
  }

  async updateSetting(id: number, user: any, createSettingDto: any) {
    const setting = await this.settingRepository.findByIdAndUserId(id, user.id);
    if (!setting) {
      throw new BadRequestException('Setting not found');
    }

    if (setting.user_id != user.id) {
      throw new BadRequestException(
        'You are not allowed to update this setting',
      );
    }

    return this.settingRepository.update(id, createSettingDto);
  }

  async initSettingDefault(userId: number) {
    await this.faqMessageSettingsService.initDataDefault(
      userId,
      FaqMessageSettingData,
    );
    await this.faqMorePageSettingsService.initSettingDefault(userId);
    return await this.settingRepository.create(userId, SettingDefault);
  }
}
