import { Module } from '@nestjs/common';
import { SettingsController } from './settings.controller';
import { SettingsService } from './settings.service';
import { SettingsRepository } from './settings.repository';
import { PrismaModule } from '../prisma/prisma.module';
import { UserModule } from '../user/user.module';
import { FaqMorePageSettingsModule } from '../faq-more-page-settings/faq-more-page-settings.module';
import { UploadModule } from '../upload/upload.module';
import { FaqMessageSettingsModule } from '../faq-message-settings/faq-message-settings.module';

@Module({
  imports: [
    PrismaModule,
    UserModule,
    FaqMorePageSettingsModule,
    FaqMessageSettingsModule,
    UploadModule,
  ],
  controllers: [SettingsController],
  providers: [SettingsService, SettingsRepository],
  exports: [SettingsService],
})
export class SettingsModule {}
