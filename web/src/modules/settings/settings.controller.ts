import {
  Body,
  Controller,
  Get,
  HttpException,
  HttpStatus,
  Param,
  Post,
  Put,
  Req,
  Res,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { SettingsService } from './settings.service';
import { ShopifyGuard } from '../shopify/shopify.guard';
import { ManagedUpload } from 'aws-sdk/clients/s3';
import { FileInterceptor } from '@nestjs/platform-express';
import * as crypto from 'crypto';
import { extname } from 'path';
import { UploadService } from '../upload/upload.service';
import { Request, Response } from 'express';

@Controller('api/settings')
@UseGuards(ShopifyGuard)
export class SettingsController {
  constructor(
    private readonly settingService: SettingsService,
    private readonly uploadService: UploadService,
  ) {}

  @Get()
  async getSetting(@Req() req: Request & { user: { id: number } }) {
    return await this.settingService.findByUserId(req.user.id);
  }

  @Put('/:id')
  async updateSetting(
    @Req() req: Request & { user: { id: number } },
    @Param('id') id: string,
    @Body() createSettingDto: any,
  ) {
    return await this.settingService.updateSetting(
      parseInt(id),
      req.user,
      createSettingDto,
    );
  }

  @UseInterceptors(
    FileInterceptor('file', {
      limits: {
        files: 1,
        fileSize: 5000000,
      },
      fileFilter: (req, file, callback) => {
        if (!file.mimetype.match(/\/(jpg|jpeg|png|gif|webp)$/)) {
          return callback(
            new HttpException(
              `Don\'t allow file type ${extname(file.originalname)}`,
              HttpStatus.UNPROCESSABLE_ENTITY,
            ),
            false,
          );
        }
        callback(null, true);
      },
    }),
  )
  @Post('upload-image')
  public async uploadFile(
    @Req() req: Request & { user: { id: number } },
    @UploadedFile() file: Express.Multer.File,
    @Res() res: Response,
  ) {
    let uploadResult:
      | ManagedUpload.SendData
      | { Key: string; Location: string }
      | undefined;

    if (file) {
      const filename = crypto
        .createHash('md5')
        .update(new Date().valueOf().toString() + req.user.id)
        .digest('hex');

      uploadResult = await this.uploadService.singleUpload(
        file,
        'banner/' + filename,
      );

      const fileExtension = extname(file.originalname).toLowerCase();

      return res.status(200).json({
        code: 200,
        message: 'success',
        name: `${filename}${fileExtension}`,
        url: uploadResult.Location,
      });
    }
  }
}
