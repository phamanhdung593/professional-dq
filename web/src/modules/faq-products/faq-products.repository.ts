import { Injectable } from '@nestjs/common';
import { PrismaService } from '../prisma/prisma.service';

@Injectable()
export class FaqProductsRepository {
  constructor(private readonly prisma: PrismaService) {}

  async index(userId: number) {
    return await this.prisma.faq_product.findMany({
      where: {
        user_id: userId,
      },
      include: {
        faq: true,
        product: true,
      },
    });
  }

  async upsert(userId: number, productId: number, faq: any) {
    await this.prisma.faq_product.deleteMany({
      where: {
        product_id: productId,
      },
    });

    return await this.prisma.faq_product.create({
      data: {
        user: {
          connect: {
            id: userId,
          },
        },
        product: {
          connect: {
            id: productId,
          },
        },
        faq: {
          connect: {
            id: faq.id,
          },
        },
        faq_identify: faq.identify,
        category_identify: faq.faq_category.identify,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    });
  }
}
