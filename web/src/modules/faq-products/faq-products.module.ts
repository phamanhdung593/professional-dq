import { Module } from '@nestjs/common';
import { FaqProductsService } from './faq-products.service';
import { FaqProductsController } from './faq-products.controller';
import { PrismaModule } from '../prisma/prisma.module';
import { UserModule } from '../user/user.module';
import { FaqProductsRepository } from './faq-products.repository';
import { ProductsModule } from '../products/products.module';
import { FaqModule } from '../faq/faq.module';

@Module({
  imports: [PrismaModule, UserModule, ProductsModule, FaqModule],
  providers: [FaqProductsService, FaqProductsRepository],
  controllers: [FaqProductsController],
})
export class FaqProductsModule {}
