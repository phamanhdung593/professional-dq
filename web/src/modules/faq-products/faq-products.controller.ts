import { Body, Controller, Get, Post, Req, UseGuards } from '@nestjs/common';
import { FaqProductsService } from './faq-products.service';
import { ShopifyGuard } from '../shopify/shopify.guard';
import { ProductsAndFaqsDto } from './faq-product.dto';

@Controller('/api/faq-products')
@UseGuards(ShopifyGuard)
export class FaqProductsController {
  constructor(private readonly faqProductsService: FaqProductsService) {}

  @Get()
  index(@Req() req: Request & { user: { id: number } }) {
    return this.faqProductsService.index(req.user.id);
  }

  @Post()
  getFaqProducts(
    @Req() req: Request & { user: { id: number } },
    @Body() body: ProductsAndFaqsDto,
  ) {
    return this.faqProductsService.createFaqProducts(req.user.id, body);
  }
}
