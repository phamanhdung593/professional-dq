import {
  IsString,
  IsNumber,
  IsArray,
  IsBoolean,
  IsDateString,
  ValidateNested,
} from 'class-validator';

class CreateVariantDto {
  @IsBoolean()
  availableForSale: boolean;

  @IsString()
  barcode: string;

  @IsString()
  displayName: string;

  @IsString()
  price: string;

  @IsString()
  title: string;

  @IsNumber()
  weight: number;

  @IsString()
  weightUnit: string;

  @IsBoolean()
  requiresShipping: boolean;
}

class CreateImageDto {
  @IsString()
  id: string;

  @IsString()
  originalSrc: string;
}

class CreateOptionDto {
  @IsString()
  id: string;

  @IsString()
  name: string;

  @IsArray()
  values: string[];
}

export class CreateProductDto {
  @IsString()
  title: string;

  @IsString()
  handle: string;

  @IsArray()
  images: CreateImageDto[];

  @IsArray()
  options: CreateOptionDto[];

  @IsString()
  productType: string;

  @IsArray()
  tags: string[];

  @IsString()
  vendor: string;

  @IsString()
  status: string;

  @IsArray()
  variants: CreateVariantDto[];
}

class CreateFaqCategoryDto {
  @IsNumber()
  id: number;

  @IsString()
  title: string;

  @IsString()
  description: string;

  @IsNumber()
  position: number;

  @IsString()
  locale: string;

  @IsBoolean()
  is_visible: boolean;

  @IsDateString()
  createdAt: string;

  @IsDateString()
  updatedAt: string;

  @IsBoolean()
  featureCategory: boolean;
}

export class CreateFaqDto {
  @IsNumber()
  id: number;

  @IsNumber()
  user_id: number;

  @IsString()
  category_identify: string;

  @IsString()
  identify: string;

  @IsString()
  title: string;

  @IsString()
  content: string;

  @IsBoolean()
  is_visible: boolean;

  @IsNumber()
  position: number;

  @IsString()
  locale: string;

  @IsBoolean()
  feature_faq: boolean;

  @IsNumber()
  category_id: number;

  @IsArray()
  faq_more_page: string[];

  @IsArray()
  faq_product: string[];

  faq_category: CreateFaqCategoryDto;

  @IsArray()
  language: string[];
}

import { Type } from 'class-transformer';

export class ProductsAndFaqsDto {
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => CreateProductDto)
  products: CreateProductDto[];

  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => CreateFaqDto)
  faqs: CreateFaqDto[];
}
