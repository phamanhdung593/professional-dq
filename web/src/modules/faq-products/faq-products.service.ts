import { BadRequestException, Injectable } from '@nestjs/common';
import { FaqProductsRepository } from './faq-products.repository';
import { ProductsService } from '../products/products.service';
import { FaqService } from '../faq/faq.service';

@Injectable()
export class FaqProductsService {
  constructor(
    private readonly faqProductsRepository: FaqProductsRepository,
    private readonly faqService: FaqService,
    private readonly productService: ProductsService,
  ) {}

  async index(userId: number) {
    return await this.faqProductsRepository.index(userId);
  }

  async createFaqProducts(userId: number, faqProducts: any) {
    const faqIds = faqProducts.faqs.map((faq) => faq.id);
    const productIds = faqProducts.products.map((product) => product.id);

    const productList = productIds.map((id) =>
      id.replace('gid://shopify/Product/', ''),
    );

    const faqs = await this.faqService.findByIds(faqIds);
    const products = await this.productService.findByIdShopify(productList);

    if (faqs.length !== faqIds.length) {
      throw new BadRequestException(
        'Ids faqs or product no have data, data not match',
      );
    }

    await Promise.all(
      faqProducts.products.map(async (product) => {
        const productData = await this.productService.create(userId, product);

        faqProducts.faqs.map((faq) => {
          faq.faq_product.push(productData.id);
          return this.faqProductsRepository.upsert(userId, productData.id, faq);
        });
      }),
    );

    return {
      faqs: faqs,
      products: products,
    };
  }
}
