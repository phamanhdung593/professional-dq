import { Injectable } from '@nestjs/common';
import { ShopifyService } from '../shopify/shopify.service';
import { GET_WEBHOOKS } from '../graphql/query/webhook';

@Injectable()
export class WebhookService {
  constructor(private readonly shopifyService: ShopifyService) {}

  public async getWebhooksByUser(user: any) {
    const result = await this.shopifyService.query(
      user.shopify_domain,
      GET_WEBHOOKS,
    );

    return result;
  }
}
