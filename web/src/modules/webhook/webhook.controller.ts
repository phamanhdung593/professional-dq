import { Controller, Post, Res } from '@nestjs/common';
import { Response } from 'express';

@Controller('/api/webhook')
export class WebhookController {
  @Post('shop-redact')
  shopRedact(@Res() res: Response) {
    return res.status(200).send('shop-redact');
  }

  @Post('uninstall')
  uninstall(@Res() res: Response) {
    return res.status(200).send('uninstall');
  }
}
