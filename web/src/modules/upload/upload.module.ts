import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { UploadService } from './upload.service';
import awsConfig from 'src/config/aws';

@Module({
  imports: [ConfigModule.forFeature(awsConfig)],
  providers: [UploadService],
  exports: [UploadService],
})
export class UploadModule {}
