import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { config, S3 } from 'aws-sdk';
import { extname } from 'path';
import * as sharp from 'sharp';

const MAX_SIZE = 1080;

@Injectable()
export class UploadService {
  constructor(private readonly configService: ConfigService) {}

  /**
   *
   * @param file
   * @param filename filename without extension
   */
  public async singleUpload(file: Express.Multer.File, filename: string) {
    const bufferData = await this._handleFileSize(file);

    try {
      config.update({
        region: this.configService.get<string>('aws.s3.region'),
        credentials: {
          accessKeyId: this.configService.get<string>(
            'aws.s3.client_id',
          ) as string,
          secretAccessKey: this.configService.get<string>(
            'aws.s3.secret_key',
          ) as string,
        },
      });

      const s3Client = new S3();
      const result = await s3Client
        .upload({
          ContentType: file.mimetype,
          ACL: 'public-read',
          Key: this._getPrefixPath() + filename + extname(file.originalname),
          Body: bufferData,
          Bucket: this.configService.get<string>('aws.s3.bucket') as string,
        })
        .promise();
      return result;
    } catch (e) {
      console.log(e);
    }
    return null;
  }

  /**
   *
   * @returns Prefix path by environment
   */
  private _getPrefixPath(): string {
    return process.env.NODE_ENV == 'production' ? '' : '';
  }

  /**
   *
   * @param file Resize image if width > MAX_SIZE or height > MAX_SIZE
   * @returns
   */
  private async _handleFileSize(file: Express.Multer.File): Promise<Buffer> {
    return new Promise((resolve) => {
      const image = sharp(file.buffer);
      image.metadata().then((metadata: sharp.Metadata) => {
        if (!metadata.width || !metadata.height) {
          throw new HttpException(
            'File metadata is invalid',
            HttpStatus.UNSUPPORTED_MEDIA_TYPE,
          );
        }
        const resizeMeta = this._getFileSizeToSize(
          metadata.width,
          metadata.height,
        );
        if (resizeMeta.hasResize && resizeMeta.size) {
          resolve(image.resize(resizeMeta.size).toBuffer());
        }
        resolve(image.toBuffer());
      });
    });
  }

  /**
   * Calculate width, height to resize
   * @param width
   * @param height
   */
  private _getFileSizeToSize(
    width: number,
    height: number,
  ): {
    hasResize: boolean;
    size?: { width: number; height: number };
  } {
    if (width > height && width > MAX_SIZE) {
      return {
        hasResize: true,
        size: {
          width: MAX_SIZE,
          height: Math.round((MAX_SIZE * height) / width),
        },
      };
    }
    if (height > width && height > MAX_SIZE) {
      return {
        hasResize: true,
        size: {
          height: MAX_SIZE,
          width: Math.round((MAX_SIZE * width) / height),
        },
      };
    }
    return {
      hasResize: false,
    };
  }

  /**
   * Get number of objects in S3 by user domain
   * @param path string
   * @returns Promise<number|undefined>
   */
  public async getCountOfFilesByPath(path = '') {
    config.update({
      region: this.configService.get<string>('aws.s3.region'),
      credentials: {
        accessKeyId: this.configService.get<string>(
          'aws.s3.client_id',
        ) as string,
        secretAccessKey: this.configService.get<string>(
          'aws.s3.secret_key',
        ) as string,
      },
    });
    const s3Client = new S3();
    const result = await s3Client
      .listObjectsV2({
        Bucket: this.configService.get<string>('aws.s3.bucket') as string,
        FetchOwner: true,
        Prefix: this._getPrefixPath() + path,
        Delimiter: '/',
      })
      .promise();
    return result.KeyCount;
  }

  /**
   * remove a object by path
   * @param filePath string
   * @returns
   */
  public async deleteFileByPath(filePath: string) {
    config.update({
      region: this.configService.get<string>('aws.s3.region'),
      credentials: {
        accessKeyId: this.configService.get<string>(
          'aws.s3.client_id',
        ) as string,
        secretAccessKey: this.configService.get<string>(
          'aws.s3.secret_key',
        ) as string,
      },
    });
    const s3Client = new S3();
    const result = await s3Client
      .deleteObject({
        Key: filePath,
        Bucket: this.configService.get<string>('aws.s3.bucket') as string,
      })
      .promise();

    return result;
  }
}
