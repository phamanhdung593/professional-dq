import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import HmacValidator from '../../utils/hmac';
export type ProxyQuery = {
  shop: string;
  path_prefix: string;
  timestamp: string;
  signature?: string;
  logged_in_customer_id?: string;
  extra?: string[];
  customerId?: string;
  quantity?: number;
  price?: string;
  name?: string;
  id?: string;
  update?: string;
};

@Injectable()
export class ProxyService {
  constructor(private readonly config: ConfigService) {}

  public verifySignature(query: ProxyQuery) {
    const hashed = query.signature as string;
    delete query.signature;
    const queryString = Object.keys(query)
      .sort()
      .map((key: string) => `${key}=${query[key as keyof typeof query]}`)
      .join('');

    const hmacValidator = new HmacValidator(
      this.config.get<string>('shopify.api_secret') as string,
    );

    return hmacValidator.verify(hashed, queryString);
  }
}
