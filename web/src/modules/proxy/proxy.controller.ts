import { UserService } from './../user/user.service';
import { FaqMessageSettingsService } from './../faq-message-settings/faq-message-settings.service';
import { Controller, Get, Query, Req, Res } from '@nestjs/common';
import { ProxyService } from './proxy.service';
import { FaqService } from '../faq/faq.service';
import { SettingsService } from '../settings/settings.service';
import { Request, Response } from 'express';
import { join } from 'path';
import { transformSettingsWithLocale } from 'src/resource/setting.resource';
import { findLocaleShopByRequest } from 'src/utils/helper';
import { CategoriesService } from '../categories/categories.service';

@Controller('api/proxy')
export class ProxyController {
  constructor(
    private readonly proxyService: ProxyService,
    private readonly faqService: FaqService,
    private readonly userService: UserService,
    private readonly settingService: SettingsService,
    private readonly faqMessageSettingsService: FaqMessageSettingsService,
    private readonly categoryService: CategoriesService,
  ) {}

  @Get()
  async getFaqProxy(
    @Req() req: Request,
    @Query('shop') shop: string,
    @Query('plan') plan: string,
    @Query('page') page: number,
    @Res() res: Response,
  ) {
    try {
      let limit = 15;
      const user = await this.userService.findByShopDomain(shop);
      let plan = 'free';

      if (user?.merchants_plan) {
        plan = user.merchants_plan.plan.toLocaleLowerCase();
      }

      if (plan === 'free_01') {
        limit = 30;
      }

      if (plan !== 'free' && plan !== 'free_01') {
        limit = 200;
      }

      const messagesSetting =
        await this.faqMessageSettingsService.getDetailByUser(user.id);

      const setting = await this.settingService.findByUserId(user.id);

      res.app.set('views', join(__dirname, 'views'));
      res.app.set('view engine', 'ejs');
      res.app.set('views', './views');

      const locale = findLocaleShopByRequest(
        req,
        JSON.parse(user?.shopLocales)?.shopLocales,
      );

      const category = await this.categoryService.getCategories(user?.id);
      console.log('category-----', category);
      const categoryIdentities = category?.map((item) => item.identify);
      console.log('categoryIdentities-------', categoryIdentities);
      const categoryLocale = plan === 'free' ? 'default' : locale;
      const categoryIdentitiesLocales =
        await this.categoryService.findByIdentitiesAndLocaleOfThemeApp(
          user?.id,
          categoryIdentities,
          categoryLocale,
        );
      console.log('categoryIdentitiesLocales------', categoryIdentitiesLocales);

      const faq = await this.faqService.getFaq(user?.id, 'default', limit);
      const faqIdentities = faq?.map((item) => item.identify);
      console.log('faqIdentities------', faqIdentities);
      const faqLocale = plan === 'free' ? 'default' : locale;
      const faqResults =
        await this.faqService.findByIdentitiesAndLocaleOfThemeApp(
          user?.id,
          faqIdentities,
          faqLocale,
        );
      console.log('faqResults-------', faqResults);

      const hiddenCategoryIdentities = category
        ?.filter((cat) => cat.is_visible === false)
        .map((cat) => cat.identify);

      const hiddenFaqIdentities = faq
        ?.filter((item) => item.is_visible === false)
        .map((item) => item.identify);

      const faqIdentitiesLocales = faqResults?.filter(
        (faq) =>
          faq.is_visible !== false &&
          !hiddenCategoryIdentities?.includes(faq.category_identify) &&
          !hiddenFaqIdentities?.includes(faq.identify),
      );

      const resultIdentitiesLocales = categoryIdentitiesLocales?.filter(
        (category) =>
          category.is_visible !== false &&
          !hiddenCategoryIdentities?.includes(category.identify),
      );
      console.log('resultIdentitiesLocales----', resultIdentitiesLocales);

      const transformedData = transformSettingsWithLocale(setting, locale);

      if (setting.template_setting[0].image_banner) {
        setting.template_setting[0].image_banner =
          process.env.HOST_IMAGE + setting.template_setting[0].image_banner;
      }

      setting.template_setting[0].banner_default =
        process.env.HOST_IMAGE + setting.template_setting[0].banner_default;

      res.set('Content-Type', 'application/liquid');

      return res.render(join(__dirname, 'views', 'views'), {
        faqs: {
          faq: faqIdentitiesLocales,
          categories: resultIdentitiesLocales,
          user_id: user?.id,
        },
        setting: {
          data: transformedData,
          templateSetting: setting.template_setting[0],
        },
        messagesSetting,
        locale,
        plan,
        shop,
      });
    } catch (error) {
      console.error('Error in ProxyController:', error);
      return res
        .status(500)
        .send('An error occurred while processing the request.');
    }
  }
}
