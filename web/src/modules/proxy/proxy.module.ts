import { Module } from '@nestjs/common';
import { ProxyController } from './proxy.controller';
import { ThrottlerModule } from '@nestjs/throttler';
import { ProxyService } from './proxy.service';
import { SettingsModule } from '../settings/settings.module';
import { FaqMessageSettingsModule } from '../faq-message-settings/faq-message-settings.module';
import { FaqModule } from '../faq/faq.module';
import { UserModule } from '../user/user.module';
import { CategoriesModule } from '../categories/categories.module';

@Module({
  imports: [
    SettingsModule,
    FaqMessageSettingsModule,
    ThrottlerModule.forRoot([
      {
        ttl: 60,
        limit: 10,
      },
    ]),
    FaqModule,
    UserModule,
    CategoriesModule,
  ],
  controllers: [ProxyController],
  providers: [ProxyService],
})
export class ProxyModule {}
