import { Injectable } from '@nestjs/common';
import { PrismaService } from '../prisma/prisma.service';

@Injectable()
export class CategoriesRepository {
  constructor(private readonly prisma: PrismaService) {}

  async getCategories(conditions: object) {
    return await this.prisma.faq_category.findMany({
      where: conditions,
      include: {
        faq: {
          orderBy: {
            position: 'asc',
          },
        },
      },
      orderBy: {
        position: 'asc',
      },
    });
  }

  async listFaqs(userId: number) {
    return await this.prisma.faq_category.findMany({
      where: {
        user_id: userId,
        locale: 'default',
      },
      include: {
        faq: true,
      },
    });
  }

  async getCategoriesById(id: number) {
    return await this.prisma.faq_category.findMany({
      where: {
        id: id,
      },
    });
  }

  async createCategories(createFaqCategoryDto: any) {
    return await this.prisma.faq_category.create({
      data: createFaqCategoryDto,
    });
  }

  async updateCategory(id: number, updateFaqCategoryDto: any) {
    return await this.prisma.faq_category.update({
      where: {
        id: id,
      },
      data: updateFaqCategoryDto,
    });
  }

  async deleteCategory(id: number) {
    await this.prisma.faq.deleteMany({
      where: {
        category_id: id,
      },
    });

    return await this.prisma.faq_category.delete({
      where: {
        id: id,
      },
    });
  }

  async findById(id: number) {
    return await this.prisma.faq_category.findUnique({
      where: {
        id: id,
      },
    });
  }

  async findByIdentify(identify: string) {
    return await this.prisma.faq_category.findFirst({
      where: {
        identify: identify,
      },
    });
  }

  async findLocalesByIdentify(userId: number, identify: string) {
    return await this.prisma.faq_category.findMany({
      select: {
        locale: true,
      },
      where: {
        user_id: userId,
        identify: identify,
      },
    });
  }

  async findByTiles(userId: number, titles: string[]) {
    return await this.prisma.faq_category.findMany({
      where: {
        user_id: userId,
        title: {
          in: titles,
        },
      },
    });
  }

  async reloadIndexPosition(sortedCategories: any[]) {
    return await this.prisma.$transaction(
      sortedCategories.map((category, index) =>
        this.prisma.faq_category.update({
          where: { id: category.id },
          data: { position: index },
        }),
      ),
    );
  }

  async changePositionOfRangeOldIndexAndNewIndex(
    oldIndex: number,
    newIndex: number,
  ) {
    return await this.prisma.faq_category.updateMany({
      where: {
        position: { gte: oldIndex + 1, lte: newIndex },
      },
      data: {
        position: { decrement: 1 },
      },
    });
  }

  async isChangePositionOfRangeOldIndexAndNewIndex(
    oldIndex: number,
    newIndex: number,
  ) {
    return await this.prisma.faq_category.updateMany({
      where: {
        position: { gte: newIndex, lte: oldIndex - 1 },
      },
      data: {
        position: { increment: 1 },
      },
    });
  }

  async swapPosition(categoryId: number, newIndex: number) {
    await this.prisma.faq_category.update({
      where: { id: categoryId },
      data: { position: newIndex },
    });
  }

  async findByIdentifyAndLocale(identify: string, locale: string) {
    return await this.prisma.faq_category.findFirst({
      where: { identify: identify, locale: locale },
    });
  }

  async findByIdentitiesAndLocale(identifies: string[], locale: string) {
    return await this.prisma.faq_category.findMany({
      where: {
        identify: {
          in: identifies,
        },
        locale: locale,
      },
    });
  }

  async findByIdentitiesAndLocaleOfThemeApp(
    userId: number,
    identifies: string[],
    locale: string,
  ) {
    return await this.prisma.faq_category.findMany({
      where: {
        AND: [
          {
            identify: {
              in: identifies,
            },
          },
          {
            user_id: userId,
          },
          {
            OR: [
              {
                locale: locale,
              },
              {
                AND: [
                  { locale: 'default' },
                  {
                    NOT: {
                      identify: {
                        in: await this.prisma.faq_category
                          .findMany({
                            where: {
                              identify: { in: identifies },
                              locale: locale,
                              user_id: userId,
                            },
                            select: { identify: true },
                          })
                          .then((items) => items.map((item) => item.identify)),
                      },
                    },
                  },
                ],
              },
            ],
          },
        ],
      },
    });
  }
}
