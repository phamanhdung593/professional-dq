import {
  Controller,
  UseGuards,
  Get,
  Post,
  Req,
  Body,
  Param,
  Put,
  Delete,
} from '@nestjs/common';
import { ShopifyGuard } from '../shopify/shopify.guard';
import { CategoriesService } from './categories.service';
import { Request } from 'express';
import { CategoriesDTO } from './categories.dto';

@Controller('api/categories')
@UseGuards(ShopifyGuard)
export class CategoriesController {
  constructor(private readonly categoriesService: CategoriesService) {}

  @Get('/faqs')
  async getFaqs(@Req() req: Request & { user: { id: string } }) {
    return this.categoriesService.listFaqs(req.user.id);
  }

  @Get()
  async getCategories(@Req() req: Request & { user: any }) {
    const keyword = req.query.keyword as string;

    const merchantsPlan =
      req.user.merchants_plan?.plan?.toLowerCase() || 'free';

    const shopLocales = JSON.parse(req.user.shopLocales).shopLocales;

    const data: any = await this.categoriesService.getCategories(
      parseInt(req.user.id),
      keyword,
    );

    const results: any = await Promise.all(
      data.map(async (category: any) => {
        const locales = await this.categoriesService.findLocalesByIdentify(
          parseInt(req.user.id),
          category.identify,
        );

        let language = locales.map((item) => item.locale);

        if (merchantsPlan === 'free') {
          language = language.filter((locale) => locale === 'default');
        }

        const primaryLocale = shopLocales.find(
          (item) => item.primary === true,
        )?.locale;

        if (primaryLocale) {
          const updatedLocales = language.map((locale) =>
            locale === 'default' ? primaryLocale : locale,
          );
          language = updatedLocales;
        } else {
          language = language;
        }

        return {
          ...category,
          language: [...new Set(language)],
        };
      }),
    );

    return results;
  }

  @Get('/:id')
  async getCategoriesById(@Param('id') id: string) {
    return this.categoriesService.getCategoriesById(id);
  }

  @Post('/')
  async createCategories(
    @Req() req: Request & { user: { id: number } },
    @Body() createCategoriesDto: CategoriesDTO,
  ) {
    createCategoriesDto.user_id = req.user.id;
    return this.categoriesService.createCategories(createCategoriesDto);
  }

  @Put('/:id')
  async updateCategory(
    @Param('id') id: string,
    @Req() req: Request & { user: { id: number } },
    @Body() updateCategoriesDto: CategoriesDTO,
  ) {
    updateCategoriesDto.user_id = req.user.id;
    delete updateCategoriesDto.id;
    return this.categoriesService.updateCategory(
      parseInt(id),
      updateCategoriesDto,
    );
  }

  @Put('/show-hide-category/:id')
  async updateVisibilityFaq(
    @Req() req: Request & { user: any },
    @Param('id') id: string,
    @Body() updateCategoryDto: CategoriesDTO,
  ) {
    return this.categoriesService.showHideCategory(
      id,
      req.user,
      updateCategoryDto,
    );
  }

  @Delete('/:id')
  async deleteCategory(@Param('id') id: string) {
    return this.categoriesService.deleteCategory(id);
  }

  @Post('change-position')
  async changePosition(
    @Req() req: Request & { user: any },
    @Body()
    body: {
      category_id: number;
      old_index: number;
      new_index: number;
    },
  ) {
    await this.categoriesService.changePosition(req.user, body);

    return { code: 200, message: 'Position changed successfully' };
  }
}
