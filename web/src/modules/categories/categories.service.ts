import {
  Injectable,
  NotFoundException,
  UnauthorizedException,
} from '@nestjs/common';
import { CategoriesRepository } from './categories.repository';
import { UserService } from '../user/user.service';
import { CategoriesDTO } from './categories.dto';
import { randomString } from 'src/utils/helper';
import { UserDto } from '../user/user.dto';

@Injectable()
export class CategoriesService {
  constructor(
    private readonly categoriesRepository: CategoriesRepository,
    private readonly userService: UserService,
  ) {}

  async listFaqs(userId: string) {
    return this.categoriesRepository.listFaqs(parseInt(userId));
  }

  async getCategories(userId: number, keyword = '') {
    return this.categoriesRepository.getCategories({
      user_id: userId,
      locale: 'default',
      title: {
        contains: keyword,
      },
    });
  }

  async getCategoriesById(id: string) {
    return this.categoriesRepository.getCategoriesById(parseInt(id));
  }

  async createCategories(createCategoriesDto: CategoriesDTO) {
    const user = await this.userService.findById(createCategoriesDto.user_id);

    if (!user) {
      throw new NotFoundException('User not found');
    }

    if (createCategoriesDto.identify === 'Uncategorized1') {
      createCategoriesDto.identify = 'Uncategorized1';
    } else {
      createCategoriesDto.identify =
        createCategoriesDto.locale + randomString(6);
    }

    createCategoriesDto.createdAt = new Date();
    createCategoriesDto.updatedAt = new Date();

    return await this.categoriesRepository.createCategories(
      createCategoriesDto,
    );
  }

  async updateCategory(id: number, updateCategoriesDto: CategoriesDTO) {
    const category = await this.categoriesRepository.findById(id);
    if (!category) {
      throw new NotFoundException('Category not found');
    }

    updateCategoriesDto.updatedAt = new Date();

    const categoryLocale =
      await this.categoriesRepository.findByIdentifyAndLocale(
        category.identify,
        updateCategoriesDto.locale,
      );

    if (categoryLocale) {
      return await this.categoriesRepository.updateCategory(
        categoryLocale.id,
        updateCategoriesDto,
      );
    }

    if (category.identify === 'Uncategorized1') {
      updateCategoriesDto.identify = 'Uncategorized1';
    } else {
      updateCategoriesDto.identify = category.identify;
    }

    updateCategoriesDto.createdAt = new Date();
    updateCategoriesDto.updatedAt = new Date();

    return await this.categoriesRepository.createCategories(
      updateCategoriesDto,
    );
  }

  async findByIdentitiesAndLocale(identify: string[], locale: string) {
    return await this.categoriesRepository.findByIdentitiesAndLocale(
      identify,
      locale,
    );
  }

  async findByIdentitiesAndLocaleOfThemeApp(
    userId: number,
    identify: string[],
    locale: string,
  ) {
    return await this.categoriesRepository.findByIdentitiesAndLocaleOfThemeApp(
      userId,
      identify,
      locale,
    );
  }

  async showHideCategory(
    id: string,
    user: UserDto,
    updateCategoriesDto: CategoriesDTO,
  ) {
    updateCategoriesDto.createdAt = new Date();
    updateCategoriesDto.updatedAt = new Date();

    const categories = await this.categoriesRepository.findById(parseInt(id));
    if (!categories) {
      throw new NotFoundException('Faq not found');
    }

    if (categories.user_id !== user.id) {
      throw new UnauthorizedException(
        'You are not authorized to update this categories',
      );
    }

    return await this.categoriesRepository.updateCategory(
      categories.id,
      updateCategoriesDto,
    );
  }

  async deleteCategory(id: string) {
    const category = await this.categoriesRepository.findById(parseInt(id));
    if (!category) {
      throw new NotFoundException('Category not found');
    }

    return await this.categoriesRepository.deleteCategory(parseInt(id));
  }

  async findByIdentify(identify: string) {
    return await this.categoriesRepository.findByIdentify(identify);
  }

  async findLocalesByIdentify(userId: number, identify: string) {
    return await this.categoriesRepository.findLocalesByIdentify(
      userId,
      identify,
    );
  }

  async findByTitles(userId: number, titles: string[]) {
    return await this.categoriesRepository.findByTiles(userId, titles);
  }

  async changePosition(
    user: any,
    body: {
      category_id: number;
      old_index: number;
      new_index: number;
    },
  ) {
    const categories = await this.categoriesRepository.getCategories({
      user_id: user.id,
    });

    const isValidPosition = categories.every(
      (category, index) => category.position === index,
    );

    if (!isValidPosition) {
      const sortedCategories = [...categories].sort(
        (a, b) =>
          new Date(a.createdAt).getTime() - new Date(b.createdAt).getTime(),
      );

      await this.categoriesRepository.reloadIndexPosition(sortedCategories);
    }

    const { old_index, new_index, category_id } = body;

    if (old_index === new_index) {
      return { message: 'Position unchanged' };
    }

    if (old_index < new_index) {
      await this.categoriesRepository.changePositionOfRangeOldIndexAndNewIndex(
        old_index,
        new_index,
      );
    } else {
      await this.categoriesRepository.isChangePositionOfRangeOldIndexAndNewIndex(
        old_index,
        new_index,
      );
    }

    await this.categoriesRepository.swapPosition(category_id, new_index);

    return { message: 'Position changed successfully' };
  }
}
