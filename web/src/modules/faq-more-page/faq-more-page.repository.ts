import { Injectable } from '@nestjs/common';
import { PrismaService } from '../prisma/prisma.service';

@Injectable()
export class FaqMorePageRepository {
  constructor(private readonly prisma: PrismaService) {}

  async getFaqMorePage(
    conditions: { user_id: number; page_name: string },
    limit = 15,
  ) {
    return await this.prisma.faq_more_page.findMany({
      where: conditions,
      take: limit,
    });
  }

  async getFaqMorePageById(id: number) {
    return await this.prisma.faq_more_page.findUnique({
      where: {
        id: id,
      },
    });
  }

  async createOrUpdateFaqMorePage(userId: number, faqs: any, pageName: string) {
    await this.prisma.faq_more_page.deleteMany({
      where: {
        user_id: userId,
        page_name: pageName,
      },
    });

    const results = [];

    for (const faq of faqs) {
      const newRecord = await this.prisma.faq_more_page.create({
        data: {
          faq_identify: faq.identify,
          category_identify: faq.category_identify,
          page_name: pageName,
          createdAt: new Date(),
          updatedAt: new Date(),
          user: {
            connect: {
              id: userId,
            },
          },
          faq: {
            connect: {
              id: faq.id,
            },
          },
        },
      });
      results.push(newRecord);
    }

    return results;
  }

  async updateFaqMorePage(id: number, faqMorePageDto: any) {
    return await this.prisma.faq_more_page.update({
      where: {
        id: id,
      },
      data: faqMorePageDto,
    });
  }

  async deleteFaqMorePage(id: number) {
    await this.prisma.faq_more_page.delete({
      where: {
        id: id,
      },
    });
  }

  async findByUserIdAndPageName(userId: number, pageName: string) {
    return await this.prisma.faq_more_page.findMany({
      where: {
        user_id: userId,
        page_name: pageName,
      },
      include: {
        faq: true,
      },
    });
  }
}
