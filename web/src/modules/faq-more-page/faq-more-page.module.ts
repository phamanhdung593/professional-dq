import { Module } from '@nestjs/common';
import { FaqMorePageService } from './faq-more-page.service';
import { FaqMorePageController } from './faq-more-page.controller';
import { PrismaModule } from '../prisma/prisma.module';
import { UserModule } from '../user/user.module';
import { FaqMorePageRepository } from './faq-more-page.repository';
import { FaqModule } from '../faq/faq.module';

@Module({
  imports: [PrismaModule, UserModule, FaqModule],
  providers: [FaqMorePageService, FaqMorePageRepository],
  controllers: [FaqMorePageController],
  exports: [FaqMorePageService],
})
export class FaqMorePageModule {}
