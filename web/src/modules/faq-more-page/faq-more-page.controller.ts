import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Req,
  UseGuards,
} from '@nestjs/common';
import { ShopifyGuard } from '../shopify/shopify.guard';
import { FaqMorePageService } from './faq-more-page.service';
import { Request } from 'express';

@Controller('/api/faq-more-page')
@UseGuards(ShopifyGuard)
export class FaqMorePageController {
  constructor(private readonly faqMorePageService: FaqMorePageService) {}

  @Get()
  async getFaqMorePage(@Req() req: Request & { user: { id: string } }) {
    const data = await this.faqMorePageService.getFaqMorePage(
      parseInt(req.user.id),
      req.query.page_name as string,
    );

    return {
      code: 200,
      message: 'success',
      data: data,
    };
  }

  @Get('/:id')
  async getFaqMorePageById(@Param('id') id: string) {
    const data = await this.faqMorePageService.getFaqMorePageById(parseInt(id));

    return {
      code: 200,
      message: 'success',
      data: data,
    };
  }

  @Post('/')
  async createOrUpdate(
    @Req() req: Request & { user: { id: number } },
    @Body() createFaqMorePageDto: any,
  ) {
    const data = await this.faqMorePageService.createFaqMorePage(
      req.user.id,
      createFaqMorePageDto,
    );

    return {
      code: 200,
      message: 'success',
      data: data,
    };
  }

  @Delete('/:id')
  async deleteFaqMorePage(@Param('id') id: string) {
    await this.faqMorePageService.deleteFaqMorePage(parseInt(id));

    return {
      code: 200,
      message: 'success',
    };
  }
}
