import { BadRequestException, Injectable } from '@nestjs/common';
import { FaqMorePageRepository } from './faq-more-page.repository';
import { FaqService } from '../faq/faq.service';
import { UserService } from '../user/user.service';

@Injectable()
export class FaqMorePageService {
  constructor(
    private readonly faqMorePageRepository: FaqMorePageRepository,
    private readonly userService: UserService,
    private readonly faqService: FaqService,
  ) {}

  async getFaqMorePage(userId: number, pageName: string) {
    let limit = 15;

    const user = await this.userService.findById(userId);

    if (user?.merchants_plan?.plan === 'free_01') {
      limit = 30;
    }

    if (
      user?.merchants_plan?.plan !== 'free' &&
      user?.merchants_plan?.plan !== 'free_01'
    ) {
      limit = 200;
    }

    const conditions = {
      user_id: userId,
      page_name: 'index',
    };

    if (pageName) {
      conditions['page_name'] = pageName;
    }

    return await this.faqMorePageRepository.getFaqMorePage(conditions, limit);
  }

  async getFaqMorePageById(id: number) {
    return await this.faqMorePageRepository.getFaqMorePageById(id);
  }

  async createFaqMorePage(userId: number, faqMorePageDto: any) {
    const faqIds = faqMorePageDto.faq_ids;
    const pageName = faqMorePageDto.page_name;

    console.log(faqIds);
    const faqs = await this.faqService.findByIds(faqIds);

    if (faqs.length !== faqIds.length) {
      throw new BadRequestException(
        'Ids faqs or product no have data, data not match',
      );
    }

    return await this.faqMorePageRepository.createOrUpdateFaqMorePage(
      userId,
      faqs,
      pageName,
    );
  }

  async updateFaqMorePage(id: number, faqMorePageDto: any) {
    return await this.faqMorePageRepository.updateFaqMorePage(
      id,
      faqMorePageDto,
    );
  }

  async deleteFaqMorePage(id: number) {
    return await this.faqMorePageRepository.deleteFaqMorePage(id);
  }

  async findByUserIdAndPageName(userId: number, pageName: string) {
    return await this.faqMorePageRepository.findByUserIdAndPageName(
      userId,
      pageName,
    );
  }
}
