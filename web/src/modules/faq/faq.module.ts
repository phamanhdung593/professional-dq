import { Module } from '@nestjs/common';
import { FaqController } from './faq.controller';
import { FaqService } from './faq.service';
import { FaqRepository } from './faq.repository';
import { PrismaModule } from '../prisma/prisma.module';
import { UserModule } from '../user/user.module';
import { CategoriesModule } from '../categories/categories.module';

@Module({
  imports: [PrismaModule, UserModule, CategoriesModule],
  controllers: [FaqController],
  providers: [FaqService, FaqRepository],
  exports: [FaqService],
})
export class FaqModule {}
