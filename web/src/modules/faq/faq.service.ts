import { UserService } from './../user/user.service';
import {
  Injectable,
  NotFoundException,
  UnauthorizedException,
} from '@nestjs/common';
import { FaqRepository } from './faq.repository';
import { CategoriesService } from '../categories/categories.service';
import { FaqDTO, UpdateFaqDTO } from './faq.dto';
import { randomString } from 'src/utils/helper';
import { UserDto } from '../user/user.dto';

@Injectable()
export class FaqService {
  constructor(
    private readonly faqRepository: FaqRepository,
    private readonly userService: UserService,
    private readonly categoriesService: CategoriesService,
  ) {}

  async getFaq(userId: number, locale = 'default', limit = 15, keyword = '') {
    const user = await this.userService.findById(userId);

    let plan = 'free';

    if (user?.merchants_plan) {
      plan = user.merchants_plan.plan.toLocaleLowerCase();
    }

    if (plan === 'free_01') {
      limit = 30;
    }

    if (plan !== 'free' && plan !== 'free_01') {
      limit = 200;
    }

    return this.faqRepository.getFaq(userId, limit, locale, {
      title: {
        contains: keyword,
      },
    });
  }

  async findLocalesByIdentify(identity: string) {
    return await this.faqRepository.findByIdentify(identity);
  }

  async getFaqById(id: number, userId: number) {
    return this.faqRepository.getFaqById(id, userId);
  }

  async createFaq(userId: number, createFaqDto: FaqDTO) {
    createFaqDto.createdAt = new Date();
    createFaqDto.updatedAt = new Date();

    let categories = await this.categoriesService.findByIdentify(
      createFaqDto.category_identify,
    );

    createFaqDto.identify = randomString(10);

    if (createFaqDto.category_identify === 'Uncategorized1') {
      categories =
        await this.categoriesService.findByIdentify('Uncategorized1');

      if (!categories) {
        categories = await this.categoriesService.createCategories({
          user_id: userId,
          title: 'Uncategorized',
          description: 'Uncategorized1',
          is_visible: false,
          identify: 'Uncategorized1',
          feature_category: false,
          locale: 'default',
        });
      }
    }

    if (!categories) {
      throw new NotFoundException('Category not found');
    }

    return this.faqRepository.createFaq(userId, categories.id, createFaqDto);
  }

  async update(id: string, user: UserDto, createFaqDto: UpdateFaqDTO) {
    createFaqDto.updatedAt = new Date();

    const faq = await this.faqRepository.findById(parseInt(id));
    if (!faq) {
      throw new NotFoundException('Faq not found');
    }

    if (faq.user_id !== user.id) {
      throw new UnauthorizedException(
        'You are not authorized to update this faq',
      );
    }

    const faqLocale = await this.faqRepository.findByIdentifyAndLocale(
      faq.identify,
      createFaqDto.locale,
    );

    if (faqLocale) {
      return await this.faqRepository.updateFaq(faqLocale.id, createFaqDto);
    }

    createFaqDto.identify = faq.identify;
    createFaqDto.createdAt = new Date();
    createFaqDto.updatedAt = new Date();

    return await this.faqRepository.createFaq(
      user.id,
      faq.category_id,
      createFaqDto,
    );
  }

  async showHideFaq(id: string, user: UserDto, createFaqDto: UpdateFaqDTO) {
    createFaqDto.updatedAt = new Date();
    createFaqDto.createdAt = new Date();

    const faq = await this.faqRepository.findById(parseInt(id));
    if (!faq) {
      throw new NotFoundException('Faq not found');
    }

    if (faq.user_id !== user.id) {
      throw new UnauthorizedException(
        'You are not authorized to update this faq',
      );
    }

    return await this.faqRepository.updateFaq(faq.id, createFaqDto);
  }

  async deleteFaq(id: string, user: UserDto) {
    const faq = await this.faqRepository.findById(parseInt(id));
    if (!faq) {
      throw new NotFoundException('Faq not found');
    }

    if (faq.user_id !== user.id) {
      throw new UnauthorizedException(
        'You are not authorized to delete this faq',
      );
    }

    return this.faqRepository.deleteFaq(parseInt(id));
  }

  async findByIds(ids: number[], locale = 'default') {
    return this.faqRepository.findByIds(ids, locale);
  }

  async createFaqs(
    data: {
      Question: string;
      Answer: string;
      Category: string;
      Faq_visible: string;
      Category_visible: string;
    }[],
    user: any,
  ) {
    for (const faq of data) {
      const category = await this.categoriesService.createCategories({
        user_id: user.id,
        title: faq.Category,
        description: '',
        is_visible: false,
        identify: randomString(10),
        feature_category: false,
        locale: 'default',
      });

      await this.faqRepository.createFaq(user.id, category.id, {
        title: faq.Question,
        content: faq.Answer,
        is_visible: parseInt(faq.Faq_visible) === 1 ? true : false,
        locale: 'default',
        feature_faq: true,
        category_identify: category.identify,
        createdAt: new Date(),
        updatedAt: new Date(),
        identify: randomString(10),
      });
    }

    return 'Success';
  }

  async findByFeatureActive(userId: number) {
    return await this.faqRepository.findByFeatureActive(userId);
  }

  async getAll(userId: number) {
    return await this.faqRepository.findAll(userId);
  }

  async changePosition(
    user: UserDto,
    body: {
      faq_id: number;
      old_index: number;
      new_index: number;
      category_id: number;
    },
  ) {
    const faqs = await this.faqRepository.findByCategoryId(body.category_id);

    const isValidPosition = faqs.every((faq, index) => faq.position === index);

    if (!isValidPosition) {
      const sortedFaqs = [...faqs].sort(
        (a, b) =>
          new Date(a.createdAt).getTime() - new Date(b.createdAt).getTime(),
      );

      await this.faqRepository.reloadIndexPosition(sortedFaqs);
    }

    const { old_index, new_index, faq_id, category_id } = body;

    if (old_index === new_index) {
      return { message: 'Position unchanged' };
    }

    if (old_index < new_index) {
      await this.faqRepository.changePositionOfRangeOldIndexAndNewIndex(
        old_index,
        new_index,
        category_id,
      );
    } else {
      await this.faqRepository.isChangePositionOfRangeOldIndexAndNewIndex(
        old_index,
        new_index,
        category_id,
      );
    }

    await this.faqRepository.swapPosition(faq_id, new_index);

    return { message: 'Position changed successfully' };
  }

  async findByIdentitiesAndLocale(identify: string[], locale: string) {
    return await this.faqRepository.findByIdentitiesAndLocale(identify, locale);
  }

  async findByIdentitiesAndLocaleOfThemeApp(
    userId: number,
    identify: string[],
    locale: string,
  ) {
    return await this.faqRepository.findByIdentitiesAndLocaleOfThemeApp(
      userId,
      identify,
      locale,
    );
  }

  async findByIdentitiesAndLocaleOfThemeAppProduct(
    userId: number,
    identify: string[],
    locale: string,
    IdProduct: number,
  ) {
    return await this.faqRepository.findByIdentitiesAndLocaleOfThemeAppProduct(
      userId,
      identify,
      locale,
      IdProduct,
    );
  }
}
