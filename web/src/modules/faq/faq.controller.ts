import {
  BadRequestException,
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Req,
  Res,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { FaqService } from './faq.service';
import { ShopifyGuard } from '../shopify/shopify.guard';
import { FaqDTO, UpdateFaqDTO } from './faq.dto';
import { Request, Response } from 'express';
import { FileInterceptor } from '@nestjs/platform-express';
import * as csvParser from 'csv-parser';
import * as stream from 'stream';
import { Parser } from 'json2csv';
import { CategoriesService } from '../categories/categories.service';

@Controller('api/faqs')
@UseGuards(ShopifyGuard)
export class FaqController {
  constructor(
    private readonly faqService: FaqService,
    private readonly categoryService: CategoriesService,
  ) {}

  @Get()
  async getFaq(@Req() req: Request & { user: any }) {
    const keyword = req.query.keyword as string;
    const merchantsPlan =
      req.user.merchants_plan?.plan?.toLowerCase() || 'free';

    const shopLocales = JSON.parse(req.user.shopLocales).shopLocales;

    const data: any = await this.faqService.getFaq(
      req.user.id,
      'default',
      15,
      keyword,
    );

    const results: any = await Promise.all(
      data.map(async (faq: any) => {
        const locales = await this.faqService.findLocalesByIdentify(
          faq.identify,
        );

        let language = locales.map((item) => item.locale);

        if (merchantsPlan === 'free') {
          language = language.filter((locale) => locale === 'default');
        }

        const primaryLocale = shopLocales.find(
          (item) => item.primary === true,
        )?.locale;

        if (primaryLocale) {
          const updatedLocales = language.map((locale) =>
            locale === 'default' ? primaryLocale : locale,
          );
          language = updatedLocales;
        } else {
          language = language;
        }

        return {
          ...faq,
          language: language,
        };
      }),
    );

    return results;
  }

  @Get('/:identify/locales')
  async getFaqLocales(@Param('identify') identify: string) {
    return this.faqService.findLocalesByIdentify(identify);
  }

  @Get('/:id')
  async getFaqById(
    @Param('id') id: string,
    @Req() req: Request & { user: { id: number } },
  ) {
    return this.faqService.getFaqById(parseInt(id), req.user.id);
  }

  @Put('/:id')
  async updateFaq(
    @Req() req: Request & { user: any },
    @Param('id') id: string,
    @Body() createFaqDto: UpdateFaqDTO,
  ) {
    return this.faqService.update(id, req.user, createFaqDto);
  }

  @Put('/show-hide-faq/:id')
  async updateVisibilityFaq(
    @Req() req: Request & { user: any },
    @Param('id') id: string,
    @Body() updateFaqDto: UpdateFaqDTO,
  ) {
    return this.faqService.showHideFaq(id, req.user, updateFaqDto);
  }

  @Post('/')
  async createFaq(
    @Req() req: Request & { user: any },
    @Body() createFaqDto: FaqDTO,
  ) {
    return this.faqService.createFaq(req.user.id, createFaqDto);
  }

  @Delete('/:id')
  async deleteFaq(
    @Req() req: Request & { user: any },
    @Param('id') id: string,
  ) {
    return this.faqService.deleteFaq(id, req.user);
  }

  @UseInterceptors(
    FileInterceptor('file', {
      limits: {
        files: 1,
        fileSize: 5 * 1024 * 1024,
      },
      fileFilter: (req, file, callback) => {
        if (file.mimetype !== 'text/csv') {
          return callback(
            new BadRequestException('Only CSV files are allowed.'),
            false,
          );
        }
        callback(null, true);
      },
    }),
  )
  @Post('/import-csv')
  async importCsv(
    @UploadedFile() file: Express.Multer.File,
    @Req() req: Request & { user: any },
  ) {
    if (!file) {
      throw new BadRequestException('File is required.');
    }
    const parsedData = await this.readCsvFromBuffer(file.buffer);
    const categoryArray = parsedData.map((item) => item.Category);

    const categories = await this.categoryService.findByTitles(
      req.user.id,
      categoryArray,
    );

    if (categories.length > 0) {
      throw new BadRequestException('Category has existed ');
    }

    await this.faqService.createFaqs(parsedData, req.user);

    return { message: 'CSV file processed successfully.' };
  }

  private readCsvFromBuffer(buffer: Buffer): Promise<any[]> {
    return new Promise((resolve, reject) => {
      const rows: any[] = [];
      const readable = new stream.Readable();
      readable.push(buffer);
      readable.push(null);

      readable
        .pipe(csvParser())
        .on('data', (row) => rows.push(row))
        .on('end', () => resolve(rows))
        .on('error', (error) => reject(error));
    });
  }

  @Post('/export-csv')
  async exportCsv(@Req() req: Request & { user: any }, @Res() res: Response) {
    try {
      const data = await this.faqService.getAll(req.user.id);

      const formattedData = data.map((item) => ({
        Question: item.title,
        Answer: `${item.content}`,
        Category: item.faq_category.title || '',
        Faq_visible: item.is_visible ? 1 : 0,
        Category_visible: item.faq_category?.is_visible ? 1 : 0,
      }));

      const fields = [
        'Question',
        'Answer',
        'Category',
        'Faq_visible',
        'Category_visible',
      ];

      const json2csvParser = new Parser({ fields });
      const csv = json2csvParser.parse(formattedData);

      res.header('Content-Type', 'text/csv');
      res.header(
        'Content-Disposition',
        'attachment; filename="exported_data.csv"',
      );
      res.status(200).send(csv);
    } catch (error) {
      console.error('Error exporting CSV:', error);
      res.status(500).json({ message: 'Failed to export CSV.' });
    }
  }

  @Post('change-position')
  async changePosition(
    @Req() req: Request & { user: any },
    @Body()
    body: {
      faq_id: number;
      old_index: number;
      new_index: number;
      category_id: number;
    },
  ) {
    await this.faqService.changePosition(req.user, body);

    return { code: 200, message: 'Position changed successfully' };
  }
}
