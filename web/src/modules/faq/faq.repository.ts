import { Injectable } from '@nestjs/common';
import { PrismaService } from '../prisma/prisma.service';

@Injectable()
export class FaqRepository {
  constructor(private readonly prisma: PrismaService) {}

  async findByIdentify(identify: string) {
    return await this.prisma.faq.findMany({
      select: {
        locale: true,
      },
      where: {
        identify: identify,
      },
    });
  }

  /**
   *
   * @param userId
   * @param conditions
   * @returns
   */
  async getFaq(
    userId: number,
    limit = 15,
    locale = 'default',
    conditions: object,
  ) {
    return await this.prisma.faq.findMany({
      where: {
        ...conditions,
        user_id: userId,
        locale: locale,
      },
      include: {
        faq_more_page: true,
        faq_product: true,
        faq_category: true,
      },
      orderBy: {
        position: 'asc',
      },
      take: limit,
    });
  }

  async getFaqById(id: number, userId: number) {
    return await this.prisma.faq.findMany({
      where: {
        id: id,
        user_id: userId,
      },
      include: {
        faq_more_page: true,
        faq_product: true,
      },
    });
  }

  async createFaq(userId: number, categoryId: number, createFaqDto: any) {
    delete createFaqDto.id;
    return await this.prisma.faq.create({
      data: {
        ...createFaqDto,
        user: {
          connect: {
            id: userId,
          },
        },
        faq_category: {
          connect: {
            id: categoryId,
          },
        },
      },
    });
  }

  async updateFaq(id: number, createFaqDto: any) {
    return await this.prisma.faq.update({
      where: {
        id: id,
      },
      data: createFaqDto,
    });
  }

  async deleteFaq(id: number) {
    return await this.prisma.faq.delete({
      where: {
        id: id,
      },
    });
  }

  async findById(id: number) {
    return await this.prisma.faq.findUnique({
      where: {
        id: id,
      },
    });
  }

  async findByIds(ids: number[], locale = 'default') {
    return await this.prisma.faq.findMany({
      where: {
        id: {
          in: ids,
        },
        locale: locale,
      },
    });
  }

  async findByFeatureActive(userId: number) {
    return await this.prisma.faq.findMany({
      where: {
        user_id: userId,
        feature_faq: true,
      },
    });
  }

  async findAll(userId: number) {
    return await this.prisma.faq.findMany({
      where: {
        user_id: userId,
      },
      include: {
        faq_category: true,
      },
    });
  }

  async changePositionOfRangeOldIndexAndNewIndex(
    oldIndex: number,
    newIndex: number,
    categoryId: number,
  ) {
    return await this.prisma.faq.updateMany({
      where: {
        category_id: categoryId,
        position: { gte: oldIndex + 1, lte: newIndex },
      },
      data: {
        position: { decrement: 1 },
      },
    });
  }

  async isChangePositionOfRangeOldIndexAndNewIndex(
    oldIndex: number,
    newIndex: number,
    categoryId: number,
  ) {
    return await this.prisma.faq.updateMany({
      where: {
        category_id: categoryId,
        position: { gte: newIndex, lte: oldIndex - 1 },
      },
      data: {
        position: { increment: 1 },
      },
    });
  }

  async findByCategoryId(categoryId: number) {
    return await this.prisma.faq.findMany({
      where: {
        category_id: categoryId,
      },
      orderBy: { position: 'asc' },
    });
  }

  async reloadIndexPosition(sortedFaqs: any[]) {
    return await this.prisma.$transaction(
      sortedFaqs.map((faq, index) =>
        this.prisma.faq.update({
          where: { id: faq.id },
          data: { position: index },
        }),
      ),
    );
  }

  async updateNewPositionById(id: number, position: number) {
    return await this.prisma.faq.update({
      where: { id: id },
      data: { position: position },
    });
  }

  async swapPosition(faqId: number, newIndex: number) {
    await this.prisma.faq.update({
      where: { id: faqId },
      data: { position: newIndex },
    });
  }

  async findByIdentifyAndLocale(identify: string, locale: string) {
    return await this.prisma.faq.findFirst({
      where: {
        identify: identify,
        locale: locale,
      },
    });
  }

  async findByIdentitiesAndLocale(identifies: string[], locale: string) {
    return await this.prisma.faq.findMany({
      where: {
        identify: {
          in: identifies,
        },
        locale: locale,
      },
    });
  }

  async findByIdentitiesAndLocaleOfThemeApp(
    userId: number,
    identifies: string[],
    locale: string,
  ) {
    return await this.prisma.faq.findMany({
      where: {
        AND: [
          {
            identify: {
              in: identifies,
            },
          },
          {
            user_id: userId,
          },
          {
            OR: [
              {
                locale: locale,
              },
              {
                AND: [
                  { locale: 'default' },
                  {
                    NOT: {
                      identify: {
                        in: await this.prisma.faq
                          .findMany({
                            where: {
                              identify: { in: identifies },
                              locale: locale,
                              user_id: userId,
                            },
                            select: { identify: true },
                          })
                          .then((items) => items.map((item) => item.identify)),
                      },
                    },
                  },
                ],
              },
            ],
          },
        ],
      },
    });
  }

  async findByIdentitiesAndLocaleOfThemeAppProduct(
    userId: number,
    identifies: string[],
    locale: string,
    IdProduct: number,
  ) {
    return await this.prisma.faq.findMany({
      where: {
        AND: [
          {
            identify: {
              in: identifies,
            },
          },
          {
            user_id: userId,
          },
          {
            faq_product: {
              some: {
                product_id: IdProduct,
              },
            },
          },
          {
            OR: [
              {
                locale: locale,
              },
              {
                AND: [
                  { locale: 'default' },
                  {
                    NOT: {
                      identify: {
                        in: await this.prisma.faq
                          .findMany({
                            where: {
                              identify: { in: identifies },
                              locale: locale,
                              user_id: userId,
                              faq_product: {
                                some: {
                                  product_id: IdProduct,
                                },
                              },
                            },
                            select: { identify: true },
                          })
                          .then((items) => items.map((item) => item.identify)),
                      },
                    },
                  },
                ],
              },
            ],
          },
        ],
      },
    });
  }
}
