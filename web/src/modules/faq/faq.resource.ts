export class FaqResource {
  id: number;
  title: string;
  content: string;
  categoryTitle: string;
  locale: string;
  categoryIdentify: string;
  isVisible: boolean;
  featureFaq: boolean;
  categoryId: number;
  faqMorePage: any[];
  faqProduct: any[];
  category: any;
  additionalInfo: any;

  constructor(faqData: any, additionalInfo: string) {
    this.id = faqData.id;
    this.title = faqData.title;
    this.content = faqData.content;
    this.categoryTitle = faqData.faq_category.title;
    this.locale = faqData.locale;
    this.categoryIdentify = faqData.faq_category.identify;
    this.isVisible = faqData.is_visible;
    this.featureFaq = faqData.feature_faq;
    this.categoryId = faqData.category_id;
    this.faqMorePage = faqData.faq_more_page;
    this.faqProduct = faqData.faq_product;
    this.category = faqData.faq_category;
    this.additionalInfo = additionalInfo;
  }

  static transform(faqData: any[], additionalInfoList: any[]): FaqResource[] {
    return faqData.map(
      (faq, index) => new FaqResource(faq, additionalInfoList[index]),
    );
  }
}
