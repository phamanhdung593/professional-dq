import {
  IsBoolean,
  IsDate,
  IsInt,
  IsString,
  IsOptional,
  Min,
  MaxLength,
  IsArray,
  ValidateNested,
} from 'class-validator';
import { Type } from 'class-transformer';
import { UserDto } from '../user/user.dto';

export class FaqDTO {
  @IsOptional()
  @IsInt()
  id?: number;

  @IsOptional()
  @IsInt()
  user_id?: number;

  @IsString()
  @MaxLength(255)
  category_identify: string;

  @IsOptional()
  @IsString()
  @MaxLength(255)
  identify?: string;

  @IsString()
  @MaxLength(255)
  title: string;

  @IsString()
  content: string;

  @IsBoolean()
  is_visible: boolean;

  @IsOptional()
  @IsInt()
  @Min(0)
  position?: number;

  @IsOptional()
  @IsString()
  @MaxLength(10)
  locale?: string;

  @IsOptional()
  @IsDate()
  createdAt?: Date;

  @IsOptional()
  @IsDate()
  updatedAt?: Date;

  @IsOptional()
  @IsBoolean()
  feature_faq?: boolean;

  @IsOptional()
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => FaqMorePage)
  faq_more_page?: FaqMorePage[];

  @IsOptional()
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => FaqProduct)
  faq_product?: FaqProduct[];

  @IsOptional()
  user?: UserDto;
}
export class FaqMorePage {
  @IsOptional()
  @IsInt()
  id?: number;

  @IsOptional()
  @IsInt()
  faq_id?: number;

  @IsString()
  @MaxLength(255)
  faq_identify: string;

  @IsString()
  @MaxLength(255)
  category_identify: string;

  @IsString()
  @MaxLength(255)
  page_name: string;

  @IsOptional()
  @IsDate()
  createdAt?: Date;

  @IsOptional()
  @IsDate()
  updatedAt?: Date;
}

export class FaqProduct {
  @IsOptional()
  @IsInt()
  id?: number;

  @IsOptional()
  @IsInt()
  faq_id?: number;

  @IsString()
  @MaxLength(255)
  faq_identify: string;

  @IsString()
  @MaxLength(255)
  category_identify: string;

  @IsOptional()
  @IsDate()
  createdAt?: Date;

  @IsOptional()
  @IsDate()
  updatedAt?: Date;
}

export class UpdateFaqDTO {
  @IsOptional()
  @IsString()
  @MaxLength(255)
  category_identify?: string;

  @IsOptional()
  @IsString()
  @MaxLength(255)
  identify?: string;

  @IsOptional()
  @IsString()
  @MaxLength(255)
  title?: string;

  @IsOptional()
  @IsString()
  content?: string;

  @IsOptional()
  @IsBoolean()
  is_visible?: boolean;

  @IsOptional()
  @IsInt()
  @Min(0)
  position?: number;

  @IsOptional()
  @IsString()
  @MaxLength(10)
  locale?: string;

  @IsOptional()
  @IsBoolean()
  feature_faq?: boolean;

  @IsOptional()
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => FaqMorePage)
  faq_more_page?: FaqMorePage[];

  @IsOptional()
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => FaqProduct)
  faq_product?: FaqProduct[];

  @IsOptional()
  @IsDate()
  createdAt?: Date;

  @IsOptional()
  @IsDate()
  updatedAt?: Date;
}
