import { SettingsService } from './../settings/settings.service';
import { Injectable } from '@nestjs/common';
import { FaqService } from '../faq/faq.service';
import { CategoriesService } from '../categories/categories.service';
import { TemplateSettingsService } from '../template-settings/template-settings.service';
import { UserService } from '../user/user.service';
import { getLocaleDefault } from 'src/utils/helper';
import { FaqMorePageService } from '../faq-more-page/faq-more-page.service';
import { FaqMorePageSettingsService } from '../faq-more-page-settings/faq-more-page-settings.service';
import { ProductsService } from '../products/products.service';

@Injectable()
export class ThemeAppBlockService {
  constructor(
    private readonly faqService: FaqService,
    private readonly categoryService: CategoriesService,
    private readonly settingService: SettingsService,
    private readonly faqMorePageService: FaqMorePageService,
    private readonly faqMorePageSettingService: FaqMorePageSettingsService,
    private readonly productService: ProductsService,
    private readonly userService: UserService,
    private readonly templateSettingService: TemplateSettingsService,
  ) {}

  async getAppBlockByShopProductLocale(
    shopDomain: string,
    productId: string,
    locale: string,
  ) {
    const user = await this.userService.findByShopDomain(shopDomain);
    let plan = 'free';

    if (user?.merchants_plan) {
      plan = user.merchants_plan.plan.toLocaleLowerCase();
    }

    const faqsLocale = plan === 'free' ? 'default' : locale;

    if (!user) {
      return {
        faq: [],
        category: [],
        templateSetting: [],
      };
    }

    const product = await this.productService.findByProductId(
      user.id,
      productId,
    );

    console.log('product-----', product);

    if (!product?.faq_product?.length) {
      return {
        faq: [],
        category: [],
        templateSetting: [],
      };
    }

    const IdProduct = product.faq_product[0].product_id;
    console.log('IdProduct-----', IdProduct);

    const faqs = product.faq_product.map((fp) => fp.faq);
    console.log('faqs-------', faqs);
    const faqIdentities = faqs.map((faq) => faq.identify);
    console.log('faqIdentities----', faqIdentities);
    const faqResults =
      await this.faqService.findByIdentitiesAndLocaleOfThemeAppProduct(
        user.id,
        faqIdentities,
        faqsLocale,
        IdProduct,
      );
    console.log('faqResults----', faqResults);

    const setting = await this.settingService.findByUserId(user.id);
    const categories = await this.categoryService.getCategories(user.id);
    const categoryIdentify = categories.map((item) => item.identify);
    const categoryIdentifyLocale =
      await this.categoryService.findByIdentitiesAndLocaleOfThemeApp(
        user.id,
        categoryIdentify,
        faqsLocale,
      );

    const hiddenCategoryIdentities = categories
      ?.filter((cat) => cat.is_visible === false)
      .map((cat) => cat.identify);

    const hiddenFaqIdentities = faqs
      ?.filter((item) => item.is_visible === false)
      .map((item) => item.identify);

    const faqIdentitiesLocales = faqResults?.filter(
      (faq) =>
        faq.is_visible !== false &&
        !hiddenCategoryIdentities?.includes(faq.category_identify) &&
        !hiddenFaqIdentities?.includes(faq.identify),
    );

    const resultIdentitiesLocales = categoryIdentifyLocale?.filter(
      (category) =>
        category.is_visible !== false &&
        !hiddenCategoryIdentities?.includes(category.identify),
    );
    console.log('faqIdentitiesLocales-----', faqIdentitiesLocales);

    return {
      faq: faqIdentitiesLocales,
      category: resultIdentitiesLocales,
      templateSetting: setting.template_setting[0],
    };
  }

  async getAppBlockByShopProductLocaleFeatureFaq(shopDomain: string) {
    console.log('proxy...   ', shopDomain);
    const user = await this.userService.findByShopDomain(shopDomain);

    if (!user) {
      return {
        faq: [],
        category: [],
        templateSetting: [],
      };
    }

    const faqs = await this.faqService.findByFeatureActive(user.id);

    const setting = await this.settingService.findByUserId(user.id);

    return {
      faq: faqs,
      category: user.faq_category,
      templateSetting: setting.template_setting[0],
    };
  }

  async getAppBlockForPage(
    shopDomain: string,
    pageName: string,
    locale: string,
  ) {
    const user = await this.userService.findByShopDomain(shopDomain);
    let plan = 'free';

    if (user?.merchants_plan) {
      plan = user.merchants_plan.plan.toLocaleLowerCase();
    }

    if (plan === 'free') {
      return {
        faq: [],
        category: [],
        templateSetting: [],
      };
    }

    const morePageSetting = await this.faqMorePageSettingService.findByUserId(
      user.id,
    );

    const pageVisibilityMap: Record<string, boolean> = {
      index: morePageSetting[0].home_page_visible,
      product: morePageSetting[0].product_page_visible,
      cart: morePageSetting[0].cart_page_visible,
      collection: morePageSetting[0].collection_page_visible,
      page: morePageSetting[0].cms_page_visible,
    };

    if (!pageVisibilityMap[pageName]) {
      return { faq: [], category: [], templateSetting: [] };
    }

    const shopLocales = JSON.parse(user.shopLocales).shopLocales;
    const localeShop =
      plan === 'free' ? 'default' : getLocaleDefault(locale, shopLocales);

    const faqIds = await this.faqMorePageService.findByUserIdAndPageName(
      user.id,
      pageName,
    );

    const faqs = faqIds.map((item) => item.faq);
    const faqIdentities = faqs.map((faq) => faq.identify);
    const faqResults =
      await this.faqService.findByIdentitiesAndLocaleOfThemeApp(
        user.id,
        faqIdentities,
        localeShop,
      );

    const categories = await this.categoryService.getCategories(user.id);
    const categoryIdentify = categories.map((item) => item.identify);
    const categoryIdentifyLocale =
      await this.categoryService.findByIdentitiesAndLocaleOfThemeApp(
        user.id,
        categoryIdentify,
        localeShop,
      );

    const setting = await this.settingService.findByUserId(user.id);

    const hiddenCategoryIdentities = categories
      ?.filter((cat) => cat.is_visible === false)
      .map((cat) => cat.identify);

    const hiddenFaqIdentities = faqs
      ?.filter((item) => item.is_visible === false)
      .map((item) => item.identify);

    const faqIdentitiesLocales = faqResults?.filter(
      (faq) =>
        faq.is_visible !== false &&
        !hiddenCategoryIdentities?.includes(faq.category_identify) &&
        !hiddenFaqIdentities?.includes(faq.identify),
    );

    const resultIdentitiesLocales = categoryIdentifyLocale?.filter(
      (category) =>
        category.is_visible !== false &&
        !hiddenCategoryIdentities?.includes(category.identify),
    );

    return {
      faq: faqIdentitiesLocales,
      category: resultIdentitiesLocales,
      templateSetting: setting.template_setting[0],
    };
  }
}
