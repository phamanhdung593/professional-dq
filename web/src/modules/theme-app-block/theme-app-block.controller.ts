import { Controller, Get, Param } from '@nestjs/common';
import { ThemeAppBlockService } from './theme-app-block.service';
import { UserService } from '../user/user.service';

@Controller('api/no-token')
export class ThemeAppBlockController {
  constructor(
    private readonly themeAppBlockService: ThemeAppBlockService,
    private readonly userService: UserService,
  ) {}

  @Get('block-more-page/:shopDomain/:pageName/:locale')
  async getAppBlockMorePage(
    @Param('shopDomain') shopDomain: string,
    @Param('pageName') pageName: string,
    @Param('locale') locale: string,
  ) {
    const appBlock = await this.themeAppBlockService.getAppBlockForPage(
      shopDomain,
      pageName,
      locale,
    );
    return {
      ...appBlock,
    };
  }

  @Get('feature-faq/:id')
  async getFeatureFaq(@Param('id') id: string) {
    return await this.themeAppBlockService.getAppBlockByShopProductLocaleFeatureFaq(
      id,
    );
  }

  @Get('block/:shopDomain/:productId/:locale')
  async getAppBlockForShopProduct(
    @Param('shopDomain') shopDomain: string,
    @Param('productId') productId: string,
    @Param('locale') locale: string,
  ) {
    return await this.themeAppBlockService.getAppBlockByShopProductLocale(
      shopDomain,
      productId,
      locale,
    );
  }
}
