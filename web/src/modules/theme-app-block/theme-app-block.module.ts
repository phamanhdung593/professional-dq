import { Module } from '@nestjs/common';
import { ThemeAppBlockController } from './theme-app-block.controller';
import { ThemeAppBlockService } from './theme-app-block.service';
import { FaqModule } from '../faq/faq.module';
import { CategoriesModule } from '../categories/categories.module';
import { SettingsModule } from '../settings/settings.module';
import { UserModule } from '../user/user.module';
import { FaqMorePageModule } from '../faq-more-page/faq-more-page.module';
import { TemplateSettingsModule } from '../template-settings/template-settings.module';
import { FaqMorePageSettingsModule } from '../faq-more-page-settings/faq-more-page-settings.module';
import { ProductsModule } from '../products/products.module';

@Module({
  imports: [
    FaqModule,
    CategoriesModule,
    SettingsModule,
    UserModule,
    TemplateSettingsModule,
    FaqMorePageModule,
    FaqMorePageSettingsModule,
    ProductsModule,
  ],
  controllers: [ThemeAppBlockController],
  providers: [ThemeAppBlockService],
})
export class ThemeAppBlockModule {}
