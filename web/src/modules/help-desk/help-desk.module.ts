import { Module } from '@nestjs/common';
import { HelpDeskController } from './help-desk.controller';
import { HelpDeskService } from './help-desk.service';
import { FaqMessageSettingsModule } from '../faq-message-settings/faq-message-settings.module';
import { UserModule } from '../user/user.module';

@Module({
  imports: [FaqMessageSettingsModule, UserModule],
  controllers: [HelpDeskController],
  providers: [HelpDeskService],
})
export class HelpDeskModule {}
