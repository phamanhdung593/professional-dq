import { UserService } from './../user/user.service';
import { Controller, Get, Param } from '@nestjs/common';
import { FaqMessageSettingsService } from '../faq-message-settings/faq-message-settings.service';

@Controller('api/help-desk')
export class HelpDeskController {
  constructor(
    private readonly faqMessageSettingService: FaqMessageSettingsService,
    private readonly userService: UserService,
  ) {}

  @Get('/messages/:shopDomain')
  async getHelpDesk(@Param('shopDomain') shopDomain: string) {
    const user = await this.userService.findByShopDomain(shopDomain);

    if (!user) {
      return { message_setting: null };
    }

    const messageSettings = await this.faqMessageSettingService.getDetailByUser(
      user.id,
    );

    return { message_setting: messageSettings };
  }
}
