import {
  ForbiddenException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { TemplateSettingsRepository } from './template_setting.repository';
import { TemplateDefault } from 'src/types/template';
import { SettingsService } from '../settings/settings.service';

@Injectable()
export class TemplateSettingsService {
  constructor(
    private readonly templateSettingRepository: TemplateSettingsRepository,
    private readonly settingService: SettingsService,
  ) {}

  async initTemplateSettingDefault(settingId: number) {
    return await this.templateSettingRepository.create(
      settingId,
      TemplateDefault,
    );
  }

  async getTemplateSettings(user: any) {
    const setting = await this.settingService.findByUserId(user.id);

    return await this.templateSettingRepository.getTemplateSettings(setting.id);
  }

  async updateTemplateSetting(
    id: number,
    user: any,
    updateTemplateSettingDto: any,
  ) {
    const setting = await this.settingService.findByUserId(user.id);

    const templateSetting =
      await this.templateSettingRepository.findBySettingId(setting.id);

    if (!templateSetting) {
      throw new NotFoundException('Template setting not found');
    }

    if (templateSetting.id && templateSetting.id !== id) {
      throw new ForbiddenException(
        'You are not allowed to update this template',
      );
    }

    if (setting.id !== updateTemplateSettingDto.setting_id) {
      throw new ForbiddenException(
        'You are not allowed to update this template',
      );
    }

    return await this.templateSettingRepository.updateTemplateSetting(
      id,
      updateTemplateSettingDto,
    );
  }
}
