import { Module } from '@nestjs/common';
import { TemplateSettingsService } from './template-settings.service';
import { TemplateSettingsController } from './template-settings.controller';
import { TemplateSettingsRepository } from './template_setting.repository';
import { PrismaModule } from '../prisma/prisma.module';
import { UserModule } from '../user/user.module';
import { SettingsModule } from '../settings/settings.module';

@Module({
  imports: [PrismaModule, UserModule, SettingsModule],
  providers: [TemplateSettingsService, TemplateSettingsRepository],
  controllers: [TemplateSettingsController],
  exports: [TemplateSettingsService],
})
export class TemplateSettingsModule {}
