import {
  Body,
  Controller,
  Get,
  Param,
  Put,
  Req,
  UseGuards,
} from '@nestjs/common';
import { TemplateSettingsService } from './template-settings.service';
import { ShopifyGuard } from '../shopify/shopify.guard';

@Controller('api/template-settings')
@UseGuards(ShopifyGuard)
export class TemplateSettingsController {
  constructor(
    private readonly templateSettingsService: TemplateSettingsService,
  ) {}

  @Get()
  async getTemplateSettings(@Req() req: Request & { user: any }) {
    const template = await this.templateSettingsService.getTemplateSettings(
      req.user,
    );

    return {
      message: 'success',
      data: template[0],
    };
  }

  @Put('/:id')
  async updateTemplateSetting(
    @Req() req: Request & { user: any },
    @Param('id') id: string,
    @Body() updateTemplateSettingDto: any,
  ) {
    const template = await this.templateSettingsService.updateTemplateSetting(
      parseInt(id),
      req.user,
      updateTemplateSettingDto,
    );

    return {
      message: 'success',
      data: template,
    };
  }
}
