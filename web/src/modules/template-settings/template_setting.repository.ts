import { Injectable } from '@nestjs/common';
import { PrismaService } from '../prisma/prisma.service';

@Injectable()
export class TemplateSettingsRepository {
  constructor(private readonly prisma: PrismaService) {}

  async create(settingId: number, createSettingsDto: any) {
    return await this.prisma.template_setting.create({
      data: {
        ...createSettingsDto,
        setting: {
          connect: {
            id: settingId,
          },
        },
      },
    });
  }

  async getTemplateSettings(settingId: number) {
    return await this.prisma.template_setting.findMany({
      where: {
        setting_id: settingId,
      },
    });
  }

  async updateTemplateSetting(id: number, updateTemplateSettingDto: any) {
    return await this.prisma.template_setting.update({
      where: {
        id: id,
      },
      data: updateTemplateSettingDto,
    });
  }

  async findBySettingId(settingId: number) {
    return await this.prisma.template_setting.findFirst({
      where: {
        setting_id: settingId,
      },
    });
  }
}
