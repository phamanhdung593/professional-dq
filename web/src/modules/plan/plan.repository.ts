import { Injectable } from '@nestjs/common';
import { PrismaService } from '../prisma/prisma.service';

@Injectable()
export class PlanRepository {
  constructor(private readonly prisma: PrismaService) {}

  async create(user: any, data: any) {
    return await this.prisma.merchants_plan.upsert({
      where: {
        user_id: user.id,
      },
      create: {
        ...data,
        user: {
          connect: {
            id: user.id,
          },
        },
      },
      update: {
        ...data,
      },
    });
  }

  async findBySubscriptionId(subscriptionId: string) {
    return await this.prisma.merchants_plan.findFirst({
      where: {
        shopify_plan_id: subscriptionId,
      },
    });
  }

  async cancelBySubscriptionId(user: any) {
    return await this.prisma.merchants_plan.update({
      where: {
        user_id: user.id,
      },
      data: {
        plan: 'Free',
      },
    });
  }
}
