import { IsString, IsNotEmpty } from 'class-validator';

export class SubscriptionDto {
  @IsString()
  @IsNotEmpty({ message: 'Subscription ID is required and must be a string.' })
  subscription_id: string;
}
