import { Module } from '@nestjs/common';
import { PlanController } from './plan.controller';
import { PlanService } from './plan.service';
import { PrismaModule } from '../prisma/prisma.module';
import { UserModule } from '../user/user.module';
import { ShopifyModule } from '../shopify/shopify.module';
import { PlanRepository } from './plan.repository';

@Module({
  imports: [ShopifyModule, PrismaModule, UserModule],
  controllers: [PlanController],
  providers: [PlanService, PlanRepository],
})
export class PlanModule {}
