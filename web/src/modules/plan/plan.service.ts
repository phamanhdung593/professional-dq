import { BadRequestException, Injectable } from '@nestjs/common';
import { ShopifyService } from '../shopify/shopify.service';
import {
  APP_SUBSCRIPTION_CANCEL,
  APP_SUBSCRIPTION_CREATE,
} from '../graphql/mutation/plan';
import { PlanRepository } from './plan.repository';
import { getDetailPlan } from 'src/utils/helper';
import { SubscriptionDto } from './plan.dto';
import { DETAIL_APP_SUBSCRIPTION } from '../graphql/query/plan';

@Injectable()
export class PlanService {
  constructor(
    private readonly shopifyService: ShopifyService,
    private readonly planRepository: PlanRepository,
  ) {}

  async subscribe(planId = 0, user: any) {
    const planDetail = getDetailPlan(planId, user.shopify_domain);

    console.log(planDetail);
    const result = await this.shopifyService.mutation(
      user.shopify_domain,
      APP_SUBSCRIPTION_CREATE,
      planDetail,
    );

    let planName = 'Free';

    if (user?.merchants_plan?.plan === 'Free') {
      planName = 'Free';
    } else if (user?.merchants_plan?.plan === 'Pro') {
      planName = 'Pro';
    } else if (user?.merchants_plan?.plan === 'Ultimate') {
      planName = 'Ultimate';
    }

    const plan = {
      plan: planName,
      shopify_plan_id: result.data.appSubscriptionCreate.appSubscription.id,
      expiry_date: '',
      purchase_date: '',
      createdAt: new Date(),
      updatedAt: new Date(),
    };

    await this.planRepository.create(user, plan);

    return result;
  }

  async cancel(createPlanDto: SubscriptionDto, user: any) {
    const data = {
      id: createPlanDto.subscription_id,
      prorate: true,
    };
    const result = await this.shopifyService.mutation(
      user.shopify_domain,
      APP_SUBSCRIPTION_CANCEL,
      data,
    );

    await this.planRepository.cancelBySubscriptionId(user);

    return result;
  }

  async detail(user: any, subscriptionId: string) {
    const merchantPan =
      await this.planRepository.findBySubscriptionId(subscriptionId);

    if (!merchantPan) {
      throw new BadRequestException('Subscription not found');
    }

    const result = await this.shopifyService.query(
      user.shopify_domain,
      DETAIL_APP_SUBSCRIPTION,
      {
        id: subscriptionId,
      },
    );

    const status = result.data.node.status;

    if (status === 'ACTIVE') {
      const plan = {
        plan: result.data.node.name,
        shopify_plan_id: subscriptionId,
        purchase_date: result.data.node.createdAt,
        updatedAt: new Date(),
        createdAt: new Date(),
      };

      await this.planRepository.create(user, plan);
    }

    return result;
  }

  async chargeReturn(createPlanDto: any, user: any) {
    const planDetail = getDetailPlan(
      createPlanDto.plan_id,
      user.shopify_domain,
    );

    const planData = await this.planRepository.findBySubscriptionId(
      createPlanDto.subscription_id,
    );

    if (planData) {
      throw new BadRequestException('Subscription already exist');
    }

    const plan = {
      plan: planDetail.name,
      shopify_plan_id: createPlanDto.subscription_id,
      expiry_date: '',
      purchase_date: '',
      createdAt: new Date(),
      updatedAt: new Date(),
    };

    await this.planRepository.create(user, plan);
  }
}
