import { Body, Controller, Post, Req, Res, UseGuards } from '@nestjs/common';
import { PlanService } from './plan.service';
import { ShopifyGuard } from '../shopify/shopify.guard';
import { Request, Response } from 'express';
import { SubscriptionDto } from './plan.dto';

@Controller('api/plan')
@UseGuards(ShopifyGuard)
export class PlanController {
  constructor(private readonly planService: PlanService) {}

  @Post('subscribe')
  async subscribe(@Req() req: Request & { user: any }, @Body() body) {
    const planId = body.plan_id;

    return await this.planService.subscribe(planId, req.user);
  }

  @Post('cancel')
  async cancel(
    @Req() req: Request & { user: any },
    @Body() body: SubscriptionDto,
  ) {
    return await this.planService.cancel(body, req.user);
  }

  @Post('charge-return')
  async chargeReturn(
    @Req() req: Request & { user: any },
    @Body() body: SubscriptionDto,
    @Res() res: Response,
  ) {
    await this.planService.chargeReturn(body, req.user);

    return res.status(200).json({
      code: 200,
      message: 'success',
    });
  }

  @Post('detail')
  async status(@Req() req: Request & { user: any }) {
    return await this.planService.detail(req.user, req.body.subscription_id);
  }
}
