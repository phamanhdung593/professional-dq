import { Injectable } from '@nestjs/common';
import {
  WEBHOOK_TOPIC,
  WebhookSubscriptionFormat,
  WebhookSubscriptionInput,
} from 'src/types/webhook';
import { ConfigService } from '@nestjs/config';
import { SUBSCRIPTION_WEBHOOK } from '../graphql/mutation/webhook';
import { ShopifyService } from '../shopify/shopify.service';

@Injectable()
export class AuthService {
  constructor(
    private configService: ConfigService,
    private readonly shopifyService: ShopifyService,
  ) {}

  async subscriptWebhooks(user) {
    const uninstallEndpoint =
      this.configService.get<string>('app.host') + `/api/webhook/uninstall`;

    await this.createWebhook(user, {
      topic: WEBHOOK_TOPIC.APP_UNINSTALLED,
      input: {
        callbackUrl: uninstallEndpoint,
        format: WebhookSubscriptionFormat.JSON,
      },
    });
  }

  async createWebhook(
    user: any,
    payload: { topic: WEBHOOK_TOPIC; input: WebhookSubscriptionInput },
  ) {
    await this.shopifyService.mutation(
      user.shopify_domain,
      SUBSCRIPTION_WEBHOOK,
      {
        topic: payload.topic,
        webhookSubscription: payload.input,
      },
    );
  }
}
