import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { ShopifyModule } from 'src/modules/shopify/shopify.module';
import { UserModule } from '../user/user.module';
import { SettingsModule } from '../settings/settings.module';
import { TemplateSettingsModule } from '../template-settings/template-settings.module';
import { WebhookModule } from '../webhook/webhook.module';

@Module({
  imports: [
    ShopifyModule,
    UserModule,
    SettingsModule,
    TemplateSettingsModule,
    WebhookModule,
  ],
  providers: [AuthService],
  exports: [AuthService],
  controllers: [AuthController],
})
export class AuthModule {}
