import { Controller, Get, Req, Res } from '@nestjs/common';
import { Response, Request } from 'express';
import { ShopifyService } from 'src/modules/shopify/shopify.service';
import { SHOP_INFO, SHOP_LOCALES } from '../graphql/query/shop.query';
import { UserService } from '../user/user.service';
import { SettingsService } from '../settings/settings.service';
import { TemplateSettingsService } from '../template-settings/template-settings.service';
import { AuthService } from './auth.service';

@Controller('api/auth')
export class AuthController {
  constructor(
    private readonly shopifyService: ShopifyService,
    private readonly userService: UserService,
    private readonly settingService: SettingsService,
    private readonly authService: AuthService,
    private readonly templateSettingService: TemplateSettingsService,
  ) {}

  @Get()
  async redirect(@Req() req: Request, @Res() res: Response) {
    await this.shopifyService.shopify.auth.begin({
      shop: req.query.shop as string,
      callbackPath: '/api/auth/callback',
      isOnline: false,
      rawRequest: req,
      rawResponse: res,
    });
  }

  @Get('callback')
  async callback(
    @Req() req: Request,
    @Res({ passthrough: true }) res: Response,
  ) {
    const { session } = await this.shopifyService.shopify.auth.callback({
      rawRequest: req,
      rawResponse: res,
    });

    const shopifySession = await this.shopifyService.store(session);

    const shopInfo = await this.shopifyService.query(
      shopifySession.shop,
      SHOP_INFO,
    );

    const shopLocales = await this.shopifyService.query(
      shopifySession.shop,
      SHOP_LOCALES,
    );

    const user = await this.userService.createOrUpdateUser({
      store_name: shopInfo.data.shop.name,
      phone: shopInfo.data.shop.billingAddress.phone,
      shopify_access_token: shopifySession.accessToken,
      shopLocales: JSON.stringify(shopLocales.data),
      email: shopInfo.data.shop.email,
      shopify_domain: shopifySession.shop,
      createdAt: new Date(),
      updatedAt: new Date(),
    });

    // init data default setting
    const setting = await this.settingService.findByUserId(user.id);

    if (!setting) {
      const setting = await this.settingService.initSettingDefault(user.id);

      await this.templateSettingService.initTemplateSettingDefault(setting.id);
    }

    // await this.authService.subscriptWebhooks(user);
    let redirectUrl = await this.shopifyService.shopify.auth.getEmbeddedAppUrl({
      rawRequest: req,
      rawResponse: res,
    });

    if (redirectUrl.endsWith('/')) {
      redirectUrl = redirectUrl.slice(0, -1);
    }

    return res.redirect(redirectUrl);
  }
}
