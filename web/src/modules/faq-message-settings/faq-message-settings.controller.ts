import {
  Body,
  Controller,
  Get,
  Post,
  Put,
  Req,
  UseGuards,
} from '@nestjs/common';
import { ShopifyGuard } from '../shopify/shopify.guard';
import { FaqMessageSettingData } from 'src/types/setting';
import { FaqMessageSettingsService } from './faq-message-settings.service';
import { FaqMessageSettingDTO } from './faq-message-setting.dto';

@Controller('api/faq-message-settings')
@UseGuards(ShopifyGuard)
export class FaqMessageSettingsController {
  constructor(
    private readonly faqMessageSettingsService: FaqMessageSettingsService,
  ) {}

  @Get()
  async getWidget(@Req() req: Request & { user: { id: number } }) {
    const result = await this.faqMessageSettingsService.getDetailByUser(
      req.user.id,
    );

    return {
      code: 200,
      message: 'success',
      data: result,
    };
  }

  @Post()
  async initDataDefault(@Req() req: Request & { user: { id: number } }) {
    return await this.faqMessageSettingsService.initDataDefault(
      req.user.id,
      FaqMessageSettingData,
    );
  }

  @Put('update')
  async updateDataDefault(
    @Req() req: Request & { user: { id: number } },
    @Body() data: FaqMessageSettingDTO,
  ) {
    return await this.faqMessageSettingsService.updateDataDefault(
      req.user.id,
      data,
    );
  }
}
