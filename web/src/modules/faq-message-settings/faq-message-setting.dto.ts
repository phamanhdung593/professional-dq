import { IsOptional, IsString, IsBoolean } from 'class-validator';

export class FaqMessageSettingDTO {
  @IsOptional()
  @IsBoolean()
  help_desk_visible?: boolean;

  @IsOptional()
  @IsString()
  sort_by?: string;

  @IsOptional()
  @IsBoolean()
  contact_us_visible?: boolean;

  @IsOptional()
  @IsBoolean()
  phone_visible?: boolean;

  @IsOptional()
  @IsBoolean()
  whatsApp_visible?: boolean;

  @IsOptional()
  @IsBoolean()
  message_visible?: boolean;

  @IsOptional()
  @IsBoolean()
  email_visible?: boolean;

  @IsOptional()
  @IsBoolean()
  animation_visible?: boolean;

  @IsOptional()
  @IsBoolean()
  show_default_locale?: boolean;

  @IsOptional()
  @IsString()
  email_link?: string;

  @IsOptional()
  @IsString()
  phone_number?: string;

  @IsOptional()
  @IsString()
  whatsApp_number?: string;

  @IsOptional()
  @IsString()
  message_link?: string;

  @IsOptional()
  @IsString()
  welcome_title?: string;

  @IsOptional()
  @IsString()
  description_title?: string;

  @IsOptional()
  @IsString()
  theme_color?: string;

  @IsOptional()
  @IsString()
  text_color?: string;

  @IsOptional()
  @IsString()
  icon_number?: string;

  @IsOptional()
  @IsString()
  button_background_color?: string;

  @IsOptional()
  @IsString()
  aligment_faq_messages?: string;

  @IsOptional()
  @IsString()
  button_title?: string;

  @IsOptional()
  @IsString()
  font_family?: string;

  @IsOptional()
  @IsString()
  send_messages_text_color?: string;

  @IsOptional()
  @IsBoolean()
  faq_messages_visible?: boolean;

  @IsOptional()
  @IsBoolean()
  feature_questions_visible?: boolean;

  @IsOptional()
  @IsBoolean()
  feature_categories_visible?: boolean;

  @IsOptional()
  @IsString()
  translation?: string;

  @IsOptional()
  @IsString()
  primary_language?: string;

  @IsOptional()
  @IsString()
  createdAt?: string | Date;

  @IsOptional()
  @IsString()
  updatedAt?: string | Date;
}
