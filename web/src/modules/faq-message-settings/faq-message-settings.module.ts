import { Module } from '@nestjs/common';
import { FaqMessageSettingsController } from './faq-message-settings.controller';
import { FaqMessageSettingsService } from './faq-message-settings.service';
import { PrismaModule } from '../prisma/prisma.module';
import { UserModule } from '../user/user.module';
import { FaqMessageSettingsRepository } from './faq-message-settings.repository';

@Module({
  imports: [PrismaModule, UserModule],
  controllers: [FaqMessageSettingsController],
  providers: [FaqMessageSettingsService, FaqMessageSettingsRepository],
  exports: [FaqMessageSettingsService],
})
export class FaqMessageSettingsModule {}
