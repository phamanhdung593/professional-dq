import { Injectable } from '@nestjs/common';
import { PrismaService } from '../prisma/prisma.service';

@Injectable()
export class FaqMessageSettingsRepository {
  constructor(private readonly prisma: PrismaService) {}

  async findByUserId(userId: number) {
    return await this.prisma.faq_messages_setting.findFirst({
      where: {
        user_id: userId,
      },
    });
  }

  async update(userId: number, data: any) {
    const faqMessageSetting = await this.prisma.faq_messages_setting.findFirst({
      where: {
        user_id: userId,
      },
    });

    return await this.prisma.faq_messages_setting.update({
      where: {
        id: faqMessageSetting.id,
      },
      data,
    });
  }

  async create(userId: number, data: any) {
    const faqMessageSetting = await this.prisma.faq_messages_setting.findFirst({
      where: {
        user_id: userId,
      },
    });

    if (faqMessageSetting) {
      return await this.prisma.faq_messages_setting.update({
        where: {
          id: faqMessageSetting.id,
        },
        data,
      });
    }

    return await this.prisma.faq_messages_setting.create({
      data: {
        user: {
          connect: {
            id: userId,
          },
        },
        ...data,
      },
    });
  }
}
