import { Injectable } from '@nestjs/common';
import { FaqMessageSettingsRepository } from './faq-message-settings.repository';
import { FaqMessageSettingDTO } from './faq-message-setting.dto';

@Injectable()
export class FaqMessageSettingsService {
  constructor(
    private readonly faqMessageSettingsRepository: FaqMessageSettingsRepository,
  ) {}

  async getDetailByUser(userId: number) {
    return await this.faqMessageSettingsRepository.findByUserId(userId);
  }

  async initDataDefault(userId: number, data: FaqMessageSettingDTO) {
    return await this.faqMessageSettingsRepository.create(userId, data);
  }

  async updateDataDefault(userId: number, data: FaqMessageSettingDTO) {
    return await this.faqMessageSettingsRepository.update(userId, data);
  }
}
