import { ShopifySessionRepository } from './shopify.repository';
import { Injectable, NotFoundException } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import {
  LATEST_API_VERSION,
  Session,
  Shopify,
  shopifyApi,
} from '@shopify/shopify-api';
import { UserService } from '../user/user.service';
import { Request, Response } from 'express';

@Injectable()
export class ShopifyService {
  shopify: Shopify;

  /**
   * Shopify constructor
   *
   * @param config
   * @param shopifySessionRepository
   */
  constructor(
    private readonly config: ConfigService,
    private readonly shopifySessionRepository: ShopifySessionRepository,
    private readonly userService: UserService,
  ) {
    this.shopify = shopifyApi({
      apiKey: this.config.get('shopify.api_key'),
      apiSecretKey: this.config.get('shopify.api_secret'),
      scopes: this.config.get('shopify.scopes').replace(/\s/g, '').split(','),
      hostName: this.config.get('app.host').replace(/https?:\/\//, ''),
      apiVersion: LATEST_API_VERSION,
      isEmbeddedApp: true,
      hostScheme: this.config.get('app.scheme'),
    });
  }

  /**
   * Create session shopify
   *
   * @param session
   * @returns
   */
  public async store(session: Session) {
    return await this.shopifySessionRepository.updateOrCreate(session);
  }

  async checkHasShopifySession(shop: string) {
    const countSession = await this.shopifySessionRepository.countSession(shop);

    return countSession > 0;
  }

  /**
   * Load session
   *
   * @param isOnline
   * @param req
   * @param res
   * @returns
   */
  public async load(isOnline: boolean, req: Request, res?: Response) {
    const sessionId = await this.shopify.session.getCurrentId({
      isOnline: isOnline,
      rawRequest: req,
      rawResponse: res,
    });

    if (sessionId) {
      console.log('sessionId', sessionId);
      const session = await this.shopifySessionRepository.findById(sessionId);

      return new Session(session);
    }

    return {} as Session;
  }

  async isActiveSession(shop: string) {
    const session = await this.loadOfflineSession(shop);

    if (!session) {
      return false;
    }

    return true;
  }

  async loadOfflineSession(shop: string) {
    const sessionId = this.shopify.session.getOfflineId(shop);
    const session = await this.shopifySessionRepository.findById(sessionId);

    if (!session) {
      throw new NotFoundException('Offline session not found');
    }

    return new Session(session);
  }

  async query(shop: string, query: string, variables = {}) {
    const session = await this.loadOfflineSession(shop);

    const shopify = shopifyApi({
      apiKey: this.config.get('shopify.api_key'),
      apiSecretKey: this.config.get('shopify.api_secret'),
      scopes: this.config.get('shopify.scopes').replace(/\s/g, '').split(','),
      hostName: this.config.get('app.host').replace(/https?:\/\//, ''),
      apiVersion: LATEST_API_VERSION,
      isEmbeddedApp: true,
      hostScheme: this.config.get('app.scheme'),
    });

    const client = new shopify.clients.Graphql({
      session: session,
    });

    return await client.request(query, {
      variables: variables,
      headers: { myHeader: '1' },
      retries: 2,
    });
  }

  async mutation(shop: string, query: string, variables: object): Promise<any> {
    const session = await this.loadOfflineSession(shop);

    const shopify = shopifyApi({
      apiKey: this.config.get('shopify.api_key'),
      apiSecretKey: this.config.get('shopify.api_secret'),
      scopes: this.config.get('shopify.scopes').replace(/\s/g, '').split(','),
      hostName: this.config.get('app.host').replace(/https?:\/\//, ''),
      apiVersion: LATEST_API_VERSION,
      isEmbeddedApp: true,
      hostScheme: this.config.get('app.scheme'),
    });

    const client = new shopify.clients.Graphql({ session });

    return await client.request(query, {
      variables: variables,
      headers: { myHeader: '1' },
      retries: 2,
    });
  }
}
