import { Injectable } from '@nestjs/common';
import { PrismaService } from '../prisma/prisma.service';
import { Session } from '@shopify/shopify-api';

@Injectable()
export class ShopifySessionRepository {
  constructor(private readonly prisma: PrismaService) {}

  /**
   * Find session online
   * @param shop
   * @returns {Promise<number>}
   */
  async countSession(shop: string): Promise<number> {
    return await this.prisma.shopify_sessions.count({
      where: {
        shop: shop,
        isOnline: false,
      },
    });
  }

  /**
   * Update or create session
   * @param session
   */
  async updateOrCreate(session: Session) {
    return await this.prisma.shopify_sessions.upsert({
      where: { id: session.id },
      update: {
        shop: session.shop,
        state: session.state,
        isOnline: session.isOnline || false,
        scope: session.scope,
        expires: session.expires ? new Date(session.expires) : null,
        accessToken: session.accessToken || '',
      },
      create: {
        id: session.id,
        shop: session.shop,
        state: session.state,
        isOnline: session.isOnline || false,
        scope: session.scope,
        expires: session.expires ? new Date(session.expires) : null,
        accessToken: session.accessToken || '',
      },
    });
  }

  /**
   * Find session by id
   * @param id
   * @returns {Promise<Session>}
   */
  async findById(id: string) {
    return this.prisma.shopify_sessions.findUnique({
      where: { id },
      select: {
        id: true,
        shop: true,
        state: true,
        isOnline: true,
        scope: true,
        expires: true,
        accessToken: true,
      },
    });
  }
}
