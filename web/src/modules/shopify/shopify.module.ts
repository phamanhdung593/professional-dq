import { Global, Module } from '@nestjs/common';
import { ShopifyService } from './shopify.service';
import { ConfigModule } from '@nestjs/config';
import { ShopifySessionRepository } from './shopify.repository';
import { PrismaModule } from '../prisma/prisma.module';
import { UserModule } from '../user/user.module';

@Global()
@Module({
  imports: [ConfigModule, PrismaModule, UserModule],
  providers: [ShopifyService, ShopifySessionRepository],
  exports: [ShopifyService, ShopifySessionRepository],
})
export class ShopifyModule {}
