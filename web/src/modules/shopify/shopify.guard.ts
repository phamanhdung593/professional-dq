import {
  CanActivate,
  ExecutionContext,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { UserService } from '../user/user.service';
import { Response, Request as ExpressRequest } from 'express';
import { Session } from '@shopify/shopify-api';
import { ShopifyService } from './shopify.service';

// bo sung type user la model user vaf session la session shopify...
export type Request = ExpressRequest & { user: any; session: Session };

@Injectable()
export class ShopifyGuard implements CanActivate {
  constructor(
    private readonly userService: UserService,
    private readonly shopifyService: ShopifyService,
  ) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const httpContext = context.switchToHttp();
    const request = httpContext.getRequest<Request>();
    const response = httpContext.getResponse<Response>();

    try {
      const token = request.headers.authorization?.split(' ')[1];
      const [headerBase64, payloadBase64, signature] = token.split('.');

      if (!headerBase64 || !payloadBase64 || !signature) {
        throw new UnauthorizedException('Invalid token format');
      }

      const payload = JSON.parse(
        Buffer.from(payloadBase64, 'base64').toString(),
      );
      const currentTimestamp = Math.floor(Date.now() / 1000);

      if (payload.exp < currentTimestamp) {
        throw new UnauthorizedException('Token expired');
      }

      const shopDomain = payload.dest.match(/^https?:\/\/([^/]+)/)[1];
      const user = await this.userService.findByShopDomain(shopDomain);

      response.header('x-shopify-shop', shopDomain);

      const shopifySession =
        await this.shopifyService.checkHasShopifySession(shopDomain);

      if (!shopifySession) {
        response.header('x-shopify-auth', '1');
      }

      if (!user) {
        throw new UnauthorizedException('User not found');
      }

      request.user = user;

      return true;
    } catch (e) {
      return false;
    }
  }
}
