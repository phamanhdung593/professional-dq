import { Module } from '@nestjs/common';
import { ComplianceWebhookController } from './compliance-webhook.controller';

@Module({
  controllers: [ComplianceWebhookController],
})
export class ComplianceWebhookModule {}
