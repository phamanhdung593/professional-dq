import { Controller, Post, Res } from '@nestjs/common';

@Controller('api/gdpr')
export class ComplianceWebhookController {
  @Post('/customer-redact')
  customerRedact(@Res() res: any) {
    return res.status(200).send({ code: 200, message: 'success' });
  }

  @Post('/customer-data')
  customerData(@Res() res: any) {
    return res.status(200).send({ code: 200, message: 'success' });
  }

  @Post('/shop-redact')
  shopRedact(@Res() res: any) {
    return res.status(200).send({ code: 200, message: 'success' });
  }
}
