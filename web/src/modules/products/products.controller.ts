import { Body, Controller, Delete, Get, Req, UseGuards } from '@nestjs/common';
import { ProductsService } from './products.service';
import { Request } from 'express';
import { ShopifyGuard } from '../shopify/shopify.guard';

@Controller('api/products')
@UseGuards(ShopifyGuard)
export class ProductsController {
  constructor(private readonly productsService: ProductsService) {}

  @Get()
  async getProducts(@Req() req: Request & { user: any }) {
    const products = await this.productsService.paginateByConditions(
      req.user.shopify_domain,
      {},
    );

    return {
      code: 200,
      message: 'success',
      data: products.data,
    };
  }

  @Get('/faq')
  async getProductFaq(@Req() req: Request & { user: any }) {
    const products = await this.productsService.getFaqs(req.user);

    return {
      code: 200,
      message: 'success',
      data: products,
    };
  }

  @Delete('/delete')
  async deleteFaqProducts(
    @Req() req: Request & { user: { id: number } },
    @Body() body: any,
  ) {
    const { ids } = body;
    return await this.productsService.deleteByIds(req.user.id, ids);
  }
}
