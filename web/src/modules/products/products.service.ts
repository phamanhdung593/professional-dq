import { Injectable, NotFoundException } from '@nestjs/common';
import { ProductsRepository } from './products.repository';
import { ShopifyService } from '../shopify/shopify.service';
import { UserRepository } from '../user/user.repository';
import { GET_PRODUCTS } from '../graphql';

@Injectable()
export class ProductsService {
  constructor(
    private readonly productsRepository: ProductsRepository,
    private readonly userRepository: UserRepository,
    private readonly shopifyService: ShopifyService,
  ) {}

  async paginateByConditions(shopDomain: string, conditions: any) {
    console.log(conditions);
    const user = await this.userRepository.findByShopDomain(shopDomain);

    if (!user) {
      throw new NotFoundException('User not found');
    }

    const products = await this.shopifyService.query(
      user.shopify_domain,
      GET_PRODUCTS,
      {
        first: 20,
      },
    );

    console.log(products);

    return products;
  }

  async findByIdShopify(ids: string[]) {
    return this.productsRepository.findByProductIdShopify(ids);
  }

  async create(userId: number, product: any) {
    const productData = {
      product_id: product.id.replace('gid://shopify/Product/', ''),
      product_title: product.title,
      product_image: '',
      createdAt: new Date(),
      updatedAt: new Date(),
    };

    if (product.images.length > 0) {
      productData.product_image = product.images[0].originalSrc;
    }

    return this.productsRepository.create(userId, productData);
  }

  async getFaqs(user: any) {
    const faqs = await this.productsRepository.getFaqs(user.id);

    return faqs;
  }

  async deleteByIds(userId: number, ids: number[]) {
    const product = await this.productsRepository.findByIds(ids);

    if (product.length !== ids.length) {
      throw new NotFoundException('Product not found');
    }

    return await this.productsRepository.deleteByIds(userId, ids);
  }

  async findByProductId(userId: number, productId: string) {
    return await this.productsRepository.findByProductId(userId, productId);
  }
}
