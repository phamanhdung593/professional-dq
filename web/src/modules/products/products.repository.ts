import { Injectable } from '@nestjs/common';
import { PrismaService } from '../prisma/prisma.service';

@Injectable()
export class ProductsRepository {
  constructor(private readonly prismaService: PrismaService) {}

  async findByProductIdShopify(ids: string[]) {
    return await this.prismaService.product.findMany({
      where: {
        product_id: {
          in: ids,
        },
      },
    });
  }

  async create(userId: number, product: any) {
    return await this.prismaService.product.upsert({
      where: {
        user_id_product_id: {
          product_id: product.product_id,
          user_id: userId,
        },
      },
      update: {
        product_title: product.product_title,
        product_image: product.product_image,
        createdAt: product.createdAt,
        updatedAt: product.updatedAt,
      },
      create: {
        user: {
          connect: {
            id: userId,
          },
        },
        ...product,
      },
    });
  }

  async getFaqs(userId: number) {
    return await this.prismaService.product.findMany({
      where: {
        user_id: userId,
      },
      include: {
        faq_product: true,
      },
    });
  }

  async findById(id: number, userId: number) {
    return await this.prismaService.product.findUnique({
      where: {
        id: id,
        user_id: userId,
      },
    });
  }

  async findByIds(ids: number[]) {
    return await this.prismaService.product.findMany({
      where: {
        id: {
          in: ids,
        },
      },
    });
  }

  async deleteByIds(userId: number, ids: number[]) {
    return await this.prismaService.product.deleteMany({
      where: {
        id: {
          in: ids,
        },
        user_id: userId,
      },
    });
  }

  async findByProductId(userId: number, productId: string) {
    return await this.prismaService.product.findFirst({
      where: {
        user_id: userId,
        product_id: productId,
      },
      include: {
        faq_product: {
          include: {
            faq: true,
          },
        },
      },
    });
  }
}
