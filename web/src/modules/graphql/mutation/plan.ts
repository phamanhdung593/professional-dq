export const APP_SUBSCRIPTION_CREATE = `
mutation AppSubscriptionCreate($name: String!, $lineItems: [AppSubscriptionLineItemInput!]!, $returnUrl: URL!, $test: Boolean) {
  appSubscriptionCreate(name: $name, returnUrl: $returnUrl, lineItems: $lineItems, test: $test) {
    userErrors {
      field
      message
    }
    appSubscription {
      id
      status
      test
      trialDays
    }
    confirmationUrl
  }
}`;

export const APP_SUBSCRIPTION_CANCEL = `
mutation AppSubscriptionCancel($id: ID!, $prorate: Boolean) {
  appSubscriptionCancel(id: $id, prorate: $prorate) {
    userErrors {
      field
      message
    }
    appSubscription {
      id
      status
    }
  }
}`;
