export const SUBSCRIPTION_WEBHOOK = `
    mutation subscriptionWebhook($topic: WebhookSubscriptionTopic!, $webhookSubscription: WebhookSubscriptionInput!) {
        webhookSubscriptionCreate(topic: $topic, webhookSubscription: $webhookSubscription) {
            webhookSubscription {
                id
                topic
                endpoint {
                    ... on WebhookHttpEndpoint {
                        callbackUrl
                    }
                }
                format
                createdAt
                updatedAt
            }
            userErrors {
                field,
                message
            }
        }
    }
`;

export const DELETE_WEBHOOK = `
    mutation deleteWebhook($id: ID!) {
        webhookSubscriptionDelete(id: $id) {
            deletedWebhookSubscriptionId
            userErrors {
                field
                message
            }
        }
    }
`;
