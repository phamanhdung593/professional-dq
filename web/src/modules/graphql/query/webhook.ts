export const GET_WEBHOOKS = `
    query getWebhookSubscriptionsByUser {
        webhookSubscriptions(first: 20) {
            edges {
                node {
                    id
                    topic
                    endpoint {
                        ... on WebhookHttpEndpoint {
                            callbackUrl
                        }
                    }
                    format
                    createdAt
                    updatedAt
                }
            }
        }
    }
`;
