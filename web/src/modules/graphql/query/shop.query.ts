export const SHOP_INFO = `query shopInfo {
  shop {
    name
    url
    email
    billingAddress {
      phone
    }
    myshopifyDomain
    plan {
      displayName
      partnerDevelopment
      shopifyPlus
    }
    alerts {
      action {
        title
        url
      }
      description
    }
    features {
      avalaraAvatax
      branding
      captcha
      captchaExternalDomains
      dynamicRemarketing
      eligibleForSubscriptionMigration
      eligibleForSubscriptions
      giftCards
    }
  }
}
`;

export const SHOP_LOCALES = `query shopLocales {
  shopLocales {
    locale
    primary
    published
  }
}
`;
