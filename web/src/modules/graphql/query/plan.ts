export const DETAIL_APP_SUBSCRIPTION = `
query getAppSubscription($id: ID!) {
  node(id: $id) {
    ...on AppSubscription {
      createdAt
      currentPeriodEnd
      id
      name
      status
      test
      lineItems {
        plan {
          pricingDetails {
            ...on AppRecurringPricing {
              interval
              price {
                amount
                currencyCode
              }
            }
            ...on AppUsagePricing {
              terms
              cappedAmount {
                amount
                currencyCode
              }
              balanceUsed {
                amount
                currencyCode
              }
            }
          }
        }
      }
    }
  }
}`;
