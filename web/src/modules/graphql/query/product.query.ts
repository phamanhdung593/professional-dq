export const GET_PRODUCTS = `query {
  products(first: 20) {
    edges {
      node {
        id
        title
        featuredImage {
          id
          url
        }
      }
    }
  }
}`;
