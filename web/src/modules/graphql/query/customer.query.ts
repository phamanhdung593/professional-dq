export const FIND_CUSTOMER_BY_ID = `query findCustomerById($customer_id: ID!) {
  customer(id: $customer_id) {
    id
    firstName
    lastName
    email
    phone
    createdAt
    updatedAt
    note
    verifiedEmail
    validEmailAddress
    tags
    lifetimeDuration
    defaultAddress {
      formattedArea
      address1
    }
    addresses {
      address1
    }
    image {
      src
    }
    canDelete
  }
}`;
