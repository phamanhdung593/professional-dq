import { SHOP_INFO } from './query/shop.query';
import { GET_PRODUCTS } from './query/product.query';

export { SHOP_INFO, GET_PRODUCTS };
