import { Module } from '@nestjs/common';
import { FaqMorePageSettingsController } from './faq-more-page-settings.controller';
import { FaqMorePageSettingsService } from './faq-more-page-settings.service';
import { PrismaModule } from '../prisma/prisma.module';
import { UserModule } from '../user/user.module';
import { FaqMorePageSettingRepository } from './faq-more-page-settings.repository';

@Module({
  imports: [PrismaModule, UserModule],
  controllers: [FaqMorePageSettingsController],
  providers: [FaqMorePageSettingsService, FaqMorePageSettingRepository],
  exports: [FaqMorePageSettingsService],
})
export class FaqMorePageSettingsModule {}
