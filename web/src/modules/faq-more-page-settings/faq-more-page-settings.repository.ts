import { Injectable } from '@nestjs/common';
import { PrismaService } from '../prisma/prisma.service';

@Injectable()
export class FaqMorePageSettingRepository {
  constructor(private readonly prisma: PrismaService) {}

  async index(userId: number) {
    return await this.prisma.faq_more_page_setting.findMany({
      where: {
        user_id: userId,
      },
    });
  }

  async findById(id: number) {
    return await this.prisma.faq_more_page_setting.findUnique({
      where: {
        id: id,
      },
    });
  }

  async create(userId: number, faqMorePageSettingDto: any) {
    return await this.prisma.faq_more_page_setting.create({
      data: {
        ...faqMorePageSettingDto,
        user: {
          connect: {
            id: userId,
          },
        },
      },
    });
  }

  async update(id: number, faqMorePageSettingDto: any) {
    return await this.prisma.faq_more_page_setting.update({
      where: {
        id: id,
      },
      data: faqMorePageSettingDto,
    });
  }

  async delete(id: number) {
    await this.prisma.faq_more_page_setting.delete({
      where: {
        id: id,
      },
    });
  }
}
