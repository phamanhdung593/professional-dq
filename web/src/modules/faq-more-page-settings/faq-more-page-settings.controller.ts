import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Req,
  UseGuards,
} from '@nestjs/common';
import { FaqMorePageSettingsService } from './faq-more-page-settings.service';
import { Request } from 'express';
import { ShopifyGuard } from '../shopify/shopify.guard';

@Controller('api/faq-more-page-settings')
@UseGuards(ShopifyGuard)
export class FaqMorePageSettingsController {
  constructor(
    private readonly faqMorePageSettingsService: FaqMorePageSettingsService,
  ) {}

  @Get()
  async getFaqMorePageSettings(@Req() req: Request & { user: { id: string } }) {
    return await this.faqMorePageSettingsService.index(parseInt(req.user.id));
  }

  @Get('/:id')
  async getFaqMorePageSettingById(@Param('id') id: string) {
    return await this.faqMorePageSettingsService.findById(parseInt(id));
  }

  @Post('/')
  async createFaqMorePageSetting(
    @Req() req: Request & { user: { id: number } },
    @Body() createFaqMorePageSettingDto: any,
  ) {
    return await this.faqMorePageSettingsService.create(
      req.user.id,
      createFaqMorePageSettingDto,
    );
  }

  @Put('/:id')
  async updateFaqMorePageSetting(
    @Param('id') id: string,
    @Body() updateFaqMorePageSettingDto: any,
  ) {
    return await this.faqMorePageSettingsService.update(
      parseInt(id),
      updateFaqMorePageSettingDto,
    );
  }

  @Delete('/:id')
  async deleteFaqMorePageSetting(@Param('id') id: string) {
    return await this.faqMorePageSettingsService.delete(parseInt(id));
  }
}
