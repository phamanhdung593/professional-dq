import { Injectable } from '@nestjs/common';
import { FaqMorePageSettingRepository } from './faq-more-page-settings.repository';

@Injectable()
export class FaqMorePageSettingsService {
  constructor(
    private readonly faqMorePageSettingRepository: FaqMorePageSettingRepository,
  ) {}

  async index(userId: number) {
    return await this.faqMorePageSettingRepository.index(userId);
  }

  async findById(id: number) {
    return await this.faqMorePageSettingRepository.findById(id);
  }

  async create(userId: number, faqMorePageSettingDto: any) {
    return await this.faqMorePageSettingRepository.create(
      userId,
      faqMorePageSettingDto,
    );
  }

  async update(id: number, faqMorePageSettingDto: any) {
    return await this.faqMorePageSettingRepository.update(
      id,
      faqMorePageSettingDto,
    );
  }

  async delete(id: number) {
    return await this.faqMorePageSettingRepository.delete(id);
  }

  async findByUserId(userId: number) {
    return await this.faqMorePageSettingRepository.index(userId);
  }

  async initSettingDefault(userId: number) {
    return await this.faqMorePageSettingRepository.create(userId, {
      home_page_visible: false,
      product_page_visible: false,
      cart_page_visible: false,
      collection_page_visible: false,
      cms_page_visible: false,
      createdAt: new Date(),
      updatedAt: new Date(),
    });
  }
}
