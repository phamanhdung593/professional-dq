import {
  UnprocessableEntityException,
  ValidationError,
  ValidationPipe,
} from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import '@shopify/shopify-api/adapters/node';
import { urlencoded } from 'express';
import { ValidationExceptionFilter } from './exceptions/validate-exception-filter';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, {
    bodyParser: false,
    rawBody: true,
  });

  app
    .useGlobalPipes(
      new ValidationPipe({
        exceptionFactory: (validationErrors: ValidationError[] = []) => {
          return new UnprocessableEntityException(validationErrors);
        },
        validationError: {
          target: false,
          value: false,
        },
        transform: true,
      }),
    )
    .useGlobalFilters(new ValidationExceptionFilter());

  app.use(urlencoded({ extended: true, limit: '50mb' }));
  await app.listen(process.env.PORT);
}

bootstrap();
