import { NestMiddleware } from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';

export class AuthenticateMiddleWare implements NestMiddleware {
  use(req: Request, res: Response, next: NextFunction) {
    next();
  }
}
