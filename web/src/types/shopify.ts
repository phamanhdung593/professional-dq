export type ShopifyEdge<T> = {
  edges: Array<{
    cursor?: string;
    node: T;
  }>;
  pageInfo: {
    hasNextPage: boolean;
    hasPreviousPage: boolean;
    endCursor?: string;
    startCursor?: string;
  };
};

export type ShopifyPaginate<K extends string, T> = Record<K, ShopifyEdge<T>>;
