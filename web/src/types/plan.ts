export const Plan = {
  Free: {
    title: 'Free',
    price: 0,
  },
  Pro: {
    title: 'Pro',
    price: 2.99,
    pricePeriod: 'USD / mo',
    trialDays: 7,
  },
  Ultimate: {
    title: 'Ultimate',
    price: 9.99,
    trialDays: 7,
  },
};
