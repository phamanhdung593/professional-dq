export enum WeightUnit {
  GRAMS = 'Grams',
  KILOGRAMS = 'Kilograms',
  OUNCES = 'Ounces',
  POUNDS = 'Pounds',
}

export interface ShopPlan {
  displayName: string;
  partnerDevelopment: boolean;
  shopifyPlus: boolean;
}

export interface ShopFeatures {
  avalaraAvatax: boolean;
  branding: string;
  captcha: boolean;
  captchaExternalDomains: boolean;
  dynamicRemarketing: boolean;
  eligibleForSubscriptionMigration: boolean;
  eligibleForSubscriptions: boolean;
  giftCards: boolean;
}

export interface Shop {
  name: string;
  url: string;
  myshopifyDomain: string;
  billingAddress: {
    phone: string;
  };
  plan: ShopPlan;
  alerts: any[];
  features: ShopFeatures;
}
