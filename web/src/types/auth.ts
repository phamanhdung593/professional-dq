export type QueryPayload = {
  code?: string;
  hmac?: string;
  host?: string;
  shop?: string;
  timestamp?: string;
};
