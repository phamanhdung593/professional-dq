export enum WEBHOOK_TOPIC {
  APP_UNINSTALLED = 'APP_UNINSTALLED',
  ORDERS_CREATE = 'ORDERS_CREATE',
  ORDERS_CANCELLED = 'ORDERS_CANCELLED',
  ORDERS_DELETE = 'ORDERS_DELETE',
  ORDERS_EDITED = 'ORDERS_EDITED',
  ORDERS_FULFILLED = 'ORDERS_FULFILLED',
  ORDERS_PAID = 'ORDERS_PAID',
  ORDERS_PARTIALLY_FULFILLED = 'ORDERS_PARTIALLY_FULFILLED',
  PRODUCTS_UPDATE = 'PRODUCTS_UPDATE',
  PRODUCTS_CREATE = 'PRODUCTS_CREATE',
  ORDERS_UPDATED = 'ORDERS_UPDATED',
  SHOP_UPDATE = 'SHOP_UPDATE',
  ORDER_REFUND = 'REFUNDS_CREATE',
  ORDER_TRANSACTIONS_CREATE = 'ORDER_TRANSACTIONS_CREATE',
}

export enum WebhookSubscriptionFormat {
  JSON = 'JSON',
  XML = 'XML',
}

export type WebhookSubscriptionInput = {
  callbackUrl?: string;
  format?: WebhookSubscriptionFormat;
  includeFields?: string[];
  metafieldNamespaces?: string[];
  privateMetafieldNamespaces?: string[];
};

export type WebhookSubscription = {
  createdAt: string;
  endpoint: {
    __typename:
      | 'WebhookHttpEndpoint'
      | 'WebhookEventBridgeEndpoint'
      | 'WebhookPubSubEndpoint';
    callbackUrl: string;
  };
  format: WebhookSubscriptionFormat;
  id: string;
  includeFields: string[];
  metafieldNamespaces: string[];
  topic: WEBHOOK_TOPIC;
  updatedAt: string;
};

export type WebhookSubscriptionCreateResponse = {
  webhookSubscriptionCreate: {
    webhookSubscription: WebhookSubscription;
    userErrors: Array<{
      message: string;
      field: string[];
    }>;
  };
  errors?: Array<{
    message: string;
    extensions: unknown[];
    locations: unknown[];
  }>;
};
