export type MoneyV2 = {
  amount: number;
  currencyCode: string;
};

function _formatPrice(amount: number, currency: string): string {
  return Intl.NumberFormat('en', {
    currency: currency,
    style: 'currency',
  }).format(amount);
}

export function priceFormatted(
  money: number | MoneyV2 | null | undefined,
  currency?: string,
): string {
  if (money === null || money === undefined) {
    return '__';
  }
  if (typeof money == 'object') {
    return _formatPrice(money.amount, money.currencyCode);
  }

  return _formatPrice(money, currency || 'USD');
}
