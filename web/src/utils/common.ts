export const pickBy = (obj: { [x: string]: unknown } | undefined) => {
  return obj
    ? Object.fromEntries(Object.entries(obj).filter(([val]) => val != null))
    : {};
};

export const sleep = (ms: number) =>
  new Promise((resolve) => setTimeout(resolve, ms));

export const generateRandomCode = () => {
  const characters = 'abcdefghijklmnopqrstuvwxyz0123456789';
  const codeLength = 16;
  let randomCode = '';

  for (let i = 0; i < codeLength; i++) {
    const randomIndex = Math.floor(Math.random() * characters.length);
    randomCode += characters[randomIndex];
  }

  return randomCode;
};
