import { Plan } from 'src/types/plan';
import { Request } from 'express';

export function randomString(length: number) {
  let result = '';
  const characters =
    'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  const charactersLength = characters.length;
  for (let i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}

export function getLocaleDefault(
  locale: string,
  locales: { locale: string; primary: boolean; published: boolean }[],
) {
  const localeShop = locales.find(
    (item) => item.locale === locale && item.primary === true,
  );
  return localeShop ? 'default' : locale;
}

export function getDetailPlan(plan: number, shopDomain: string) {
  const domain = shopDomain.split('.')[0];
  const returnUrl = `https://admin.shopify.com/store/${domain}/apps/${process.env.SHOPIFY_API_KEY}/plans`;

  switch (plan) {
    case 1:
      return {
        name: Plan.Pro.title,
        returnUrl: returnUrl,
        lineItems: [
          {
            plan: {
              appRecurringPricingDetails: {
                price: {
                  amount: Plan.Pro.price,
                  currencyCode: 'USD',
                },
                interval: 'EVERY_30_DAYS',
              },
            },
          },
        ],
        test: process.env.PAYMENT_SANDBOX === 'development' ? true : false,
        trialDays: Plan.Pro.trialDays,
      };
    case 2:
      return {
        name: Plan.Ultimate.title,
        returnUrl: returnUrl,
        lineItems: [
          {
            plan: {
              appRecurringPricingDetails: {
                price: {
                  amount: Plan.Ultimate.price,
                  currencyCode: 'USD',
                },
                interval: 'EVERY_30_DAYS',
              },
            },
          },
        ],
        test: process.env.PAYMENT_SANDBOX === 'development' ? true : false,
        trialDays: Plan.Ultimate.trialDays,
      };
    default:
      return {
        name: Plan.Free.title,
      };
  }
}

export function findLocaleShopByRequest(req: Request, shopLocales: any) {
  const acceptLanguage = req.headers['accept-language'];

  const localesFromHeader =
    acceptLanguage
      ?.split(',')
      .map((lang) => lang.split(';')[0].trim().split('-')[0]) || [];

  const primaryLocale = shopLocales.find((locale) => locale.primary)?.locale;

  for (const locale of localesFromHeader) {
    if (locale === primaryLocale) {
      return 'default';
    }

    if (shopLocales.some((shopLocale) => shopLocale.locale === locale)) {
      return locale;
    }
  }

  return 'default';
}
