export type PaginateData = {
  prev_page: string;
  next_page: string;
};

export default class Pagination {
  public resolve(navigateData: string): PaginateData {
    if (navigateData == '') {
      return {
        prev_page: '',
        next_page: '',
      };
    }
    const dataArray = navigateData.split(', ');
    if (dataArray.length > 1) {
      return {
        prev_page: this._resolvePreviousPage(dataArray[0]),
        next_page: this._resolveNextPage(dataArray[1]),
      };
    }
    if (dataArray.length == 1) {
      const dataString: string = dataArray[0];

      if (dataString.includes('next')) {
        return {
          prev_page: '',
          next_page: this._resolveNextPage(dataString),
        };
      }
      return {
        prev_page: this._resolvePreviousPage(dataString),
        next_page: '',
      };
    }

    return {
      prev_page: '',
      next_page: '',
    };
  }

  private _resolvePreviousPage(data: string): string {
    const dataArray = data.split('; ');
    if (dataArray.length > 1) {
      return dataArray[0].replace('<', '').replace('>', '');
    }
    return '';
  }

  private _resolveNextPage(data: string): string {
    const dataArray = data.split('; ');
    if (dataArray.length > 1) {
      return dataArray[0].replace('<', '').replace('>', '');
    }
    return '';
  }
}
