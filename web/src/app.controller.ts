import { Controller, Get, Post, Req, Res } from '@nestjs/common';
import { Request, Response } from 'express';
import { join } from 'path';
import { ShopifyService } from './modules/shopify/shopify.service';
import { readFileSync } from 'fs';

@Controller()
export class AppController {
  constructor(private readonly shopifyService: ShopifyService) {}

  @Get()
  async index(@Req() req: Request, @Res() res: Response) {
    console.log(req.query.shop);

    if (!req.query.shop) {
      return res.status(200).send(this.getHtml());
    }

    const shop = this.shopifyService.shopify.utils.sanitizeShop(
      req.query.shop as string,
    );

    const session = await this.shopifyService.checkHasShopifySession(shop);

    // reinstall app
    if (session && req.query.embedded !== '1') {
      return this.redirectToAuth(req, res);
    }

    if (!session && !req.originalUrl.match(/^\/exit-iframe/i)) {
      return this.redirectToAuth(req, res);
    }

    const isActiveSession = await this.shopifyService.isActiveSession(shop);

    if (!isActiveSession) {
      return this.redirectToAuth(req, res);
    }

    console.log('env', process.env.NODE_ENV);
    const entryFile = join(
      __dirname,
      process.env.NODE_ENV == 'production' ? `../frontend/dist` : `../frontend`,
      'index.html',
    );

    const htmlContent = readFileSync(entryFile)
      .toString()
      .replace('%VITE_SHOPIFY_API_KEY%', process.env.SHOPIFY_API_KEY || '');

    return res.status(200).set('Content-Type', 'text/html').send(htmlContent);
  }

  private async redirectToAuth(req: Request, res: Response) {
    if (!req.query.shop) {
      return res.status(500).send('No shop provided');
    }

    if (req.query.embedded === '1') {
      return this.clientSideRedirect(req, res);
    }

    return await this.serverSideRedirect(req, res);
  }

  private clientSideRedirect(req: Request, res: Response) {
    const shop = this.shopifyService.shopify.utils.sanitizeShop(
      req.query.shop as string,
    );

    const redirectUriParams = new URLSearchParams({
      shop,
      host: req.query.host as string,
    }).toString();

    const queryParams = new URLSearchParams({
      ...req.query,
      shop,
      redirectUri: `https://${this.shopifyService.shopify.config.hostName}/api/auth?${redirectUriParams}`,
    }).toString();

    return res.redirect(`/exit-iframe?${queryParams}`);
  }

  private async serverSideRedirect(req: Request, res: Response) {
    await this.shopifyService.shopify.auth.begin({
      shop: this.shopifyService.shopify.utils.sanitizeShop(
        req.query.shop as string,
        true,
      ),
      callbackPath: '/api/auth/callback',
      isOnline: false,
      rawRequest: req,
      rawResponse: res,
    });
  }

  private getHtml() {
    const entryFile = join(
      __dirname,
      process.env.NODE_ENV == 'production' ? `../frontend/dist` : `../frontend`,
      'index.html',
    );

    const htmlContent = readFileSync(entryFile)
      .toString()
      .replace('%VITE_SHOPIFY_API_KEY%', process.env.SHOPIFY_API_KEY || '');

    return htmlContent;
  }

  @Post('uninstall')
  async webhook(@Req() req: Request, @Res() res: Response) {
    return res.status(200).send({ message: 'ok' });
  }
}
