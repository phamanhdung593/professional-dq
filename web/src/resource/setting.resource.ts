export function transformSettingsWithLocale(
  settings: Record<string, any>,
  locale: string,
): Record<string, any> {
  const transformedSettings: Record<string, any> = {};

  for (const key in settings) {
    if (Object.prototype.hasOwnProperty.call(settings, key)) {
      const value = settings[key];

      if (typeof value === 'string') {
        try {
          // Thử parse JSON
          const parsedValue = JSON.parse(value);

          // Nếu giá trị là mảng và có `locale`, lọc theo `locale`
          if (Array.isArray(parsedValue)) {
            const localizedContent = parsedValue.find(
              (item: any) => item.locale === locale,
            );
            const defaultContent = parsedValue.find(
              (item: any) => item.locale === 'default',
            );

            // Nếu không tìm thấy locale phù hợp, trả về giá trị mặc định
            transformedSettings[key] = localizedContent
              ? localizedContent.content
              : defaultContent
                ? defaultContent.content
                : value; // Nếu không có cả mặc định, giữ nguyên giá trị gốc
          } else {
            transformedSettings[key] = parsedValue; // Nếu là Object JSON
          }
        } catch {
          transformedSettings[key] = value; // Không phải JSON, giữ nguyên
        }
      } else {
        transformedSettings[key] = value; // Không phải string, giữ nguyên
      }
    }
  }

  return transformedSettings;
}
